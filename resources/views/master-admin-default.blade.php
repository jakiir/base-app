<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        @yield('title')

        @section('title')
        <title>..::Admin-Dashboard</title>
        @endsection

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/AdminLTE.min.css') }}">

        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{{ asset('back-end/bootstrap/css/bootstrap.min.css') }}">

        <link rel="stylesheet" href="{{ asset('back-end/plugins/datatables/dataTables.bootstrap.css') }}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/skins/_all-skins.min.css') }}">

        <!--For  Chose jquery plugging -->
        <link rel="stylesheet" href="{{ asset('front-end/css/chosen.min.css') }}">

        <link href="{{ asset('front-end/css/jquery-ui.css') }}" rel="stylesheet"/>

        <link rel="stylesheet" href="{{ asset('front-end/css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/custom.css') }}">
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">


            @include('admin-header')

            @include('admin-sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div><!-- /.content-wrapper -->

        </div>
        <!-- jQuery 2.1.4 -->
        <script src="{{ asset('back-end/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="{{ asset('back-end/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('back-end/plugins/fastclick/fastclick.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('back-end/dist/js/app.min.js') }}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('back-end/dist/js/demo.js') }}"></script>
        <script src="{{ asset('back-end/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('back-end/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('front-end/js/chosen.jquery.js') }}"></script>
        <script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>
        
         <script src="{{ asset('front-end/js/jquery-ui.js') }}"></script>
         <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>
//        tinymce.init({ selector:'#description_editor' });
        tinymce.init({
          selector: '#description_editor',
          height: 50,
          theme: 'modern',
          plugins: [
            'autosave advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
          ],
          toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          toolbar2: 'print preview media | forecolor backcolor emoticons',
          image_advtab: true,
            content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
          ]
        });
        </script>

        <script>
$(function () {
    $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
});
        </script>

        @yield('footer-script')
    </body>
</html>