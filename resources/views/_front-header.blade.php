<header style="width: 100%; height: auto; background: mintcream; opacity:0.7;">
    <div class="col-md-12 text-center">
        {!! Html::image("back-end/dist/img/logo.png", "Logo",['width'=>70]) !!}
        <h3>Bangladesh Council of Scientific and Industrial Research</h3>
    </div>
    <div class="clearfix"></div>
    <br/>
</header>
