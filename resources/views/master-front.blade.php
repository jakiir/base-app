<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('title')</title>

        <!-- Bootstrap -->
        <link href="{{ asset('front-end/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('front-end/css/jquery-ui.css') }}" rel="stylesheet"/>

        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <style>
            footer {
                background-color: mintcream;
                opacity: 0.9;
                border-bottom: 1px solid #474646;
                border-top: 1px solid #474646;
                line-height: 24px;
                vertical-align: middle;
                width: 100%;
                margin-top: 30px;
            }

            body {
                /*background-image:  url({{  asset('front-end/images/back_4.png')  }});*/
                background-image:  url(<?php echo url(); ?>/front-end/images/back_4.png);
                background-repeat: repeat-y repeat-x;
            }
            h3, h4, h6{
                font-weight: bold;
            }
            h6{
                font-size: 14px;
            }
            ul{
                list-style-type: none;
            }
            .img-thumbnail{
                border: none;
            }
            .required-star:after{
                color: red;
                content: " *";
            }
        </style>
        @yield('header-script')

    </head>
    <body>

        @include('_front-header')

        @yield('content')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('front-end/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('front-end/js/jquery-ui.js') }}"></script>

        @yield('footer-script')

        <footer>
            <center>
                <p class="footer-p">
                    <br/>
                    Developed By Business Automation Ltd. in association with OCPL.</p>
            </center>
        </footer>

    </body>

</html>