<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="{{ asset('front-end/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('front-end/css/jquery-ui.css') }}" rel="stylesheet"/>

        <!--For  Chose jquery plugging -->
        <link rel="stylesheet" href="{{ asset('front-end/css/chosen.min.css') }}">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <style>
            body {
                /*background-image:  url({{  asset('front-end/images/back_4.png')  }});*/
                background-image:  url(<?php echo url(); ?>/front-end/images/back_4.png);
                background-repeat: repeat-y repeat-x;
            }
            h3, h4, h6{
                font-weight: bold;
            }
            h6{
                font-size: 14px;
            }
            ul{
                list-style-type: none;
            }
            .img-thumbnail{
                border: none;
            }
        </style>
        @yield('header-script')

    </head>
    <body>

        @yield('content')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('front-end/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('front-end/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('front-end/js/chosen.jquery.js') }}"></script>
        <script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>

        @yield('footer-script')

    </body>

</html>