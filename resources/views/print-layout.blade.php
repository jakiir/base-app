<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @yield('title')

    @section('title')
        <title>Invoice print</title>
        @endsection

                <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('back-end/dist/css/AdminLTE.min.css') }}">

        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{{ asset('back-end/bootstrap/css/bootstrap.min.css') }}">

        <link rel="stylesheet" href="{{ asset('back-end/plugins/datatables/dataTables.bootstrap.css') }}">
    <style>
        @media print{
        .footer{position:absolute;left:0;height:0px;bottom:0;}
        }
    </style>

        <!--For  Chose jquery plugging -->
        <link rel="stylesheet" href="{{ asset('front-end/css/chosen.min.css') }}">

        <link rel="stylesheet" href="{{ asset('front-end/css/custom.css') }}">


        <link rel="stylesheet" href="{{ asset('back-end/dist/css/custom.css') }}">

        @yield('header-script')
</head>
<!--     onload="window.print();"   -->
<body class="hold-transition skin-blue sidebar-mini" onload="window.print()">
<div class="wrapper">

    <!-- Content Wrapper. Contains page content -->
    <div class="">
        @yield('content')
    </div><!-- /.content-wrapper -->

</div>

<script src="{{ asset('back-end/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script src="{{ asset('back-end/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('back-end/plugins/fastclick/fastclick.min.js') }}"></script>
<script src="{{ asset('back-end/dist/js/app.min.js') }}"></script>
<script src="{{ asset('back-end/dist/js/demo.js') }}"></script>
<script src="{{ asset('back-end/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back-end/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('front-end/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>
<script>
    $(function () {
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>