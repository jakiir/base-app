<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>-->
        <!-- /.search form -->

        <?php $url_segment = Request::segment(1); ?>
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
<!--            <li class="<?php // if ($url_segment == 'dashboard') { ?>active <?php // } ?>">
                <a href="{{ url('users/dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                </a>
            </li>-->

            <?php
            $user_member_type = CommonFunction::getUserType();
            if ($user_member_type != '4') { // 4 is for unassigned desk users
                ?>

                <?php if (Session::get('member_type') == 5) { ?> 
                    <li class="<?php if ($url_segment == 'company/') { ?>active<?php } ?>"></li>
                    <li>
                        <a href="{{ url('/company/associated-company-list') }}">
                            <i class="fa fa-briefcase"></i>
                            <span>Organization</span>
                        </a>
                    </li>
                <?php } ?>
                <?php if (Session::get('member_type') != 11) { ?> 
                <li>
                    <a href="{{ url('/apps') }}">
                        <i class="fa fa-briefcase"></i>
                        <span>Application </span>
                    </a>

                </li>
                <?php } ?>
                <?php if (Session::get('member_type') == 11) { // 11 is Bank User  ?>                
                <li>
                    <a href="{{ url('/apps/bank-payment') }}">
                        <i class="glyphicon glyphicon-transfer"></i>
                        <span>Bank Payment</span>
                    </a>
                </li>           
                <?php } ?>

                <?php if (in_array(Session::get('member_type'), array(1,8,7,6))){ ?>
                    <li class="<?php if ($url_segment == 'reports') { ?>active<?php } ?>"></li>
                    <li class="<?php if ($url_segment == 'reports') { ?>active<?php } ?>">

                    </li>
                    <li class=" {{ Request::is('reports/*') ? 'treeview  active' : ''}}">
                        <a href="{{ url('/reports') }}">
                            <i class="fa fa-pie-chart"></i>
                            <span>Reports</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul>
                            <li class="{{ Request::is('reports/') ? ' active' : ''}}">
                                <a href="{{ url('/reports') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Report List
                                </a>
                            </li>
                            <?php if (in_array(Session::get('member_type'), array(1))) { ?>
                            <li class="{{ Request::is('reports/create') ? ' active' : ''}}">
                                <a href="{{ url('/reports/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    New Report
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>                

                <?php if (Session::get('member_type') == 1 || Session::get('member_type') == 2) { ?>                

                    <?php
                    if ($user_member_type == '1') { // 1 is for admin users
                        ?>
                        <li class=" {{ Request::is('users/*') ? 'treeview  active' : ''}}">
                            <a href="{{ url('users/lists') }}">
                                <i class="fa fa-th"></i> <span>Users</span>
                                <!--<small class="label pull-right bg-green"> @if(!empty($total)) {{ $total }} @endif</small>-->
                            </a>
                        </li>
                    <?php } 
                    ?> <!-- Not showing if user type is not 1-->
                    <li class="menu-open {{ Request::is('settings/*') ? 'treeview  active' : ''}}">
                        <a href="{{ url('/settings') }}">
                            <i class="fa fa-pie-chart"></i>
                            <span>Settings</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu menu-open">
                            <li class="{{ Request::is('settings/list-test') ? 'active' : ''}}">
                                <a href="{{ url('settings/list-test') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Test Sample
                                </a>
                            </li>
                            <li class="{{ Request::is('settings/list-lab') ? 'active' : ''}}">
                                <a href="{{ url('settings/list-lab') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Lab
                                </a>
                            </li>
                            <li class="{{ Request::is('company/index/*') ? 'active' : ''}}">
                                <a href="{{ url('company/index') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Organization
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
            <?php } ?> <!-- Not showing to unassigned desk users of type 4-->

        </ul>
        <br/>
                <div class="panel panel-default">

            <div class="panel-header text-center">
                <br/>Implemented by<br/><br/>
                {!!  Html::image('back-end/dist/img/business_automation.png','Business Automation logo',['width'=>'75']) !!}<br/>
                <br/>Supported by <br/><br/>
                <div class="">
                    {!!  Html::image('back-end/dist/img/logo.png','BCSIR logo',['width'=>'60']) !!}
                    {!!  Html::image('back-end/dist/img/logo_NABL.jpg','INARS logo',['width'=>'50']) !!}
                </div>

            </div>
            <div class="panel-body">
                <small>Developed By <a href="http://batworld.com">Business Automation Ltd</a> in association with OCPL.</small>
            </div>
        </div>
    </section>
    <!-- /.sidebar -->
</aside>
