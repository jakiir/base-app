@extends("master-admin-default")

@section("title")
<title>Associations with Companies</title>
@endsection <!-- title-->

@section("content")
<section class="content-header">
    <h1><i class="fa fa-tasks"></i> Associations with the Companies</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('users/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li><a href="{{ url('company/company_associate') }}"><i class="fa fa-briefcase"></i> Organization</a></li>
    </ol>
</section>


<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.7; border-radius:8px;">
            <h3 class="text-center">Company / Organization Details </h3>

            @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">{{ Session::get('error') }}</div>
            @endif

            <hr/>
            <div class="box box-default col-md-8 col-sm-8 col-md-offset-2">

                <br/>
                {!! Form::open(array('url' => '/company/get-company-name','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'associate_company_form')) !!}

                <div class="form-group has-feedback {{ $errors->has('company_name') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">Name of the Company / Organization</label>
                    <div class="col-lg-6">                                                                     
                        <!--    {!! Form::text('company_id', $value = null, '' , $attributes = array('class'=>'form-control required',
                                                'placeholder'=>'Enter the Name of the Company','id'=>"company_id", 'onkeyup' => 'suggest(this.value)')) !!}-->
                        <input type="text" name="company_name" id="company_name" class="form-control required"
                               placeholder="Enter the Name of the Company" value="" />
                        @if($errors->first('company_name'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_name','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('company_type') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">Organization Type</label>
                    <div class="col-lg-6">
                            {!! Form::select('company_type', $value = $company_types, '', $attributes = array('class'=>'form-control required',
                            'placeholder' => 'Select a Type', 'id'=>"company_type")) !!}
                        @if($errors->first('company_type'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_type','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('company_city') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">City of the Organization</label>
                    <div class="col-lg-6"> 
                        <input type="text" name="company_city" id="company_city" class="form-control required"
                               placeholder="Enter the Name of the Company" value="" />
                        @if($errors->first('company_city'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_city','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>

            </div> <!-- End of company_details--->

            <div class="clearfix"><br/></div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-5">
                    <button type="submit" class="btn btn-lg btn-success"><b>Next</b></button>
                </div>
            </div>

            {!! Form::close() !!}
            <div class="clearfix"><br/><br/><br/><br/></div>
        </div>

    </div>
</div>
@endsection <!--- content--->

@section('footer-script')

@endsection <!--- footer script--->