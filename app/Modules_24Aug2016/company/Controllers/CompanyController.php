<?php

namespace App\Modules\Company\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\CompanyAssoc;
use App\Modules\Company\Models\CompanyType;
use Encryption;
use DB;

class CompanyController extends Controller {

    public function __construct(Request $request) {
        CommonFunction::getUserId();

        $user_member_type = CommonFunction::getUserType();
        if ($user_member_type != 5) { // 5 is for applicant
            if ($request->segment(2) == 'associated-company-list') {
                \Session::flash('error', 'Accessed Denied!');
                return redirect()->to('users/dashboard')->send();
            }
        }

        if ($user_member_type != 1  && $user_member_type != 2) { // 1 is for admin and 2 is for IT Officer
            if ($request->segment(2) == 'index') {
                \Session::flash('error', 'Accessed Denied!');
                return redirect()->to('users/dashboard')->send();
            }
        }
    }

    /**
     * Display a listing of the Company.
     */
    public function index() {
        $getList['result'] = Company::all();
        return view("company::index", ['getList' => $getList]);
    }

    /**
     * Show the form for creating a new Company.
     */
    public function create($encrypted_company_info) {
        $company_info = Encryption::decodeId($encrypted_company_info);
        $company = explode(",", $company_info);
        $company_name = $company[0];
        $company_type = $company[1];
        $company_city = $company[2];

        $company_types = CompanyType::lists('ct_name', 'ct_id');

        return view("company::create", compact('company_name', 'company_type', 'company_city', 'company_types'));
    }

    /**
     * Store a newly created Company in storage.
     */
    public function store(CompanyRequest $companyRequest) {

        $company = Company::create(
                        array(
                            'company_name' => $companyRequest->get('company_name'),
                            'company_type' => $companyRequest->get('company_type'),
                            'company_house_no' => $companyRequest->get('company_house_no'),
                            'company_flat_no' => $companyRequest->get('company_flat_no'),
                            'company_street' => $companyRequest->get('company_street'),
                            'company_city' => $companyRequest->get('company_city'),
                            'company_area' => $companyRequest->get('company_area'),
                            'company_zip' => $companyRequest->get('company_zip'),
                            'company_fax' => $companyRequest->get('company_fax'),
                            'company_web' => $companyRequest->get('company_web'),
                            'company_tin' => $companyRequest->get('company_tin'),
                            'head_of_org' => $companyRequest->get('head_of_org'),
                            'head_contact' => $companyRequest->get('head_contact'),
                            'head_email' => $companyRequest->get('head_email'),
                            'contact_person' => $companyRequest->get('contact_person'),
                            'contact_phone' => $companyRequest->get('contact_phone'),
                            'contact_email' => $companyRequest->get('contact_email'),
                            'phone_no' => $companyRequest->get('phone_no'),
                            'fax_no' => $companyRequest->get('fax_no'),
                            'company_status' => 1
                        )
        );

        $company_id = $company->id;
        $encrypted_company_id = Encryption::encodeId($company_id);

        // file uploading
        $company_type = $companyRequest->get('company_type');
        $unique_prefix = uniqid();

        if ($company_type == 1 || $company_type == 2 || $company_type == 3) { // type 3 is for Propertieship
            if ($companyRequest->file('com_trade_license') != '') {
                $trade_license = $companyRequest->file('com_trade_license');
                $trade_license->move('company/upload', $unique_prefix . $trade_license->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'com_trade_license' => $unique_prefix . $trade_license->getClientOriginalName(),
                ]);
            }
        }

//        if ($company_type == 1) { // 1 is for Limited Company
            if ($companyRequest->file('incorporation_certificate') != '') {
                $incorporation_certificate = $companyRequest->file('incorporation_certificate');
                $incorporation_certificate->move('company/upload', $unique_prefix . $incorporation_certificate->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'incorporation_certificate' => $unique_prefix . $incorporation_certificate->getClientOriginalName(),
                ]);
            }
//        }
        if ($company_type == 2) { // 2 is for Partership
            if ($companyRequest->file('partnership_deed') != '') {
                $partnership_deed = $companyRequest->file('partnership_deed');
                $partnership_deed->move('company/upload', $unique_prefix . $partnership_deed->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'partnership_deed' => $unique_prefix . $partnership_deed->getClientOriginalName(),
                ]);
            }
        }
        // file uploading end.

        $email = $companyRequest->get('contact_email');
        $body_msg = "Dear Respected Company Representative,
                                        <br/>&nbsp;<br/>Your company has been successfully registered for BCSIR Application.
                                         <br/>&nbsp;<br/>Thanks<br/>Administration";

        $data = array(
            'header' => 'Registration is Successfull',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function($message) use ($email) {
            $message->from('no-reply@bcsir.com', 'BCSIR')
                    ->to($email)
                    ->subject('Successful Registration in BCSIR');
        });

        \Session::flash('success', 'Thanks for signing up! An email has been sent to the given company email address.');
        return redirect('company/edit/' . $encrypted_company_id);
    }

    /**
     * Show the form for editing the specified Company.
     */
    public function edit($encrypted_company_id) {
        $company_id = Encryption::decodeId($encrypted_company_id);

        $company_details = DB::table('companies')
                ->where('company_id', $company_id)
                ->first();
        $company_types = CompanyType::lists('ct_name', 'ct_id');
        return view('company::edit', ["company" => $company_details, 'company_types' => $company_types]);
    }

    /**
     * Update the specified Company in storage.
     */
    public function update($id, CompanyRequest $companyRequest) {
        $company_id = Encryption::decodeId($id);

        Company::where('company_id', $company_id)->update([
            'company_name' => $companyRequest->get('company_name'),
            'company_type' => $companyRequest->get('company_type'),
            'company_house_no' => $companyRequest->get('company_house_no'),
            'company_street' => $companyRequest->get('company_street'),
            'company_city' => $companyRequest->get('company_city'),
            'company_area' => $companyRequest->get('company_area'),
            'company_zip' => $companyRequest->get('company_zip'),
            'company_fax' => $companyRequest->get('company_fax'),
            'company_web' => $companyRequest->get('company_web'),
            'company_tin' => $companyRequest->get('company_tin'),
            'head_of_org' => $companyRequest->get('head_of_org'),
            'head_contact' => $companyRequest->get('head_contact'),
            'head_email' => $companyRequest->get('head_email'),
            'contact_person' => $companyRequest->get('contact_person'),
            'contact_phone' => $companyRequest->get('contact_phone'),
            'contact_email' => $companyRequest->get('contact_email'),
            'phone_no' => $companyRequest->get('phone_no'),
            'fax_no' => $companyRequest->get('fax_no'),
            'company_status' => 1,
            'updated_by' => CommonFunction::getUserId()
        ]);

        // file uploading
        $company_type = $companyRequest->get('company_type');
        $unique_prefix = uniqid();

        if ($company_type == 1 || $company_type == 2 || $company_type == 3) { // is for propertieship
            if ($companyRequest->file('com_trade_license') != "") {
                $trade_license = $companyRequest->file('com_trade_license');
                $trade_license->move('company/upload', $unique_prefix . $trade_license->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'com_trade_license' => $unique_prefix . $trade_license->getClientOriginalName(),
                ]);
            }
        }

        if ($company_type == 1) { // 1 is for Limited company
            if ($companyRequest->file('incorporation_certificate') != '') {
                $incorporation_certificate = $companyRequest->file('incorporation_certificate');
                $incorporation_certificate->move('company/upload', $unique_prefix . $incorporation_certificate->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'incorporation_certificate' => $unique_prefix . $incorporation_certificate->getClientOriginalName(),
                ]);
            }
        }
        if ($company_type == 2) { // 2 is for partnership
            if ($companyRequest->file('partnership_deed') != '') {
                $partnership_deed = $companyRequest->file('partnership_deed');
                $partnership_deed->move('company/upload', $unique_prefix . $partnership_deed->getClientOriginalName());

                Company::where('company_id', $company_id)->update([
                    'partnership_deed' => $unique_prefix . $partnership_deed->getClientOriginalName(),
                ]);
            }
        }
        // file uploading 

        \Session::flash('success', "Your Company's Profile has been updated Successfully!");
        return redirect('company/edit/' . $id);
    }

    /**
     * Show the Details information of the specified Company.
     */
    public function view($encrypted_company_id = '') {
        $company_id = Encryption::decodeId($encrypted_company_id);
        $company = DB::table('companies')
                ->where('company_id', $company_id)
                ->first();
        $company_types = CompanyType::lists('ct_name', 'ct_id');
        return view('company::view', compact('company_id', 'company', 'company_types'));
    }

    /**
     * Show the form to associate an user to a specific Company.
     */
    public function company_associate() {
        $company_types = CompanyType::lists('ct_name', 'ct_id');
        return view("company::associate.associate_company", compact('company_types'));
    }

    public function associate_company(Request $associateCompanyRequest) {
        $session_member_id = session('member_id');
        $company_id = $associateCompanyRequest->get('company_id');

        CompanyAssoc::create(
                array(
                    'company_id' => $associateCompanyRequest->get('company_id'),
                    'member_id' => $session_member_id,
//                    'ca_status' => 0,
                )
        );

        $member = DB::select("select member_first_name from members where member_id = '" . $session_member_id . "'");
        $member_name = $member[0]->member_first_name;

        $company = DB::select("select contact_email from companies where company_id = '" . $company_id . "'");
        $contact_email = $company[0]->contact_email;

        $body_msg = 'Dear Respected Company Representative,
                                        <br/><br/> BCSIR Application User ' . $member_name . ' is wanted to associate himself/herself to your company.
                                           <br/>If you acknowledged this action, please ignore this email.
                                           <br/><br/>But if you have any confusion or query, please contact BCSIR App\'s Administrator immediately.
                                         <br/>&nbsp;<br/>Thanks<br/>Administration';

        $data = array(
            'header' => 'A new User is trying to associate to your company',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function($message) use ($contact_email) {
            $message->from('no-reply@bcsir.com', 'BCSIR')
                    ->to($contact_email)
                    ->subject('New Users are trying to associate to your company');
        });

        \Session::flash('success', 'Thanks for the information! An email has been sent to the email address of the company.');
        return redirect('company/company_associate');
    }

    public function getCompanyName(Request $request) {
        $company_types = CompanyType::lists('ct_name', 'ct_id');

        $company_name = $request->get('company_name');
        $company_type = $request->get('company_type');
        $company_city = $request->get('company_city');

        if (!empty($company_name)) {
            $suggestedCompany = DB::select("SELECT company_id, company_name, company_house_no, company_flat_no, company_street, company_city,
                                                            phone_no, contact_email
                                                        FROM companies 
                                                        WHERE company_name LIKE '%$company_name%'
                                                        AND company_type = '$company_type'
                                                        AND company_city LIKE '%$company_city%'
                                                             LIMIT 10");
//            $suggestedCompany = Company::where('company_name', 'LIKE', "%$company_name%")->get(['company_name', 'company_id']);
        } else {
            $suggestedCompany = '';
        }

        $company_info = $company_name . ',' . $company_type . ',' . $company_city;
        $encrypted_company_info = Encryption::encodeId($company_info);

        return view("company::associate.associate", compact('suggestedCompany', 'company_types', 'company_name', 'company_type', 'company_city', 'encrypted_company_info'));
    }

    //Shows the list of company associated with a User
    public function associatedCompanyList() {
        $session_member_id = CommonFunction::getUserId();

        $getList = DB::select("SELECT co.company_id as company_id, company_name, company_house_no, company_flat_no, company_street, company_city,
                                                            phone_no, contact_email
                                                        FROM companies co
                                                        LEFT JOIN company_assoc ca
                                                        ON co.company_id = ca.company_id
                                                        WHERE ca.member_id = '$session_member_id'");

        return view("company::associate.list_associated", compact('getList'));
    }

    /*     * ***************************************End of Controller Class****************************************************** */
}
