<?php

Route::group(array('module' => 'Membertype', 'namespace' => 'App\Modules\Membertype\Controllers'), function() {

    Route::resource('membertype', 'MembertypeController');
    
});	