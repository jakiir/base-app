<?php
namespace App\Modules\Membertype\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Membertype extends Model {

    protected $table = 'member_types';
    protected $fillable = array(
        'mt_id',
        'mt_type_name',
        'mt_is_registarable',
        'mt_access_code',
        'mt_permission_json',
        'mt_status'
    );

    function getMemberTypeData($typeId){
        return DB::table($this->table)
            ->where('mt_id', $typeId)
            ->get();
    }
}
