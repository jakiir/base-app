<?php
namespace App\Modules\Settings\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Settings\Models\Lab;
use App\Modules\Settings\Models\LabTestMapping;
use Illuminate\Http\Request;

use App\Modules\Settings\Models\TestName;
use App\Modules\Settings\Models\Parameter;
use App\Modules\Settings\Models\Method;
use App\Modules\Settings\Models\LabMethodMapping;
use Session;
use DB;
use Event;

class SettingsController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
//        dd((base64_decode(Crypt::encrypt(123))));
        return view("settings::index");
    }


    /**
     * @return \Illuminate\View\View
     */
    public function createTest() {

        return view("settings::test.create",compact('lab'));
    }


    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editTest($id) {
        $tst_id = Encryption::decodeId($id);
        $data = TestName::where('tst_id', $tst_id)->first();

        $lab_data = Lab::leftJoin('tbl_lab_test_mapping as tm', function($join) use ($tst_id) {
            $join->on('tbl_lab.lab_id', '=', 'tm.lab_id');
            $join->on('tm.tst_id', '=', DB::raw($tst_id));
        })
            ->get(['tbl_lab.lab_id','tbl_lab.lab_name','tm.tst_id','tm.price','tm.duration']);

        $parameter_data = Parameter::where('tst_id',$tst_id)->orderBy('param_name')->get(['param_id','param_name']);

        return view("settings::test.edit",compact('data','id','lab_data','parameter_data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeTest(Request $request) {

        $this->validate($request, [
            'tst_name' => 'required|unique:tbl_test_name|max:255',
        ]);
        $insert = TestName::create(['tst_name' => $request->get('tst_name')]);

        Session::flash('success', 'Successfully Inserted the Test Name.');
        return redirect('/settings/edit-test/' . Encryption::encodeId($insert->id));
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateTest($id, Request $request) {
        $this->validate($request, [
            'tst_name' => 'required',
        ]);
        $tst_id = Encryption::decodeId($id);
        TestName::where('tst_id', $tst_id)->update([
            'tst_name' => $request->get('tst_name'),
            'updated_by' => CommonFunction::getUserId()
            ]);

        LabTestMapping::where('tst_id', '=', $tst_id)->delete();

        if($request->get('editParameter')) {
            foreach ($request->get('editParameter') as $id => $editedValue) {
                Parameter::where('param_id', $id)->update([
                    'param_name' => $editedValue,
                    'updated_by' => CommonFunction::getUserId()
                ]);
            }
        }

        if($request->get('newParameter')) {
            foreach ($request->get('newParameter') as $newValue) {
                if ($newValue) {
                    Parameter::create([
                        'param_name' => $newValue,
                        'tst_id' => $tst_id
                    ]);
                }
            }
        }

        if($request->get('lab_id')) {
            foreach ($request->get('lab_id') as $lab_id):
                LabTestMapping::create([
                    'tst_id' => $tst_id,
                    'lab_id' => $lab_id
                ]);
            endforeach;
        }

        Session::flash('success', 'Successfully Updated the Test Name.');
        return redirect('/settings/edit-test/' . Encryption::encodeId($tst_id));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function listTest() {
        $getList = TestName::leftJoin('tbl_lab_test_mapping as tm', function($join) {
            $join->on('tbl_test_name.tst_id', '=', 'tm.tst_id');
        })
            ->leftJoin('tbl_lab as tl', function($join) {
                $join->on('tl.lab_id', '=', 'tm.lab_id');
            })
            ->groupBy('tbl_test_name.tst_id')
            ->orderBy('tbl_test_name.tst_name')
            ->get(['tbl_test_name.tst_id','tbl_test_name.tst_name','tbl_test_name.updated_at', DB::raw('group_concat(tl.lab_name order by tl.lab_name separator ", ") lab_names')]);

//        dd($getList);
        return view('settings::test.list',  compact("getList"));
    }



    /**
     * @return \Illuminate\View\View
     */
    public function createParameter() {
        $test_list = TestName::lists('tst_name','tst_id');
        return view("settings::parameter.create",compact('test_list'));
    }


    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editParameter($id) {
        $param_id = Encryption::decodeId($id);
        $data = Parameter::where('param_id', $param_id)->first();

        $sample_data = Method::where('param_id',$param_id)->orderBy('method_name')->get(['method_id','method_name']);
        $test_list = TestName::lists('tst_name','tst_id');
        return view("settings::parameter.edit",compact('test_list','data','id','sample_data'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeParameter(Request $request) {
        $this->validate($request, [
            'param_name' => 'required',
            'tst_id' => 'required'
        ]);
        $insert = Parameter::create($request->all());
        Session::flash('success', 'Successfully Inserted the Test Parameter.');
        return redirect('/settings/edit-parameter/' . Encryption::encodeId($insert->id));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateParameter($id, Request $request) {
        $this->validate($request, [
            'param_name' => 'required',
            'tst_id' => 'required'
        ]);



        $param_id = Encryption::decodeId($id);
        Parameter::where('param_id', $param_id)->update([
            'param_name' => $request->get('param_name'),
            'tst_id' => $request->get('tst_id'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        if($request->get('editMethod')) {
            foreach ($request->get('editMethod') as $id => $editedValue) {
                Method::where('method_id', $id)->update([
                    'method_name' => $editedValue,
                    'updated_by' => CommonFunction::getUserId()
                ]);
            }
        }

        if($request->get('newMethod')) {
            foreach ($request->get('newMethod') as $newValue) {
                if ($newValue) {
                    Method::create([
                        'method_name' => $newValue,
                        'param_id' => $param_id
                    ]);
                }
            }
        }

        Session::flash('success', 'Successfully Updated the Test Parameter.');
        return redirect('/settings/edit-parameter/' . Encryption::encodeId($param_id));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function listParameter() {
        $getList = Parameter::all();
//        $getList = Parameter::leftJoin('tbl_sample as s', function($join) {
//            $join->on('s.param_id', '=', 'tbl_parameter.param_id');
//        })
//            ->leftJoin('tbl_lab as tl', function($join) {
//                $join->on('tl.lab_id', '=', 'tm.lab_id');
//            })
//            ->groupBy('tbl_test_name.tst_id')
//            ->orderBy('tbl_test_name.tst_name')
//            ->get(['tbl_test_name.tst_id','tbl_test_name.tst_name','tbl_test_name.updated_at', DB::raw('group_concat(tl.lab_name order by tl.lab_name separator ", ") lab_names')]);

        return view('settings::parameter.list',  compact("getList"));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function createSample() {
        $test_list = TestName::lists('tst_name','tst_id','id');

        return view("settings::sample.create",compact('test_list'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editSample($id) {
        $method_id = Encryption::decodeId($id);
        $data = Method::where('method_id', $method_id)->first();
        $test_list = TestName::lists('tst_name','tst_id','id');

        return view("settings::sample.edit",compact('test_list','data','id'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeSample(Request $request) {
        $this->validate($request, [
            'method_name' => 'required',
            'param_id' => 'required'
        ]);
        $insert = Method::create($request->all());
        Session::flash('success', 'Successfully Inserted the Test Sample.');
        return redirect('/settings/edit-sample/' . Encryption::encodeId($insert->id));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateSample($id, Request $request) {
        $method_id = Encryption::decodeId($id);
        $this->validate($request, [
            'method_name' => 'required',
            'param_id' => 'required'
        ]);
        Method::where('method_id', $method_id)->update([
            'method_name' => $request->get('method_name'),
            'param_id' => $request->get('tst_id'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Successfully Updated the Test Sample.');
        return redirect('/settings/edit-sample/' . Encryption::encodeId($method_id));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function listSample() {
        $getList = Method::leftJoin('tbl_parameter', function($join) {
            $join->on('tbl_sample.param_id', '=', 'tbl_parameter.param_id');
        })->get();

        return view('settings::sample.list',  compact("getList"));
    }

    /*
     * Lab part start
     */

    /**
     * @return \Illuminate\View\View
     */
    public function createLab() {
        return view("settings::lab.create");
    }


    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editLab($id) {
        $lab_id = Encryption::decodeId($id);
        $data = Lab::where('lab_id', $lab_id)->first();

        $test_data = TestName::leftJoin('tbl_lab_test_mapping as tm', function($join) use ($lab_id)  {
            $join->on('tbl_test_name.tst_id', '=', 'tm.tst_id');
        })
            ->join('tbl_parameter as tp', 'tbl_test_name.tst_id', '=', 'tp.tst_id')
            ->join('tbl_method as td', 'tp.param_id', '=', 'td.param_id')
            ->leftJoin('tbl_lab_method_mapping as tlm', function($join) use ($lab_id)  {
                $join->on('td.method_id', '=', 'tlm.method_id');
                $join->on('tlm.lab_id', '=', DB::raw($lab_id));
            })
            ->where('tm.lab_id', '=', DB::raw($lab_id))
            ->get(['tbl_test_name.tst_name','tm.lab_id','tp.param_name','td.method_id','td.method_name','tlm.lm_id','tlm.price','tlm.duration']);
        return view("settings::lab.edit",compact('data','id','test_data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function storeLab(Request $request) {
        $this->validate($request, [
            'lab_name' => 'required'
        ]);
               
        $insert = Lab::create(['lab_name' => $request->get('lab_name')]);
        
        $lab_logo = $request->file('lab_logo');
        if ($request->hasFile('lab_logo')) {
            $lab_logo->move('uploads', $lab_logo->getClientOriginalName());
            Lab::where('lab_id',$insert->id)->update(['lab_logo'=>$lab_logo->getClientOriginalName()]);
        }
        Session::flash('success', 'Successfully Inserted the Test Name.');
        return redirect('/settings/edit-lab/' . Encryption::encodeId($insert->id));
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateLab($id, Request $request) {
        $lab_id = Encryption::decodeId($id);
        $this->validate($request, [
            'lab_name' => 'required'
        ]);

        Lab::where('lab_id', $lab_id)->update([
            'lab_name' => $request->get('lab_name'),
            'updated_by' => CommonFunction::getUserId()
        ]);
        
        $lab_logo = $request->file('lab_logo');
        if ($request->hasFile('lab_logo')) {
            $lab_logo->move('uploads', $lab_logo->getClientOriginalName());
            Lab::where('lab_id',$lab_id)->update(['lab_logo'=>$lab_logo->getClientOriginalName()]);
        }
        
        /*
        LabTestMapping::where('lab_id', '=', $lab_id)->delete();

        if($request->get('tst_id')){
            foreach($request->get('tst_id') as $tst_id):
                LabTestMapping::create([
                    'tst_id'=>$tst_id,
                    'lab_id' =>$lab_id,
                    'price' =>$request->get('price')[$tst_id],
                    'duration' =>$request->get('duration')[$tst_id],
                ]);
            endforeach;
        }

        */


        Session::flash('success', 'Successfully Updated the Test Name.');
        return redirect('/settings/edit-lab/' . Encryption::encodeId($lab_id));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function listLab() {
        $getList = Lab::leftJoin('tbl_lab_test_mapping as tm', function($join) {
            $join->on('tbl_lab.lab_id', '=', 'tm.lab_id');
        })
            ->leftJoin('tbl_test_name as tn', function($join) {
                $join->on('tn.tst_id', '=', 'tm.tst_id');
            })
            ->groupBy('tbl_lab.lab_id')
            ->orderBy('tbl_lab.lab_name')
            ->get(['tbl_lab.lab_id','tbl_lab.lab_name','tbl_lab.updated_at', DB::raw('group_concat(tn.tst_name order by tn.tst_name separator ", ") tst_names')]);

        return view('settings::lab.list',  compact("getList"));
    }

    /*
     * Lab part end
     */
    public function saveMethodPriceDuration(Request $request){
        $response_data = ['response_code'=>'99'];
        if($request->get('data')['lm_id']){
            LabMethodMapping::where('lm_id',$request->get('data')['lm_id'])->update([
                'price' =>$request->get('data')['price'],
                'duration' =>$request->get('data')['duration'],
                'updated_by' => CommonFunction::getUserId()
            ]);
            $response_data = ['response_code' => 2];
        }else {
            $insert = LabMethodMapping::create([
                'lab_id' => $request->get('data')['lab_id'],
                'method_id' => $request->get('data')['method_id'],
                'price' => $request->get('data')['price'],
                'duration' => $request->get('data')['duration']
            ]);
            $response_data = ['response_code' => 1, 'id' => $insert->id];
        }
        return response()->json($response_data);
    }
    /* End of Users Controller Class */
}
