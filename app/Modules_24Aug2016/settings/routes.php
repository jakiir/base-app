<?php

Route::group(array('module' => 'Settings', 'namespace' => 'App\Modules\Settings\Controllers'), function() {

    //Test section
    Route::get('settings/list-test', "SettingsController@listTest")->middleware(['ocpl.checkvalid']);
    Route::get('settings/create-test', "SettingsController@createTest")->middleware(['ocpl.checkvalid']);
    Route::get('settings/edit-test/{id}', "SettingsController@editTest")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/store-test', "SettingsController@storeTest")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/update-test/{id}', "SettingsController@updateTest")->middleware(['ocpl.checkvalid']);

    // Parameter section
    Route::get('settings/list-parameter', "SettingsController@listParameter")->middleware(['ocpl.checkvalid']);
    Route::get('settings/create-parameter', "SettingsController@createParameter")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/store-parameter', "SettingsController@storeParameter")->middleware(['ocpl.checkvalid']);
    Route::get('settings/edit-parameter/{id}', "SettingsController@editParameter")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/update-parameter/{id}', "SettingsController@updateParameter")->middleware(['ocpl.checkvalid']);

    // Sample section
    Route::get('settings/list-sample', "SettingsController@listSample")->middleware(['ocpl.checkvalid']);
    Route::get('settings/create-sample', "SettingsController@createSample")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/store-sample', "SettingsController@storeSample")->middleware(['ocpl.checkvalid']);
    Route::get('settings/edit-sample/{id}', "SettingsController@editSample")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/update-sample/{id}', "SettingsController@updateSample")->middleware(['ocpl.checkvalid']);

    // Lab section
    Route::get('settings/list-lab', "SettingsController@listLab")->middleware(['ocpl.checkvalid']);
    Route::get('settings/create-lab', "SettingsController@createLab")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/store-lab', "SettingsController@storeLab")->middleware(['ocpl.checkvalid']);
    Route::get('settings/edit-lab/{id}', "SettingsController@editLab")->middleware(['ocpl.checkvalid']);
    Route::patch('settings/update-lab/{id}', "SettingsController@updateLab")->middleware(['ocpl.checkvalid']);

    Route::get('settings/list-lab', "SettingsController@listLab")->middleware(['ocpl.checkvalid']);

    //post ajax call for save lab_method_mapping table
    Route::post('settings/save-method-price-duration', "SettingsController@saveMethodPriceDuration")->middleware(['ocpl.checkvalid']);

    Route::get('/settings/{any?}', "SettingsController@index")->middleware(['ocpl.checkvalid']);

});