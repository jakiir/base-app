<?php

namespace App\Modules\Settings\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model {

    protected $table = 'tbl_parameter';
    protected $fillable = array(
        'param_name','tst_id','is_locked','updated_by'
    );

 



    /* Users Model Class ends here */
}
