@extends('master-admin-default')

@section('title')
    <title>parameter  entry</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Parameter
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">parameter</a></li>
            <li class="active">create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="col-md-6">
            <div class="box box-success">


                <div class="box-header with-border">
                    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                </div>


                {!! Form::open(['url' => '/settings/store-parameter', 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                <div class="box-body">

                    <div class="form-group {{$errors->has('tst_id') ? 'has-error' : ''}}">
                        {!! Form::label('tst_id','Test Title',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('tst_id', $test_list, '',['class' => 'form-control']) !!}
                            {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <!-- text input -->
                    <div class="form-group {{$errors->has('param_name') ? 'has-error' : ''}}">
                        {!! Form::label('param_name','Parameter Title',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('param_name','',['class'=>'form-control col-sm-10','placeholder'=>'Enter test name...']) !!}
                            {!! $errors->first('param_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div><!-- /.box-body -->


                <div class="box-footer">
                    <a href="{{ url('/settings/list-parameter') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div><!-- /.box-footer -->


                {!! Form::close() !!}

            </div>
        </div>
    </section><!-- /.content -->
@endsection
