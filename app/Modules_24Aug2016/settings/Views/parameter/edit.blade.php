@extends('master-admin-default')

@section('title')
    <title>parameter  entry</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Parameter
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">parameter</a></li>
            <li class="active">create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="col-md-6">
            <div class="box box-success">


                <div class="box-header with-border">
                    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                </div>


                {!! Form::open(['url' => '/settings/update-parameter/'.$id, 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                <div class="box-body">

                    {!! Form::hidden('tst_id',$data->tst_id) !!}


                    <!-- text input -->
                    <div class="form-group {{$errors->has('param_name') ? 'has-error' : ''}}">
                        {!! Form::label('param_name','Parameter Title',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('param_name',$data->param_name,['class'=>'form-control col-sm-10','placeholder'=>'Enter test name...']) !!}
                            {!! $errors->first('param_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <table class="table sample_table">
                                <caption>Method <button type="button" class="btn btn-sm btn-primary add_sample pull-right">+Add Method</button> </caption>
                                <tbody>
                                @foreach($sample_data as $row)
                                    <tr style="border:1px solid yellowgreen">
                                        <td> {!! Form::text('editMethod['. $row->method_id .']',$row->method_name,['class'=>'form-control','placeholder'=>'Enter Sample name...','required'=>'required']) !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div><!-- /.box-body -->

                <div class="col-md-12">
                    {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                </div>


                <div class="box-footer">
                    <a href="#" class="close_tab">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div><!-- /.box-footer -->


                {!! Form::close() !!}

            </div>
        </div>
    </section><!-- /.content -->
@endsection


@section('footer-script')

    <script>
        $(function(){
            $('.add_sample').click(function () {
//                alert(2);
                var tr = '<tr style="border:1px solid rosybrown"><td>{!! Form::text("newMethod[]","",["class"=>"form-control","placeholder"=>"Enter Sample name..."]) !!} </td>';
                tr += '<tr>';
                $('.sample_table tbody').append(tr);
            });
            $('.close_tab').click(function (event) {
                window.close();
            })
        });
    </script>

@endsection