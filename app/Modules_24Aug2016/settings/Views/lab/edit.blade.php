@extends('master-admin-default')

@section('title')
<title>lab edit</title>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Lab Name
        {{--<small>it all starts here</small>--}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li><a href="#">Lab</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">

        <div class="box-header with-border">
            {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        </div>

    
{!! Form::open(['url' => '/settings/update-lab/'.$id, 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form','enctype' =>'multipart/form-data', 'files' => 'true']) !!}

        <div class="box-body">

            <!-- text input -->
            <div class="form-group {{$errors->has('lab_name') ? 'has-error' : ''}}">
                {!! Form::label('lab_name','Lab Name',['class'=>'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('lab_name',$data->lab_name,['class'=>'form-control','placeholder'=>'Enter test name...']) !!}
                    {!! $errors->first('lab_name','<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{$errors->has('lab_logo') ? 'has-error' : ''}}">
                {!! Form::label('lab_logo','Lab Logo',['class'=>'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    <?php if (!empty($data->lab_logo)) { ?>
                        <a href="<?php echo url(); ?>/uploads/<?php echo $data->lab_logo; ?>" target="_blank">
                            <?php echo $data->lab_logo; ?>
                            <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download" title="<?php echo $data->lab_logo; ?>"/>
                        </a>
                    <?php } ?>
                    {!! Form::file('lab_logo') !!}
                    {!! $errors->first('lab_logo','<span class="help-block">:message</span>') !!}
                </div>
            </div>
            {{--<!-- Checkbox input -->--}}
            {{--<div class="form-group">--}}
            {{--@foreach($test_data as $test)--}}
            {{--<label class="col-md-3">--}}
            {{--</label>--}}
            {{--<label class="col-md-9">--}}
            {{--<div class="col-md-12">--}}
            {{--{!! Form::checkbox('tst_id[]',$test->tst_id, $test->lab_id==$data->lab_id ? false:true , ['class'=>'lab_checkbox']) !!}--}}
            {{--{{$test->tst_name}}--}}
            {{--</div>--}}
            {{--<div class="col-md-12" id="lab_conf_{{$test->tst_id}}" style="display: none;">--}}
            {{--<label class="col-md-6">--}}
            {{--Price--}}
            {{--{!! Form::number('price['. $test->tst_id .']', $test->price,['class'=>'form-control','placeholder'=>'Taka', 'required'=>'required']) !!}--}}
            {{--</label>--}}
            {{--<label class="col-md-6">--}}
            {{--Duration--}}
            {{--{!! Form::number('duration['. $test->tst_id .']', $test->duration,['class'=>'form-control','placeholder'=>'Day', 'required'=>'required']) !!}--}}
            {{--</label>--}}
            {{--</div>--}}
            {{--</label>--}}
            {{--@endforeach--}}
            {{--</div>--}}


        </div><!-- /.box-body -->


        <div class="box-footer">
            <div class="col-md-4">
                <a href="{{ url('settings/list-lab') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
            </div>
            <div class="col-md-4">
                {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
            </div>
            <div class="col-md-4">
                {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
            </div>
        </div><!-- /.box-footer -->


        {!! Form::close() !!}
    </div>
    <div class="box">
        <h3>
            List of Sample with test fees
            {{--<small>it all starts here</small>--}}
        </h3>
        <table class="table dataTable">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th>Sample Type/<br/>Name of Sample</th>
                    <th>Test Parameter</th>
                    <th>Methodology</th>
                    <th>Fees</th>
                    <th>Duration</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $count = 1; ?>
                @foreach($test_data as $row)
                <tr>
                    <td>{{$count++}}</td>
                    <td>{!! $row->tst_name !!}</td>
                    <td>{!! $row->param_name !!}</td>
                    <td>{!! $row->method_name !!}</td>
                    <td>
                        <span class="label_price">{!! $row->price !!}</span>
                        {!! Form::number('price',$row->price,['class'=>'form-control price','placeholder'=>'Taka','style'=>'display:none']) !!}
                        {!! Form::hidden('lm_id',$row->lm_id,['class'=>'lm_id']) !!}
                        {!! Form::hidden('lab_id',$row->lab_id,['class'=>'lab_id']) !!}
                        {!! Form::hidden('method_id',$row->method_id,['class'=>'method_id']) !!}
                    </td>
                    <td>
                        <span class="label_duration">{!! $row->duration !!}</span>
                        {!! Form::number('duration',$row->duration,['class'=>'form-control duration','placeholder'=>'Day','style'=>'display:none']) !!}
                    </td>
                    <td>
                        {!! Form::button('<i class="fa fa-edit"></i> Edit', array('type' => 'button', 'class' => 'change btn btn-default')) !!}
                        {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'button', 'class' => 'save btn btn-success','style'=>'display:none')) !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</section><!-- /.content -->
@endsection




@section('footer-script')
<script>
    $(function() {
        $('.change').click(function() {
            var getObject = $(this).parent().parent();

            getObject.find('.label_price').hide();
            getObject.find('.price').show();

            getObject.find('.label_duration').hide();
            getObject.find('.duration').show();

            getObject.find('.change').hide();
            getObject.find('.save').show();
        });

        $('.save').click(function() {
            var getObject = $(this).parent().parent();
            var price = getObject.find('.price').val();
            var duration = getObject.find('.duration').val();
            var lm_id = getObject.find('.lm_id').val();
            var lab_id = getObject.find('.lab_id').val();
            var method_id = getObject.find('.method_id').val();

            var data = {
                price, duration, lm_id, lab_id, method_id
            };
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '/settings/save-method-price-duration',
                type: 'POST',
                data: {data: data, _token: _token},
                dataType: 'json',
                success: function(data) {
                    if (data.response_code == 1 || data.response_code == 2) {
                        if (data.response_code == 1) {
                            getObject.find('.lm_id').val(data.id);
                        }
                        getObject.find('.label_price').html(price);
                        getObject.find('.label_price').show();
                        getObject.find('.price').hide();

                        getObject.find('.label_duration').html(duration);
                        getObject.find('.label_duration').show();
                        getObject.find('.duration').hide();

                        getObject.find('.change').show();
                        getObject.find('.save').hide();
                    } else {
                        alert('Unable to update');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Unable to update due to error. See the console log');
                    console.log(errorThrown);
                }
            });


        });

        $('.dataTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 100
        });
    });
</script>
@endsection