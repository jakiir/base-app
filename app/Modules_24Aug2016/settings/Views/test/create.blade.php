@extends('master-admin-default')

@section('title')
    <title>test entry</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Sample Test
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Test</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
            </div>

            {!! Form::open(['url' => '/settings/store-test', 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form']) !!}

            <div class="box-body">

                <!-- text input -->
                <div class="form-group {{$errors->has('tst_name') ? 'has-error' : ''}}">
                    {!! Form::label('tst_name','Sample Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('tst_name','',['class'=>'form-control','placeholder'=>'Enter test name...']) !!}
                        {!! $errors->first('tst_name','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="box-footer">
                    <a href="{{ url('/settings/list-test') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div><!-- /.box-footer -->


                {!! Form::close() !!}

            </div>
        </div>
    </section><!-- /.content -->
@endsection


@section('footer-script')
    <script>
        $(function () {
            $('.lab_checkbox').click(function(){
                var id = $(this).val();
                if($(this).is(":checked")) {
                    $('#lab_conf_' + id).show();
                    $('#lab_conf_' + id + ' input').prop('required',true);
                }else {
                    $('#lab_conf_' + id).hide();
                    $('#lab_conf_' + id + ' input').prop('required',false);
                }
            });


            $('.lab_checkbox').trigger('click');
        });
    </script>
@endsection