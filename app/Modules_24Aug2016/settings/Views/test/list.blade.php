@extends('master-admin-default')

@section('title')
    <title>Test List</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Sample list
            <!--<small>it all starts here</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Test</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <a href="{{ url('/settings/create-test') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> Add Sample', array('type' => 'button', 'class' => 'btn btn-primary')) !!}
                    </a>
                </h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="test_list" class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>Sample Name</th>
                        <th>Labs</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($getList as $row)
                        <tr>
                            <td>{!! $row->tst_name !!}</td>
                            <td>{!! $row->lab_names !!}</td>
                            <td>{!! CommonFunction::updatedOn($row->updated_at) !!}</td>
                            <td>
                                {{--<i class="glyphicon glyphicon-edit"></i>--}}
                                {!! link_to('settings/edit-test/'. Encryption::encodeId($row->tst_id),'Edit',['class' => 'btn btn-info btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.content -->

@endsection

@section('footer-script')
    <script>

        $(function () {
            $('#test_list').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength":100
            });
        });

    </script>
@endsection
