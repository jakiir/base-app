<?php

Route::group(array('module' => 'Users', 'namespace' => 'App\Modules\Users\Controllers'), function() {

    Route::get('/users', function() {
        return view("users::index");
    });

    Route::get('/users/create', [
        'as' => 'user_create_url',
        'uses' => 'UsersController@create'
    ]);
    Route::patch('/users/store', "UsersController@store");
    Route::patch('/users/update/{id}', "UsersController@update");

    Route::get('users/verify/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@verify'
    ]);

    Route::get('users/activation/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@verify'
    ]);

    Route::get('users/message/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@message'
    ]);

    Route::get('/users/login', "UsersController@login");

    Route::patch('/users/checklogin', [
        'as' => 'pages',
        'uses' => 'UsersController@checklogin'
    ])->middleware(['ocpl.auth']);

    Route::group(['middleware' => 'ocpl.checkvalid'], function () {
        Route::any('/users/dashboard', "UsersController@dashboard");
    });

    Route::get('/users/logout', "UsersController@logout");

    Route::any('login/google', 'UsersController@loginWithGoogle');
    Route::get('/users/message', "UsersController@message");

    Route::get('users/social_registration/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@social_registration'
    ]);

    Route::patch('users/social_reg_store/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@social_reg_store'
    ]);


//calling profile update form
    Route::get('users/profileinfo', "UsersController@profileinfo")->middleware(['ocpl.checkvalid']);

//calling to update the user profile info
    Route::patch('users/profile_update', [
        'uses' => 'UsersController@profile_update'
    ])->middleware(['ocpl.checkvalid']);

//Forget Password
    Route::get('users/forgetPassword', "UsersController@forgetPassword");
    Route::patch('users/resetPass', "UsersController@resetPass");
    Route::patch('users/update-password-from-profile', "UsersController@updatePassFromProfile")->middleware(['ocpl.checkvalid']);
    Route::get('users/passVerification/{token_no}', "UsersController@passVerification");
    Route::get('users/reset-password/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@resetPassword'
    ]);

//User List
    Route::get('users/lists', "UsersController@lists")->middleware(['ocpl.checkvalid']);
    Route::get('users/lists/{type}', "UsersController@listByType")->middleware(['ocpl.checkvalid']);
    Route::get('users/create_new', "UsersController@create_new")->middleware(['ocpl.checkvalid']);
    Route::get('/users/edit/{id}', "UsersController@edit")->middleware(['ocpl.checkvalid']);
    Route::get('/users/activate/{id}', "UsersController@activate")->middleware(['ocpl.checkvalid']);
    Route::get('/users/assign/{id}', "UsersController@assign")->middleware(['ocpl.checkvalid']);
    Route::patch('/users/assignTypeDesk/{id}', "UsersController@assignTypeDesk")->middleware(['ocpl.checkvalid']);


//Mail Re-sending
    Route::get('users/reSendEmail', "UsersController@reSendEmail");
    Route::patch('users/reSendEmailConfirm', "UsersController@reSendEmailConfirm");


//post ajax call for save lab_method_mapping table
    Route::patch('/users/save-google', [
        'as' => 'google-save',
        'middleware' => 'ocpl.auth',
        'uses' => 'UsersController@saveGoogleInfo'
    ]);

//Route::patch('users/save-google', "UsersController@saveGoogleInfo");
Route::get('users/support', "UsersController@support");
    /*     * ********************** End of Route Group *********************** */
});
