@extends('master-admin-default')

@section('title')
<title>Profile Info</title>
@endsection

@section('content')

<div class="col-md-12">
    
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible" >
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon fa fa-check"></i>  {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon fa fa-check"></i>  {{ Session::get('error') }}		
</div>
@endif 

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Update Profile</a></li>
            <?php if ($users->member_social_type == '') { ?>
                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Update Password</a></li>
            <?php } ?>
            <!--            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Access Log</a></li>
                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="true">Activity Log</a></li>-->

            <!--<li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>-->
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="tab_1">
                <h2>Update Profile:</h2>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-6 col-sm-6">
                        {!! Form::open(array('url' => '/users/profile_update','method' => 'patch', 'class' => 'form-horizontal','enctype' =>'multipart/form-data')) !!}
                        <fieldset>
                            {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                            {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                            {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}
                            <div class="form-group has-feedback {{ $errors->has('member_first_name') ? 'has-error' : ''}}">
                                <label  class="col-lg-4 text-left">Name</label>

                                <div class="col-lg-8">
                                    {!! Form::text('member_first_name',$users->member_first_name, $attributes = array('class'=>'form-control required',
                                    'placeholder'=>'Enter your Name','id'=>"member_first_name")) !!}
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if($errors->first('member_first_name'))
                                    <span class="control-label">
                                        <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_first_name','') }}</em>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-lg-4 text-left">Birth Date:</label>
                                <div class="col-lg-7 input-group" style="left: 15px !important;">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::text('member_DOB', $users->member_DOB, $attributes = array('class'=>'form-control required datepicker',
                                    'id'=>"member_DOB",' data-inputmask' => "'alias': 'yyyy-mm-dd'", "data-mask" => "", 'readonly' => 'readonly')) !!}
                                    <!--<input type="text" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>-->
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                            <div class="form-group has-feedback {{ $errors->has('member_phone') ? 'has-error' : ''}}">
                                <label  class="col-lg-4 text-left">Mobile Number  </label>
                                <div class="col-lg-8">
                                    {!! Form::text('member_phone',$users->member_phone, $attributes = array('class'=>'form-control required',
                                    'placeholder'=>'Enter your Mobile Number','id'=>"member_phone")) !!}
                                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                    @if($errors->first('member_phone'))
                                    <span  class="control-label">
                                        <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_phone','') }}</em>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('member_username') ? 'has-error' : ''}}">
                                <label  class="col-lg-4 text-left">User Name</label>
                                <div class="col-lg-8">
                                    {!! Form::text('member_username', $users->member_username, $attributes = array('class'=>'form-control required',
                                    'placeholder'=>'Enter your desired User Name','id'=>"member_username")) !!}
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if($errors->first('member_username'))
                                    <span  class="control-label">
                                        <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_username','') }}</em>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label  class="col-lg-4 text-left">Profile Image</label>
                                <div class="col-lg-4 text-left">
                                    <!--{!! Form::file('image') !!}-->
                                    <input type='file' onchange="readURL(this);" name="image"  />
                                           <!--<img  src="#" alt="" />-->
                                </div>
                                <div class="col-lg-2">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-1 col-sm-1"></div>
                    <div class="col-md-5 col-sm-5"><br/>
                        {{--$anonymous_image = '<img src="' . url() . '/users/upload/anonymous_image.jpg" alt="Anonymous Profile Picture" class="img-responsive img-circle"  />';--}}
                        {{--$db_image = '<img src="' . url() . '/users/upload/' . $users->member_image . '" class="img-responsive img-circle" />';--}}
                        {{--$image_html = ($users->member_image != "") ? $db_image : $anonymous_image;--}}
                        {{--echo $image_html;--}}
                        {{--if($users->member_image != "")--}}

                        @if(!empty(  $users->member_pic))
                        @if(filter_var($users->member_pic, FILTER_VALIDATE_URL))
                        {!! '<img src="' . $users->member_pic . '" class="img-responsive img-circle"  alt="Profile Picture" id="blah"  width="200" />' !!}
                        @else
                        {!! '<img src="' . url() . '/users/upload/' . $users->member_pic . '" class="img-responsive img-circle"  alt="Profile Picture" id="blah"  width="200" />' !!}
                        @endif
                        @else
                        {!! '<img src="' . url() . '/front-end/images/default_profile.jpg' .  '" class="img-responsive img-circle"  alt="Profile Picture" id="blah"  width="200" />' !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="reset" class="btn btn-default"><b>Cancel</b></button>
                            <button type="submit" class="btn btn-primary"><b>Update</b></button>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::close() !!}
                </div>
                <div class="clearfix"></div>
            </div><!-- /.tab-pane -->

            <?php if ($users->member_social_type == '') { ?>
                <div class="tab-pane" id="tab_2">
                    <h2>Update Password</h2>

                    {!! Form::open(array('url' => '/users/update-password-from-profile','method' => 'patch', 'class' => 'form-horizontal',
                            'id' => 'update_pass_form')) !!}
                    <fieldset>
                        <div class="clearfix"><br/><br/></div>
                        <div class="form-group has-feedback {{ $errors->has('member_email') ? 'has-error' : ''}}">
                            <label  class="col-lg-2 text-left">Email Address</label>
                            <div class="col-lg-4">
                                {!! Form::text('member_email', $users->member_email, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter your email', 'readonly' => 'readonly', 'id'=>"member_email")) !!}
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if($errors->first('member_email'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_email','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('member_old_password') ? 'has-error' : ''}}">
                            <label  class="col-lg-2 text-left">Old Password</label>
                            <div class="col-lg-4">
                                {!! Form::password('member_old_password', $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Old password','id'=>"member_old_password")) !!}
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if($errors->first('member_old_password'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_old_password','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('member_new_password') ? 'has-error' : ''}}">
                            <label  class="col-lg-2 text-left">New Password</label>
                            <div class="col-lg-4">
                                {!! Form::password('member_new_password', $attributes = array('class'=>'form-control required','placeholder'=>'Enter your New password','id'=>"member_new_password")) !!}
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if($errors->first('member_new_password'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_new_password','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-8 col-lg-offset-2">
                                <div class="clearfix"><br></div>
                                <button type="submit" class="btn btn-primary"><b>Update Password</b></button>
                            </div>
                        </div>
                    </fieldset>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::close() !!}

                </div><!-- /.tab-pane -->
            <?php } ?>

            <div class="tab-pane" id="tab_3">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S/L</th>
                            <th>Log in time</th>
                            <th>Log out time</th>
                            <th>Access ip</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.tab-pane -->

            <div class="tab-pane" id="tab_4">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                like Aldus PageMaker including versions of Lorem Ipsum.
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->
</div>


<div class="clearfix"></div>

@endsection

@section('footer-script')
<!-- InputMask -->
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $(document).ready(
            function () {
                $("#update_pass_form").validate({
                    errorPlacement: function () {
                        return false;
                    }
                });
            });

        $(function () {
            //Money Euro
            $("[data-mask]").inputmask();
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result)
                            .width(100)
                            .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function ()
        {
            $(".datepicker").datepicker({
                maxDate: "+20Y",
                //showOn: "button",
                //buttonText: "Select date",
                buttonText: "Select date",
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'scale',
                yearRange: "-100:+40",
                minDate: "-200Y",
            });
        });
</script>

@endsection