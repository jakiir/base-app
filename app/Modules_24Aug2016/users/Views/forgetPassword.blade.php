@extends("master-front")

@section("header-script")

@section('title')
Forget Password
@endsection

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />
<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >
<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >
<style>
    *{font-family:"Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;}
    .box-div{background: #edeff1; border-radius:8px;}
    .hr{border-top: 1px solid #d3d3d3;
        box-shadow: 0 1px #fff inset;}
    .footer-p{
        background: #333 none repeat scroll 0 0;
        color: #999;
        padding-bottom:10px;
    }
    footer{
        margin-top:20px;
        line-height:12px;
    }
</style>
@endsection  <!-- header script-->

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-6 col-md-offset-1 box-div">
            <h3 class="text-center"> Password Reset</h3>
            <hr style="border: 1px white solid"><br/>
            
            
            @if(Session::has('success'))<div class="alert alert-success"> {{ Session::get('success') }}</div>@endif
            @if(Session::has('error'))<div class="alert alert-warning">{{ Session::get('error') }}</div> @endif

            <fieldset>
                {!! Form::open(array('url' => 'users/resetPass','method' => 'patch', 'class' => 'form-horizontal')) !!}

                <div class="form-group col-md-5">
                    {!! Form::label('login_social','Email Address') !!}
                    <br/>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    <div class="form-group has-feedback {{ $errors->has('member_email') ? 'has-error' : ''}}">
                        {!! Form::text('member_email', $value = null, $attributes = array('class'=>'form-control required',
                        'placeholder'=>'Enter your Email Address','id'=>"member_email")) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if($errors->first('member_email'))
                        <span  class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_email','') }}</em>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="clearfix"><br/></div>
                    <div class="col-md-7 col-md-offset-2">
                        <button type="submit" class="btn btn-block btn-success"><b>Submit</b></button><br/>
                        <span class="col-md-offset-2">
                            <b>Don't have an account? {!! link_to('users/create', 'Sign Up', array("class" => " ")) !!}</b>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                {!! Form::close() !!}
                
            </fieldset>
        </div>
    </div>
</div>
@endsection 