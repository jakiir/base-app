@extends('master-admin-default')

@section('title')
<title>{{ $title }}</title>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><i class="fa fa-tasks"></i> {{ $title }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('users/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('users/lists') }}">User List</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    @if(Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-warning"> {{ Session::get('error') }}	</div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <span class="col-md-11"></span>
                    <!--<span class="col-md-1"><a href="#"><i class="fa fa-print"></i> Print</a></span>-->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_user_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Email Address<br/> Mobile</th>
                                <!--<th>National Id</th> Not required--> 
                                <th>User Type<br/>Assigned lab</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lists as $list)
                            <?php
                            $memId = Session::get('member_id');
                            $member_type = CommonFunction::getFieldName($list->member_type, 'mt_id', 'mt_type_name', 'member_types');
                            $lab = CommonFunction::getFieldName($list->lab_id, 'lab_id', 'lab_name', 'tbl_lab');
                            ?>
                            @if($list->member_id != $memId)
                            <tr>
                                <td> {{ $list->member_username }}</td>
                                <td>{{ $list->member_email }}<br/>{{ $list->member_phone }}</td>
                                <!--<td>{{ $list->member_nid }}</td>-->
                                <td>{{ $member_type }}<br/>{{ $lab }}</td>
                                <td>
                                    {!! link_to('users/edit/'. Encryption::encodeId($list->member_id),'Edit', ['class' => 'btn btn-primary']) !!}

                                    <?php if ($list->member_social_type == '') { ?>
                                        {!! link_to('users/reset-password/'. Encryption::encodeId($list->member_id),'Reset password', ['class' => 'btn btn-info']) !!}
                                    <?php } ?>
                                    <?php if ($list->member_status == 'inactive') { ?>
                                        {!! link_to('users/activate/'. Encryption::encodeId($list->member_id),'Activate User', ['class' => 'btn btn-success']) !!}
                                    <?php } else if ($list->member_status == 'active') { ?>
                                        {!! link_to('users/activate/'. Encryption::encodeId($list->member_id),'Deactivate User', ['class' => 'btn btn-warning']) !!}
                                    <?php } ?>                                    
                                    <?php
                                    $user_member_type = CommonFunction::getUserType();
                                    if ($user_member_type == '1') { // 1 is for admin users
                                        ?> 
                                        <?php if ($list->member_type == 4) { ?><!---4 is for BCSIR Executives---->
                                            {!! link_to('users/assign/'. Encryption::encodeId($list->member_id),'Assign User Desk', ['class' => 'btn btn-danger']) !!}
                                        <?php } ?>
                                    <?php } ?>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


@endsection <!--content section-->

@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(
            function () {
                $("#user_assign_form").validate({
                    errorPlacement: function () {
                        return false;
                    }
                });
            });

    $(function () {
        $('#tbl_user_list').DataTable({
            "paging": true,
            "lengthChange": false,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 20
        });
    });
</script>

@endsection <!--- footer-script--->
