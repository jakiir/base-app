@extends('master-admin-default')

@section('title')
<title>{{ $title }}</title>
@endsection

@section('content')

    <section class="content-header">
        <h1><i class="fa fa-tasks"></i> {{ $title }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('users/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('users/lists') }}">User List</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                             <div class="box">
                    <div class="box-header">
                        <span class="col-md-11"></span>
                        <span class="col-md-1"><a href="#"><i class="fa fa-print"></i> Print</a></span>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                            {!! Form::open(array('url' => '/users/update','method' => 'patch', 'class' => 'form-horizontal','enctype' =>'multipart/form-data')) !!}
                            <fieldset>
                                {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                                {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                                {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}
                                <div class="form-group has-feedback {{ $errors->has('member_first_name') ? 'has-error' : ''}}">
                                    <label  class="col-lg-4 text-left">Name</label>

                                    <div class="col-lg-8">
                                        {!! Form::text('member_first_name',$users->member_first_name, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Name','id'=>"member_first_name")) !!}
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @if($errors->first('member_first_name'))
                                            <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_first_name','') }}</em>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <?php
                                $user_types = array(
                                        1 => 'Banks',
                                        2 => 'Union Information Center (UIC)',
                                        3 => 'Hajj Agency',
                                        4 => 'SB',
                                        5 => 'Ministry of Religion',
                                        6 => 'Hajj Office',
                                        7 => 'IT Department of Ministry',
                                );
                                ?>

                                <div class="form-group">
                                    <label for="userTypeSelect" class="col-lg-4 text-left">User Type</label>
                                    <div class="col-lg-8">
                                        <select name="USER_TYPE" id="USER_TYPE" class="form-control required">
                                            <option value="">Select One</option>
                                            <?php
                                            foreach ($user_types as $key => $u_type) {
                                            ?>
                                            <option value="<?php echo $key; ?>">
                                                <?php echo $u_type; ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <!--<div class="form-group has-feedback {{ $errors->has('member_DOB') ? 'has-error' : ''}}">
            <label  class="col-lg-4 text-left"> Date of Birth </label>
            <div class="col-lg-8">
                {!! Form::text('member_DOB', $users->member_DOB, $attributes = array('class'=>'form-control required datepicker',
                'placeholder'=>'Enter your Birth Date','id'=>"member_DOB", 'readonly' => "readonly")) !!}
                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                        @if($errors->first('member_DOB'))
                                        <span class="control-label">
                                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_DOB','') }}</em>
                </span>
                @endif
                                        </div>
                                    </div>-->
                                <!-- Date dd/mm/yyyy -->
                                <div class="form-group">
                                    <label  class="col-lg-4 text-left">Date masks:</label>
                                    <div class="col-lg-7 input-group" style="left: 15px !important;">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('member_DOB', $users->member_DOB, $attributes = array('class'=>'form-control required',
                                        'id'=>"member_DOB",' data-inputmask' => "'alias': 'yyyy-mm-dd'", "data-mask" => "")) !!}
                                                <!--<input type="text" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>-->
                                    </div><!-- /.input group -->
                                </div><!-- /.form group -->

                                <div class="form-group has-feedback {{ $errors->has('member_phone') ? 'has-error' : ''}}">
                                    <label  class="col-lg-4 text-left">Mobile Number  </label>
                                    <div class="col-lg-8">
                                        {!! Form::text('member_phone',$users->member_phone, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Mobile Number','id'=>"member_phone")) !!}
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                        @if($errors->first('member_phone'))
                                            <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_phone','') }}</em>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group has-feedback {{ $errors->has('member_username') ? 'has-error' : ''}}">
                                    <label  class="col-lg-4 text-left">User Name</label>
                                    <div class="col-lg-8">
                                        {!! Form::text('member_username', $users->member_username, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your desired User Name','id'=>"member_username")) !!}
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @if($errors->first('member_username'))
                                            <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_username','') }}</em>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label  class="col-lg-4 text-left">Profile Image</label>
                                    <div class="col-lg-4 text-left">
                                        <!--{!! Form::file('image') !!}-->
                                        <input type='file' onchange="readURL(this);" name="image"  />

                                        <!--<img  src="#" alt="" />-->
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->


@endsection
