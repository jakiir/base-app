@extends("email-template")

@section('title')
    {!! $header !!}
@endsection

@section('content')
    Your account password is: {!! $member_password !!}
    <br /> <br /> <br />
    For security purpose please change your password after you logged in to the system.                                       
@endsection 
