@extends('master-admin-default')

@section('title')
<title>Edit User's Information</title>
@endsection

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="background: snow; opacity:0.7; border-radius:8px;">

            <h3 class="text-center">Edit User's Information</h3>
            @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning"> {{ Session::get('error') }}	</div>
            @endif             
            <hr/>
            <div class="col-md-8 col-sm-8 col-md-offset-2">

                {!! Form::open(array('url' => '/users/update/'.Encryption::encodeId($users->member_id),'method' => 'patch', 'class' => 'form-horizontal', 
                'id'=> 'user_edit_form')) !!}
                <fieldset>

                    {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                    {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                    {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}

                    <?php
                    $random_number = str_random(30);
                    ?>
                    {!! Form::hidden('TOKEN_NO', $random_number) !!}

                    <div class="form-group has-feedback {{ $errors->has('member_first_name') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">Name</label>
                        <div class="col-lg-8">                                                                     
                            {!! Form::text('member_first_name', $value = $users->member_first_name, $attributes = array('class'=>'form-control',
                            'id'=>"member_first_name",  'readonly' => "readonly")) !!}
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if($errors->first('member_first_name'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_first_name','') }}</em>
                            </span>
                            @endif     
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">User Specification </label>
                        <div class="col-lg-8">
                            {!! Form::select('member_type', $value = $user_types, $users->member_type, $attributes = array('class'=>'form-control required', 
                            'placeholder' => 'Select One', 'id'=>"member_type")) !!}
                            @if($errors->first('member_type'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_type','') }}</em>
                            </span>
                            @endif    
                        </div>
                    </div>

                    <div class="<?php
                    if (!in_array($users->member_type, array(6, 7, 8))) {
                        echo 'hidden';
                    }
                    ?> form-group has-feedback {{ $errors->has('lab_id') ? 'has-error' : ''}}" id="lab_div">                        
                        <label  class="col-lg-4 text-left required-star"> User's Lab </label>
                        <div class="col-lg-8">
                            {!! Form::select('lab_id', $value = $user_desks, $users->lab_id, $attributes = array('class'=>'form-control', 
                            'placeholder' => 'Select One','id'=>"lab_id")) !!}
                            @if($errors->first('lab_id'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('lab_id','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_nid') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">National ID No.  </label>                                                
                        <div class="col-lg-8">
                            {!! Form::text('member_nid', $value = $users->member_nid, $attributes = array('class'=>'form-control required', 'id'=>"member_nid", )) !!}
                            <span class="glyphicon glyphicon-flag form-control-feedback"></span>
                            @if($errors->first('member_nid'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_nid','') }}</em>
                            </span>
                            @endif    
                        </div>                   
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_DOB') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left"> Date of Birth </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_DOB', $value = $users->member_DOB, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter your Birth Date','id'=>"member_DOB", 'readonly' => "readonly")) !!}
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            @if($errors->first('member_DOB'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_DOB','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_phone') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">Mobile Number  </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_phone', $value = $users->member_phone, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter the Mobile Number','id'=>"member_phone")) !!}
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            @if($errors->first('member_phone'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_phone','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('member_email') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">Email Address 

                        </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_email', $value = $users->member_email, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter your Email Address','id'=>"member_email")) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if($errors->first('member_email'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_email','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label  class="col-lg-4 text-left">User Name</label>
                        <div class="col-lg-8">
                            <?php echo  $users->member_username;?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-2">
                            <button type="submit" class="btn btn-lg btn-block btn-primary"><b>Submit</b></button>
                        </div>
                    </div>

                    <div class="col-md-8 col-md-offset-2">
                        {!! CommonFunction::showAuditLog($users->updated_at, $users->updated_by) !!}
                    </div>
                </fieldset>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!--</form>-->
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection <!--- content--->

@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(
            function () {
                $("#user_edit_form").validate({
                    errorPlacement: function () {
                        return false;
                    }
                });
            });

    $(document).ready(function () {
        $('#member_type').trigger("change");
        $('#member_type').on('change', function () {
            if ($(this).val() == 6 || $(this).val() == 7 || $(this).val() == 8) {
                $('#lab_div').removeClass('hidden');
                $('#lab_id').addClass('required');
            } else {
                $('#lab_div').addClass('hidden');
                $('#lab_id').removeClass('required');
            }
        });
    });
</script>
@endsection <!--- footer-script--->