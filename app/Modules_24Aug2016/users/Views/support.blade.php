@extends("master-front")

@section("header-script")

@section('title')
বিসিএসআইআর  | Bangladesh Council of Scientific and Industrial Research (BCSIR)-Government of the People's Republic of Bangladesh | বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ (বিসিএসআইআর)-গণপ্রজাতন্ত্রী বাংলাদেশ সরকার
@endsection

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />
<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >
<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >
<style type="text/css">
		.container{background:#fff;}
        *{margin:0; padding:0; outline:none}
        img,fieldset{border:0}
        a{text-decoration:none; color:#000;}
        p{color:#000; font-size:13px;}

        .clear{
            clear:both;
        }

        body {
            font-size:12px;
            color:#000;
            font-family:Arial, Helvetica, sans-serif;
        }
        .wrapper-s {
            width:850px;
            margin:20px auto;
            overflow:hidden;
            padding:10px 30px;
            background:#f3f3f3;
        }
        .q-support {
            padding:20px 20px;
             text-align: left;
        }
        .item-s h3{
            font-size:16px;
            line-height:20px;
            padding-bottom:6px;
            text-decoration:underline;
            color:#039;
            font-style:italic;
            text-align: left;
        }
        .item-s p{
            font-size:12px;
            line-height:20px;
            padding-bottom:20px;
            text-align: justify;
        }
        .q-support p a{
            font-size:17px;
            color:#1953a1;
            line-height:30px;
        }
        .q-support p a:hover{
            text-decoration:underline;
            color:#039;
        }
        .q-support h2{
            color:#0a6829;
            font-size:25px;
            padding-bottom:3px;
            margin-bottom:6px;
            border-bottom:1px solid #e1dede;
            text-shadow:0px 1px 0px #999;
        }
        .item-s p span{
            font-size:15px;
            color:#05326e;
        }
    </style>
@endsection  <!-- header script-->

@section("content")

<div class="container">
  <div class="q-support">
                <p><a href="support/#signup">Why I need to Sign-up to submit application?</a></p>
                <p><a href="support/#appstatus">How can I get updates on the application status?</a></p>
                <p><a href="support/#itsupport">To whom I should contact for technical support?</a></p>

                <div class="item-s">
                    <h3 id="appstatus">পরিকল্পনা ও উন্নয়ন বিভাগ</h3>
                    <p>বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ (বিসিএসআইআর)-এর বিভিন্ন গবেষণাগার, ইনস্টিটিউট, বিভাগের মধ্যে পরিকল্পনা ও উন্নয়ন বিভাগ অন্যতম। সরকারের উন্নয়ন বাজেট হতে অর্থায়নকৃত উন্নয়ন প্রকল্পের সফল বাস্তবায়নের মাধ্যমে অত্র পরিষদের গবেষণালব্ধ সুফল দেশের জনসাধারণের কাছে পৌছে দেয়া, গবেষণার যুগপযোগী পরিবেশ ও সুযোগ সৃষ্টি করা, প্রযুক্তির বিকাশ ও সম্প্রসারনের সহায়তা করা এ বিভাগের প্রধান কাজ। দেশের উন্নয়নে সরকার কর্তৃক সময় সময় গৃহীত পদক্ষেপের বাস্তবায়ন এবং উন্নয়ন কর্মকান্ডের তদারকীর মাধ্যমে এ বিভাগ জাতীয় গুরুত্বপূর্ণ ভূমিকা পালন করে আসছে। পরিষদের স্বল্প, মধ্য ও দীর্ঘ পরিকল্পনা প্রণয়ন ও বাস্তবায়নেও পরিকল্পনা ও উন্নয়ন বিভাগ কাজ করে আসছে। পরিকল্পনা ও উন্নয়ন বিভাগ কাজের ধরণ ও গুরুত্ব বিবেচনায় বিভাগটি পরিষদের চেয়ারম্যান মহোদয়ের সরাসরি তত্ত্বাবধানে একজন পরিচালকের মাধ্যমে পরিচালিত হয়। </p>       
                </div>

                <div class="item-s">
                                            <h3 id="itsupport">To whom I should contact for technical support?</h3>
                                            <p>
                                                Business Automation Ltd. provides technical support for Investor eService. You can contact with the respective officer for your necessary technical support during office hour. <br>
                                                <span>Phone: +88-02-9587353, Ext.1802.</span><br>
                                                <span>Fax: +880-2-914-3656</span><br>
                                                 <span>Mobile: +8801755676725</span><br>
                                                <span>Email: support@batworld.com </span><br>
                                                Online Support portal: <b><a href="http://support.batworld.com" target="_blank">http://support.batworld.com</a></b>
                                            </p>
                                        </div>

   </div>

</div>
@endsection 