@extends('master-admin-default')

@section('title')
<title>Admin Blank Page</title>
@endsection

@section('content')
    <script src="{{ asset('back-end/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back-end/amcharts/pie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('back-end/amcharts/serial.js') }}" type="text/javascript"></script>

<section class="content-header">
    <h1>
        Dashboard
        <!--<small>It all starts here</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<!--        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>-->
    </ol>
</section>  

<!-- Main content -->
<section class="content">



    @if(Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-warning"> {{ Session::get('error') }}	</div>
    @endif  

    <!-- Default box -->

        <?php if ($user_member_type == '4') { // 4 is for unassigned desk users ?>
            <div class="box">
                <div class=" box-header with-border box- warning jumbotron">
                <h3>
                    Please contact to System Administrator or IT Help Desk officer to assign your Desk.<br/><br/>
                    You will get all the available functionality once your desk is assigned!<br/><br/>
                    Thank you!
                </h3>
                </div>
                </div>
        <?php } else { ?>

            <?php foreach($DeshboardObject as $row){
            $div = 'dbobj_'. $row->db_obj_id;
            ?>
            <div class="box col-md-6" style="margin-right: 8px; width: 49%">
                <?php $para1 = DB::select( DB::raw($row->db_obj_para1));

                switch($row->db_obj_type) {
                case 'SCRIPT':
                ?>
                <div id="<?php echo $div;?>" style="width: 500px; height: 320px; text-align:center; "><br /><br />Chart will be loading in 5 sec...</div>
                <?php
                $script = $row->db_obj_para2;
                $datav['charttitle'] = $row->db_obj_title;
                $datav['chartdata'] = json_encode($para1);
                $datav['baseurl'] = url();
                $datav['chartediv'] = $div;
                echo '<script type="text/javascript">'. CommonFunction::updateScriptPara($script, $datav).'</script>';
                break;
                //case 'LIST':
                //  $data= $para1;
                // report_gen($row->db_obj_id,$data,$row->db_obj_title,$row->db_obj_para2);
                //  break;
                default:
                    break;
                }
                ?>
            </div>
            <?php  }  ?>
    <?php } ?>
<div class="col-md-12" style="padding: 0;margin-bottom: 10px;">
    <?php
    if (is_array($labList)) {
        echo Calendar::generate($labList['year'], $labList['month'], $labList['data'], $labList['title']);
    } else {
        echo Calendar::generate($this->uri->segment(3), $this->uri->segment(4));
    }
        ?>
        </div>
</section><!-- /.content -->

@endsection