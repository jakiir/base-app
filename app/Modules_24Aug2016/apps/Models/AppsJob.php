<?php namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;

class AppsJob extends Model {

    protected $table = 'apps_job';
    protected $fillable = array(
        'app_id','job_name','updated_by','tst_id','lab_id','test_sample_id','lab_reference_no', 'lab_incharge_id', 'test_start', 'test_end'
    );

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

    }

}
