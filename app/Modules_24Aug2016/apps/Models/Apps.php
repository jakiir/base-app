<?php namespace App\Modules\Apps\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Apps extends Model {

    protected $table = 'apps_details';
    protected $fillable = array(
        'app_status','updated_by','tracking_number','company_id','user_id','submitted_by','submitted_contact','status_color'
    );

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /**
     * @return mixed
     */
    public static function getAppList(){

        $userType =  CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
//        dd($userType);
        if($userType == 5) //Applicant
        {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->leftJoin('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
                ->where('apps_details.user_id','=', CommonFunction::getUserId())
                ->groupBy('apps_details.app_id')
                ->orderBy('apps_details.created_at','desc')
                ->get(['as1.status_color','apps_details.app_id','apps_details.tracking_number','c.company_name','as1.status_name','apps_details.created_at',DB::raw('group_concat(job_name order by job_name separator ", ") job_names')]);
        }
        elseif($userType == 1) //Sys Admin
        {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->leftJoin('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
//                ->where('apps_details.user_id','=', CommonFunction::getUserId())
                ->groupBy('apps_details.app_id')
                ->where('apps_details.app_status','!=',0)
                ->orderBy('apps_details.created_at','desc')
                ->get(['as1.status_color','apps_details.app_id','apps_details.tracking_number','c.company_name','as1.status_name','apps_details.created_at',DB::raw('group_concat(job_name order by job_name separator ", ") job_names')]);
        }
        elseif($userType == 3)//duty officer
        {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->leftJoin('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
//            ->where('apps_details.user_id','=', CommonFunction::getUserId())
                ->where('apps_details.app_status','!=',0)
                ->orderBy('apps_details.created_at','desc')
                ->groupBy('apps_details.app_id')
                ->get(['as1.status_color','as1.status_name','apps_details.app_id','apps_details.app_status','apps_details.tracking_number','c.company_name','apps_details.app_status','apps_details.created_at',DB::raw('group_concat(job_name order by job_name separator ", ") job_names')]);
        }
        elseif($userType == 6 )//Lab Incharge
        {

            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->join('apps_test_sample as ats', 'ats.job_id', '=', 'aj.job_id')
                ->leftJoin('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
//            ->where('apps_details.user_id','=', CommonFunction::getUserId())
                ->where('apps_details.app_status','=',2)  //4 = Received in Lab // It has been changed to 2 and delete the Received in Lab status
                ->where('aj.lab_id','=',CommonFunction::getLabId())
                ->whereIn('aj.test_status', array(5,7))
                 ->where('ats.test_status','=',0)
                ->where('ats.lab_incharge_id','=',$userId)
                ->groupBy('apps_details.app_id')
                ->orderBy('apps_details.created_at','desc')
                ->get(['aj.test_status','as1.status_color','as1.status_name','apps_details.app_id','apps_details.tracking_number','c.company_name','apps_details.app_status','apps_details.created_at',DB::raw('group_concat(job_name order by job_name separator ", ") job_names')]);


        }
        elseif($userType == 8)//Lab Director
        {

            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->leftJoin('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
//            ->where('apps_details.user_id','=', CommonFunction::getUserId())
                ->where('apps_details.app_status','=',2) // 2= Sent to Lab
                ->where('aj.lab_id','=',CommonFunction::getLabId())
                ->whereIn('aj.test_status',array(4,6,7, 5))
                ->groupBy('apps_details.app_id')
                ->orderBy('apps_details.created_at','desc')
                ->get(['aj.test_status','as1.status_color','as1.status_name','apps_details.app_id','apps_details.tracking_number','c.company_name','apps_details.app_status','apps_details.created_at',DB::raw('group_concat(job_name order by job_name separator ", ") job_names')]);


        }


        elseif($userType==7) //Scientist
        {
            return Apps::join('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->join('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->join('apps_status as as1', 'as1.status_id', '=', 'apps_details.app_status')
                ->join('apps_test_sample as ats','ats.job_id','=','aj.job_id')
                ->join('apps_result as ar','ar.ts_id','=','ats.ts_id')
                ->where('apps_details.app_status','=',2)
                ->where('aj.lab_id','=',CommonFunction::getLabId())
                ->where('ats.scientist_id','=', CommonFunction::getUserId())
                ->where('aj.test_status','!=',6)
                ->where('ats.test_status','!=',1)
//                ->where('aj.test_status','!=',7)                    
//                ->where('ar.status','!=','Verified')
//                ->where('ar.status','!=','Discard')
                ->groupBy('apps_details.app_id')
                ->orderBy('apps_details.created_at','desc')
                ->get(['aj.test_status','as1.status_color','apps_details.app_id','apps_details.tracking_number','c.company_name','as1.status_name','apps_details.created_at',DB::raw('group_concat(distinct job_name order by job_name separator ", ") job_names')]);

        }

        else {
            return [];
        }
    }

    /**
     * @param $app_id
     * @return mixed
     */
    public static function getAppData($app_id){

        return
            Apps::leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                ->leftJoin('apps_test_sample as at', 'at.job_id', '=', 'aj.job_id')
                ->leftJoin('companies as cm', 'apps_details.company_id', '=', 'cm.company_id')
                ->where('apps_details.app_id', $app_id)
                ->orderBy('aj.job_id')
                ->first(
                    [
                        'apps_details.*',
                        'aj.job_name',
                        'at.method_id',
                        'at.lab_id',
                        'at.price',
                        'at.duration',
                        'at.remarks',
                        'cm.company_name',
                        DB::raw('concat("House: ",company_house_no,", Flat: ",company_flat_no,", Street: ",company_street,", ",company_city,"-",company_zip) location'),
                        DB::raw('concat(contact_person,", Phone: ",contact_phone,", Email: ",contact_email) contact')
                    ]);
    }

    /**
     * @param $app_id
     * @return mixed
     */
    public static function getAppDataMaster($app_id){
        $lab_id= CommonFunction::getLabId();
        $user_type= CommonFunction::getUserType();
        $user_id= CommonFunction::getUserId();

        if($user_type== 8) ///Lab Director
        {
            return
                Apps::leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                    ->leftJoin('apps_test_sample as at', 'at.job_id', '=', 'aj.job_id')
                    ->leftJoin('apps_result as ar', 'ar.ts_id', '=', 'at.ts_id')
                    ->leftJoin('tbl_method as tm', 'tm.method_id', '=', 'at.method_id')
                    ->groupBy('aj.job_id')
                    ->orderBy('aj.job_id')
                    ->where('apps_details.app_id', $app_id)
                    ->where('aj.lab_id','=',$lab_id)
                    ->get([
                        'aj.job_name','aj.job_id','aj.tst_id','aj.remarks', 'aj.test_status', 'at.lab_incharge_id','aj.test_sample_id','aj.lab_reference_no',
                        DB::raw('group_concat(distinct at.method_id ORDER BY at.method_id asc) method_ids'),
                        DB::raw('group_concat(distinct tm.param_id) param_ids'),
                        DB::raw('group_concat(at.remarks ORDER BY at.method_id asc  SEPARATOR "@@") remarks2'),
                        DB::raw('group_concat(at.unit  SEPARATOR "@@") unit2'),
                        DB::raw('group_concat(at.limit  SEPARATOR "@@") limit2'),
                        DB::raw('group_concat(at.price ORDER BY at.method_id asc  SEPARATOR "@@") prices2'),
//                        DB::raw('group_concat(at.scientist_id SEPARATOR "@@") scientist2'),
                        DB::raw('group_concat(at.scientist_id ORDER BY at.method_id asc SEPARATOR "@@") scientist2'),
                        DB::raw('group_concat(at.lab_incharge_id  ORDER BY at.method_id asc   SEPARATOR "@@") lab_incharge_id2'),
                        DB::raw('group_concat(ar.status ORDER BY at.method_id asc  SEPARATOR "@@") MethodTestStatus2'),
                        DB::raw('group_concat(at.duration ORDER BY at.method_id asc  SEPARATOR "@@") durations2') ,
                        DB::raw('group_concat(distinct aj.lab_id  SEPARATOR "@@") lab_ids2')
                    ]);
        }
        elseif($user_type == 6) //Lab Incharge
        {
            return
                Apps::leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                    ->leftJoin('apps_test_sample as at', 'at.job_id', '=', 'aj.job_id')
                    ->leftJoin('apps_result as ar', 'ar.ts_id', '=', 'at.ts_id')
                    ->leftJoin('tbl_method as tm', 'tm.method_id', '=', 'at.method_id')
                    ->groupBy('aj.job_id')
                    ->orderBy('at.job_id')
                    ->where('apps_details.app_id', $app_id)
                    ->where('aj.lab_id','=',$lab_id)
                    ->where('at.lab_incharge_id','=',$user_id)
                    ->get([
                        'aj.job_name','aj.job_id','aj.tst_id','aj.remarks', 'aj.test_status', 'at.lab_incharge_id','aj.test_sample_id','aj.lab_reference_no',
                        DB::raw('group_concat(distinct at.method_id ORDER BY at.method_id asc) method_ids'),
                        DB::raw('group_concat(distinct tm.param_id) param_ids'),
                        DB::raw('group_concat(at.remarks ORDER BY at.method_id asc  SEPARATOR "@@") remarks2'),
                        DB::raw('group_concat(at.unit  SEPARATOR "@@") unit2'),
                        DB::raw('group_concat(at.limit  SEPARATOR "@@") limit2'),
                        DB::raw('group_concat(at.price ORDER BY at.method_id asc   SEPARATOR "@@") prices2'),
                        DB::raw('group_concat(at.scientist_id ORDER BY at.method_id asc SEPARATOR "@@") scientist2'),
                        DB::raw('group_concat(at.lab_incharge_id ORDER BY at.method_id asc SEPARATOR "@@") lab_incharge_id2'),
                        DB::raw('group_concat(ar.status ORDER BY at.method_id asc SEPARATOR "@@") MethodTestStatus2'),
                        DB::raw('group_concat(at.duration ORDER BY at.method_id asc   SEPARATOR "@@") durations2') ,
                        DB::raw('group_concat(distinct aj.lab_id  SEPARATOR "@@") lab_ids2')
                    ]);
        }

        else
        {
            return
                Apps::leftJoin('apps_job as aj', 'aj.app_id', '=', 'apps_details.app_id')
                    ->leftJoin('apps_test_sample as at', 'at.job_id', '=', 'aj.job_id')
                    ->leftJoin('tbl_method as tm', 'tm.method_id', '=', 'at.method_id')
                    ->groupBy('aj.job_id')
                    ->orderBy('aj.job_id')
                    ->where('apps_details.app_id', $app_id)
                    ->get([
                        'aj.job_name','aj.job_id','aj.tst_id', 'at.lab_incharge_id','aj.test_sample_id','aj.lab_reference_no',
                        DB::raw('group_concat(distinct at.method_id ORDER BY at.method_id asc) method_ids'),
                        DB::raw('group_concat(distinct tm.param_id) param_ids'),
                        DB::raw('group_concat(at.remarks  SEPARATOR "@@") remarks2'),
                        DB::raw('group_concat(at.unit  SEPARATOR "@@") unit2'),
                        DB::raw('group_concat(at.limit  SEPARATOR "@@") limit2'),
                        DB::raw('group_concat(at.price  SEPARATOR "@@") prices2'),
                        DB::raw('group_concat(at.duration  SEPARATOR "@@") durations2') ,
                        DB::raw('group_concat(distinct aj.lab_id  SEPARATOR "@@") lab_ids2')
                    ]);
        }

    }

    public static function getMethodsData($app_id){
        return
            DB::table('apps_job as j')
                ->leftJoin('tbl_test_name as t', 'j.tst_id', '=', 't.tst_id')
                ->leftJoin('tbl_parameter as p', 'j.tst_id', '=', 'p.tst_id')
                ->join('tbl_method as m', 'm.param_id', '=', 'p.param_id')
                ->where('j.app_id','=', $app_id)
                ->orderBy('p.param_name','asc')
                ->get(['m.method_name','m.method_id','p.param_name','p.param_id','t.tst_id','t.tst_name','j.job_id','j.job_name']);
    }

    public static function getMethodsDataForCorrespondingLab($app_id){
        $lab_id= CommonFunction::getLabId();
        $user_type= CommonFunction::getUserType();
        $user_id= CommonFunction::getUserId();

        if($user_type == 8) {
            return
                DB::table('apps_job as j')
                    ->leftJoin('tbl_test_name as t', 'j.tst_id', '=', 't.tst_id')
                    ->leftJoin('tbl_parameter as p', 'j.tst_id', '=', 'p.tst_id')
                    ->join('tbl_method as m', 'm.param_id', '=', 'p.param_id')
                    ->where('j.app_id', '=', $app_id)
                    ->where('j.lab_id', '=', $lab_id)
                    ->orderBy('m.method_id','desc')
                    ->get(['m.method_name', 'm.method_id', 'p.param_name', 'p.param_id', 't.tst_id', 't.tst_name', 'j.job_id', 'j.job_name']);
        }
        elseif($user_type == 6) {
            return
                DB::table('apps_job as j')
                    ->leftJoin('tbl_test_name as t', 'j.tst_id', '=', 't.tst_id')
                    ->leftJoin('apps_test_sample as ats', 'ats.job_id', '=', 'j.job_id')
                    ->leftJoin('tbl_parameter as p', 'j.tst_id', '=', 'p.tst_id')
                    ->join('tbl_method as m', 'm.param_id', '=', 'p.param_id')
                    ->where('j.app_id', '=', $app_id)
                    ->where('ats.lab_incharge_id', '=', $user_id)
                    ->orderBy('m.method_id','desc')
                    ->get(['m.method_name', 'm.method_id', 'p.param_name', 'p.param_id', 't.tst_id', 't.tst_name', 'j.job_id', 'j.job_name']);
        }
    }


    public static function generateReportByAPP($app_id){
        $test_samples = array();

        $userType = CommonFunction::getUserType();
        if($userType == 6 || $userType == 8) {
            $jobs = DB::table('apps_job as aj')
                ->leftJoin('apps_details as ad', 'ad.app_id', '=', 'aj.app_id')
                ->leftJoin('companies as c', 'c.company_id', '=', 'ad.company_id')
                ->leftJoin('apps_test_sample as ats', 'aj.job_id', '=', 'ats.job_id')
                ->leftJoin('apps_result as ar','ar.ts_id','=','ats.ts_id')
                ->where('aj.lab_id','=',CommonFunction::getLabId())
                ->where('aj.app_id', $app_id)
                ->groupBy('ats.scientist_id')
                ->get([
                    'aj.job_id',
                    'aj.job_name',
                    'aj.remarks',
                    'aj.test_sample_id',
                    'aj.lab_reference_no',
                    'ats.lab_incharge_id',
                    'ats.scientist_id',
                    'ats.remarks as remark',
                    'ats.unit',
                    'ats.limit',
                    'aj.updated_at',
                    'aj.created_at',
                    'aj.test_start',
                    'aj.test_end',
                    'aj.test_status',
                    'ad.tracking_number',
                    'ad.submitted_by',
                    'ad.created_at as application_date',
                    'ad.submitted_contact',
                    'c.company_name',
                    'c.company_house_no',
                    'c.company_flat_no',
                    'c.company_street',
                    'c.company_city',
                    'c.company_zip',
                    'c.contact_phone',
                    'ar.description',
                    'ar.remarks as remarks_li',
                    'ar.results_file',
                    'ar.iso_logo',
                    'ar.status as rstatus',
                    'ar.created_at as rcreate',
                    'ar.updated_at as rupdate',
                ]);
        }
        else{
            $jobs = DB::table('apps_job as aj')
                ->leftJoin('apps_details as ad', 'ad.app_id', '=', 'aj.app_id')
                ->leftJoin('companies as c', 'c.company_id', '=', 'ad.company_id')
                ->leftJoin('apps_test_sample as ats', 'aj.job_id', '=', 'ats.job_id')
                ->leftJoin('apps_result as ar','ar.ts_id','=','ats.ts_id')
                ->where('aj.app_id', $app_id)
                ->groupBy('ats.scientist_id')
                ->get([
                    'aj.job_id',
                    'aj.job_name',
                    'aj.remarks',
                    'aj.test_sample_id',
                    'aj.lab_reference_no',
                    'ats.lab_incharge_id',
                    'ats.scientist_id',
                    'aj.updated_at',
                    'aj.created_at',
                    'aj.test_start',
                    'aj.test_end',
                    'aj.test_status',
                    'ad.tracking_number',
                    'ad.submitted_by',
                    'ad.submitted_contact',
                    'ad.created_at as application_date',
                    'c.company_name',
                    'c.company_house_no',
                    'c.company_flat_no',
                    'c.company_street',
                    'c.company_city',
                    'c.contact_phone',
                    'c.company_zip',
                    'ats.remarks as remark',
                    'ats.unit',
                    'ats.limit',
                    'ar.description',
                    'ar.remarks as remarks_li',
                    'ar.results_file',
                    'ar.iso_logo',
                    'ar.status as rstatus',
                    'ar.created_at as rcreate',
                    'ar.updated_at as rupdate',
                ]);


        }
        $strJob = "-1";
        foreach ($jobs as $key => $value) {
            $test_samples['app_header'][$value->scientist_id] = $value;

            $strJob .=",$value->scientist_id";
            $test_samples['job_details'][$value->scientist_id] =  DB::table('apps_test_sample as ats')
                ->leftJoin('tbl_lab as lab','lab.lab_id','=','ats.lab_id')
                ->leftJoin('tbl_method as tm','tm.method_id','=','ats.method_id')
                ->leftJoin('tbl_parameter as tp','tp.param_id','=','tm.param_id')
                ->leftJoin('apps_result as ar','ar.ts_id','=','ats.ts_id')
                ->where('ats.job_id',$value->job_id)
                ->where('ats.scientist_id',$value->scientist_id)
//                ->groupBy('ats.scientist_id')
                ->get([
                    'ats.ts_id',
                    'ats.job_id',
                    'ats.method_id',
                    'ats.unit',
                    'ats.limit',
                    'ats.lab_id',
                    'lab.lab_name',
                    'tm.method_name',
                    'tp.param_name',
                    'ar.status',
                    'ar.remarks',
                    'ats.remarks as remarks_li',
                    'ar.results_file',
                    'ar.description',
                    'ar.iso_logo',
                ]);
        }
//        dd($test_samples);
        return $test_samples;
    }

    public static function getAppDetails($app_id){
        return
            DB::table('apps_job as aj')
                ->leftJoin('apps_details as ad','ad.app_id','=','aj.app_id')
                ->leftJoin('apps_test_sample as ats','ats.job_id','=','aj.job_id')
                ->leftJoin('tbl_lab as lab','lab.lab_id','=','ats.lab_id')
                ->leftJoin('tbl_test_name as tn','tn.tst_id','=','aj.tst_id')
                ->where('ad.app_id',$app_id)
                ->get([
                    'ad.tracking_number',
                    'lab.lab_name',
                    'tn.tst_name',
                    'lab.lab_id'
                ]);
    }

    public static function getResultData($app_id){

        $user_type = CommonFunction::getUserType();
        if($user_type == 7) { // Scientist
            return DB::table('apps_test_sample as ats')//*
            ->leftJoin('apps_result as a', 'a.ts_id', '=', 'ats.ts_id')//data section *

            ->leftJoin('tbl_method as m', 'm.method_id', '=', 'ats.method_id')//additional data section->method name
            ->leftJoin('tbl_parameter as p', 'p.param_id', '=', 'm.param_id')//
            ->leftJoin('tbl_test_name as t', 't.tst_id', '=', 'p.tst_id')
                ->leftJoin('apps_job as j', 'j.job_id', '=', 'ats.job_id')
                ->leftJoin('apps_details as d', 'd.app_id', '=', 'j.app_id')
                ->where('d.app_id', '=', $app_id)
//                ->where('a.status', '!=', 'Verified')
                ->where('a.status', '!=', 'Discard')
                ->where('ats.scientist_id','=', CommonFunction::getUserId())
                ->orderBy('ats.method_id','asc')
                ->get(['m.method_name', 'm.method_id', 'p.param_name', 'p.param_id', 't.tst_id', 't.tst_name', 'd.app_id', 'ats.ts_id','ats.unit','ats.limit', 'a.status', 'a.remarks','ats.remarks as remarks_li', 'a.description','a.results_file','a.iso_logo', 'a.updated_at', 'a.updated_by']);

        }
        else {
            return DB::table('apps_test_sample as ats')//*
            ->leftJoin('apps_result as a', 'a.ts_id', '=', 'ats.ts_id')//data section *
            ->leftJoin('tbl_method as m', 'm.method_id', '=', 'ats.method_id')//additional data section->method name
            ->leftJoin('tbl_parameter as p', 'p.param_id', '=', 'm.param_id')//
            ->leftJoin('tbl_test_name as t', 't.tst_id', '=', 'p.tst_id')
                ->leftJoin('apps_job as j', 'j.job_id', '=', 'ats.job_id')
                ->leftJoin('apps_details as d', 'd.app_id', '=', 'j.app_id')
                ->where('d.app_id', '=', $app_id)
                ->orderBy('m.method_name')
                ->get(['m.method_name', 'm.method_id', 'p.param_name', 'p.param_id', 't.tst_id', 't.tst_name', 'd.app_id', 'ats.ts_id','ats.unit','ats.limit', 'a.status', 'a.remarks','a.iso_logo', 'ats.remarks as remarks_li',  'a.description','a.results_file', 'a.updated_at', 'a.updated_by']);
        }
    }
    
    public function getJobListByDates($date)
    {
        $sql = "select lab.lab_name, job.job_name,asd.tracking_number,SUBSTR(DATE_ADD(ts.created_at, INTERVAL (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) DAY),1,10) as deliverydate, (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) as duration
        from apps_test_sample ts
        INNER JOIN tbl_lab lab on lab.lab_id = ts.lab_id
        INNER JOIN apps_job job on ts.job_id = job.job_id
        INNER JOIN apps_details asd on asd.app_id = job.app_id
        where SUBSTR(DATE_ADD(ts.created_at, INTERVAL (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) DAY),1,10)
        = '$date'
        GROUP BY job.job_id
        ORDER BY lab.lab_name";
        return DB::select($sql);

    }


    public static function generateInvoiceByAPP($app_id){
        $test_samples = array();

        $appsInfo = DB::table('apps_details as ad')
            ->where('ad.app_id',$app_id)
            ->first();



        $jobs = DB::table('apps_job as aj')
            ->where('aj.app_id',$app_id)
            ->get([
                'aj.job_id',
                'aj.job_name'
            ]);

        foreach($jobs as $job){
            $jobparametter[$job->job_id] = DB::table('apps_job as aj')
                ->where('aj.app_id',$app_id)
                ->where('aj.job_id',$job->job_id)
                ->leftJoin('apps_test_sample', 'aj.job_id', '=', 'apps_test_sample.job_id')
                ->leftJoin('tbl_method', 'apps_test_sample.method_id', '=', 'tbl_method.method_id')
                ->leftJoin('tbl_parameter as tp', 'tbl_method.param_id', '=', 'tp.param_id')
                ->leftJoin('tbl_lab_method_mapping as mPrice', 'tbl_method.method_id', '=', 'mPrice.method_id')
                ->select('aj.job_name','apps_test_sample.price','tbl_method.method_name','tp.param_name')
                ->get([DB::raw('concat(method_name, " /", tp.param_name) method_name'), 'apps_test_sample.price']);
//            dd($jobparametter);
        }

        //dd($jobs);
        $test_samples['jobparametter'] = $jobparametter;
        $test_samples['app_header'] = $appsInfo;
        $test_samples['job_details'] = $jobs;
        
        $duration = DB::table('apps_test_sample as ats')
            ->leftJoin('apps_job as j', 'j.job_id', '=', 'ats.job_id')
            ->where('app_id','=',$app_id)
            ->max('ats.duration');
        $test_samples['max_duration'] = $duration;

		$company = DB::table('apps_job as aj')
                ->leftJoin('apps_details as ad', 'ad.app_id', '=', 'aj.app_id')
                ->leftJoin('companies as c', 'c.company_id', '=', 'ad.company_id')
                ->leftJoin('apps_test_sample as ats', 'aj.job_id', '=', 'ats.job_id')
                ->where('aj.app_id', $app_id)
				->groupBy('aj.job_id')
                ->first([
                    'aj.job_id',
                    'aj.job_name',
                    'aj.remarks',
                    'aj.test_sample_id',
                    'aj.lab_reference_no',
                    'ats.lab_incharge_id',
                    'aj.updated_at',
                    'aj.created_at',
                    'aj.test_start',
                    'aj.test_end',
                    'aj.test_status',
                    'ad.tracking_number',
                    'ad.submitted_by',
                    'ad.submitted_contact',
                    'ad.created_at as application_date',
                    'c.company_name',
                    'c.company_house_no',
                    'c.company_flat_no',
                    'c.company_street',
                    'c.company_city',
                    'c.company_zip',
                    'c.contact_phone',
                    'ats.remarks as remark',
                    'ats.unit',
                    'ats.limit'
                ]);
        $test_samples['company'] = $company;

        return $test_samples;
    }

    /*     * ****************************** End of Model Class************************* */
}
