<?php namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;

class AppsTestSample extends Model {

    protected $table = 'apps_test_sample';
    protected $fillable = array(
        'job_id','method_id','lab_id','updated_by','price','duration','remarks'
    );

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

    }

}
