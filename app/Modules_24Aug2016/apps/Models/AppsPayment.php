<?php namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;

class AppsPayment extends Model {

    protected $table = 'apps_payment';
    protected $fillable = array(
        'app_id','due_payment','new_payment','user_id','is_locked','updated_by'
    );

}
