<?php
/**
 * Created by PhpStorm.
 * User: khaleda
 * Date: 10/29/15
 * Time: 4:41 PM
 */

namespace App\Modules\Apps\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class AppsResult extends Model
{
    protected $table = 'apps_result';
    protected $fillable = array(
        'ts_id','updated_at','updated_by','status','remarks','description','iso_logo'
    );

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

    }
}