<?php

namespace App\Modules\Apps\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppsRequest;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Apps\Models\Apps;
use App\Modules\Apps\Models\AppsResult;
use App\Modules\Apps\Models\AppsJob;
use App\Modules\Apps\Models\AppsTestSample;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\CompanyAssoc;
use App\Modules\Settings\Models\Parameter;
use App\Modules\Settings\Models\Method;
use App\Modules\Settings\Models\TestName;
use App\Modules\Settings\Models\LabMethodMapping;
use App\Modules\Users\Models\UsersModel;
use App\Modules\Apps\Models\AppsPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AppsController extends Controller {

    public function index() {
        $userId = CommonFunction::getUserId();
        $userType = CommonFunction::getUserType();
        $scientist_status = '';
        $lab_incharge_status = '';
        $status = '';

        $getList = Apps::getAppList();

//        dd($getList);


        foreach ($getList as $row) {
            if ($userType == 6) {//Lab Incharge
                $scientist_status[$row->app_id] = AppsJob::leftjoin('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->where('ats.lab_incharge_id', '=', $userId)
                        ->where('ats.scientist_id', '>', 0)
                        ->where('apps_job.app_id', '=', $row->app_id)
                        ->first(['ats.scientist_id']);

                $status[$row->app_id] = AppsJob::leftjoin('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->leftjoin('apps_result as ar', 'ar.ts_id', '=', 'ats.ts_id')
                        ->where('ats.lab_incharge_id', '=', $userId)
                        ->where('ats.scientist_id', '>', 0)
                        ->where('apps_job.app_id', '=', $row->app_id)
                        ->select(DB::raw('SUM(if(ar.status = "Verified", 1, 0)) AS Verified,
                       SUM(if(ar.status = "Pending", 1, 0)) AS Pending,
                       SUM(if(ar.status = "Discard", 1, 0)) AS Discard,
                       SUM(if(ar.status = "Recheck", 1, 0)) AS Recheck'))
                        ->first();
            }

            if ($userType == 8) {//Lab Director
                $lab_incharge_status[$row->app_id] = AppsJob::leftjoin('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
//                    ->where('ats.lab_incharge_id', '=', $userId)
                    ->where('ats.lab_incharge_id', '>', 0)
                    ->where('apps_job.app_id', '=', $row->app_id)
                    ->first(['ats.lab_incharge_id']);
//dd($lab_incharge_status);
                $status[$row->app_id] = AppsJob::leftjoin('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                    ->leftjoin('apps_result as ar', 'ar.ts_id', '=', 'ats.ts_id')
//                    ->where('ats.lab_incharge_id', '=', $userId)
                    ->where('ats.lab_incharge_id', '>', 0)
                    ->where('apps_job.app_id', '=', $row->app_id)
                    ->select(DB::raw('SUM(if(ar.status = "Verified", 1, 0)) AS Verified,
                       SUM(if(ar.status = "Pending", 1, 0)) AS Pending,
                       SUM(if(ar.status = "Discard", 1, 0)) AS Discard,
                       SUM(if(ar.status = "Recheck", 1, 0)) AS Recheck'))
                    ->first();
//                dd($status);
            }

            if ($userType == 7) {//scientist
//                $status_name = ['Discard', 'Verified', 'Pending'];
                $status[$row->app_id] = AppsJob::leftjoin('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->leftjoin('apps_result as ar', 'ar.ts_id', '=', 'ats.ts_id')
                        ->where('ats.scientist_id', '=', $userId)
                        ->where('apps_job.app_id', '=', $row->app_id)
                        ->select(DB::raw('SUM(if(ar.status = "Verified", 1, 0)) AS Verified,
                       SUM(if(ar.status = "Pending", 1, 0)) AS Pending,
                       SUM(if(ar.status = "Discard", 1, 0)) AS Discard,
                       SUM(if(ar.status = "Recheck", 1, 0)) AS Recheck'))
                        ->first();
            }
            if ($userType == 3) { //duty officer
                $pstatus[$row->app_id] = AppsJob::where('app_id', '=', $row->app_id)
                        ->select(DB::raw('SUM(if(test_status not in (8), 1, 0)) AS Pending,
                       SUM(if(test_status = 8, 1, 0)) AS Completed'))
                        ->first();
            }
        }



//        dd($pstatus);

        return view("apps::list", compact('getList', 'scientist_status', 'lab_incharge_status', 'discard', 'status','pstatus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $testSample = ['' => 'Select one'] + TestName::orderBy('tst_name')->lists('tst_name', 'tst_id')->all();
        $company = CompanyAssoc::join('companies as c', 'c.company_id', '=', 'company_assoc.company_id')
                ->where('company_assoc.member_id', '=', DB::raw(CommonFunction::getUserId()))
                ->orderBy('c.company_name')
                ->get(['c.company_name', 'c.company_id']);

        $companyList = ['' => 'Select Company'];
        foreach ($company as $row) {
            $companyList[$row->company_id] = $row->company_name;
        }
//        dd($companyList);
        $userData = UsersModel::where('member_id', CommonFunction::getUserId())->firstOrFail(['member_first_name', 'member_phone']);
        return view("apps::create", compact('testSample', 'companyList', 'userData'));
    }

    /**
     * @param AppsRequest $request
     * @return \Illuminate\View\View
     */
    public function store(AppsRequest $request) {

        $insert = Apps::create([
                    'tracking_number' => '',
                    'app_status' => 0,
                    'company_id' => $request->get('company_id'),
                    'submitted_by' => $request->get('submitted_by'),
                    'submitted_contact' => $request->get('contact_no'),
                    'user_id' => CommonFunction::getUserId()
        ]);
        if ($insert->id > 0) {
            $tracking_number = date("MY") . str_pad($insert->id, 6, '0', STR_PAD_LEFT);
//            $tracking_number =  date('MY'). dechex($tracking_number);

            Apps::where('app_id', $insert->id)->update([
                'tracking_number' => $tracking_number
            ]);

            if ($request->get('job_name')) {
                foreach ($request->get('job_name') as $key => $job) {
                    $jobInsert = AppsJob::create([
                                'app_id' => $insert->id,
                                'job_name' => $job,
                                'test_status' => 4, //For first time entry = 4
                                'tst_id' => $request->get('tst_id')[$key]
                    ]);

                    if (isset($request->get('method_id')[$key])) {
                        foreach ($request->get('method_id')[$key] as $row) {
                            AppsTestSample::create([
                                'method_id' => $row,
                                'job_id' => $jobInsert->id
                            ]);
                        }
                    }
                }
            }
        }
        Session::flash('success', 'Successfully Saved the Application.');
        return redirect('/apps/edit/' . Encryption::encodeId($insert->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $app_id = Encryption::decodeId($id);
        $testMothods = Apps::getResultData($app_id);
        $app_data = Apps::generateReportByAPP($app_id);
        $sampleLists[] = 'Select one';
        $process_status = array('' => 'Select one', 'Discard' => 'Discard', 'Verified' => 'Verified', 'Pending' => 'Pending');
        foreach ($testMothods as $key => $value) {
            $sampleLists[$value->method_id] = $value->param_name . '-> ' . $value->method_name;
        }

        return view("apps::process-desk", compact('id', 'app_id', 'testMothods', 'sampleLists', 'process_status','app_data'));
    }
    public function printShow($id) {

        $app_id = Encryption::decodeId($id);
        $testMothods = Apps::getResultData($app_id);
        $app_data = Apps::generateReportByAPP($app_id);
        $sampleLists[] = 'Select one';
        $process_status = array('' => 'Select one', 'Discard' => 'Discard', 'Verified' => 'Verified', 'Pending' => 'Pending');
        foreach ($testMothods as $key => $value) {
            $sampleLists[$value->method_id] = $value->param_name . '-> ' . $value->method_name;
        }

//             dd($testMothods);
        return view("apps::print-process-desk", compact('id', 'app_id', 'testMothods', 'sampleLists', 'process_status','app_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function updateResult($id, Request $request) {
//        dd($request->all());
        $app_id = Encryption::decodeId($request->get('app_id'));
        $testMothods = Apps::getResultData($app_id);
        $result_file = $request->file('results_file');

        $update_data = [
            'iso_logo'=>$request->get('iso_logo'),
            'remarks'=>$request->get('remarks'),
            'description'=>$request->get('description'),
        ];
        if ($request->hasFile('results_file')) {
            $results_file = time()."_".$result_file->getClientOriginalName();
            $result_file->move('results-file', $results_file);
            $update_data['results_file'] =$results_file;
        }
//        if ($request->hasFile('iso_logo')) {
//            $iso_logo = time()."_".$isoo_logo->getClientOriginalName();
//            $isoo_logo->move('iso-logo', $iso_logo);
//            $update_data['iso_logo'] =$iso_logo;
//        }

        if(count($testMothods)>0) {
            foreach ($testMothods as $key => $methold) {
                AppsResult::where('ts_id', $methold->ts_id)->update([
                    'updated_by' => CommonFunction::getUserId()
                ]);
                if ($key == 0) {
                    AppsResult::where('ts_id', $testMothods[$key]->ts_id)->update($update_data);
                    $job_id = AppsTestSample::where('apps_test_sample.ts_id', $testMothods[$key]->ts_id)
                        ->pluck('job_id');
                    AppsJob::where('job_id', $job_id)
                        ->update([
                            'job_name'=>$request->get('job_name')
                        ]);
                }
                if ($request->get('submit') == 'Submit') {
                    AppsResult::where('ts_id', $methold->ts_id)->update([
                        'status' => 'Verified'
                    ]);
                }
            }
        }


        $track_no = Apps::where('app_id', '=', $app_id)->pluck('tracking_number');

        Session::flash('success', 'Successfully Updated <b class="label-warning"> ' . $track_no . ' </b>  Application.');
        return redirect('apps');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $app_id = Encryption::decodeId($id);
        $dataMaster = Apps::getAppDataMaster($app_id);
//        dd($dataMaster);
        $data = Apps::getAppData($app_id);
        $user_type = CommonFunction::getUserType();
        $lab_id = CommonFunction::getLabId();
        $scientist = null;
        $testStatus = null;

        $MethodTestStatus = [
            'Pending' => 'Pending',
            'Verified' => 'Verified',
            'Discard' => 'Discard',
            'Recheck' => 'Recheck'
        ];


        if ($user_type == 6) {

            $scientist = ['' => 'Select One'] + DB::table('members')
                            ->where('member_type', 7)
                            ->where('lab_id', $lab_id)
                            ->orderBy('member_first_name')
                            ->lists('member_first_name', 'member_id');
            // test status for drop down
            $test_Status = DB::table('apps_job as aj')
                    ->join('apps_details as ats', 'aj.app_id', '=', 'ats.app_id')
                    ->where('ats.app_status', '=', 2)
                    ->where('aj.lab_id', '=', CommonFunction::getLabId())
                    ->get(['aj.test_status']);

            $testStatus = [
                5 => 'Pending',
                6 => 'Verified',
//                7 => 'Sent For Recheck By Lab-Director'
            ];
        }

        $app_status_check = DB::select(DB::raw("select count(*) as countt from `apps_job` where `app_id` = $app_id and (`test_status` = 5 or `test_status` = 4)"))[0];
        $app_status_check = $app_status_check->countt;

        $testSample = ['' => 'Select one'] + TestName::orderBy('tst_name')->lists('tst_name', 'tst_id')->all();
        $company = CompanyAssoc::join('companies as c', 'c.company_id', '=', 'company_assoc.company_id')
                ->where('company_assoc.member_id', '=', $data['user_id'])
                ->orderBy('c.company_name')
                ->get(['c.company_name', 'c.company_id', 'c.company_type']);

        $companyList = ['' => 'Select one'];
        foreach ($company as $row) {
            $companyList[$row->company_id] = $row->company_name;
//            $companyList['company_type'] = $row->company_type;
        }

        $userData = UsersModel::where('member_id', CommonFunction::getUserId())->firstOrFail(['member_first_name', 'member_phone']);

        //$user_type = CommonFunction::getUserType();
        if ($user_type == 5) { //for end user edit when it is in draft status
            if ($data->app_status == 0) {
                return view("apps::edit", compact('lab_id', 'scientist', 'testSample', 'companyList', 'userData', 'data', 'id', 'dataMaster'));
            } else {
                Session::flash('success', 'Application submitted successfully.');
                return redirect('apps/view/' . $id);
            }
        } elseif ($user_type == 3 || $user_type == 6 || $user_type == 8) {//for duty officer or lab incharge or director
            if ((in_array($data->app_status, array(1, 6, 7)) && $user_type == 3) || ($data->app_status == 2 && $user_type == 6)) { //duty officer can edit when it submitted
                $testMothods = Apps::getMethodsData($app_id);

                if ($user_type == 6 || $user_type == 8)
                    $testMothods = Apps::getMethodsDataForCorrespondingLab($app_id);

                $sampleLists[] = 'Select one';

                foreach ($testMothods as $key => $value) {

                    $sampleLists[$value->method_id] = $value->param_name . '-> ' . $value->method_name;
                }
//                dd($testMothods);
                return view("apps::edit-desk", compact('app_status_check', 'test_Status', 'MethodTestStatus', 'testStatus', 'lab_id', 'scientist', 'testSample', 'companyList', 'userData', 'data', 'id', 'dataMaster', 'testMothods', 'sampleLists'));
            } else {
                return redirect('apps/view/' . $id);
            }
        } else {
            // not access
            return 'You have not access.';
        }
    }

    public function appsView($id) {

        $app_id = Encryption::decodeId($id);
        $dataMaster = Apps::getAppDataMaster($app_id);

        $data = Apps::getAppData($app_id);
        $labId = CommonFunction::getLabId();
        $userTypeId = CommonFunction::getUserType();
        $labInchargeList = '';
        $app_status_pending_in_lab = 0;
        if ($userTypeId == 8) {

            $app_status_pending_in_lab = DB::select(DB::raw("select count(*) as countt from `apps_job` where `app_id` = $app_id and (`test_status` = 4 ) and (`lab_id` = $labId)"));
            $app_status_pending_in_lab = $app_status_pending_in_lab[0]->countt;

            $app_status_completed_in_lab = DB::select(DB::raw("select count(*) as countt from `apps_job` where `app_id` = $app_id and (`test_status` = 6 ) and (`lab_id` = $labId)"));
            $app_status_completed_in_lab = $app_status_completed_in_lab[0]->countt;


            $labInchargeList = ['' => 'Select Incharge'] + DB::table('members as mem')
                            ->where('mem.member_type', 6)
                            ->where('mem.lab_id', $labId)
                            ->orderBy('member_first_name')
                            ->lists('mem.member_first_name', 'mem.member_id');
        }

        $trackId = Apps::where('app_id', $app_id)
                ->lists("tracking_number");

        $totalAmount = Apps::where('app_id', $app_id)
                ->lists("total_price");

        $paymentList = Apps::leftJoin('companies', 'companies.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_payment', 'apps_payment.app_id', '=', 'apps_details.app_id')
                ->leftJoin('members', 'members.member_id', '=', 'apps_payment.user_id')
                ->where('apps_details.tracking_number', "=", $trackId[0])
                ->select('apps_details.app_id', 'apps_details.total_price', 'discount_price', 'total_discount_price', 'apps_details.payable', 'apps_details.tracking_number', 'apps_details.total_vat', 'members.member_first_name', 'members.member_middle_name', 'apps_payment.created_at', 'apps_payment.due_payment', 'apps_payment.new_payment', 'companies.company_name', 'companies.company_house_no', 'companies.company_flat_no', 'companies.company_street', 'companies.company_city', 'companies.company_zip', 'companies.company_fax', 'companies.company_web', 'companies.contact_phone')
                ->orderBy('apps_payment.created_at', 'desc')
                ->get();


        $app_status_check = DB::select(DB::raw("select count(*) as countt from `apps_job` where `app_id` = $app_id and (`test_status` = 5 or `test_status` = 4)"))[0];
        $app_status_check = $app_status_check->countt;


        $testSample = ['' => 'Select one'] + TestName::lists('tst_name', 'tst_id')->all();
        $company = CompanyAssoc::join('companies as c', 'c.company_id', '=', 'company_assoc.company_id')
                ->where('company_assoc.member_id', '=', DB::raw(CommonFunction::getUserId()))
                ->orderBy('c.company_name')
                ->get(['c.company_name', 'c.company_id']);

        $companys = Company::where('updated_by', '=', DB::raw(CommonFunction::getUserId()))->first();



        $companyList = ['' => 'Select one'];
        foreach ($company as $row) {
            $companyList[$row->company_id] = $row->company_name;
        }
        $userData = UsersModel::where('member_id', CommonFunction::getUserId())->firstOrFail(['member_first_name', 'member_phone']);

        $testMothods = Apps::getMethodsData($app_id);
        $sampleLists[] = 'Select one';
        foreach ($testMothods as $key => $value) {
            $sampleLists[$value->method_id] = $value->param_name . '-> ' . $value->method_name;
        }
//dd($testMothods);

        return view("apps::view-desk", compact('testSample', 'companys', 'app_status_pending_in_lab', 'app_status_completed_in_lab', 'app_status_check', 'companyList', 'userData', 'data', 'id', 'dataMaster', 'testMothods', 'sampleLists', 'paymentList', 'totalAmount', 'labInchargeList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param AppsRequest $request
     * @return Response
     */
    public function update($id, AppsRequest $request) {

        $app_id = Encryption::decodeId($id);
        Apps::where('app_id', $app_id)->update([
            'app_status' => $request->get('submitted'),
            'company_id' => $request->get('company_id'),
            'submitted_by' => $request->get('submitted_by'),
            'submitted_contact' => $request->get('contact_no'),
            'user_id' => CommonFunction::getUserId(),
            'updated_by' => CommonFunction::getUserId()
        ]);


        $job_ids = AppsJob::where('app_id', $app_id)->get(['job_id']);
        foreach ($job_ids as $row) {
            AppsTestSample::where('job_id', $row->job_id)->delete();
        }
        AppsJob::where('app_id', $app_id)->delete();


        if ($request->get('job_name')) {
            foreach ($request->get('job_name') as $key => $job) {
                $jobInsert = AppsJob::create([

                            'app_id' => $app_id,
                            'job_name' => $job,
                            'test_status' => 4, //For first time entry = 4
                            'tst_id' => $request->get('tst_id')[$key]
                ]);

                if (isset($request->get('method_id')[$key])) {
                    foreach ($request->get('method_id')[$key] as $row) {
                        AppsTestSample::create([
                            'method_id' => $row,
                            'job_id' => $jobInsert->id
                        ]);
                    }
                }
            }
        }

        Session::flash('success', 'Successfully Updated the Application.');
        return redirect('/apps/edit/' . Encryption::encodeId($app_id));
    }

    /**
     * Update the specified Application for Desk-user.
     *
     * @param  int $id
     * @param AppsRequest $request
     * @return Response
     */
    public function updateDesk($id, AppsRequest $request) {
        $app_id = Encryption::decodeId($id);
        $user_type = CommonFunction::getUserType();
        $user_id = CommonFunction::getUserId();
        $lab_id = CommonFunction::getLabId();
        $send_to_bank = $request->get('$send_to_bank');
        $track_no = Apps::where('app_id', '=', $app_id)->pluck('tracking_number');
//        dd($request->all());

        if ($user_type == 6) { // Lab Incharge
            $jobList = AppsJob::where('app_id', $app_id)
                    ->join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                    ->where('ats.lab_id', $lab_id)
                    ->where('ats.lab_incharge_id', $user_id)
                    ->orderBy('ats.method_id','asc')
                    ->get();
            $tmpI = 0;
            foreach ($jobList as $tmpJob) {

//                dd($tmpJob);
                $tmpTsId = $tmpJob->ts_id;
                AppsTestSample::where('ts_id', $tmpTsId)->update([
                    'scientist_id' => $request->get('scientist_id')[$tmpI],
                    'remarks' => $request->get('remarks')[$tmpI]
                ]);


                $res = AppsResult::where('ts_id', '=', $tmpTsId)->first();

                if (count($res) <= 0) {
                    AppsResult::create([
                        'ts_id' => $tmpTsId,
                        'status' => 'Pending',
                        'remarks' => '',
                        'description' => '',
                        'updated_by' => ''
                    ]);
                }

                if ($request->get('method_test_status')[$tmpI] == "Recheck") {
                    AppsResult::where('ts_id', '=', $tmpTsId)
                            ->update([
                                'status' => $request->get('method_test_status')[$tmpI]
//                                'remarks' => '',
                                    //                              'description' => '',
                                    //                            'updated_by' => CommonFunction::getUserId()
                    ]);
                } else {
                    AppsResult::where('ts_id', $tmpTsId)->update([
                        'status' => $request->get('method_test_status')[$tmpI]
                    ]);
                }
                $tmpI++;
            }


            $jobList2 = AppsJob::where('app_id', $app_id)
                    ->join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                    ->where('apps_job.lab_id', $lab_id)
                    ->where('ats.lab_incharge_id', $user_id)
                    ->orderBy('apps_job.job_id')
                    ->groupBy('apps_job.job_id')
                    ->get();

            $tmpI = 0;
            foreach ($jobList2 as $tmpJob) {

                AppsJob::where('job_id', $tmpJob->job_id)->update([
//                    'remarks' => $request->get('comment')[$tmpI],
//                    'test_status' => $request->get('test_status_id')[$tmpI],
                    'test_sample_id' => $request->get('test_sample_id')[$tmpI]
                ]);

                $test_status = $request->get('test_status_id')[$tmpI];
                if (6 == $test_status) {
                    $tmpTsId = $tmpJob->ts_id;
                    AppsTestSample::where('job_id', $tmpJob->job_id)->where('lab_incharge_id', $user_id)->update([
                        'test_status' => 1
//                        'remarks' => $request->get('remarks')[$tmpI]
                    ]);
                }

                $tmpI++;
            }

            /*
             * Completed job start
             */
            $jobList4 = AppsJob::where('app_id', $app_id)
                    ->join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                    ->where('apps_job.lab_id', $lab_id)
//                    ->where('ats.test_status', '!=', 0)
                    ->groupBy('apps_job.job_id')
                    ->get();


            foreach ($jobList4 as $tmpJob4) {
                $job_id = $tmpJob4->job_id;
                $job_id_check = AppsTestSample::where('test_status', 0)->where('job_id', $job_id)->first();
                if (empty($job_id_check)) {
                    AppsJob::where('job_id', $job_id)->update(['test_status' => 6, 'test_end' => date('Y-m-d H:i:s')]);
                }
            }
            /* Completed job end */
        } elseif ($user_type == 8) { // Lab Director

            $checkSubmit = $request->get('submitFormLD');
            if ($checkSubmit == 'Received_in_lab') {
                ///Update the id of Lab-incharge in test_sample table and received/Released from lab for each job
                $jobList2 = AppsJob::join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->where('app_id', $app_id)
                        ->where('apps_job.lab_id', $lab_id)
                        ->whereIn('apps_job.test_status', array(4, 5, 6))
                        ->get();
                $tmpI = 0;
                $tmpI2 = 0;

//                dd($jobList2);
                $tmpJobId2 = 0;
                foreach ($jobList2 as $tmpJob) {
                    $tmpTsId = $tmpJob->ts_id;
                    AppsTestSample::where('ts_id', $tmpTsId)->update(['lab_incharge_id' => $request->get('lab_incharge')[$tmpI]]);
                    if ($tmpJobId2 != $tmpJob->job_id) {
                        $tmpJobId2 = $tmpJob->job_id;
                        AppsJob::where('job_id', $tmpJob->job_id)->update(['lab_reference_no' => $request->get('lab_reference_no')[$tmpI2], 'test_status' => 5, 'test_start' => date('Y-m-d H:i:s')]);
                        $tmpI2++;
                    }
                    $tmpI++;
                }

                Session::flash('success', 'Tracking No: ' . $track_no . ' Received in Lab Successfully and Lab-Incharge assigned');
            } elseif ($checkSubmit == 'Recheck_in_lab') {

                ///Update the id of Lab-incharge in apps-job table and received/Released from lab for each job
                $jobList2 = AppsJob::join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->where('app_id', $app_id)
                        ->where('apps_job.lab_id', $lab_id)
//                        ->whereIn('apps_job.test_status', array(4, 6))
                        ->get();

                $tmpI = 0;
                foreach ($jobList2 as $tmpJob) {
//                  $udata = ['lab_reference_no' => $request->get('lab_reference_no')[$tmpI], 'test_status' => 7]; // This will be if multiple job and multiple lab
                    $udata = ['lab_reference_no' => $request->get('lab_reference_no')[0], 'test_status' => 7];

                    AppsJob::where('job_id', $tmpJob->job_id)->update($udata); // Status 7 For Recheck in Lab

                    $tmpTsId = $tmpJob->ts_id;
                    AppsTestSample::where('ts_id', $tmpTsId)->update([
                        'lab_incharge_id' => $request->get('lab_incharge')[$tmpI],
                        'test_status' => 0
                    ]);
                    $tmpI++;
//                    AppsTestSample::where('job_id', $tmpJob->job_id)->update(['scientist_id' => 0]);
                }

                Session::flash('success', 'Tracking No: ' . $track_no . ' Send for Recheck in Lab Successfully.');
            } elseif ($checkSubmit == 'Released_from_lab') {
                $jobList2 = AppsJob::join('apps_test_sample as ats', 'apps_job.job_id', '=', 'ats.job_id')
                        ->where('app_id', $app_id)
                        ->where('apps_job.lab_id', $lab_id)
                        ->whereIn('apps_job.test_status', array(6, 7))
                        ->get();
                $tmpI = 0;

                foreach ($jobList2 as $tmpJob) {
                    AppsJob::where('job_id', $tmpJob->job_id)->update(['test_status' => 8]);  // 8 Means Verified from Lab Director
//                    AppsTestSample::where('job_id', $tmpJob->job_id)->update([
//                        'lab_incharge_id' => $request->get('lab_incharge_id')[$tmpI]
//                    ]);
                    $tmpI++;
                }

                Session::flash('success', 'Tracking No: ' . $track_no . ' Released from Lab Successfully.');
            }

            if ($send_to_bank == 'send_to_bank') {
                Session::flash('success', 'Tracking No: ' . $track_no . ' Send to Bank Successfully.');
            }
            return redirect('/apps');
        } else {


            if ($request->get('g_total_price') < $request->get('payable')) {
                Session::flash('error', 'First Time Payable amount should be less than or equal to Total Payable(' . $request->get('g_total_price') . ').');
                return redirect('/apps/edit/' . Encryption::encodeId($app_id));
            }

            if ($request->get('submitted') == 2) {
//                Apps::where('app_id', $app_id)->update([
//                    'app_status' => $request->get('submitted'),
//                    'updated_by' => CommonFunction::getUserId()
//                ]);

                Apps::where('app_id', $app_id)->update([
                    'app_status' => $request->get('submitted'),
                    //'company_id' => $request->get('company_id'),
                    'total_price' => $request->get('total_price2'),
                    'discount_price' => $request->get('discount_price'),
                    'total_vat' => $request->get('total_vat'),
                    'total_discount_price' => $request->get('total_discount_price'),
                    'payable' => $request->get('payable'),
                    'updated_by' => CommonFunction::getUserId()
                ]);


                Session::flash('success', $track_no . ' Application sent to lab successfully.');
                return redirect('apps/view/' . $id);
            }



            if ($request->get('submitted') == 6 || $request->get('submitted') == 5) {
                $applicantEmail = DB::table('members as mem')
                        ->leftJoin('apps_details as app', 'mem.member_id', '=', 'app.user_id')
                        ->where('app.app_id', $app_id)
                        ->lists('mem.member_email');
                if ($request->get('submitted') == 6) {
                    Session::flash('success', $track_no . ' Application status updated to Waiting for Sample and applicant notified already.');
                    $body_msg = "Your application (" . $track_no . ") has been accepted successfully and now waiting for sample submission.<br/>
                                        Please submit your samples as soon as possible to proceed further.<br/>";
                } elseif ($request->get('submitted') == 5) {
                    Session::flash('success', $track_no . ' Application status updated to Sent to Bank and applicant notified already.');
                    $body_msg = "Your application (" . $track_no . ") has been accepted successfully and now waiting for Bank Payment.<br/>
                                        Please submit your payment as soon as possible to proceed further.<br/>";
                }

                $data = array(
                    'header' => 'Your application update',
                    'param' => $body_msg
                );


                \Mail::send('users::message', $data, function ($message) use ($applicantEmail) {
                    $message->from('bcsir@gmail.com', 'BCSIR')
                            ->to($applicantEmail)
                            ->subject('Your application update');
                });
            }


            Apps::where('app_id', $app_id)->update([
                'app_status' => $request->get('submitted'),
                //'company_id' => $request->get('company_id'),
                'total_price' => $request->get('total_price2'),
                'discount_price' => $request->get('discount_price'),
                'total_vat' => $request->get('total_vat'),
                'total_discount_price' => $request->get('total_discount_price'),
                'payable' => $request->get('payable'),
                'updated_by' => CommonFunction::getUserId()
            ]);


            $job_ids = AppsJob::where('app_id', $app_id)->get(['job_id']);
            foreach ($job_ids as $row) {
                AppsTestSample::where('job_id', $row->job_id)->delete();
            }
            AppsJob::where('app_id', $app_id)->delete();


            if ($request->get('job_name')) {

                foreach ($request->get('job_name') as $key => $job) {

                    $jobInsert = AppsJob::create([

                                'app_id' => $app_id,
                                'job_name' => $job,
                                'tst_id' => $request->get('tst_id')[$key],
                                'lab_id' => $request->get('lab_id')[$key]
                    ]);

                    if (isset($request->get('method_id')[$key])) {

                        foreach ($request->get('method_id')[$key] as $k => $row) {

                            $lab_data = LabMethodMapping::where('lab_id', $request->get('lab_id')[$key])
                                    ->where('method_id', $row)
                                    ->first(['price', 'duration']);

                            AppsTestSample::create([
                                'method_id' => $row,
                                'job_id' => $jobInsert->id,
                                'lab_id' => $request->get('lab_id')[$key],
                                'price' => $lab_data->price,
                                'duration' => $lab_data->duration,
//                                'remarks' => $request->get('remarkss')[$key][$k] Need to check
                            ]);
                        }
                    }
                }
            }
        }
        Session::flash('success', 'Successfully Updated the Application (' . $track_no . ').');
        return redirect('/apps/edit/' . Encryption::encodeId($app_id));
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request) {
        $userId = CommonFunction::getUserId();
        $data = ['responseCode' => 0];
        if ($param == 'parameter') {
            $list = Parameter::where('tst_id', $request->get('id'))->orderBy('param_name')->lists('param_id', 'param_name');
            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'company') {
            $list = Company::where('company_id', $request->get('id'))->orderBy('company_name')->get([DB::raw('concat("House: ",company_house_no,", Flat: ",company_flat_no,", Street: ",company_street,", ",company_city,"-",company_zip) location'), DB::raw('concat(contact_person,", Phone: ",contact_phone,", Email: ",contact_email) contact')]);
            $data = ['responseCode' => 1, 'data' => $list[0]];
        } elseif ($param == 'method') {
            $list = Method::leftJoin('tbl_parameter as tp', 'tbl_method.param_id', '=', 'tp.param_id')
                    ->leftJoin('tbl_lab_method_mapping as mPrice', 'tbl_method.method_id', '=', 'mPrice.method_id')
                    ->whereIn('tbl_method.param_id', $request->get('id'))
                    ->orderBy('tp.param_name')
                    ->orderBy('method_name')
                    ->get([DB::raw('concat(method_name, " /", tp.param_name, " (Price: ", mPrice.price, " Excluding 15% vat)") method_name'), 'tbl_method.method_id']);

//            $responseData = '';
//            foreach($list as $row){
//                $responseData .= '<label class="col-md-12"><input type="checkbox" name="method_id[]" value="'. $row->method_id.'"> <b>'. $row->method_name . '</b> (' . $row->param_name.')</label>';
//            }

            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'lab') {
            $count = count(array_unique($request->get('id')));
            $ids = implode(',', array_unique($request->get('id')));
//            echo "SELECT tmm.lab_id, lab.lab_name, count(distinct tmm.method_id) cnt FROM `tbl_lab_method_mapping` tmm left join tbl_lab lab on tmm.lab_id =lab.lab_id WHERE tmm.`method_id` in ($ids) group by lab_id having cnt=$count order by lab_name";
            $list = DB::select(DB::raw("SELECT tmm.lab_id, lab.lab_name, count(distinct tmm.method_id) cnt FROM `tbl_lab_method_mapping` tmm left join tbl_lab lab on tmm.lab_id =lab.lab_id WHERE tmm.`method_id` in ($ids) group by lab_id having cnt=$count order by lab_name"));
            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'lab-price-dur') {
            $method_idss = $request->get('method');
            $method_id = implode(',',$method_idss);
            $lab_id = $request->get('lab_id');
            $list = DB::select(DB::raw("select `method_id`, `duration`, `price` from `tbl_lab_method_mapping`
where `lab_id` = $lab_id and `method_id` in ($method_id)
order by `method_id` in ($method_id)  desc"));

//            $list = LabMethodMapping::where('lab_id', $request->get('lab_id'))->whereIn('method_id', $request->get('method'))
//                    ->orderBy('method_id')
//                    ->get(['method_id', 'duration', 'price']);
            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'parameter-method') {
//            dd( $request->get('id'));
            $list = Parameter::where('tst_id', $request->get('id'))
                    ->leftJoin('tbl_method', 'tbl_parameter.param_id', '=', 'tbl_method.param_id')
                    ->get(['method_id', 'param_name', 'method_name']);
            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'publish-result') {
            $app_id = Encryption::decodeId($request->get('id'));

            Apps::where('app_id', $app_id)->update([
                'app_status' => 3,
                'updated_by' => $userId
            ]);


            $applicantEmail = DB::table('members as mem')
                    ->leftJoin('apps_details as app', 'mem.member_id', '=', 'app.user_id')
                    ->where('app.app_id', $app_id)
                    ->lists('mem.member_email');
            $body_msg = "Your Sample Testing has been completed and the testing result has been published.<br/>
                                        Please login using your credentials and get the result.<br/>";

            $data = array(
                'header' => 'Your application result published',
                'param' => $body_msg
            );


            \Mail::send('users::message', $data, function ($message) use ($applicantEmail) {
                $message->from('bcsir@gmail.com', 'BCSIR')
                        ->to($applicantEmail)
                        ->subject('Your application update');
            });

            \Session::put('success-msg', 'Result Published Successfully.');

            $data = ['responseCode' => 1];
        }
//        elseif ($param == 'received-lab') {
//            $app_id = Encryption::decodeId($request->get('id'));
//
//            $labId = CommonFunction::getLabId();
//            AppsJob::where('app_id', $app_id)
//                ->where('lab_id', $labId)
//                ->update([
//                'test_status' => 5,  //For Pending status = 5
//            ]);
//
//            Session::flash('success', 'Application Received Successfully.');
//            Apps::where('app_id', $app_id)->update([
//                'app_status' => 4,
//                'updated_by' => $userId
//
//            $labId = CommonFunction::getLabId();
//            AppsJob::where('app_id', $app_id)
//                ->where('lab_id', $labId)
//                ->update([
//                'test_status' => 5,  //For Pending status = 5
//            ]);
//
//            Session::flash('success', 'Application Received Successfully.');
////            Apps::where('app_id', $app_id)->update([
////                'app_status' => 4,
////                'updated_by' => $userId
////
////            ]);
//
//            $data = ['responseCode' => 1];
//        }



        return response()->json($data);
    }

//Bank payment controller functions are starting here. Prepared By Tonoy Bhattacharjee Date: 27-10-2015

    public function bankPayment() {
        return view('apps::bank_payment');
    }

    public function trackingNoSearch(Request $request) {
        $this->validate($request, ['tracking_no' => 'required']);
        $encrypted_tracking_no = Encryption::encodeId($request->get('tracking_no'));
        return redirect('apps/bank-payment-process/' . $encrypted_tracking_no);
    }

    public function bankPaymentProcess($tracking_no) {
        $tracking_no = Encryption::decodeId($tracking_no);
        $result = Apps::leftJoin('companies', 'companies.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_payment', 'apps_payment.app_id', '=', 'apps_details.app_id')
                ->leftJoin('members', 'members.member_id', '=', 'apps_payment.user_id')
                ->where('apps_details.tracking_number', "=", $tracking_no)
                ->select(
                        'apps_details.app_id', 'apps_details.total_price', 'discount_price', 'total_discount_price', 'apps_details.payable', 'apps_details.tracking_number', 'apps_details.total_vat', 'members.member_first_name', 'members.member_middle_name', 'apps_payment.created_at', 'apps_payment.due_payment', 'apps_payment.new_payment', 'companies.company_name', 'companies.company_house_no', 'companies.company_flat_no', 'companies.company_street', 'companies.company_city', 'companies.company_zip', 'companies.company_fax', 'companies.company_web', 'companies.contact_phone')
                ->orderBy('apps_payment.created_at', 'desc')
                ->get();

        if (empty($result[0])) {
            Session::flash('error', 'No data found!');
        }
        return view("apps::bank_payment", compact('result', 'tracking_no'));
    }

    public function moneySave(Request $request) {
        $app_id = $request->get('app_id');
        $new_payment = AppsPayment::where('app_id', $app_id)->count();
        $track_no = Apps::where('app_id', '=', $app_id)->pluck('tracking_number');

        if ($new_payment > 0) {
            $min = 1;
        } else {
            $min = $request->get('payable');
        }
//        echo $request->get('due_payment');
//        exit();
        $this->validate($request, ['new_payment' => 'required|numeric|max:' . $request->get('due_payment') . '|min:' . $min]);
        $encrypted_tracking_no = Encryption::encodeId($request->get('tracking_no'));
        $insert = AppsPayment::create([
                    'app_id' => $request->get('app_id'),
                    'due_payment' => $request->get('due_payment'),
                    'new_payment' => $request->get('new_payment'),
                    'user_id' => Session::get('member_id') //session user id
        ]);


        if ($new_payment == 0) {
            $currentAppStat = DB::table('apps_details')
                    ->where('app_id', $request->get('app_id'))
                    ->lists('app_status');



            //if first payment, updating app status to "waiting for sample"
            if ($currentAppStat[0] == 5) {
                $updateAppStatus = array(
                    'app_status' => 6
                );

                DB::table('apps_details')
                        ->where('app_id', $request->get('app_id'))
                        ->update($updateAppStatus);
                $applicantEmail = DB::table('members as mem')
                        ->leftJoin('apps_details as app', 'mem.member_id', '=', 'app.user_id')
                        ->where('app.app_id', $request->get('app_id'))
                        ->lists('mem.member_email');
                $body_msg = "Your payment has been accepted successfully and now waiting for sample (" . $track_no . ") submission.<br/>
                                        Please submit your samples as soon as possible to proceed further.<br/>";

                $data = array(
                    'header' => 'Your application (' . $track_no . ') payment information',
                    'param' => $body_msg
                );


                \Mail::send('users::message', $data, function ($message) use ($applicantEmail) {
                    $message->from('bcsir@gmail.com', 'BCSIR')
                            ->to($applicantEmail)
                            ->subject('Your application payment information');
                });
            }
        }

        Session::flash('success', 'Successfully Paid <code>' . $request->get('new_payment') . '</code>Taka Only.');
        return redirect('apps/bank-payment-process/' . $encrypted_tracking_no);
    }

    public function printview($id) {
        $app_id = Encryption::decodeId($id);

        $testMothods = Apps::getResultData($app_id);
        $app_data = Apps::generateReportByAPP($app_id);
        //dd($app_data['job_details']);
        $sampleLists[] = 'Select one';
        $process_status = array('' => 'Select one', 'Discard' => 'Discard', 'Verified' => 'Verified', 'Pending' => 'Pending');
/*        foreach ($testMothods as $key => $value) {
            $sampleLists[$value->method_id] = $value->param_name . '-> ' . $value->method_name;
        } */

        return view('apps::printview', compact('app_data','testMothods','process_status','app_id'));
    }

    public function printInvoiceView($id) {
//        dd($id);
        $app_id = Encryption::decodeId($id);

        $app_data = Apps::generateReportByAPP($app_id);


        return view('apps::printInvoice', compact('app_data'));
    }

    public function printApps($app_id) {
        $app_id = Encryption::decodeId($app_id);

        $app_data = Apps::getAppDetails($app_id);
        return view('apps::analysis-print-page', compact('app_data'));
    }

//Ending bank payment controller functions
    public function printInvoice($id) {
        $app_id = Encryption::decodeId($id);
        $app_data = Apps::generateInvoiceByAPP($app_id);
        $new_payment = AppsPayment::where('app_id', $app_id)->sum('new_payment');
        $app_data['total_given_amt'] = $new_payment;

//dd($app_data);
        return view('apps::printInvoiceView', compact('app_data'));
    }

    public function getJobListbyDate($year) {
        $dateArr = explode("-", $year);
        $dates = $dateArr[0] . "-" . $dateArr[1] . "-" . str_pad($dateArr[2], 2, '0', STR_PAD_LEFT);
        $ob = new Apps();
        $data = $ob->getJobListByDates($dates);
        return view('apps::job-list', compact('data'));
    }

    public function getInvoice($app_id) {
        $app_id = Encryption::decodeId($app_id);
        $app_data = Apps::generateInvoiceByAPP($app_id);

        $new_payment = AppsPayment::where('app_id', $app_id)->sum('new_payment');
        $app_data['total_given_amt'] = $new_payment;
        return view('apps::invoice-print-page', compact('app_data'));
    }

    public function getInvoiceDetails($id) {
        $app_id = Encryption::decodeId($id);
        $app_data = Apps::generateReportByAPP($app_id);
        return view('apps::invoice-details', compact('app_data'));
    }

    /*     * ************************End of Controller***************************** */
}
