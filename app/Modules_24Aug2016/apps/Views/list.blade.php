@extends('master-admin-default')

@section('title')
    <title>Test List</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Applications
            <!--<small>it all starts here</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Application</a></li>
            <li class="active">list</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        <!-- Default box -->
        <div class="box">
                <?php if(Session::get('member_type') ==5){ ?>  
            <div class="box-header">              
                <h3 class="box-title">
                    <a href="{{ url('/apps/create') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> New Application', array('type' => 'button', 'class' => 'btn btn-primary')) !!}
                    </a>
                </h3>
            </div><!-- /.box-header -->
                <?php } ?>         
            <div class="box-body">
                <table id="test_list" class="table table-striped table-bordered dt-responsive nowrap">
                       <thead>
                            <tr>
                                <th>Application Id</th>
                                <th>Company Name</th>
                                <th>Description</th>
                            <?php if (in_array(Session::get('member_type'), array(6,7,8))) {//Scientist , Lab Incharge, Director ?>
                                <th>Status (Pending/Verified/Recheck)</th>
                            <?php }elseif (Session::get('member_type') == 3) { ?>
                                <th>Status (Pending / Completed Job)</th>
                                <?php }else{ ?>
                                <th>Status</th>
                            <?php } ?>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    <tbody>
                    @foreach($getList as $row)
                        <?php
                        if($row->tracking_number == '') continue;
                        ?>
                        <tr>
                            <td>{!! $row->tracking_number !!}</td>
                            <td>{!! $row->company_name !!}</td>
                            <td>{!! $row->job_names !!}</td>
                            
                            <?php if (Session::get('member_type') == 7 || Session::get('member_type') == 6  || Session::get('member_type') == 8) { //Scientist and lab incharge, Director ?>
                            <td>
                                <span style="color: #fff; font-weight: bold;" class="bg-olive-active label btn-sm">
                            <?php

                            $numberOfTask = $status[$row->app_id]->Pending + $status[$row->app_id]->Verified + $status[$row->app_id]->Discard + $status[$row->app_id]->Recheck ;
                                if(Session::get('member_type') == 8 && !($lab_incharge_status >0))
                                {
                                               echo "No Lab Incharge assigned";
                                }
                                elseif(Session::get('member_type') == 8 && $lab_incharge_status >0)
                                {
                                    if($numberOfTask == 0)
                                        echo "Pending in Lab incharge desk";

                                }

                                if(!($numberOfTask >0) && Session::get('member_type') == 6)
                                {
                                               echo "No Scientist assigned"; 
                                }

                               if (Session::get('member_type') == 6 && 0) {//Lab Incharge
                                    if (!empty($scientist_status[$row->app_id]->scientist_id) && $scientist_status[$row->app_id]->scientist_id > 0) {
                                        ?>

                                        <span style="color: #fff; font-weight: bold;" class="bg-aqua-active label btn-sm">
                                            Scientist Assigned
                                        </span>
                                        <?php
                                    }

                              }


                                if($numberOfTask > 0 && in_array(Session::get('member_type'), array(6,7,8)))
                                {
                            ?>
                                    <?php echo $status[$row->app_id]->Pending; ?> /
                                    <?php echo $status[$row->app_id]->Verified; ?> /
                                    <!--<?php echo $status[$row->app_id]->Discard; ?> / -->
                                    <?php echo $status[$row->app_id]->Recheck; ?>                                    

                            <?php    }?>
                                </span>

                                <?php
                                  if($row->test_status == 7){ ?>
                                     <br/>
                                        <br/>

                                       <span class="label bg-blue-gradient">
                                            Recheck (Lab-Director)
                                        </span>
                               <?php } ?>

                            </td>
                         <?php } else if (Session::get('member_type') == 3) { ?>
                                <td>   
                                    <span style="background-color:{{ $row->status_color  }}" class="label">
                                        {!! $row->status_name !!}
                                    </span> &nbsp; &nbsp;
                                    <?php if($row->app_status == 2) {?> <br/>
                                       <span style="color: #fff; font-weight: bold;margin-right: 5px;" class="bg-aqua-active label">
                                                 {!! $pstatus[$row->app_id]->Pending !!} 
                                           </span>
                                          <span style="color: #fff; font-weight: bold;" class="bg-olive-active label">
                                        {!! $pstatus[$row->app_id]->Completed !!} 
                                      </span>
                                    <?php } ?>
                                </td>
                          <?php } else { ?>
                            <td>
                                <span style="background-color:{{ $row->status_color  }}" class="label">
                                    {!! $row->status_name !!}
                                </span>
                                </td>
                                <?php } ?>



                            <td>{!! date('d M Y',strtotime($row->created_at))  !!}</td>
                            <td>
                                <?php $user_type = CommonFunction::getUserType(); ?>

                                <!-- for Applicant user or Duty Officer*/-->
                                @if( $user_type == 5 || $user_type == 3)

                                    {!! link_to('apps/edit/'. Encryption::encodeId($row->app_id),'Open',['class' => 'btn btn-info btn-sm']) !!}

                                @elseif( $user_type == 1 || $user_type == 2)

                                    {!! link_to('apps/view/'. Encryption::encodeId($row->app_id),'Open',['class' => 'btn btn-info btn-sm']) !!}

                                <?php //for Scientist Result ?>
                                @elseif( $user_type == 7)

                                    {!! link_to('apps/show/'. Encryption::encodeId($row->app_id),'Open',['class' => 'btn btn-primary btn-sm']) !!}

                                @elseif( $user_type == 4)

                                    {!! link_to('apps/printview/'. Encryption::encodeId($row->app_id),'Print',['class' => 'btn btn-info btn-sm']) !!}
                                @elseif( $user_type == 6 || $user_type == 8)

                                    {!! link_to('apps/edit/'. Encryption::encodeId($row->app_id),'Open',['class' => 'btn btn-primary btn-sm']) !!}

                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.content -->

@endsection

@section('footer-script')
    <script>

        $(function () {
            $('#test_list').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true,
                "iDisplayLength": 100
            });
        });

    </script>
@endsection
