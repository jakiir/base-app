@extends('print-layout')

@section('title')
    <title>Invoice Report</title>
@endsection

@section('content')
    <section class="invoice">
         <style>
         @media print{
            td{border: none !important;}
             }
            .footer {
                   	width:100%;
                   	height: 0px;
                   	position:absolute;
                   	bottom:0;
                   	left:0;
                   }
          </style>
        <div class="col-xs-12 text-center">
            <div class="col-xs-4">
        </div>
       <div class="col-xs-4">
            <p class="lead analytics_heading">{!! Html::image("back-end/dist/img/logo.jpg", "Logo",['width'=>85]) !!}</p>
       </div>
        <div class="col-xs-4 text-right" style="text-align: right;font-weight: normal;font-family: NikoshBAN !important;">
                   জীবনের জন্য বিজ্ঞান
               </div>

        </div><!-- /.col -->
        <div class="col-xs-12 text-center">
                          <span class="tp page-header print_heading" style="border:none;">
                      BANGLADESH COUNCIL OF SCIENTIFIC AND INDUSTRIAL
                      RESEARCH (BCSIR) <br/>
                  </span>
                  <span class="tpp print_heading">
                      বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )<br/>
                  </span>
        </div>

        <div class="col-xs-12 text-center">
        <br/>
            <p class="lead analytics_heading" style="text-decoration: underline;"><b>INVOICE REPORT</b></p>
          </div><!-- /.col -->
          
        <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
                <div>
                   <table class="table table-condensed table_report">
                          <tr>
                              <th style="width:25%">Ref. No of the Analytical Cell:</th>
                              <td>{{ $app_data['app_header']->tracking_number }}</td>
                          </tr>
                          <tr>
                              <th>Application Date:</th>
                              <td>{{ date('d/m/Y',strtotime($app_data['app_header']->created_at)) }}</td>
                          </tr>
                          <tr>
                              <th>Sample Description:</th>
                              <td>{{ $app_data['job_details'][0]->job_name  }}</td>
                          </tr>
                          <tr>
                              <th>Duration: </th>
                              <td>{{$app_data['max_duration']}} Day(s) (It will be counted after sample submission)</td>
                          </tr>
                          <tr>
							<th>Client's Details: </th>
							<td>{!! ucwords($app_data['company']->submitted_by).'<br/> <b>'.   ucwords($app_data['company']->company_name).'</b><br/> '.
								' House#'.$app_data['company']->company_house_no.$app_data['company']->company_flat_no.', '.  $app_data['company']->company_street.
								', '.$app_data['company']->company_city.'-'.$app_data['company']->company_zip.'<br/>Phone#'.$app_data['company']->contact_phone !!}</td>
						</tr>
                      </table>
                </div>
            </div><!-- /.col -->
        </div>
    <!-- new table -->
    <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">

        <table class="table table-bordered table-condensed table_report">
               <thead class="text-center">
                   <tr>
                       <th>Job Name</th>
                       <th>Parameter</th>
                       <th>Method</th>
                       <th>price</th>
                   </tr>
               </thead>
               <tbody>
               <?php
                   $count_jobs = count($app_data['job_details']);
               ?>
                   @foreach($app_data['job_details'] as $value)
                   <tr>
                       <td>{{ $value->job_name}}</td>
                       <td>
                       @foreach($app_data['jobparametter'][$value->job_id] as $value2)
                        <table class="table_report">
                         <tr>
                               <td> {!! $value2->param_name !!}</td>
                          </tr>
                         </table>
                       @endforeach
                       </td>
                       <td>
                        @foreach($app_data['jobparametter'][$value->job_id] as $value2)
                        <table class="table_report">
                         <tr>
                               <td> {!! $value2->method_name !!}</td>
                          </tr>
                         </table>
                       @endforeach
                       </td>
                       <td>
                       @foreach($app_data['jobparametter'][$value->job_id] as $value2)
                      <table class="table_report">
                           <tr>
                              <td>{{ $value2->price}}</td>
                           </tr>
                       </table>
                       @endforeach
                       </td>
               </tr>
                   @endforeach
               </tbody>
               </table>
         </div><!-- /.col -->
        <div class="col-xs-1"></div>

    </div>
    <!-- new table end -->

    <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">
            <table class="table table-bordered table-condensed table_report">
                    <tbody>
                    <tr>
                        <td>Total Price: {{ @$app_data['app_header']->total_price }} BDT</td>
                        <td>Total Vat (15%) : {{ @$app_data['app_header']->total_vat }} BDT</td>
                    </tr>

                <?php if ($app_data['app_header']->discount_price > 0) {
                            ?>
                    <tr>
                        <td>Total Price After Discount: {{ @$app_data['app_header']->total_discount_price}} BDT</td>
                        <td>Percentage of Discount: {{ @$app_data['app_header']->discount_price }} %</td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td>Total Payable: {!! $app_data['app_header']->total_discount_price
                            + $app_data['app_header']->total_vat !!} BDT <br /></td>
                        <td>First Time Minimum Payable: {{ @$app_data['app_header']->payable }} BDT</td>
                    </tr>

                    <tr>
                        <td>Total Amount Paid: {{ $app_data['total_given_amt'] }} BDT <br /></td>
                       <td>Total Due : <?php $total = ($app_data['app_header']->total_discount_price
                            + $app_data['app_header']->total_vat) - $app_data['total_given_amt']; echo $total; ?> BDT <br /></td>
                    </tr>

                </tbody>
            </table>
        </div><!-- /.col -->
        <div class="col-xs-1"></div>

    </div><!-- /.row -->

        <div class="row invoice-info ">
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
            <div class="col-sm-10">
                <div style="border-bottom:1px solid black;">&nbsp;</div>
                    <div class="text-center" style="font-size: 12px;"><strong>Analytical Service Cell (ASC)</strong></div>
                             <div class="text-center" style="font-size: 12px;">
                                 <small>
                                     Dr. Qudrat-I-Khuda Road, Dhanmondi, Dhaka-1205,Bangladesh<br/>
                                     Telephone:9671108,Fax: 880-02-9671108 E-mail:asc@bcsir.gov.bd
                                 </small>
                             </div>
                     </div>
            </div><!-- /.col -->
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div><br />
        <div class="row invoice-info">
            <div class="col-sm-2  invoice-col"></div><!-- /.col -->
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div>


    </section>
@endsection

