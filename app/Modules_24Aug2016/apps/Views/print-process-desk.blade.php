@extends('print-layout')
@section('title')
<title>Application Result</title>
@endsection
@section('content')

@foreach($app_data['app_header'] as $key=>$job)
<section class="invoice" style="page-break-after: always;">
    <div class="col-xs-12 text-center">
        <div class="col-xs-3">
                @if($testMothods[0]->iso_logo == 1)
                    {!! Html::image("iso-logo/1459590232_logo_NABL.jpg", "iso_logo",['width'=>55,'class'=>'pull-left']) !!}
                @endif
        </div><!-- /.col -->
        <div class="col-xs-5">
            <p class="lead analytics_heading">{!! Html::image("back-end/dist/img/logo.jpg", "Logo",['width'=>85]) !!}</p>
        </div>
        <div class="col-xs-4 text-right" style="text-align: right;font-weight: normal;font-family: NikoshBAN !important;">
            জীবনের জন্য বিজ্ঞান
        </div>
    </div><!-- /.col -->
    <style>
    @media print{
    .footer{page-break-after: always;}
    }
    table{ font-size: 12px !important; }
          .tp{ font-size: 13px !important;font-family: "Times New Roman", Times, serif  !important; }
          .body {
             padding:10px;
             padding-bottom:60px;   /* Height of the footer */
          }
          table td{
          padding: 0px !important;
          }
          td.tr{
          padding: 0px;
          }
          .footer {
          	width:100%;
          	height: 0;
          	position:absolute;
          	bottom:0;
          	left:0;
          }
         .tpp{font-size:20px !important;font-family: "NikoshBAN"  !important;font-weight: normal; }
         .ts{font-size: 12px !important;font-family: "Times New Roman", Times, serif  !important; }
         .borders td, .ps p{ border: none !important; padding: 3px !important;font-size: 13px !important; font-family: "Times New Roman", Times, serif  !important;}
     </style>

    <div class="col-xs-12 text-center" style="border-bottom: 1px solid #eee;margin-bottom: 10px;">

    @if($app_data['job_details'][$job->scientist_id][0]->lab_id == 8)

        <span class="tp page-header print_heading" style="border:none;font-size: 16px !important;">
            Institute of National Analytical Research and Service (INARS)<br/>
        </span>
                <span class="tpp print_heading">
            ইনস্টিটিউট অব ন্যাশনাল এনালাইটিক্যাল রিসার্চ এন্ড সার্ভিস <br/>
        </span>
     @endif


                <span class="tp page-header print_heading" style="border:none;">
            BANGLADESH COUNCIL OF SCIENTIFIC AND INDUSTRIAL
            RESEARCH (BCSIR) <br/>
        </span>
        <span class="tpp print_heading">
            বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )<br/>
        </span>
    </div>

    <div class="col-xs-12">
        <p class="lead analytics_heading ts">Laboratories/Institute/Center: {{ $app_data['job_details'][$job->scientist_id][0]->lab_name }}</p>
    </div><!-- /.col -->
<div class="body">
    <div class="col-xs-12 text-center">
        <p class="lead analytics_heading"><b style="text-decoration: underline;">ANALYSIS REPORT</b></p>
    </div>
    <div class="row">
        <div class="col-xs-12">
               <table class="table borders">
                    <tr>
                        <td>ASC Ref No </td>
                        <td width="400">: {{ $job->tracking_number }}</td>
                        <td width="244">Unit (Lab/Inst.) Ref No</td>
                        <td width="273">: {{ $job->lab_reference_no }}</td>
                    </tr>

                    <tr>
                        <td>Lab/Sample ID</td>
                        <td>: {{ $job->test_sample_id }}</td>
                        <td>Number of Sample </td>
                        <td>: {{ count($app_data['job_details']) }}</td>
                    </tr>
                    <tr>
                        <td width='157'>Sample Description</td>
                        <td>: {{ $job->job_name }}</td>
                        <td>Application Date</td>
                        <td>:  {{ date('d/m/Y',strtotime($job->application_date)) }}</td>
                    </tr>

                    <tr>
                        <td rowspan="2">Client's Details</td>
                        <td rowspan="2">:  {!! ucwords($job->submitted_by).'<br/> <b> &nbsp;&nbsp;'.   ucwords($job->company_name).'</b><br/> &nbsp;'.
                            ' Hourse#'.$job->company_house_no.$job->company_flat_no.', '.$job->company_street.'<br/>&nbsp;&nbsp;'.$job->company_city.'-'.$job->company_zip.', Phone#'.$job->contact_phone !!}
                        </td>
                         <?php if($job->lab_incharge_id > 0)  { ?>
                          @if($job->rcreate != '')
                        <td> Test Commencement date </td>
                         <td>: {{ date('d/m/Y',strtotime($job->rcreate)) }}</td>
                          @endif
                         <?php } ?>
                    </tr>
                    <tr>
                      <?php  if($job->test_status == 8 )  {?>
                          @if($job->rstatus == 'Verified')
                              <td> Test Completion date  </td>
                              <td> :  {{ date('d/m/Y',strtotime($job->rupdate)) }}</td>
                          @endif
                      <?php } ?>
                      </tr>
                </table>
        </div><!-- /.col -->
    </div>

<div class="row">
    <div class="col-xs-12 form-group">

    {!! Form::hidden('app_id',Encryption::encodeId($app_id)) !!}
    <label>Result:</label>
    {!! $job->description !!}
    </div>
    <div class="col-xs-12 form-group">
        <label>Comments:</label>
        {!! $job->remarks_li !!}
    </div>
</div>
</div>
       <div class="row footer" style="position:absolute;bottom: 0 !important;">
               <div class="col-xs-12 invoice-info">
                 <div class="col-sm-4 invoice-col ts">
                     <p class="text-center">-------------------------------</p>
                     <p class="text-center">Analyst</p>
                 </div><!-- /.col -->
                 <div class="col-sm-4 invoice-col ts">
                     <p class="text-center">-------------------------------</p>
                     <p class="text-center">Supervisor</p>
                 </div><!-- /.col -->
                 <div class="col-sm-4 invoice-col ts">
                     <p class="text-center">--------------------------------</p>
                     <p class="text-center"> Director/Officer In-Charge</p>
                 </div><!-- /.col -->
               </div><!-- /.row -->
        <div class="col-xs-12">
            <p class="ps" style="font-size: 12px;"><strong>Note:</strong><br/>
            <ul>
                <li style="list-style-type: lower-alpha;font-size: 12px;font-family:"Times New Roman", Times, serif  !important;">The results reported here is based only on the supplied sample’s in this laboratory</li>
                <li style="list-style-type: lower-alpha;font-size: 12px;font-family: "Times New Roman", Times, serif  !important;">Any complain about test report will not be acceptable after one month from the date of issuing of the said report.</li>
                <li style="list-style-type: lower-alpha;font-size: 12px;font-family: "Times New Roman", Times, serif  !important;">This report/result shall not be reproduced/published without prior approval of the authority.</li>
            </ul>
            </p>
        </div>
        <div class="col-xs-12">
            <p class="ts" style="border-bottom:1px solid black;font-size: 12px;">*The results relate only to the items tested</p>
            <div class="text-center" style="font-size: 12px;"><strong>Analytical Service Cell (ASC)</strong></div>
                <div class="text-center" style="font-size: 12px;">
                    <small>
                        Dr. Qudrat-I-Khuda Road, Dhanmondi, Dhaka-1205,Bangladesh<br/>
                        Telephone:9671108,Fax: 880-02-9671108 E-mail:asc@bcsir.gov.bd
                    </small>
                </div>
        </div>
        </div><!-- /.col -->
</section>
<?php // break;?>
@endforeach


@endsection

