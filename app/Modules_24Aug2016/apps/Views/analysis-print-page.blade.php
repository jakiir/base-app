@extends('print-layout')

@section('header-script')
@section('title')
<title>Print Sample Tag</title>
@endsection
<style>
    @page
    {
        size: auto;   /* auto is the current printer page size */
        margin: 6mm;  /* this affects the margin in the printer settings */
    }

</style>
@endsection

@section('content')
<section class="invoice">
@foreach($app_data as $sample)
    <div class="row">
        <div class="col-xs-3"></div>        
        <div class="col-xs-6"  style="border:1px solid black;">
            <strong>Tracking Number:</strong> {{$sample->tracking_number}}
           <br /><strong>Lab Name:</strong> {{$sample->lab_name}}
            <br /><strong>Sample Name:</strong> {{$sample->tst_name}}
          <!--<br /> <strong>Test Parameter:</strong>
           <br /> <strong>Test Name:</strong>-->
            </div><!-- /.col -->            
            <div class="col-xs-3"></div>
    </div> <br /><br /> <br /> 
@endforeach
       
</section>
@endsection

