@extends('print-layout')

@section('header-script')
@section('title')
<title>Test Report</title>
@endsection
<style>
    @page
    {
        size: auto;   /* auto is the current printer page size */
        margin: 6mm;  /* this affects the margin in the printer settings */
    }

</style>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<!-- Main content -->
<?php
$result_counter = 1;
$t_result = count($app_data['app_header']);
?>
@foreach(@$app_data['app_header'] as $key=>$job)
<section class="invoice" style="page-break-after:always;">
    <div class="col-xs-12 text-center">
            <div class="col-xs-3">
            @if($app_data['job_details'][$job->job_id][0]->lab_id == 8)
            <b>ISO/IEC 17025:2005 Certified</b><br/>
            {!! Html::image("back-end/dist/img/logo_NABL.jpg", "Logo",['width'=>60]) !!}
            <br/><b>Certificate No: T-1676</b>
            @endif
        </div><!-- /.col -->
        <div class="col-xs-5">
            <p class="lead analytics_heading">{!! Html::image("back-end/dist/img/logo.jpg", "Logo",['width'=>80]) !!}</p>
        </div>
        <div class="col-xs-2">
            <!--{!! link_to('apps/get-invoice/'.Request::segment(3),'Print',['class' => 'btn btn-primary btn-sm pull-right', 'target'=>'_blank']) !!}-->
        </div> 
        <div class="col-xs-2 text-right" style="text-align: right;font-weight: normal;font-family: NikoshBAN !important;">
            জীবনের জন্য বিজ্ঞান
        </div> 
    </div><!-- /.col -->
     <style>
        .footer {
               	width:100%;
               	height:30px;
               	position:absolute;
               	bottom:0;
               	left:0;
               }
         .tp{ font-size: 13px !important;font-family: "Times New Roman", Times, serif  !important; } 
         .tpp{font-size:20px !important;font-family: "NikoshBAN"  !important;font-weight: normal; } 
         .ts{font-size: 12px !important;font-family: "Times New Roman", Times, serif  !important; } 
         .borders td, .ps p{ border: none !important; padding: 3px !important;font-size: 13px !important; font-family: "Times New Roman", Times, serif  !important;}
     </style>
    <div class="col-xs-12 text-center" style="border-bottom: 1px solid #333;margin-bottom: 10px;">
        @if($app_data['job_details'][$job->job_id][0]->lab_id == 8)
        <span class="tp page-header print_heading" style="border:none;font-size: 16px !important;">
            Institute of National Analytical Research and Service (INARS)<br/>
        </span>
                <span class="tpp print_heading">
            ইনস্টিটিউট অব ন্যাশনাল এনালাইটিক্যাল রিসার্চ এন্ড সার্ভিস <br/>
        </span>
        @endif
                <span class="tp page-header print_heading" style="border:none;">
            BANGLADESH COUNCIL OF SCIENTIFIC AND INDUSTRIAL
            RESEARCH (BCSIR) <br/>
        </span>
        <span class="tpp print_heading">
            বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )<br/><br/>
        </span>

    </div>

    <div class="row">
        <div class="col-xs-1"></div>
            <div class="col-xs-11">
        <p class="lead analytics_heading ts  text-left">Laboratories/Institute/Center: {{ $app_data['job_details'][$job->job_id][0]->lab_name }}</p>
    </div><!-- /.col -->

    <div class="col-xs-12 text-center">
        <p class="lead analytics_heading tp"><b style="text-decoration: underline;">ANALYSIS REPORT</b></p>
    </div><!-- /.col -->
        <div class="col-xs-10 col-xs-offset-1">
            <div> 
			
                <table class="table borders">
                    <tr>
                        <td>ASC Ref No </td>
                        <td width="500">: {{ $job->tracking_number }}</td>
                        <td width="183">Unit (Lab/Inst.) Ref No</td>
                        <td width="261">: {{ $job->lab_reference_no }}</td>
                    </tr>
                    <tr>
                        <td>Lab/Sample ID</td>
                        <td>: {{ $job->test_sample_id }}</td>
                        <td>Number of Sample </td>
                        <td>: {{ count($app_data['job_details']) }}</td>
                    </tr>
                    <tr>
                        <td width='130'>Sample Description</td>
                        <td>: {{ $job->job_name }}</td>
                        <td>Date of Received</td>
                        <td>:  {{ date('d/m/Y',strtotime($job->updated_at)) }}</td>
                       
                    </tr>
                    <tr>
                        <td rowspan="2">Client's Details</td>
                        <td rowspan="2">:  {!! ucwords($job->submitted_by).'<br/> <b>'.   ucwords($job->company_name).'</b><br/> '.
                            ' Hourse#'.$job->company_house_no.$job->company_flat_no.', '.  $job->company_street.
                            $job->company_city.'-'.$job->company_zip !!}
                        </td>
                         <?php if($job->lab_incharge_id > 0)  { ?>
                        <td> Test Commencement date </td>
                         <td>: {{ date('d/m/Y',strtotime($job->test_start)) }} </td>
                         <?php } ?>
                    </tr>
                    <tr>
                      <?php  if($job->test_status == 8 )  {?>
                      <td> Test Completion date  </td>
                      <td> : {{ date('d/m/Y',strtotime($job->test_end)) }} </td>
                           <?php } ?>
                      </tr>
                </table>
            </div>
        </div><!-- /.col -->
    </div>

    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <p class="ps">Test Result :</p>
			<style>.table_report,.ts{ padding:0px !important;font-size:7px !important;}</style>

            <table class="table-bordered table_report">
                <thead class="text-center">
                    <tr>
                        <th width='45' class="text-center">Sl. No.</th>
                        <th width='160' class="text-center">Test Parameters</th>
                        <th width='80' class="text-center">Unit</th>
                        <th width='100' class="text-center">Method / Equipment</th>
                        <th width='70' class="text-center">Test Value</th>
                        <th width='60' class="text-center">Reference Value</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                     $sld = 1;
                    $sl = 0;
                    ?>
                    @foreach($app_data['job_details'][$job->job_id] as $sample_details)
                    <tr>
                        <td class="text-center">{{ $sld }}</td>
                        <td class="">{{ @$sample_details->param_name }}</td>
                        <td class="text-center">{{ @$sample_details->unit }}</td>
                        <td class="">{{ @$sample_details->method_name }}</td>
                        <td class=""><?php
                            if ($sample_details->status == "Verified")
                            echo $sample_details->description;
                            ?>
                        </td>
                        <td class="text-center">{{ @$sample_details->limit }}</td>
                    </tr>
                    <?php $sl++;  $sld++;  ?>
                    @endforeach
                </tbody>
            </table>
            <p class="ps"><strong>Comments: </strong><?php echo $job->remarks; ?></p><br/><br/>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 ts">
            <table width="100%">
                <tr>
                    <td width="30%">
                        <p class="text-center">-------------------------------</p>
                        <p class="text-center tp">Analyst</p>
                    </td>
                    <td width="30%">
                        <p class="text-center">------------------------------</p>
                        <p class="text-center tp">Supervisor</p>
                    </td>
                    <td width="40%">
                        <p class="text-center">-----------------------------------</p>
                        <p class="text-center tp">Director/In Charge Officer</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="col-xs-10 col-xs-offset-1">
            <table width="100%">
                <tr>
                    <th class="tp"><small>Note:</small></th>
                </tr>
                <tr>
                    <td class="tp"><small>
                            <ul>
                                <li style="list-style-type: lower-alpha;">The results reported here is based only on the supplied sample’s in this laboratory</li>
                                <li style="list-style-type: lower-alpha;">Any complain about test report will not be acceptable after one month from the date of issuing of the said report.</li>
                                <li style="list-style-type: lower-alpha;">This report/result shall not be reproduced/published without prior approval of the authority.</li>
                            </ul>
                        </small>
                    </td>
                </tr>
            </table>
        </div><!-- /.col -->
    </div>

    <div class="row invoice-info">
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        <div class="col-sm-10">
            <div class="ts" style="border-bottom:1px solid black;font-size: 12px;">*The results relate only to the items tested</div>
            <div>
                  <div class="text-center ts"><strong>Analytical Service Cell (ASC)</strong></div>
                <div class="text-center ts">
                    <small>
                       Dr. Qudrat-I-Khuda Road, Dhanmondi, Dhaka-1205,Bangladesh<br/>
                        Telephone:9671108,Fax: 880-02-9671108 E-mail:asc@bcsir.gov.bd
                    </small>
                </div>
            </div>
        </div><!-- /.col -->
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
    </div><br />
    <div class="row invoice-info">
        <div class="col-sm-2  invoice-col"></div><!-- /.col -->
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
    </div>


</section>
<?php
$result_counter++;
if ($result_counter <= $t_result)
    echo '<div class="printPageBreak"></div>';
?>
@endforeach


@endsection

