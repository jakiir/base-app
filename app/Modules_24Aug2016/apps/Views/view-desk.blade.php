@extends('master-admin-default')

@section('title')
<title>Application View</title>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box-solid">
        <div class="box-header with-border bg-teal-active">
            <h3 class="box-title"><strong><i class="fa fa-life-saver"></i> Application Edit (Tracking Number: <?php echo $data['tracking_number']; ?>)</strong></h3>
        </div>
        <ol class="breadcrumb">
            <h4><strong>বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )</strong></h4>
        </ol>
    </div>
</section>
<!-- Main content -->
<section class="content">

{!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
{!! Session::has('success-msg') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success-msg") .'</div>' : '' !!}
{!! Session::has('error') ? '<div class="alert alert-warning alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}

    {!! Form::open(['url' => '/apps/update-desk/'.$id, 'method'=>'patch', 'class' => 'form apps_from', 'id'=>'app_entry_form','role' => 'form']) !!}


    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="col-md-12">
                <a href="{{url('apps/printApps/'.Request::segment(3))}}" target="_blank">
                    {!! Form::button('<i class="fa fa-print"></i> Print Sample Token', array('type' => 'button', 'value'=> 'print', 'class' => 'btn btn-default no_hide pull-right')) !!}
                </a>
                <?php if (!in_array($data['app_status'], array(0, 1))) { ?>

                    {!! link_to('apps/print-invoice/'. Encryption::encodeId($data->app_id), 'Invoice / Money Receipt ',
                    ['class' => 'btn btn-info no_hide btn-mid pull-right']) !!}
                <?php } ?>

                <?php
                $user_type = \App\Libraries\CommonFunction::getUserType();
                ?>
                <?php if (in_array($user_type, array(3, 6, 8, 10)) || $data['app_status'] == 3) { ?>
                    {!! link_to('apps/printview/'. Encryption::encodeId($data->app_id),'Result',
                    ['class' => 'btn btn-primary btn-mid pull-right']) !!}
                    <!--                        {!! link_to('apps/get-invoice-details/'. Encryption::encodeId($data->app_id),'Result',
                                            ['class' => 'btn btn-primary btn-mid pull-right']) !!}-->

                <?php } ?>



            </div>

            <div class="form-group col-md-6 {{$errors->has('company_id') ? 'has-error' : ''}}">
                {!! Form::label('company_id','Application for:',['class'=>'control-label']) !!}
                {!! Form::hidden('company_id', $data["company_id"]) !!}
                <strong>{!! $data["company_name"] !!}</strong>
                {!! $errors->first('company_id','<span class="help-block">:message</span>') !!}
            </div>

            <div class="col-md-12 ">
                <?php
                $user_type = CommonFunction::getUserType();
                ?>
                @if( $app_status_check == 0 && $user_type == 3 && $data["app_status"] == 2)
                {!! Form::button('<i class="fa "></i> Publish Result', array('type' => 'button', 'class' => 'changeStatus btn btn-primary pull-right no_hide')) !!}

                @endif
            </div>

            <div class="col-md-2"></div>
            <hr/>

            <div class="col-md-12 company_details">

                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                <p class="text-muted location">{!! $data->location !!}</p>

                <strong><i class="fa fa-file-text-o margin-r-5"></i>Contact</strong>
                <p class="contact">{!! $data->contact !!}</p>
            </div>
        </div><!-- /.box-body -->
    </div>
    <div class="clearfix clear box box-danger ">
        <div class="box-header">
            <h4 class="pull-left">Test Section</h4>
            {{--{!! Form::button('<i class="fa fa-plus"></i> Add', array('type' => 'button', 'class' => 'btn btn-primary pull-right new_section')) !!}--}}
        </div>

        <div class="box-body">
            <?php
            $i = 0;

            $loopNum = 0;
            ?>
            @foreach($dataMaster as $dataM)
            <?php // @if((Session::get('member_type') == 6 OR Session::get('member_type') ==  7 OR Session::get('member_type') ==  8) AND $dataM->lab_ids2 == Session::get('lab_id')) ?>
            <?php
            $loopNum++;
            ?>
            <div class="clearfix">
                <div class="add_section_{!! $i !!}">
                    <?php echo $i > 0 ? '<div class="col-md-12 span_border">' . '' : ''; ?>
                    <div class="form-group clearfix col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                        {!! Form::label('job_name','Sample Description',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-5">
                            {!! Form::text('job_name['.$i.']',$dataM['job_name'], ['class' => 'required form-control','required']) !!}
                            {!! Form::hidden('job_id['.$i.']',$dataM['job_id'], ['class' => 'form-control']) !!}
                            {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="col-sm-5"></div>
                    </div>

                    <div class="form-group clearfix col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                        {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-5">
                            {!! Form::select('tst_id['.$i.']', $testSample, $dataM['tst_id'], ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                            {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-sm-5"></div>
                    </div>
                    <div class="col-md-12">
                        <!-- <div class="col-md-1"></div> -->

                        <div class="col-md-12 method_section">
                            <?php $m = 1; ?>
                            <div class="form-group">
                                {!! Form::label('method_id','Test Methods',['class'=>' control-label']) !!}
                                <?php $method_ids_exp = explode(',', $dataM['method_ids']); ?>
                                <?php $remarks2 = explode('@@', $dataM['remarks2']); ?>
                                <?php $scientist2 = explode('@@', $dataM['scientist2']); ?>
                                <?php $lab_incharge2 = explode('@@', $dataM['lab_incharge_id2']); ?>                                
                                <?php $prices2 = explode('@@', $dataM['prices2']); ?>
                                <?php $lab_ids2 = explode('@@', $dataM['lab_ids2']); ?>
                                
                                
                                <?php $xx = 0; ?>
                                <?php $user_type = CommonFunction::getUserType(); ?>

                                <div class="col-md-12">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-5">
                                        <label for="">Parameter => Method</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="">Remarks</label>
                                    </div>
                                    <div class="col-sm-2">
                                        @if($user_type==6)
                                        <label for="">Scientist</label>
                                        @endif
                                        @if($data->app_status == 2 && $user_type==8)
                                        <label for="">Lab Incharge</label>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="">Price</label>
                                        <label for="">Duration</label>
                                    </div>
                                </div>
                                @foreach($method_ids_exp as $key=>$methodRow)
                                <?php // print_r($method_ids_exp); ?>
                                <?php // print_r($sampleLists);  ?>

                                <div class="col-md-12 add_method_{!! $m++ !!} method_row">
                                    <div class="col-sm-1">
                                        {{--<button type="button" class="btn btn-default method_hide" onclick="hide_method(this)"><i class="fa fa-remove"></i></button>--}}
                                    </div>
                                    <div class="col-sm-5">
                                        {!! Form::hidden('method_id_selected', $methodRow, ['class'=>'method_id_selected m_'.$key]) !!}
                                        {!! Form::select('method_id['.$i.'][]', $sampleLists, $methodRow, ['class' => 'form-control method_id']) !!}
                                        {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::text('remarks['.$i.'][]',$remarks2[$xx], ['class' => 'required form-control remarks_','required','placeholder'=>'Remarks']) !!}
                                    </div>
                                    <div class="col-sm-2 ">

                                        @if($user_type==6)
                                        {!! Form::select('scientist_id[]',$scientist,$scientist2[$xx],array('class' => 'form-control')) !!}
                                        @endif
                                        @if($data->app_status == 2 && $user_type==8)
                                        <span  style="{{($user_type == 5)?"display:none;":'' }}">
                            
                                        {!! Form::select('lab_incharge[]', $labInchargeList, @$lab_incharge2[$xx], ['class' => 'required form-control no_hide']) !!}

                                        {!! $errors->first('lab_incharge','<span class="help-block">:message</span>') !!}
                                        </span>
                                        @endif
                                    </div>                                       

                                    <div class="col-sm-2 price_duration">  </div>
                                    <?php $xx++; ?>      
                                </div>                                        
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    {{--{!! Form::button('<i class="fa fa-plus"></i> Add Method', array('type' => 'button', 'class' => 'btn btn-sm btn-default pull-right new_method')) !!}--}}
                </div>

                <div class="form-group col-sm-12  {{$errors->has('lab_id') ? 'has-error' : ''}}" style="border-top:1px gray solid;{{($user_type == 5)?"display:none;":'' }}">
                    {!! Form::label('lab_id','Assign Lab',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::hidden('lab_id2', @$lab_ids2[0], ['class'=>'lab_id2']) !!}
                        {!! Form::select('lab_id['.$i.']', [], $dataM['lab_id'], ['class' => 'required form-control lab_id assigned_lab_list','id'=>'assigned_lab_'.$i,'required'=>1]) !!}

                        {!! $errors->first('lab_id','<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::button('<i class="fa fa-refresh"></i> Load Lab', array('type' => 'button','class' => 'load_lab load_lab11  btn btn-xs btn-default  ','onclick'=>'load_lab(this,0)')) !!}

                    </div>
                </div>
                <?php if ($data->app_status == 2 && $user_type == 8) { ?>
                    <div class="form-group col-sm-12  {{$errors->has('lab_reference_no') ? 'has-error' : ''}}" class="no_hide">
                        {!! Form::label('lab_reference_no','Lab Reference',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::text('lab_reference_no['.$i.']',  $dataM['lab_reference_no'], ['class' => 'no_hide required form-control lab_reference_no','id'=>'lab_reference_no_'.$i,'required'=>1]) !!}
                            {!! $errors->first('lab_reference_no','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                <?php } ?>
                <div class="lab_price_dur"></div>
            </div>
            <?php $i++; ?>
            @endforeach

            <?php $j = $i; ?>
            @for($i=$j;$i<5;$i++)
            <div class="add_section_{!! $i !!} " <?php echo $i > 0 ? 'style="display: none;"' : 'style=""'; ?> >
                <?php echo $i > 0 ? '<div class="col-md-12 span_border">' . Form::button('<i class="fa fa-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger pull-right', 'onclick' => 'hide_section(' . $i . ')')) . '</div>' : ''; ?>

                <div class="form-group col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                    {!! Form::label('job_name','Sample Description',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('job_name['.$i.']',$dataM['job_name'], ['class' => 'required form-control','required']) !!}
                        {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-sm-5"></div>
                </div>

                <div class="form-group col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                    {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::select('tst_id['.$i.']', $testSample, $dataM['tst_id'], ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                        {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="col-md-12">
                    <div class="method_section">
                        <?php $m = 1; ?>
                        <div class="form-group {{$errors->has('method_id') ? 'has-error' : ''}}">
                            {!! Form::label('method_id','Test Methods',['class'=>' control-label']) !!}
                            <?php $method_ids_exp = explode(',', $dataM['method_ids']); ?>
                            @foreach($method_ids_exp as $methodRow)
                            @if(1==1)
                            <div class="col-md-12 add_method_{!! $m++ !!} method_row">

                                <div class="col-sm-5">

                                    {!! Form::select('method_id['.$i.'][]', $sampleLists, $methodRow, ['class' => 'form-control method_id']) !!}
                                    {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="col-sm-2">
                                    {!! Form::text('remarks['.$i.'][]','', ['class' => 'required form-control remarks_','required','placeholder'=>'Remarks']) !!}
                                </div>
                                <div class="col-sm-3 price_duration">

                                </div>

                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    {{--{!! Form::button('<i class="fa fa-plus"></i> Add Method', array('type' => 'button', 'class' => 'btn btn-sm btn-default pull-right new_method')) !!}--}}
                </div>
                <hr/>
                <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}"  style="border-top:1px gray solid; {{($user_type == 5)?"display:none;":'' }}">
                    {!! Form::label('lab_id','Assign Lab',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::select('lab_id['.$i.']', [], $dataM['lab_id'], ['class' => 'required form-control lab_id assigned_lab_list','id'=>'assigned_lab_'.$i,'required'=>1]) !!}

                        {!! $errors->first('lab_id','<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::button('<i class="fa fa-refresh"></i> Load Lab', array('type' => 'button','class' => 'load_lab btn btn-xs btn-default','onclick'=>'load_lab(this,0)')) !!}
                    </div>
                </div>
                <div class="lab_price_dur">

                </div>
                <br/>
            </div>
            @endfor
        </div><!-- /.box- -->
    </div>
        @if(Session::get('member_type') == 3 OR Session::get('member_type') == 5)
    <div class="box box-body">

        <?php /* @if((Session::get('member_type') != 6 OR Session::get('member_type') !=  7 OR Session::get('member_type') !=  8) AND $dataM->lab_ids2 == Session::get('lab_id')) */ ?>

            <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                {!! Form::label('total_price','Net Price',['class'=>'col-sm-3 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::hidden('total_price', $data->total_price, ['class' => 'form-control']) !!}
                    <span class="total_price">{{$data->total_price}}</span>
                </div>
            </div>
            <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                {!! Form::label('payable','First Time Minimum Payable',['class'=>'col-sm-3 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::hidden('total_price', $data->total_price, ['class' => 'form-control']) !!}
                    {!! Form::text('payable', $data->payable, ['class' => 'required number form-control','required'=>1,'placeholder'=>'Taka']) !!}

                    {!! $errors->first('payable','<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div><!-- /.box-footer -->



        @endif
 @if(Session::get('member_type') == 3 OR Session::get('member_type') == 5)

    <div class="clearfix clear box box-danger">
        @if(count($paymentList) > 0)
        <div class="col-md-12">
            <h4>Payment History:</h4>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <!--<th>Due Payment</th>-->
                        <th>Amount Paid</th>
                        <th>Paid By</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sum = 0; //dd($totalAmount); ?>
                    @foreach ($paymentList as $result_view)
                    <tr>
                        <td>
                            {!! $result_view->new_payment !!}
                            <?php $sum = $sum + $result_view->new_payment; ?>
                        </td>
                        <td>{!! $result_view->member_first_name . ' ' . $result_view->member_middle_name !!}</td>
                        <td>{!! $result_view->created_at !!}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=3>
                            Total Paid: <strong>{!! $sum !!}</strong> BDT <br /></td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            Total Price: <strong> {!! $paymentList[0]->total_price !!}	</strong> BDT <br /></td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            Percentage of Discount: <strong> {!! $paymentList[0]->discount_price !!}	</strong> %<br /></td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            Price after Discount: <strong> {!! $paymentList[0]->total_discount_price !!}	</strong> BDT <br /></td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            VAT (15%): <strong> {!! $paymentList[0]->total_vat !!}	</strong> BDT <br /></td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            Total Payable:<strong> {!! $paymentList[0]->total_discount_price + $paymentList[0]->total_vat !!}	</strong> BDT <br /></td>
                    </tr>

                    <tr>
                        <td colspan=3>
                            Due: <strong>{!! ($paymentList[0]->total_discount_price + $paymentList[0]->total_vat) - $sum !!}</strong> BDT
                            {!! Form::hidden('due_payment',($paymentList[0]->total_price + $paymentList[0]->total_vat) - $sum,
                            ['class'=>'required form-control','placeholder'=>'']) !!}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        @endif
    </div>

        @endif
    <div class="box-footer">
        <div class="">
            {!! Form::label('submitted_by','Submitted by:',['class'=>'control-label']) !!}
            {{$data->submitted_by}}
            <br/>
            {!! Form::label('contact_no','Contact No:',['class'=>'control-label']) !!}
            {{$data->submitted_contact}}
        </div>
        <div class="pull-left col-md-2">
            <a href="{{ url('/apps') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
        </div>
        <div class="col-md-4">
            {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
        </div>
        <div class=" col-md-6">
            <div class=" pull-right">
                {{--{!! Form::button('<i class="fa fa-print"></i> Print', array('type' => 'button', 'value'=> 'print', 'class' => 'btn btn-default print')) !!} --}}
                {{--@if( $user_type == 8 && $app_status_pending_in_lab > 0)--}}
                @if( $user_type == 8)
                {!! Form::button('<i class="fa fa-save"></i> Assign Lab-Incharge', array('type' => 'submit', 'name'=>'submitFormLD', 'value'=> 'Received_in_lab', 'class' => 'btn btn-success no_hide')) !!}
                @endif
                @if( $user_type == 8 && $app_status_completed_in_lab > 0)
                {!! Form::button('<i class="fa fa-save"></i> Released from Lab', array('type' => 'submit', 'name'=>'submitFormLD' , 'value'=> 'Released_from_lab', 'class' => 'btn btn-success no_hide')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Send for Recheck', array('type' => 'submit', 'name'=>'submitFormLD' ,'value'=> 'Recheck_in_lab', 'class' => 'btn btn-success no_hide')) !!}                            
                @endif


            </div>
        </div>
    </div><!-- /.box-footer -->


    {!! Form::close() !!}


</section><!-- /.content -->
@endsection

@section('footer-script')

<script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>

<script>
        var _token = $('input[name="_token"]').val();
        $(function () {
        for (var i =<?php echo $j; ?>; i < 5; i++) {
        hide_section(i);
        }

        $(".assignInch").click(function () {
        $('#app_entry_form').submit();
        });
                $(".send").click(function () {
        if (confirm('Are you sure to Submit this Application?')) {
        $('.submitted').val(2);
                $('#app_entry_form').submit();
        }
        });
                $(".apps_from").validate({
        errorPlacement: function () {
        return false;
        }
        });
                $('.company_id').change(function () {

        var id = $(this).val();
                $.post('/apps/ajax/company', {id: id, _token: _token}, function (response) {
                if (response.responseCode == 1) {
                $('.location').html(response.data.location);
                        $('.contact').html(response.data.contact);
                        $('.company_details').show();
                }
                });
        });
//var method
                $('.new_method').click(function () {
        var obj = $(this).parent().parent();
                var new_method;
                new_method = '<div class="col-md-12 add_method_k method_row">';
                new_method += obj.find('.add_method_1').html();
                new_method += '</div>';
                obj.find('.method_section .form-group').append(new_method);
                obj.find('.method_section .form-group .remarks_').last().val('');
                obj.find('.method_section .form-group .price_duration').last().html('');
                $(this).parent().parent().find('.lab_id').html('<option value="">Select Lab</option>');
        });
                $('.new_section').click(function () {
        var found = false;
                for (i = 1; i <= 4; i++) {
        if ($('.add_section_' + i).attr('style') == 'display: none;') {
        var obj = $('.add_section_' + i);
//                        alert('.add_section_' + i);
                obj.find('input[name="job_name[' + i + ']"]').val('');
                obj.find('.method_id').html('<option value="">Select Sample</option>');
                obj.find('.tst_id ').val('');
//                        alert("done empty");
                $('.add_section_' + i).show();
                $('.add_section_' + i + ' input').prop('disabled', false);
                $('.add_section_' + i + ' select').prop('disabled', false);
                found = true;
                break;
        }
        }
        if (!found)
                alert('You have added Maximum in one Application!');
//                $('html, body').animate({ scrollTop: $('.add_section_'+ section_count).offset().top }, 'slow');
        });
                $('.hide_section').click(function () {
        var section_id = $(this).attr('section-id');
                $('.add_section_' + section_id).hide();
        })
        });
        function hide_section(i) {
        $('.add_section_' + i).hide();
                $('.add_section_' + i + ' input').prop('disabled', true);
                $('.add_section_' + i + ' select').prop('disabled', true);
        }

function hide_method(object) {
if (confirm('Are you sure to Delete this?')) {
var obj = $(object).parent().parent();
        // obj.find('input').prop('disabled',true);
        // obj.find('select').prop('disabled',true);

        obj.html('');
        obj.parent().parent().parent().parent().find('.lab_id').html('<option value="">Select Lab</option>');
}
}

var loopTesting = 0;
        function load_lab(object, flag) {
        //      loopTesting++;

        var obj = $(object).parent().parent().parent();
                method = [];
                obj.find('.method_id :selected').each(function (i, selected) {
        method[i] = $(selected).val();
        });
                console.log(method);
                if ((typeof method[0] != 'undefined') && method[0] != '') {
        obj.find('.fa-refresh').addClass('animate');
                $.post('/apps/ajax/lab', {id: method, _token: _token}, function (response) {

                if (response.responseCode == 1) {

                var option = '<option value="">Select Labratory</option>';
                        if (response.data.length >= 1) {
                $.each(response.data, function (id, value) {

                var selected = obj.find('.lab_id2').val() == value.lab_id ? 'selected' : '';
                        option += '<option value="' + value.lab_id + '" ' + selected + ' >' + value.lab_name + '</option>';
                });
                } else {

                }
                obj.find('.lab_id').html(option);
                        obj.find('.lab_id').trigger('change');
                        obj.find('.fa-refresh').removeClass('animate');
                }
                });
        }
        else {
//                                                alert('Please select the Method');
        //       loopTesting++;
        }

        }

var total_samples_price = 0;
        (function ($) {
        $(".assigned_lab_list").on('change', function () {

        var lab_id = $(this).val();
                var obj = $(this).parent().parent().parent();
                obj.find('.fa-refresh').addClass('animate');
                method = [];
                obj.find('.method_id :selected').each(function (i, selected) {
        method[i] = $(selected).val();
        });
                // obj.find('.price_duration').html('');
                var total_samples_price = 0;
                var total_price = 0;
                var total_day = [];
                var max_days;
                $.post('/apps/ajax/lab-price-dur', {method: method, lab_id: lab_id, _token: _token}, function (response) {
//                console.log(response);
                if (response.responseCode == 1) {

                // price_duration
                obj.find('.method_id :selected').each(function (i, selected) {
                if (typeof response.data[i] != 'undefined') {
                $(selected).parent().parent().parent().find('.price_duration').html("" + response.data[i].price + " TK &nbsp;(" + response.data[i].duration + " days)");
                        total_price += parseInt(response.data[i].price);
                        total_day[i] = parseInt(response.data[i].duration);
                }
                });
                        if (total_day.length){
                max_days = Math.max.apply(Math, total_day);
                } else{
                max_days = 0;
                }
                obj.find('.lab_price_dur').html('<div class="col-sm-3"></div><div class="col-sm-4"><label>Total: </label> <b>Price:</b> <span class="t_price">' + total_price + '</span> <b>TK, &nbsp;&nbsp;Duration:</b> ' + max_days + ' days</div>');
                        obj.find('.fa-refresh').removeClass('animate');
                        var i = 0;
                        $(".content-wrapper").find('.t_price').each(function (i) {
                total_samples_price += parseFloat($(this).html());
                });
                        $(".total_price").html(total_samples_price + " BDT");
                        loopTesting++;
                }
                //   alert(loopTesting);
                if (loopTesting == {{$loopNum}})
                {
                makeView();
                }

                });
        })
        })(jQuery);
        $(".content-wrapper").find('.t_price').each(function (i) {
total_samples_price += parseFloat($(this).html());
});
        function makeView() {
        $('#app_entry_form').find('input:not([type=checkbox],[type=from], [type=hidden])').not('input.no_hide').each(function () {
        $(this).replaceWith("<span><b>" + this.value + "</b></span>");
        });
                $('#app_entry_form').find('textarea').each(function () {
        $(this).replaceWith("<span class=\"col-md-3\"><b>" + this.value + "</b></span>");
        });
                $("#app_entry_form").find('select').not('select.no_hide').replaceWith(function () {
        var selectedText = $(this).find('option:selected').text();
                var selectedTextBold = "<b>" + selectedText + "</b>";
                return selectedTextBold;
        });
//            $("#app_entry_form").find('button').replaceWith(function () {
//                return '';
//            });

                // if not button no hide
                $("#app_entry_form").find('button').not('button.no_hide').replaceWith(function () {
        return '';
        });
                /*$("#app_entry_form").find('select.receivedInLab').replaceWith(function () {
                 return '';
                 });*/
                // if select is not assigned_lab_list
//            $("#app_entry_form").find('select').not(".assigned_lab_list").replaceWith(function () {
//                var selectedText = $(this).find('option:selected').text();
//                var selectedTextBold = "<b>" + selectedText + "</b>";
//                return selectedTextBold;
//            });
        }


$(".total_price").html(total_samples_price + " BDT");
        var flagFirst = 0;
        var ss = 0;
        function changeTest(object) {
        flagFirst++;
                var id = $(object).val();
                var obj = $(object).parent().parent().parent();
//        obj.find('.lab_id').html('');
                obj.find('.lab_id').html('<option value="">Select Lab</option>');
                obj.find('.price_duration').html('');
                obj.find('.lab_price_dur').html('');
                $.post('/apps/ajax/parameter-method', {id: id, _token: _token}, function (response) {
                if (response.responseCode == 1) {
                var option = '';
//                console.log(response.data);
                        var c = 0;
                        $.each(response.data, function (id, value) {
//                    alert(response.data);
                        //alert(obj.find('.m_' + c).val() + '=' + c);
                        var selected = obj.find('.m_' + c).val() == value.method_id ? 'selected' : '';
                                c++;
                                option += '<option value="' + value.method_id + '" ' + selected + ' >' + value.param_name + '=>' + value.method_name + '</option>';
                        });
                        obj.find('.method_id').html(option);
                        if (flagFirst == 5)
                {
                $(".method_id").each(function (e) {
                var xxx = $(this).prev().val();
                        $(this).val(xxx);
                });
                        ss++;
                        //    alert('333333333');
                        //obj.find('.load_lab').trigger('click');

                }

                //  alert(ss);

                if (ss == 5)
                {
                $('.load_lab').trigger('click');
                }

//                    obj.find('.load_lab11').trigger('click');

//                    $('.load_lab11').trigger('click');                                        



                }

                });
        }

$(".changeStatus").click(function(){
var id = '{!!$id!!}';
{{--var baseUrl = '{{url()}}'; --}}
{{--alert(ss); --}}
{{--$.post(baseUrl + 'parameter-method', {id: ss, _token: _token}, function (response)--}}
{{--{--}}
{{--alert(response); --}}
{{--//                location.reload();--}}

{{--}); --}}

$.post('/apps/ajax/publish-result', {id: id, _token: _token}, function (response) {
location.reload();
});
});
        $(".receivedInLab").click(function(){
var id = '{!!$id!!}';
        $.post('/apps/ajax/received-lab', {id: id, _token: _token}, function (response) {
        alert("Application has been received successfully");
                location.replace('/apps');
        });
});
        $(function () {
        $('.tst_id :selected').trigger('change');
//        $('.load_lab').trigger('click');


        });
        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
</script>
@endsection
