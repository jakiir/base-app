<?php namespace App\Modules\Reports\Models;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model {

	//
	protected $table = 'custom_reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['report_title', 'report_para1','status','user_id','updated_by'];
}
