<?php

Route::group(array('module' => 'Reports', 'namespace' => 'App\Modules\Reports\Controllers'), function() {

    Route::get('/reports', "ReportsController@index")->middleware(['ocpl.checkvalid']);

    Route::get('/reports/create', "ReportsController@create")->middleware(['ocpl.checkvalid']);
    Route::get('/reports', "ReportsController@index")->middleware(['ocpl.checkvalid']);

    Route::get('/reports/show/{id}', "ReportsController@show")->middleware(['ocpl.checkvalid']);
    Route::get('/reports/view/{id}', "ReportsController@view")->middleware(['ocpl.checkvalid']);
    Route::get('/reports/edit/{id}', "ReportsController@edit")->middleware(['ocpl.checkvalid']);

    Route::post('/reports/verify', "ReportsController@reportsVerify")->middleware(['ocpl.checkvalid']);
    Route::get('/reports/tables', "ReportsController@showTables")->middleware(['ocpl.checkvalid']);

    Route::post('/reports/show-report/{report_id}', "ReportsController@showReport")->middleware(['ocpl.checkvalid']);


    Route::patch('/reports/store', "ReportsController@store")->middleware(['ocpl.checkvalid']);
    Route::patch('/reports/update/{id}', "ReportsController@update")->middleware(['ocpl.checkvalid']);
    // Route::controller('/admin', "AdminsController@index");

    // Route::patch('/admin/store', "AdminsController@store");
    // Route::resource('/admin', 'AdminsController');
    
});
