@extends('master-admin-default')

@section('title')
    <title>Reports</title>
@endsection

@section('content')

    <section class="content-header">
        <h1>
            Reports
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">List of report</li>
        </ol>
    </section>


<section class="content">
        <div class="box box-success">

            <div class="box-body">

            {!! Form::open(['url' => 'reports/show-report/'.$report_id, 'method' => 'post', 'class' => 'form', 'role' => 'form']) !!}
            @foreach($reportParameter as $input=>$value)
            <?php $data = explode('|', $input);
            ?>
            <div class="form-group {{$errors->has($input) ? 'has-error' : ''}}">
                {!! Form::label($data[0],count($data)>1?($data[1]?$data[1]:$data[0]):$data[0]) !!}
                @if(count($data)>2)
                @if($data[2]=='numeric')
                {!! Form::number($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @elseif($data[2]=='date')
                {!! Form::date($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @else
                {!! Form::text($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @endif
                @else
                {!! Form::text($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @endif
                {!! $errors->first($input,'<span class="help-block">:message</span>') !!}
            </div>

            @endforeach
            {!! Form::submit('Show',['class'=>'btn btn-primary','name'=>'show_report']) !!}

            {!! Form::close() !!}

</div>
</div>
</section><!-- /.content -->

@endsection

@section('footer-script')
<script>
    $(function () {
        // $("#report_list").DataTable();
        $('#report_data').DataTable({
            "paging": true,
            "lengthChange": false,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "iDisplayLength": 20
        });

        $("#rpt_date").datepicker({
            maxDate: "+20Y",
            //showOn: "button",
            //buttonText: "Select date",
            buttonText: "Select date",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showAnim: 'scale',
            yearRange: "-100:+40",
            minDate: "-200Y",
        });
    });
</script>
@endsection
