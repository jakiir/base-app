@extends('master-admin-default')

@section('title')
    <title>Reports</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Report Edit
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">reports</a></li>
            <li class="active">create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
            </div>

            <div class="box-body">
                {!! Form::open(['url' => '/reports/update/'.Encryption::encodeId($report_data->report_id), 'method' => 'patch', 'class' => 'form report_form', 'role' => 'form']) !!}

                <div class="form-group {{$errors->has('report_title') ? 'has-error' : ''}}">
                    {!! Form::label('report_title','Report Title') !!}
                    {!! Form::text('report_title',$report_data->report_title,['class'=>'form-control report_title']) !!}
                    {!! $errors->first('report_title','<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group">
                    <div class="radio">
                        <label>
                            {!! Form::radio('status', '1', $report_data->status==1? true:false) !!}
                            Publish
                        </label>
                        <label>
                            {!! Form::radio('status', '0', $report_data->status==0? true:false) !!}
                            Unpublished
                        </label>
                    </div>
                </div>

                <div class="form-group {{$errors->has('user_id') ? 'has-error' : ''}}">
                    {!! Form::label('user_id','Who can view?') !!}
                    <?php
                    $userTypes = explode(',', $report_data->user_id);

                    ?>
                    {!! Form::select('user_id[]', $usersTypeList, $userTypes, ['class' => 'form-control user_id', 'multiple'=>'multiple']) !!}
                    {!! $errors->first('user_id','<span class="help-block">:message</span>') !!}
                </div>

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab_1" aria-expanded="true">SQL</a></li>
                        <li class="results_tab"><a data-toggle="tab" href="#tab_2" aria-expanded="false">Results</a></li>
                        <li class="db_tables"><a data-toggle="tab" href="#tab_3" aria-expanded="false">Database objects</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab_1" class="tab-pane active">
                            <div class="form-group {{$errors->has('report_para1') ? 'has-error' : ''}}">
                                {!! Form::textarea('report_para1', Encryption::dataDecode($report_data->report_para1), ['class'=>'sql form-control well fa-code-fork']) !!}
                                {!! $errors->first('report_para1','<span class="help-block">:message</span>') !!}
                            </div>
                        </div><!-- /.tab-pane -->

                        <div id="tab_2" class="tab-pane">
                            <div class="results">
                                Please click on Verify button to run the SQL.
                            </div>
                        </div><!-- /.tab-pane -->
                        <div id="tab_3" class="tab-pane">
                            <div class="db_fields">
                                Please click on Show Tables button to run the SQL.
                            </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>


                <div class="col-md-12">
                    {!! CommonFunction::showAuditLog($report_data->updated_at, $report_data->updated_by) !!}
                </div>

                {!! Form::hidden('redirect_to_new',0,['class'=>'form-control redirect_to_new']) !!}

                <div class="form-group">
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'button', 'value'=> 'save', 'class' => 'btn btn-success save')) !!}
                    {!! Form::button('<i class="fa fa-credit-card"></i> Save & New', array('type' => 'button', 'value'=> 'save_new', 'class' => 'btn btn-warning save')) !!}
                    <a href="/reports">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-danger')) !!}</a>
                    {!! Form::button('<i class="fa fa-files-o"></i> Save as new',  array('type' => 'button', 'value'=>'save_as_new', 'class' => 'btn btn-primary save')) !!}

                    <span class="pull-right">
                        <button class="btn btn-sm btn-primary" id="verifyBtn" type="button">
                            <i class="fa fa-check"></i>
                            Verify
                        </button>
                        <button class="btn btn-sm btn-primary" id="showTables" type="button">
                            <i class="fa fa-list"></i> Show Database
                        </button>
                    </span>
                </div>

                {!! Form::close() !!}

            </div><!-- /.box-body -->
        </div>
    </section><!-- /.content -->
@endsection


@section('footer-script')
    <script language="javascript">
        $(document).ready(
                function () {

                    $('.save').click(function(){
                        switch ($(this).val()) {
                            case 'save_as_new' :
                                $('.report_title').val($('.report_title').val() + "-{{ Carbon\Carbon::now() }}");
                                $('.report_form').attr('action',"{!! URL::to('/reports/store') !!}");
                                break;
                            case 'save_new':
                                $('.redirect_to_new').val(1);
                                break;
                            default:
                        }
                        $('.report_form').submit();

                    });



                    $('#verifyBtn').click(function(){
                        var sql = $('.sql').val();
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url: '/reports/verify',
                            type: 'POST',
                            data: {sql: sql, _token: _token},
                            dataType: 'text',
                            success: function (data) {
                                $('.results').html(data);
                                $('.results_tab a').trigger('click');
                            },
                            error:function (jqXHR, textStatus, errorThrown) {
                                $('.results').html(jqXHR.responseText);
                                $('.results_tab a').trigger('click');
                            }
                        });
                    });

                    $('#showTables').click(function(){
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url: '/reports/tables',
                            type: 'GET',
                            data: {_token: _token},
                            dataType: 'text',
                            success: function (data) {
                                $('.db_fields').html(data);
                                $('.db_tables a').trigger('click');
                            }
                        });
                    });
                });
    </script>
@endsection
