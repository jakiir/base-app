@extends('master-admin-default')

@section('title')
<title>Reports</title>
@endsection

@section('content')

    <section class="content-header">
        <h1>
            Reports
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">List of report</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

<div class="box box-success">
    <div class="box-body">
        <div class="">
        <div class="box-body  col-md-5">
            {!! Form::open(['url' => 'reports/show-report/'.Encryption::encodeId($report_id), 'method' => 'post', 'class' => 'form', 'role' => 'form']) !!}
            @foreach($reportParameter as $input=>$value)
            <?php $data = explode('|', $input);
            ?>
            <div class="form-group {{$errors->has($input) ? 'has-error' : ''}}">
                {!! Form::label($data[0],count($data)>1?($data[1]?$data[1]:$data[0]):$data[0]) !!}
                @if(count($data)>2)
                @if($data[2]=='numeric')
                {!! Form::number($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @elseif($data[2]=='date')
                {!! Form::date($data[0],Session::get($data[0]),['class'=>'form-control datepicker']) !!}
                @else
                {!! Form::text($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @endif
                @else
                {!! Form::text($data[0],Session::get($data[0]),['class'=>'form-control']) !!}
                @endif
                {!! $errors->first($input,'<span class="help-block">:message</span>') !!}
            </div>

            @endforeach
            {!! Form::submit('Show',['class'=>'btn btn-primary','name'=>'show_report']) !!}

            {!! Form::close() !!}
        </div><!-- /.box-body -->
    </div>
        <div id="report_list_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
        <div class="row">
            <div class="col-sm-12">
                <?php
                $report = new \App\Libraries\ReportHelper();
                $reportList = $report->report_gen($report_id, $recordSet, $report_data->report_title, '');
                ?>
            </div>
        </div>
    </div>
    </div>
</div>
</section><!-- /.content -->

@endsection

@section('footer-script')
<script>
    $(function () {
        $('#report_data').DataTable({
            "paging": true,
            "lengthChange": false,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "iDisplayLength": 20
        });
        
       $("#rpt_date").datepicker({
            maxDate: "+20Y",
            //showOn: "button",
            //buttonText: "Select date",
            buttonText: "Select date",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showAnim: 'scale',
            yearRange: "-100:+40",
            minDate: "-200Y",
        });
    });
</script>
@endsection
