@extends('master-admin-default')

@section('title')
    <title>Reports</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List of Reports
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">List of report</li>
        </ol>
    </section>


    <section class="content">

        <div class="box">

            <div class="box-body">
                <table id="report_list" class="table table-striped" role="grid">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Report Title</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($result as $row)
                        <tr>
                            <td>{!! $i !!}</td>
                            <td>{!! $row->report_title !!}</td>
                            <td>{!! $row->status==1? '<span class="label label-success">Published</span>':'<span class="label label-warning">Un-published</span>' !!}</td>
                            <td>{!! date('d M Y',strtotime($row->created_at))  !!}</td>
                            <td>
                                {!! link_to('reports/view/'. Encryption::encodeId($row->report_id),'View',['class' => 'btn btn-primary btn-xs']) !!}

                               @if(CommonFunction::getUserType() == 1)
                                    {!! link_to('reports/edit/'. Encryption::encodeId($row->report_id),'Edit',['class' => 'btn btn-info btn-xs']) !!}
                               @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.content -->

@endsection

@section('footer-script')
    <script>

        $(function () {
            // $("#report_list").DataTable();
            $('#report_list').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength":20
            });
        });

    </script>
@endsection
