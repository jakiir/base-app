<?php
namespace App\Modules\Reports\Controllers;

//use App\Http\Request;
use App\Http\Requests\ReportsRequest;
use App\Http\Controllers\Controller;
use App\Modules\Membertype\Models\Membertype;
use App\Modules\Reports\Models\Reports;
use App\Modules\Users\Models\UsersModel;
use Illuminate\Http\Request;
use App\Libraries\ReportHelper;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\Auth;
use Session;
use Encryption;
use DB;

class ReportsController extends Controller {

    private $report;
    public function __construct(){
        $this->report = new ReportHelper();
    }

    public function index()
    {

        $result = Reports::where('user_id','like','%'.CommonFunction::getUserType().'%')->get();
        return view("reports::list",compact('result'));
    }

    public function create()
    {
//        $usersList = UsersModel::whereIn('lab_id',['1','8'])->lists('member_username','member_id');
        $usersTypeList = Membertype::lists('mt_type_name','mt_id');
        return view("reports::create", ['usersTypeList' => $usersTypeList]);
    }

    public function store(ReportsRequest $request)
    {
        $userIds = implode(',',$request->get('user_id'));

        $reports = Reports::create([
            'report_title' => $request->get('report_title'),
            'report_para1' => Encryption::dataEncode($request->get('report_para1')),
            'status' => $request->get('status'),
            'user_id' => $userIds,
            'updated_by' => 1
        ]);
        Session::flash('success', 'Successfully Saved the Report.');
        return $request->redirect_to_new == 1 ? redirect('/reports/create') : redirect('/reports/edit/' . Encryption::encodeId($reports->id));
    }

    public function show($id)
    {
//        $report_id = Encryption::decodeId($id);
//
//        $report_data = Reports::where('report_id', $report_id)->first();
    }

    public function edit($id, Request $request)
    {
        $report_id = Encryption::decodeId($id);

        $report_data = Reports::where('report_id', $report_id)->first();
//        $usersList = UsersModel::whereIn('lab_id',['1','8'])->lists('member_username','member_id');
        $usersTypeList = Membertype::lists('mt_type_name','mt_id');

        return view("reports::edit",compact('report_data','usersTypeList'));
    }

    public function update($id, ReportsRequest $request)
    {
        $report_id = Encryption::decodeId($id);
        $userIds = implode(',',$request->get('user_id'));

        Reports::where('report_id', $report_id)->update([
            'report_title' => $request->get('report_title'),
            'report_para1' => Encryption::dataEncode($request->get('report_para1')),
            'status' => $request->get('status'),
            'user_id' => $userIds,
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Successfully Updated the Report.');
        return $request->redirect_to_new == 1 ? redirect('/reports/create') : redirect('/reports/edit/' . Encryption::encodeId($report_id));
    }

    public function destroy($id)
    {
        //
    }

    public function reportsVerify(Request $request) {
        $sql = $request->get('sql');

        $sql = preg_replace('/&gt;/','>',$sql);
        $sql = preg_replace('/&lt;/','<',$sql);

        echo '<hr /><code>'.$sql.'</code><hr />';
        $sql = $this->sqlSecurityGate($sql);

        $query = DB::select( DB::raw($sql));

        if (!$query) {
            dd('Query Error');
        } else {
            $result = $query; //json_decode(json_encode($query), true);
            $result2 = array();
            foreach ($result as $value):
                $result2[] = $value;
                if (count($result2) > 19)
                    break;
            endforeach;
            echo '<p></p><pre>';
            print_r($result2);
            echo '</pre>';
            echo 'showing ' . count($result2) . ' of '.count($result);
            echo '</p>';
        }
    }

    public function sqlSecurityGate($sql) {
        $sql = trim($sql);
        $select_keyword = substr($sql, 0, 6);
        $semicolon = strpos($sql, ';');
        if (($select_keyword == 'select' OR $select_keyword == 'SELECT' OR $select_keyword == 'Select') AND $semicolon == '') {
            return $sql;
        } else {
            dd('Sql is not Valid: ' . $sql);
        }
    }

    public function showTables(Request $request) {
        if($request->session()->has('db_tables')){
            echo $request->session()->get('db_tables');
        } else {
            $tables = DB::select(DB::raw('show tables'));
            $count = 1;
            $ret = '<ul class="table_lists">';
            foreach ($tables as $table) {
                $table2 = json_decode(json_encode($table), true);

                $ret .= '<li class="table_name table_' . $count . '">' . $table2[key($table2)];
                $fields = DB::select(DB::raw('show fields from ' . $table2[key($table2)]));

                $ret .= '<ul>';
                foreach ($fields as $field) {
                    $ret .= '<li>' . $field->Field . '</li>';
                }
                $ret .= '</ul>';

                $ret .= '</li>';
                $count++;
            }
            $ret .= '</ul>';

            $request->session()->put('db_tables', $ret);
            echo $ret;
        }
    }

    public function view($report_id = '')
    {
        $report_id2 = Encryption::decodeId($report_id);
        $report_data = Reports::where('report_id', $report_id2)->first();
        $reportParameter=$this->report->getSQLPara(Encryption::dataDecode($report_data->report_para1));
        return view('reports::reportInputForm',compact('reportParameter','report_id'));
    }

    public function showReport($report_id, Request $request) {
        if (!$request->all()) {
            return redirect('reports/view' . $report_id);
        }
        $report_id = Encryption::decodeId($report_id);
        $report_id = is_numeric($report_id) ? $report_id : null;
        if (!$report_id) {
            return redirect('dashboard');
        }

        $data = array();
        foreach ($request->all() as $key => $row) {
            if (substr($key, 0, 4) == 'rpt_') {
                $data[$key] = $request->get($key);
                $request->session()->put($key,$request->get($key));
            } else {
                $data[$key] = $request->get($key);
            }
        }


        if ($request->get('show_report')) {
            $report_data = Reports::where('report_id', $report_id)->first();
            $reportParameter=$this->report->getSQLPara(Encryption::dataDecode($report_data->report_para1));
            $SQL = $this->report->ConvParaEx(Encryption::dataDecode($report_data->report_para1), $data);
            $recordSet = DB::select(DB::raw($SQL));

            return view('reports::reportGenerate',compact('recordSet','report_id','report_data','reportParameter'));
//        } elseif ($this->input->post('export_csv')) {
//            $this->exportCSV($report_id, $data);
//        } elseif ($this->input->post('export_csv_zip')) {
//            $this->exportCSV_Zip($report_id, $_SESSION);
//        } elseif ($this->input->post('export_pdf')) {
//            $this->exportPDF($report_id, $data);
        } else {
            return redirect('reports');
        }
    }
}
