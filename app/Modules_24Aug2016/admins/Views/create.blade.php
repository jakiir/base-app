  @extends('master-admin-login')



  @section('content')
  <div class="register-box" style="margin-top: 3% !important;">
    <div class="register-logo">
      <a href="javascript:void(0);"><b>- LOGO -</b></a>
    </div>

    <div class="register-box-body">
      <p class="login-box-msg">Register a new membership</p>
                  
      
      {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}

      {!! Form::open(array('url' => '/admin/store','method' => 'PATCH', 'class' => 'form-horizontal')) !!}

        <div class="form-group has-feedback {{$errors->has('admin_full_name') ? 'has-error' : ''}}">
          <input type="text" name="admin_full_name" class="form-control" placeholder="Full name">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
          {!! $errors->first('admin_full_name','<span class="help-block">:message</span>') !!}
        </div>
        <div class="form-group has-feedback {{$errors->has('admin_email') ? 'has-error' : ''}}">
          <input type="email" name="admin_email" class="form-control" placeholder="Email">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          {!! $errors->first('admin_email','<span class="help-block">:message</span>') !!}

        </div>
        <div class="form-group has-feedback {{$errors->has('admin_password') ? 'has-error' : ''}}">
          <input type="password" name="admin_password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          {!! $errors->first('admin_password','<span class="help-block">:message</span>') !!}
        </div>
        <div class="form-group has-feedback {{$errors->has('admin_repassword') ? 'has-error' : ''}}">
          <input type="password"name="admin_repassword" class="form-control" placeholder="Retype password">
          <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          {!! $errors->first('admin_repassword','<span class="help-block">:message</span>') !!}
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <!-- <label>
                <input type="checkbox"> I agree to the <a href="#">terms</a>
              </label> -->
            </div>
          </div><!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div><!-- /.col -->
        </div>
      {!! Form::close() !!}

      <div class="social-auth-links text-center">
        <p>- OR -</p>
        <!-- <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a> -->
        <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
      </div>

      <a href="login.html" class="text-center">I already have a membership</a>
    </div><!-- /.form-box -->
  </div><!-- /.register-box -->

  @endsection
