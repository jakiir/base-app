<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WorkPermitNextRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        //dd($_POST);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'company_id' => 'required',
            'issued_id' => 'required',
            'track_no' => 'required|unique:work_permit_form'
        ];
    }

    public function messages() {
        return [
            'company_id.required' => 'Company field is required',
            'issued_id.required' => 'Issued ID No. field is required',
            'track_no.numeric' => 'Track No. field is required'
        ];
    }

}
