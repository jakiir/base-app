<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Libraries\Encryption;

class UserEditRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        //dd($_POST);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = null;
        $segment = $this->segment(3) ? $this->segment(3) : '';
        if ($segment) {
            $id = Encryption::decodeId($segment);
        }

        return [
            'member_first_name' => 'required',
            'member_nid' => "digits_between:17,17|required|numeric|unique:members,member_nid,$id,member_id",
            'member_DOB' => 'required',
            'member_phone' => 'required',
            'member_email' => "required|email|unique:members,member_email,$id,member_id",
        ];
    }

    public function messages() {
        return [
            'member_first_name.required' => 'Name field is required',
            'member_nid.required' => 'National ID No. field is required',
            'member_nid.numeric' => 'National ID No. must be numeric',
            'member_nid.digits_between' => 'National ID No. must be 17 digits',
            'member_nid.unique' => 'National ID No. must be unique',
            'member_DOB.required' => 'Date of Birth field is required',
            'member_phone.required' => 'Mobile Number field is required',
            'member_email.required' => 'Email Address field is required',
            'member_email.unique' => 'Email Address must be unique',
        ];
    }

}
