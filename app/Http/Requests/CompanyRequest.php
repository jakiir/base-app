<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Libraries\Encryption;

class CompanyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = null;
        $segment = $this->segment(3) ? $this->segment(3) : '';
        if ($segment) {
            $id = Encryption::decodeId($segment);
        }

        return [
            'company_name' => 'required',
            'company_type' => 'required',
            'company_house_no' => 'required',
            'company_street' => 'required',
            'company_city' => 'required',
            'company_zip' => 'required',
            'phone_no' => 'required',
            'contact_email' => "required|email|unique:companies,contact_email,$id,company_id",
        ];
    }

    public function messages() {
        return [
            'company_name.required' => 'Company Name field is required',
            'company_type.required' => 'Company Type field is required',
            'company_house_no.required' => 'House / Plot / Holding Number. field is required',
            'company_city.required' => 'City field is required',
            'company_zip.required' => 'Zip / Post Code field is required',
            'contact_email.required' => 'Company Email Address field is required',
            'contact_email.unique' => 'Company Email Address must be unique',
            'phone_no.required' => 'Phone No Field is required',
        ];
    }

    /*     * ***********************End of Request Class************************** */
}
