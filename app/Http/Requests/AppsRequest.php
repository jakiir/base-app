<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\Input;
use App\Modules\Company\Models\Company;

class AppsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_type = CommonFunction::getUserType();
        $company_type = CommonFunction::getCompanyType();

        $appStatus = $this->get("submitted");
        if($user_type == 8):
            return [
            ];
        elseif($user_type == 6 || $appStatus == 2): //if user type is Lab Incharge or App Status is Sent to Lab
            return [];
        else:

            if($company_type == 6 || $company_type == 7):
            return [
                'job_name' => 'required',
                'tst_id' => 'required',
            ];
            else:
                return [
                        'job_name' => 'required',
                        'tst_id' => 'required',
                        'company_id' => 'required'
                    ];

            endif;
        endif;
    }
}
