<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SocialRegRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_nid' => 'digits_between:13,17|numeric|unique:members',
            'member_DOB' => 'required',
            'member_phone' => 'required',            
            'member_type' => 'required'            
        ];
    }
    public function messages() {
        return [
            'member_nid.required' => 'National ID No. field is required',
            'member_nid.numeric' => 'National ID No. must be numeric',
            'member_nid.digits_between' => 'National ID No. must be 17 digits',
            'member_nid.unique' => 'National ID No. must be unique',            
            'member_DOB.required' => 'Date of Birth field is required',
            'member_phone.required' => 'Mobile Number field is required',
            'member_type.required' => 'Member type field is required',
        ];
    }    
}
