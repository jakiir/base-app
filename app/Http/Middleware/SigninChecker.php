<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class SigninChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('member_id') == "" AND Session::get('member_username') == "" AND Session::get('access_code') == ""){
            \Session::flash('error', 'You need to Login to view this page.');
            return redirect('users/login');
        }

        return $next($request);
    }
}
