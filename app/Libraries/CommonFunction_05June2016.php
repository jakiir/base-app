<?php

namespace App\Libraries;

use App\User;
use Carbon\Carbon;
use Session;
use DB;

class CommonFunction {

    /**
     * @param Carbon|string $updated_at
     * @param string $updated_by
     * @return string
     * @internal param $Users->id /string $updated_by
     */
    public static function showAuditLog($updated_at = '', $updated_by = '') {
        $update_was = 'Unknown';
        if ($updated_at) {
//            $update_was = $updated_at->diffForHumans();
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::where('member_id', $updated_by)->first();
            if ($name) {
                $user_name = $name->member_username;
            }
        }
        return '<span class="help-block">Last updated : <i>' . $update_was . '</i> by <b>' . $user_name . '</b></span>';
    }

    public static function updatedOn($updated_at = '') {
        $update_was = '';
        if ($updated_at && $updated_at> '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }
        return $update_was;
    }

    public static function showDate($updated_at = '') {
         $update_was = '';
        if ($updated_at && $updated_at> '0') {
            $update_was = Carbon::createFromFormat('Y-m-d', $updated_at)->diffForHumans();
        }
        return $update_was;
    }
    
    public static function getUserId() {
        if (Session::has('member_id')) {
            $member_id = Session::get('member_id');
            Session::put('sess_user_id', $member_id);

            return Session::get('member_id');
        } else {
            dd('Invalid Login Id');
        }
    }

    public static function getUserType() {
        if (Session::has('member_type')) {
            return Session::get('member_type');
        } else {
            // return 1;
            dd('Invalid User Type Id');
        }
    }

    /**
     * Get Name of frontend.
     * @return boolean
     */
    public static function getFieldName($id, $field, $search, $table) {

        if ($id == NULL || $id == '') {
            return '';
        } else {
            return DB::table($table)->where($field, $id)->pluck($search);
        }
    }

    public static function getUserTypeName() {
        if (Session::has('member_type')) {
            $member_type_id = Session::get('member_type');
            $member_type_name = DB::table('member_types')
                    ->where('mt_id', $member_type_id)
                    ->pluck('mt_type_name');
            return $member_type_name;
        } else {
            dd('Invalid Login Id');
        }
    }

    public static function getLabId() {
        if (Session::has('lab_id')) {
            return Session::get('lab_id');
        } else {
            return 0;
        }
    }



    public static function getUserLabName() {
        if (Session::has('lab_id') && Session::get('lab_id') != 0) {
            $member_lab_id = Session::get('lab_id');
            $member_lab_name = DB::table('tbl_lab')
                    ->where('lab_id', $member_lab_id)
                    ->pluck('lab_name');
            return "<br/>(".$member_lab_name.")";
        } else {
            return "";
        }
    }    
    
    public static function getUserEmail() {
        if (Session::has('member_email')) {
            return Session::get('member_email');
        } else {
            return 0;
        }
    }
    
    public static function getCompanyType() {
        if (Session::has('member_id')) {
            $user_id = CommonFunction::getUserId();
            $company_type = DB::table('companies')->where('updated_by', $user_id)->pluck('company_type');
            return $company_type;
        } else {
            return 0;
        }
    }

    public static function updateScriptPara($sql, $data) {
        $start = strpos($sql, '{$');
        while ($start > 0) {
            $end = strpos($sql, '}', $start);
            if ($end > 0) {
                $filed = substr($sql, $start + 2, $end - $start - 2);
                $sql = substr($sql, 0, $start) . $data[$filed] . substr($sql, $end + 1);
            }
            $start = strpos($sql, '{$');
        }
        return $sql;
    }

    /*     * ****************************End of Class***************************** */
}
