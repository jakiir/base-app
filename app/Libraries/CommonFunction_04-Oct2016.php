<?php

namespace App\Libraries;

use App\User;
use Carbon\Carbon;
use Session;
use DB;

class CommonFunction {

    /**
     * @param Carbon|string $updated_at
     * @param string $updated_by
     * @return string
     * @internal param $Users->id /string $updated_by
     */
    public static function showAuditLog($updated_at = '', $updated_by = '') {
        $update_was = 'Unknown';
        if ($updated_at) {
//            $update_was = $updated_at->diffForHumans();
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::where('member_id', $updated_by)->first();
            if ($name) {
                $user_name = $name->member_username;
            }
        }
        return '<span class="help-block">Last updated : <i>' . $update_was . '</i> by <b>' . $user_name . '</b></span>';
    }

    public static function updatedOn($updated_at = '') {
        $update_was = '';
        if ($updated_at && $updated_at> '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }
        return $update_was;
    }

    public static function showDate($updated_at = '') {
         $update_was = '';
        if ($updated_at && $updated_at> '0') {
            $update_was = Carbon::createFromFormat('Y-m-d', $updated_at)->diffForHumans();
        }
        return $update_was;
    }
    
    public static function getUserId() {
        if (Session::has('member_id')) {
            $member_id = Session::get('member_id');
            Session::put('sess_user_id', $member_id);
            Session::put('user_id', $member_id);            

            return Session::get('member_id');
        } else {
            dd('Invalid Login Id');
        }
    }

    public static function validateMobileNumber($mobile_no)
    {
        $mobile_validation_err = '';
        $first_digit = substr($mobile_no, 0, 1);
        $first_two_digit = substr($mobile_no, 0, 2);
        $first_four_digit = substr($mobile_no, 0, 5);
        // if first two digit is 01
        if (strlen($mobile_no) < 11) {
            $mobile_validation_err = 'Mobile number should be minimum 11 digit';
        } elseif ($first_two_digit == '01') {
            if (strlen($mobile_no) != 11) {
                $mobile_validation_err = 'Mobile number should be 11 digit';
            }
        } // if first two digit is +880
        else if ($first_four_digit == '+8801') {
            if (strlen($mobile_no) != 14) {
                $mobile_validation_err = 'Mobile number should be 14 digit';
            }
        } // if first digit is only
        else if ($first_digit == '+') {
            // Mobile number will be ok
        } else {
            $mobile_validation_err = 'Please enter valid Mobile number';
        }

        if (strlen($mobile_validation_err) > 0) {
            return $mobile_validation_err;
        } else {
            return 'ok';
        }
    }


    public static function getUserType() {
        if (Session::has('member_type')) {
            return Session::get('member_type');
        } else {
            // return 1;
            dd('Invalid User Type Id');
        }
    }

    /**
     * Get Name of frontend.
     * @return boolean
     */
    public static function getFieldName($id, $field, $search, $table) {

        if ($id == NULL || $id == '') {
            return '';
        } else {
            return DB::table($table)->where($field, $id)->pluck($search);
        }
    }

    public static function getUserTypeName() {
        if (Session::has('member_type')) {
            $member_type_id = Session::get('member_type');
            $member_type_name = DB::table('member_types')
                    ->where('mt_id', $member_type_id)
                    ->pluck('mt_type_name');
            return $member_type_name;
        } else {
            dd('Invalid Login Id');
        }
    }

    public static function getLabId() {
        if (Session::has('lab_id')) {
            return Session::get('lab_id');
        } else {
            return 0;
        }
    }



    public static function getUserLabName() {
        if (Session::has('lab_id') && Session::get('lab_id') != 0) {
            $member_lab_id = Session::get('lab_id');
            $member_lab_name = DB::table('tbl_lab')
                    ->where('lab_id', $member_lab_id)
                    ->pluck('lab_name');
            return "<br/>(".$member_lab_name.")";
        } else {
            return "";
        }
    }    
    
    public static function getUserEmail() {
        if (Session::has('member_email')) {
            return Session::get('member_email');
        } else {
            return 0;
        }
    }
    
    public static function getCompanyType() {
        if (Session::has('member_id')) {
            $user_id = CommonFunction::getUserId();
            $company_type = DB::table('companies')->where('updated_by', $user_id)->pluck('company_type');
            return $company_type;
        } else {
            return 0;
        }
    }

    public static function updateScriptPara($sql, $data) {
        $start = strpos($sql, '{$');
        while ($start > 0) {
            $end = strpos($sql, '}', $start);
            if ($end > 0) {
                $filed = substr($sql, $start + 2, $end - $start - 2);
                $sql = substr($sql, 0, $start) . $data[$filed] . substr($sql, $end + 1);
            }
            $start = strpos($sql, '{$');
        }
        return $sql;
    }
//    send sms or email
    public static function sendMessageFromSystem($param=array()) {

        $mobileNo = $param[0]['mobileNo'] ==''? '0' : $param[0]['mobileNo'];
        $smsYes = $param[0]['smsYes'] ==''? '0' : $param[0]['smsYes'];
        $smsBody = $param[0]['smsBody'] ==''? '' :$param[0]['smsBody'] ;
        $emailYes = $param[0]['emailYes'] ==''? '1' :$param[0]['emailYes'] ;
        $emailBody = $param[0]['emailBody'] ==''? '' :$param[0]['emailBody'] ;
        $emailHeader = $param[0]['emailHeader'] ==''? '0' :$param[0]['emailHeader'] ;
        $emailAdd= $param[0]['emailAdd']==''? 'base@gmail.com' :$param[0]['emailAdd'];
        $template= $param[0]['emailTemplate']==''? '' :$param[0]['emailTemplate'];
        $emailSubject= $param[0]['emailSubject']==''? '' :$param[0]['emailSubject'];

        if ($emailYes == 1) {
            $email = $emailAdd;
            $data = array(
                'header' => $emailHeader,
                'param' => $emailBody
            );
            \Mail::send($template, $data, function($message) use ($email,$emailSubject) {
                $message->from('base@gmail.com', 'BCSIR');
                $message->to($email);
                $message->cc('shahin@batworld.com');
                $message->subject($emailSubject);
            });
        }

//        $smsYes = 1;
        if ($smsYes == 1) {
            $sms = $smsBody;
            $sms = str_replace(" ", "+", $sms);
            //        $sms = str_replace("<br>", "%0a", $sms);
            $mobileNo = str_replace("+88", "", "$mobileNo");
                $url = "http://202.4.119.45:777/syn_sms_gw/index.php?txtMessage=$sms&msisdn=$mobileNo&usrname=bus_auto_user&password=bus_auto_user@sms";
//            echo $url;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_exec($curl);
            curl_close($curl);

        }
        return true;
    }

    /*     * ****************************End of Class***************************** */
}
