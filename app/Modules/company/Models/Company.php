<?php

namespace App\Modules\Company\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Company extends Model {

    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'company_id',
        'company_name',
        'company_type',
        'company_house_no',
        'company_flat_no',
        'company_street',
        'company_area',
        'company_city',
        'company_zip',
        'company_fax',
        'company_web',
        'company_tin',
        'head_of_org',
        'head_contact',
        'head_email',
        'contact_person',
        'contact_phone',
        'contact_email',
        'company_status',
        'phone_no',
        'fax_no',
        'incorporation_certificate',
        'partnership_deed',
        'com_trade_license',
        'updated_by',
        
    ];

    /*     * ***************************************End of Model Class****************************************************** */
}
