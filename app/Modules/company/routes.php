<?php

Route::group(array('module' => 'Company', 'namespace' => 'App\Modules\Company\Controllers'), function() {

    /* Routes For General Company */
    Route::get('company', 'CompanyController@index')->middleware(['ocpl.checkvalid']);
    Route::get('company/index', 'CompanyController@index')->middleware(['ocpl.checkvalid']);
    Route::get('company/create/{encrypted_company_info}', 'CompanyController@create')->middleware(['ocpl.checkvalid']);
    
    Route::patch('company/store', 'CompanyController@store')->middleware(['ocpl.checkvalid']);
    Route::patch('/company/update/{id}', "CompanyController@update")->middleware(['ocpl.checkvalid']);

    Route::get('/company/edit/{id}', "CompanyController@edit")->middleware(['ocpl.checkvalid']);
    Route::get('/company/view/{id}', "CompanyController@view")->middleware(['ocpl.checkvalid']);

//    Route::get('company/{any?}', 'CompanyController@index');

    /* Routes For Documents uploaded for a Company */
    Route::get('company/create_doc', 'CompanyController@create_doc')->middleware(['ocpl.checkvalid']);

    /* Routes For Associating a User to a Company */
    Route::get('company/company_associate', 'CompanyController@company_associate')->middleware(['ocpl.checkvalid']);
    Route::patch('company/associate_company', 'CompanyController@associate_company')->middleware(['ocpl.checkvalid']);
    Route::post('company/get-company-suggestion', 'CompanyController@getCompanySuggestion')->middleware(['ocpl.checkvalid']);
    Route::patch('company/get-company-name', 'CompanyController@getCompanyName')->middleware(['ocpl.checkvalid']);

    Route::get('company/associated-company-list', 'CompanyController@associatedCompanyList')->middleware(['ocpl.checkvalid']);
    /*     * *************************End of Routing Group****************** */
});
