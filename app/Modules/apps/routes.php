<?php

Route::group(array('module' => 'Apps', 'namespace' => 'App\Modules\Apps\Controllers'), function() {

    //Bank Payment section routing starts, prepared by Tonoy Bhattacharjee Date 27-10-2015
    Route::get('apps/bank-payment', "AppsController@bankPayment")->middleware(['ocpl.checkvalid']);
    Route::get('apps/bank-payment-process/{tracking_no}', "AppsController@bankPaymentProcess")->middleware(['ocpl.checkvalid']);
    Route::post('apps/tracking-no-search', 'AppsController@trackingNoSearch')->middleware(['ocpl.checkvalid']);
    Route::post('apps/money-save', 'AppsController@moneySave')->middleware(['ocpl.checkvalid']);
    // End Bank Payment routing section

    Route::get('apps/view/{id}', 'AppsController@appsView')->middleware(['ocpl.checkvalid']);                    //routing for application view* 28-10-15 prepared by Tonoy Bhattacharjee
    Route::get('apps/edit/{id}', 'AppsController@edit')->middleware(['ocpl.checkvalid']);
    Route::patch('apps/update/{id}', 'AppsController@update');
    Route::patch('apps/update-desk/{id}', 'AppsController@updateDesk')->middleware(['ocpl.checkvalid']);
    Route::get('apps/printview/{id}', 'AppsController@printview')->middleware(['ocpl.checkvalid']);
    Route::get('apps/printApps/{id}', 'AppsController@printApps')->middleware(['ocpl.checkvalid']);
    Route::post('apps/parameter-method', 'AppsController@updateStatus')->middleware(['ocpl.checkvalid']);


    Route::post('apps/ajax/{param}', 'AppsController@ajaxRequest')->middleware(['ocpl.checkvalid']);
    Route::post('apps/store', 'AppsController@store')->middleware(['ocpl.checkvalid']);


    Route::resource('apps', 'AppsController');
    Route::get('apps/show/{id}', 'AppsController@show')->middleware(['ocpl.checkvalid']);
    Route::get('apps/print-show/{id}', 'AppsController@printShow')->middleware(['ocpl.checkvalid']);
    Route::get('apps/app-result-download/{id}', 'AppsController@appResultDownload')->middleware(['ocpl.checkvalid']);
    Route::patch('apps/update-result/{id}', 'AppsController@updateResult')->middleware(['ocpl.checkvalid']);
//    Route::get('apps/print-invoice/{id}', 'AppsController@printInvoiceView')->middleware(['ocpl.checkvalid']);
    Route::get('apps/get-invoice/{id}', 'AppsController@getInvoice')->middleware(['ocpl.checkvalid']);

    Route::get('apps/print-invoice/{id}', 'AppsController@printInvoice')->middleware(['ocpl.checkvalid']);
    Route::get('apps/print-invoice-view/{id}', 'AppsController@printInvoiceView')->middleware(['ocpl.checkvalid']);
    Route::get('apps/get-invoice/{id}', 'AppsController@getInvoice')->middleware(['ocpl.checkvalid']);
    Route::get('apps/get-invoice-details/{id}', 'AppsController@getInvoiceDetails')->middleware(['ocpl.checkvalid']);

    Route::get('apps/get-job-listby-date/{dates}', 'AppsController@getJobListbyDate')->middleware(['ocpl.checkvalid']);

});	