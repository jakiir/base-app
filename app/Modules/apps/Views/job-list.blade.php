@extends('master-admin-default')

@section('title')
    <title>Job List</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-list"></i> Applications Job List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Application</a></li>
            <li class="active">list</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <table id="_list" class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>Application Tracking Number</th>
                        <th>lab Name</th>
                        <th>Job Name</th>
                        <th>Duration</th>
                        <th>Delivery Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td>{!! $row->tracking_number !!}</td>
                            <td>{!! $row->lab_name !!}</td>
                            <td>{!! $row->job_name !!}</td>
                            <td>{!! $row->duration !!}</td>
                            <td>{!! $row->deliverydate !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.content -->

@endsection

@section('footer-script')
    <script>

        $(function () {
            $('#_list').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true
            });
        });

    </script>
@endsection
