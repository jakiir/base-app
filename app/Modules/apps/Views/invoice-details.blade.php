@extends('master-admin-default')
@section('title')
<title>Analysis Report</title>
@endsection
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="callout callout-info lead">
        <h4>Result in Details</h4>
        <p>{{ date('d F Y') }}</p>
    </div>
</section>

<!-- Main content -->
@foreach(@$app_data['app_header'] as $key=>$job)
    <section class="invoice">
        <div class="col-xs-12 text-center">
             <div class="col-xs-1">
                
          </div><!-- /.col -->
            <div class="col-xs-10">
            <p class="lead analytics_heading">{!! Html::image("back-end/dist/img/logo.jpg", "Logo",['width'=>80]) !!}</p>
       </div>
            <div class="col-xs-1">
                <!--{!! link_to('apps/get-invoice/'.Request::segment(3),'Print',['class' => 'btn btn-primary btn-sm pull-right', 'target'=>'_blank']) !!}-->
            </div> 
        </div><!-- /.col -->
        <div class="col-xs-12 text-center">
            <span class="print_heading">
                বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )<br/>
            </span>
            <span class="page-header print_heading">
                    BANGLADESH COUNCIL OF SCIENTIFIC AND INDUSTRIAL
                    RESEARCH (BCSIR) <br/><br/>
            </span>
        </div>
          <div class="col-xs-12 text-center">
            <p class="lead analytics_heading">TEST REPORT / ANALYSIS REPORT</p>
          </div><!-- /.col -->
          
          <div class="row">
              <div class="col-xs-1"></div>
              <div class="col-xs-10">
                  <div>
                      <table class="table table-striped table-condensed table_report">
                          <tr>
                              <th style="width:50%">Ref. No. of the Analytical Cell:</th>
                              <td>{{ $job->tracking_number }}</td>
                          </tr>
                          <tr>
                              <th>Ref. No. of the Unit:</th>
                              <td>{{ $app_data['job_details'][$job->job_id][0]->lab_name }}</td>
                          </tr>
                          <tr>
                              <th>Customer’s details:</th>
                              <td>{!!
                              ucwords($job->submitted_by).', '.
                              ucwords($job->company_name).', '.
                              'Hourse#'.$job->company_house_no.$job->company_flat_no.', '.
                              $job->company_street.$job->company_city.'-'.$job->company_zip
                               !!}</td>
                          </tr>
                          <tr>
                              <th>Particulars of supplied Sample:</th>
                              <td>{{ $job->job_name }}</td>
                          </tr>
                          <tr>
                              <th>Sample Received Date:</th>
                              <td>{{date('d-m-Y')}}</td>
                          </tr>
                          <tr>
                              <th>Visual observation/Remarks:</th>
                              <td>{{ $job->remark }}</td>
                          </tr>
                      </table>
                  </div>
              </div><!-- /.col -->
          </div>

        <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">
            <table class="table table-bordered table-striped  table-condensed table_report">
                <thead class="text-center">
                    <tr>
                        <th>Particulars of supplied Sample</th>
                        <th>Parameters</th>
                        <th>Test Method / Equipment Used</th>
                        <th>Results</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;

                ?>
                @foreach($app_data['job_details'][$job->job_id] as $sample_details)
                    <tr>
                    @if($sl==0)
                        <td rowspan="{{ count($app_data['job_details'][$job->job_id]) }}">{{ $job->job_name }}</td>
                        @endif
                        <td>{{ @$sample_details->param_name }}</td>
                        <td>{{ @$sample_details->method_name }}</td>
                        <td><?php
                           if($sample_details->status == "Verified")
                                 echo $sample_details->remarks;
                            if($sample_details->status != '') echo "(".$sample_details->status.")";
                                else echo "(Pending)";
                            
//                            if($sample_details->status == "Verified")
  //                           echo $sample_details->description;
                            
                            
                            ?></td>
                    </tr>
                    <?php $sl++ ;?>
                  @endforeach
                </tbody>
            </table>
            <p><strong>Comments: <?php echo $job->remarks ;?></strong></p><br/>
        </div><!-- /.col -->
        <div class="col-xs-1"></div>

    </div><!-- /.row -->

        <div class="row invoice-info">
          <div class="col-sm-1 invoice-col"></div><!-- /.col -->
          <div class="col-sm-3 invoice-col">
              <p class="text-center">-------------------------------</p>
              <p class="text-center">Analyst</p>
          </div><!-- /.col -->
          <div class="col-sm-3 invoice-col">
              <p class="text-center">-------------------------------</p>
              <p class="text-center">Supervisor</p>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
              <p class="text-center">-----------------------------------------------------------</p>
              <p class="text-center"> Counter of the Director / Officer Incharge</p>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <br />
        <div class="row invoice-info">
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
          <div class="col-sm-10 invoice-col">
            <p>
                Note:<br/>
            <ul>
                <li>The results reported here is based only on the supplied sample’s in this laboratory</li>
                <li>The report or any aprt of this should not be published without prior permission of the issuing authority.</li>
                <li>Any complain about test report will not be acceptable after one month from the date of issuing of the said report.</li>
                <li>This report is free from any legal litigation and  Unit name, BCSIR is not liable for any legal implication relating the report.</li>
            </ul>
            </p>
          </div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row invoice-info">
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
          <div class="col-sm-10">
              <div style="border-bottom:1px solid black;">&nbsp;</div>
              <div>
                  <div class="text-center"><strong>Laboratory Address</strong></div>
                  <div class="text-center">
                           Dr, Qudrat-I-Khuda Road, Dhanmondi, Dhaka-1205
                  </div>
              </div>
          </div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div><br />
        <div class="row invoice-info">
            <div class="col-sm-2  invoice-col"></div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div>


    </section>
@endforeach


@endsection

