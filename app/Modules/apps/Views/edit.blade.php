@extends('master-admin-default')

@section('title')
    <title>Application Edit</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Application Edit
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Application</a></li>
            <li class="active">App Edit</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        {!! Form::open(['url' => '/apps/update/'.$id, 'method'=>'patch', 'class' => 'form apps_from', 'id'=>'app_entry_form','role' => 'form']) !!}


        <!-- Default box -->
        <div class="box box-success">
            <div class="box-body">

                <div class="box-body">

                    <div class="form-group col-md-6 {{$errors->has('company_id') ? 'has-error' : ''}}">
                        {!! Form::label('company_id','Application for:',['class'=>'control-label']) !!}
                       {!! Form::select('company_id', $companyList, $data['company_id'], ['class' => 'required form-control company_id']) !!}
                         {!! $errors->first('company_id','<span class="help-block">:message</span>') !!}
                    </div>

                    <hr/>

                    <div class="col-md-12 company_details" style="display: none">

                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                        <p class="text-muted location">Kawranbazar, Dhaka</p>

                        <strong><i class="fa fa-file-text-o margin-r-5"></i> Contact </strong>
                        <p class="contact">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                    </div>


                </div><!-- /.box-body -->
            </div><!-- /.box-body -->
        </div>


        <div class="box box-danger">
            <div class="box-header">
                <h4 class="pull-left">Test Section</h4>
                {{--{!! Form::button('<i class="fa fa-plus"></i> Add', array('type' => 'button', 'class' => 'btn btn-primary pull-right new_section')) !!}--}}
            </div>


            <div class="box-body section_body">
                <?php $i=0; ?>
                @foreach($dataMaster as $dataM)
                    <div class="add_section_{!! $i !!}">
                        <?php echo $i>0?'<div class="col-md-12 span_border">'. Form::button('<i class="fa fa-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger pull-right','onclick'=>'hide_section('.$i .')')) .'</div>':'';?>
                        <div class="form-group col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                            {!! Form::label('job_name','Sample Description',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">

                                {{--
                                {!! Form::text('job_name['.$i.']',$dataM->job_name, ['class' => 'required form-control','required']) !!}
                                --}}
                                {!! Form::text('job_name['.$i.']',$dataM->job_name, ['class' => 'required form-control','required']) !!}

                                {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                            {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('tst_id['.$i.']', $testSample, $dataM->tst_id, ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                                {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('param_id','Test Parameter',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('param_id['.$i.']', [''=>'Select One'], '', ['class' => 'form-control param_id', 'multiple']) !!}
                                {!! Form::button('<i class="fa fa-refresh"></i> Load Method', array('type' => 'button','class' => 'load_method btn btn-xs btn-default pull-right','onclick'=>'load_method(this,0)')) !!}
                                {!! Form::button('<i class="fa fa-refresh"></i> iniLoad', array('type' => 'button','class' => 'load_method1 btn btn-xs btn-default pull-right','onclick'=>'load_method(this,1)','style'=>'display:none;')) !!}

                                {!! Form::hidden('job_id['.$i.']',$dataM->job_id) !!}


                                {!! Form::hidden('selected_param',$dataM->param_ids,['class'=>'selected_param']) !!}
                                {!! Form::hidden('selected_method',$dataM->method_ids,['class'=>'selected_method']) !!}

                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('method_id') ? 'has-error' : ''}}">
                            {!! Form::label('method_id','Test Method',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('method_id['.$i.'][]', [''=>'Please click on load method after selecting Sample and Parameter'], '', ['class' => 'form-control method_id', 'multiple']) !!}
                                {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div><?php $i++; ?>
                @endforeach
                <?php $j=$i;?>
                @for($i=$j;$i<5;$i++)
                    <div class="add_section_{!! $i !!}" <?php echo $i>0?'style="display: none;"':'style=""';?> >
                        <?php echo $i>0?'<div class="col-md-12 span_border">'. Form::button('<i class="fa fa-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger pull-right','onclick'=>'hide_section('.$i .')')) .'</div>':'';?>
                        <div class="form-group col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                            {!! Form::label('job_name','Sample Description',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::text('job_name['.$i.']','', ['class' => 'required form-control','required']) !!}
                                {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                            {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('tst_id['.$i.']', $testSample, '', ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                                {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('param_id','Test Parameter',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('param_id['.$i.']', [''=>'Select One'], '', ['class' => 'form-control param_id', 'multiple']) !!}
                                {!! Form::button('<i class="fa fa-refresh"></i> Load Method', array('type' => 'button','class' => 'load_method btn btn-xs btn-default pull-right','onclick'=>'load_method(this,0)')) !!}

                                {!! Form::hidden('selected_param','',['class'=>'selected_param']) !!}
                                {!! Form::hidden('selected_method','',['class'=>'selected_method']) !!}

                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('method_id') ? 'has-error' : ''}}">
                            {!! Form::label('method_id','Test Method',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('method_id['.$i.'][]', [''=>'Please click on load method after selecting Sample and Parameter'], '', ['class' => 'form-control method_id', 'multiple']) !!}
                                {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                @endfor
            </div><!-- /.box- -->
        </div>


        <div class="box">
            <div class="box-header">
                <h4>Submitted by: </h4>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-sm-8 {{$errors->has('method_id') ? 'has-error' : ''}}">
                        {!! Form::label('submitted_by','Submitted by:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('submitted_by',$userData->member_first_name, ['class' => 'required form-control']) !!}
                            {!! $errors->first('submitted_by','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-sm-8 {{$errors->has('method_id') ? 'has-error' : ''}}">
                        {!! Form::label('contact_no','Contact No:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('contact_no',$userData->member_phone, ['class' => 'required form-control']) !!}
                            {!! $errors->first('contact_no','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="box-footer">
                <div class="pull-left col-md-2">
                    <a href="/apps/">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                </div>
                <div class="col-md-6">

                {{--
                    {!! CommonFunction::showAuditLog($data[0]->updated_at, $data[0]->updated_by) !!}
                --}}
                {!! CommonFunction::showAuditLog($data['updated_at'], $data['updated_by']) !!}

                </div>
                <div class=" col-md-4">
                    <div class=" pull-right">
                        @if($data['app_status'] == 0)
                        {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success')) !!}

                        {!! Form::hidden('submitted','0',['class'=>'submitted']) !!}
                        {!! Form::button('<i class="fa fa-credit-card"></i> Submit Application', array('type' => 'button', 'value'=> 'send', 'class' => 'btn btn-primary send')) !!}
                        @else
                            {!! Form::button('<i class="fa fa-print"></i> Print', array('type' => 'button', 'value'=> 'print', 'class' => 'btn btn-default print')) !!}
                        @endif
                    </div>
                </div>
            </div><!-- /.box-footer -->
        </div>

        {!! Form::close() !!}
    </section><!-- /.content -->
@endsection

@section('footer-script')
    {{--<script src="{{ asset('front-end/js/jquery-1.11.1.min.js') }}"></script>--}}
    <script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>

    <script>

        var _token = $('input[name="_token"]').val();
        $(function(){
            for(var i=<?php echo $j;?>;i<5;i++){
                hide_section(i);
            }

            $(".send").click(function () {
                if(confirm('Are you sure to Submit this Application?')) {
                    $('.submitted').val(1);
                    $('#app_entry_form').submit();
                }
            });

            $(".apps_from").validate({
                errorPlacement: function () {
                    return false;
                }
            });

            $('.company_id').change(function (){

                var id = $(this).val();
                $.post('/apps/ajax/company', {id:id, _token:_token}, function(response){
                    if(response.responseCode == 1){
                        $('.location').html(response.data.location);
                        $('.contact').html(response.data.contact);
                        $('.company_details').show();
                    }
                });
            });
            $('.new_section').click(function () {
                var found =false;
                for(i=1;i<=30;i++){
                    if($('.add_section_'+i).attr('style') =='display: none;'){
                        $('.add_section_'+i).show();
                        $('.add_section_'+i+' input').prop('disabled',false);
                        $('.add_section_'+i+' select').prop('disabled',false);
                        found=true;
                        break;
                    }
                }
                if(!found) alert('You have added Maximum in one Application!');
//                $('html, body').animate({ scrollTop: $('.add_section_'+ section_count).offset().top }, 'slow');
            });

            $('.hide_section').click(function () {
                var section_id = $(this).attr('section-id');
                $('.add_section_'+section_id).hide();
            })
        });

        function changeTest(object){
            var obj = $(object).parent().parent().parent();
            var param= obj.find('.selected_param').val().split(',');
            var id = $(object).val();
            $.post('/apps/ajax/parameter', {id:id, _token:_token}, function(response){
                if(response.responseCode == 1){
                    var option = '';
                    $.each(response.data, function (id,value) {
                        var selected = param.indexOf(value)>-1?'selected':'';
                        option += '<option value="'+ value +'" '+ selected +' >'+ id + '</option>';
                    });
                    obj.find('.param_id').html(option);
                }
            });

        }

        function load_method(object,flag) {
            var obj = $(object).parent().parent().parent();
            obj.find('.fa-refresh').addClass('animate');
            var param = [];
            if (flag == 1){
                param = obj.find('.selected_param').val().split(',');
            }else {
                obj.find('.param_id :selected').each(function (i, selected) {
                    param[i] = $(selected).val();
                });

                method=[];
                obj.find('.method_id :selected').each(function(i, selected){
                    method[i] = $(selected).val();
                });
                $(object).parent().find('.selected_method').val(method.join());

            }
            $(object).parent().find('.selected_param').val(param.join());
            var method= obj.find('.selected_method').val().split(',');

            $.post('/apps/ajax/method', {id:param, _token:_token}, function(response){
                if(response.responseCode == 1){
                    var option ='';
                    $.each(response.data, function (id,value) {
                        var selected = method.indexOf(String(value.method_id))> -1 ?'selected':'';
                        option += '<option value="'+ value.method_id +'" '+ selected +' >'+ value.method_name + '</option>';
                    });
                    obj.find('.method_id').html(option);
                    obj.find('.fa-refresh').removeClass('animate');
                }
            });
        }

        function hide_section(i){
            $('.add_section_'+i).hide();

            $('.add_section_'+i+' input').prop('disabled',true);
            $('.add_section_'+i+' select').prop('disabled',true);

        }

        $(function () {
            //Triggers
            $('.company_id').trigger('change');
            $('.tst_id :selected').trigger('change');
            $('.load_method1').trigger('click');

        });
    </script>

@endsection

