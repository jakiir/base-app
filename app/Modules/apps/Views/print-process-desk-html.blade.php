<?php $i=1; ?>

<?php
$comments = '';
$description = '';
$result_file = '';
if(isset($testMothods[0]))
{
    $comments = $testMothods[0]->remarks;
    $description = $testMothods[0]->description;
    $result_file = $testMothods[0]->results_file;
    $iso_logo = $testMothods[0]->iso_logo;
}
?>

@foreach($app_data['app_header'] as $key=>$job)
    <section class="invoice" style="width:100%;height:1000px;">
        @if (!empty($job->iso_logo) && $job->iso_logo == 1)
            <div class="col-xs-3" style="margin-top:-150px;">
                <img src="iso-logo/1459590232_logo_NABL.jpg" width="55" alt="iso logo" style="text-align:left;float:left;" class="iso_logo">
            </div><!-- /.col -->
        @else
            @if (!empty($iso_logo) && $iso_logo == 1)
                <div class="col-xs-3" style="margin-top:-150px;">
                    <img src="iso-logo/1459590232_logo_NABL.jpg" width="55" alt="iso logo" style="text-align:left;float:left;" class="iso_logo">
                </div><!-- /.col -->
            @endif
        @endif
        <div class="col-xs-12">
            <p class="lead analytics_heading ts text-center" style="">Laboratories/Institute/Center: {{ $app_data['job_details'][$job->scientist_id][0]->lab_name }}</p>
        </div><!-- /.col -->
        <div class="body">
            <div class="col-xs-12 text-center">
                <p class="lead analytics_heading"><b style="text-decoration: underline;">ANALYSIS REPORT</b></p>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div>
                        <table class="table tableNoBorder">
                            <tr>
                                <td width="120">ASC Ref No </td>
                                <td width="1">:</td>
                                <td>{{ $job->tracking_number }}</td>
                                <td width="135">Unit (Lab/Inst.) Ref No</td>
                                <td width="1">:</td>
                                <td>{{ $job->lab_reference_no }}</td>
                            </tr>
                            <tr>
                                <td>Lab/Sample ID</td>
                                <td width="1">:</td>
                                <td>{{ $job->test_sample_id }}</td>
                                <td>Number of Sample </td>
                                <td width="1">:</td>
                                <td>{{ count($app_data['job_details']) }}</td>
                            </tr>
                            <tr>
                                <td>Sample Description</td>
                                <td width="1">:</td>
                                <td>{{ $job->job_name }}</td>
                                <?php if($job->lab_incharge_id > 0)  { ?>
                                @if($job->rcreate != '')
                                    <td> Test Commencement date </td>
                                    <td width="1">:</td>
                                    <td>{{ date('d/m/Y',strtotime($job->rcreate)) }}</td>
                                @endif
                                <?php } ?>
                            </tr>
                            <tr>
                                <td rowspan="2" valign="top">Client's Details</td>
                                <td width="1">:</td>
                                <td rowspan="2">{!! ucwords($job->submitted_by).'<br/><b>'.   ucwords($job->company_name).'</b><br/>'.
                            'House#'.$job->company_house_no.$job->company_flat_no.', '.  $job->company_street.', '.
                            $job->company_city.'-'.$job->company_zip !!}
                                </td>
                                <?php  //if($job->test_status == 8 )  { ?>
                                @if($job->rstatus == 'Verified')
                                    <td> Test Completion date  </td>
                                    <td width="1">:</td>
                                    <td>{{ date('d/m/Y',strtotime($job->rupdate)) }}</td>
                                @endif
                                <?php //} ?>
                            </tr>                            
                        </table>
                    </div>
                </div><!-- /.col -->
            </div>
            @if(1==6)
            <div class="form-group {{$errors->has('method_id') ? 'has-error' : ''}}">
                <?php $sl = 0;
                $testMothods = $app_data['job_details'][$job->scientist_id];
                ?>
                <table class="table" cellspacing="1" cellpadding="6">
                    <tr>
                        <th>SN#</th>
                        <th>Method/Parameter</th>
                        <th>Status</th>
                        <th>Lab-Incharge’ Remarks</th>
                    </tr>
                    @foreach($testMothods as $methodRow)
                        <div class="">
                            <tr>
                                <td>{!! ++$sl !!}</td>
                                <td>{!! $methodRow->param_name."/".$methodRow->method_name !!}</td>
                                <td>{!! $methodRow->status !!}</td>
                                <td>{!! $methodRow->remarks_li  !!}</td>
                            </tr>
                        </div>
                    @endforeach
                </table>
            </div>
            @endif


            <div class="form-group">
                @if(!empty($job->description))
                    <label>Details:</label>
                    <div class="detailsPdf">
                        {!! $job->description !!}
                    </div>
                @else
                    @if(!empty($description))
                        <label>Details:</label>
                        <div class="detailsPdf">
                            {!! $description !!}
                        </div>
                    @endif
                @endif
                @if(!empty($job->results_file) || !empty($result_file))
                    <div class="form-group">
                        <label>Attachment:</label>
                        {{--<object width="980" height="250" data="{{ url() }}/results-file/{{ $job->results_file }}"#toolbar=1&amp;navpanes=0&amp;scrollbar=1&amp;page=1&amp;view=FitH" type="application/pdf"></object>--}}
                    </div>
                @endif
                @if(!empty($job->remarks_li))
                    <div class="form-group">
                        <label>Comments:</label>
                        {!! $job->remarks_li !!}
                    </div>
                @else
                    @if(!empty($comments))
                        <div class="form-group">
                            <label>Comments:</label>
                            {!! $comments !!}
                        </div>
                    @endif
                @endif
            </div>
            <table style="width:100%;margin-top:30px;" class="tableNoBorder">
                <tr>
                    <td style="text-align: center;">
                        <p class="text-center">-----------------------------------</p>
                        <p class="text-center">Analyst</p>
                    </td>
                    <td>&nbsp;</td>
                    <td style="text-align: center;">
                        <p class="text-center">-----------------------------------</p>
                        <p class="text-center">Supervisor</p>
                    </td>
                    <td>&nbsp;</td>
                    <td style="text-align: center;">
                        <p class="text-center">-----------------------------------</p>
                        <p class="text-center"> Director/Officer In-Charge</p>
                    </td>
                </tr>
            </table>
        </div>
    </section>
    <?php
    $i++;
        if(count($app_data['app_header']) > $i){
    ?>
    <pagebreak />
    <?php } ?>
    @endforeach