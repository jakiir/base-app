@extends('master-admin-default')

@section('title')
    <title>Application Edit</title>
@endsection

@section('content')

    <!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box-solid">
        <div class="box-header with-border bg-teal-active">
            <h3 class="box-title"><strong><i class="fa fa-life-saver"></i> Application Edit (Tracking Number: <?php echo $data['tracking_number']; ?>)</strong></h3>
        </div>
        <ol class="breadcrumb">
            <h4><strong>বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )</strong></h4>
        </ol>
    </div>
</section>
    <!-- Main content -->
    <section class="content">

        @if(Session::has('error'))
            <div class="alert alert-warning">
                {{ Session::get('error') }}
            </div>
        @endif
         @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('success') }}
            </div>
        @endif

        {!! Form::open(array('url' => '/apps/update-desk/'.$id, 'method'=>'patch', 'class' => 'form apps_from', 'id'=>'app_entry_form','role' => 'form')) !!}

        <div class="box box-success">
            <div class="col-md-12 ">
                <?php
                $user_type = CommonFunction::getUserType();
                //dd($data["app_status"]);
                ?>
                @if( $app_status_check != 0 && $user_type == 3 && $data["app_status"] == 2)
                    {!! Form::button('<i class="fa "></i> Publish Result', array('type' => 'button', 'class' => 'changeStatus btn btn-primary pull-right')) !!}

                @endif
            </div>
            <div class="box-body">
                <div class="col-md-7 pull-right">
                    <a href="{{url('apps/printApps/'.Request::segment(3))}}" target="_blank">
                        {!! Form::button('<i class="fa fa-print"></i> Print Sample Token', array('type' => 'button', 'value'=> 'print', 'class' => 'btn btn-default no_hide pull-right')) !!}
                    </a>

                    {!! link_to('apps/print-invoice/'. Encryption::encodeId($data->app_id), 'Invoice / Money Receipt ',
                    ['class' => 'btn btn-info btn-mid  pull-right']) !!}
                    <?php if (in_array($user_type, array(3,6,8,10)) || $data['app_status'] == 3) { ?>
                    {!! link_to('apps/printview/'. Encryption::encodeId($data->app_id),'Result',
                    ['class' => 'btn btn-primary btn-mid pull-right']) !!}
<!--                    {!! link_to('apps/get-invoice-details/'. Encryption::encodeId($data->app_id),'Result',
                    ['class' => 'btn btn-primary btn-mid pull-left']) !!}-->
                    
                    <?php } ?>
                </div>

                <div class="box-body">
               <?php // echo $data['company_id']; ?>
                     <div class="form-group col-md-6 {{$errors->has('company_id') ? 'has-error' : ''}}">
                        {!! Form::label('company_id','Application for:',['class'=>'control-label']) !!}
                        {!! Form::select('company_id', $companyList, $data['company_id'], ['class' => 'required form-control company_id']) !!}
                        {!! $errors->first('company_id','<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="col-md-12 ">
                        <?php
                        $user_type = CommonFunction::getUserType();
                        ?>
                        @if( $app_status_check == 0 && $user_type == 3 && $data["app_status"] == 2)
                            {!! Form::button('<i class="fa "></i> Publish Result', array('type' => 'button', 'class' => 'changeStatus btn btn-primary pull-right no_hide')) !!}

                        @endif
                    </div>

                    <div class="col-md-2"></div>
                    <hr/>

                    <div class="col-md-12 company_details">

                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                        <p class="text-muted location">{!! $data->location !!}</p>

                        <strong><i class="fa fa-file-text-o margin-r-5"></i>Contact</strong>
                        <p class="contact">{!! $data->contact !!}</p>
                    </div>

                
                </div><!-- /.box-body -->
            </div><!-- /.box-body -->
        </div>


        <div class="box box-danger ">
            <div class="box-header">
                <h4 class="pull-left">Test Section</h4>
                {!! Form::button('<i class="fa fa-plus"></i> Add', array('type' => 'button', 'class' => 'btn btn-primary pull-right new_section')) !!}
            </div>


            <div class="box-body section_body">
                <?php
                $i = 0;
                //dd($dataMaster->all());
                $loopNum = 0;
                $labInchargeVerified = 1;
                ?>
                
                @foreach($dataMaster as $dataM)
                    <?php
                    $labInchargeVerified = 1;
                    $loopNum++;
                    ?>

                    <div class="add_section_{!! $i !!}">
                        <?php echo $i > 0 ? '<div class="col-md-12 span_border">' . Form::button('<i class="fa fa-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger pull-right', 'onclick' => 'hide_section(' . $i . ')')) . '</div>' : ''; ?>
                        <div class="form-group col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                            {!! Form::label('job_name','Caption',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-5">
                                {!! Form::text('job_name['.$i.']',$dataM->job_name, ['class' => 'required form-control','required']) !!}
                                {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                            {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-5">
                                {!! Form::select('tst_id['.$i.']', $testSample, $dataM->tst_id, ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                                {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        <div class="col-md-12">
                            <!-- <div class="col-md-1"></div> -->

                            <div class="col-md-12 method_section">
                                <?php $m = 1; ?>
                                <div class="form-group">
                                    {!! Form::label('method_id','Test Methods',['class'=>' control-label']) !!}
                                    <?php $method_ids_exp = explode(',', $dataM->method_ids); ?>
                                    <?php 
                                    $remarks2 = explode('@@', $dataM->remarks2); 
                                    ?>

                                    <?php $scientist2 = explode('@@', $dataM->scientist2); ?>
                                    <?php $MethodTestStatus2 = explode('@@', $dataM->MethodTestStatus2); ?>
                                    
                                    <?php $prices2 = explode('@@', $dataM->prices2); ?>
                                    <?php $lab_ids2 = explode('@@', $dataM->lab_ids2); ?>
                                    <?php $xx = 0; ?>
                                    <?php $user_type = CommonFunction::getUserType(); ?>

                                    <div class="col-md-12">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-3">
                                            <label for="">Parameter => Method</label>
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="">Remarks</label>
                                        </div>
                                        <div class="col-sm-2">
                                            @if($user_type==6)
                                                <label for="">Scientist</label>
                                            @endif
                                        </div>

                                        <div class="col-sm-2">
                                            @if($user_type==6)
                                                <label for="">Status</label>
                                            @endif
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Price</label>
                                            <label for="">(Duration)</label>

                                        </div>
                                    </div>

                                    <?php $remTmp = -1;?>
                                    @foreach($method_ids_exp as $key=>$methodRow)
                                        <?php $remTmp++;?>
                                        <?php // print_r($method_ids_exp); ?>
                                        <?php // print_r($sampleLists);  ?>

                                        <div class="col-md-12 add_method_{!! $m++ !!} method_row">
                                            <div class="col-sm-1">
                                                <button type="button" class="btn btn-default method_hide" onclick="hide_method(this)"><i class="fa fa-remove"></i></button>
                                            </div>
                                            <div class="col-sm-3">
                                                {!! Form::hidden('method_id_selected', $methodRow, ['class'=>'method_id_selected m_'.$key]) !!}
                                                {!! Form::select('method_id['.$i.'][]', $sampleLists, $methodRow, ['class' => 'form-control method_id']) !!}
                                                {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-sm-1">
                                                {!! Form::text('remarks[]',$remarks2[$xx], ['class' => 'no_hide form-control remarks_','placeholder'=>'Remarks']) !!}
<!--                                                {!! Form::text('remarkss',$remarks2[$xx], ['class' => 'no_hide form-control remarks_','placeholder'=>'Remarks']) !!}-->
                                            </div>
                                            <div class="col-sm-2 ">
                                                @if($user_type==6)

                                                    {!! Form::select('scientist_id['.$i.']',$scientist,$scientist2[$xx],array('class' => 'required form-control no_hide','id'=>'scientist_'.$remTmp)) !!}
                                                @endif
                                            </div>
                                            <div class="col-sm-2 ">
                                                @if($user_type==6)
                                                <?php $MethodTestStatusTmp = array();?>
                                                @if(empty($MethodTestStatus2[$xx]))
                                                    <?php $MethodTestStatus2[$xx] = 'Pending'; ?>
                                                @endif
                                                <?php
                                                $tmpScientistStatus = $MethodTestStatus2[$xx];
                                                $MethodTestStatusTmp[$tmpScientistStatus] = $tmpScientistStatus;
                                                if($tmpScientistStatus != "Pending")
                                                    $MethodTestStatusTmp["Recheck"] = "Recheck";
                                                
                                                if($tmpScientistStatus == "Pending" || $tmpScientistStatus == "Recheck" )
                                                    $labInchargeVerified = 0;
                                                
                                                ?>
                                                    {!! Form::select('method_test_status[]',$MethodTestStatusTmp, $MethodTestStatus2[$xx],array('class' => 'form-control no_hide', 'onchange' => "setStatusForLabIncharge(this.value, $loopNum)")) !!}
                                                @endif
                                            </div>
                                            <div class="col-sm-2 price_duration">
                                                <?php // echo $prices2[$xx]; ?>
                                            </div>
                                        </div>
                                        <?php $xx++; ?>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            {!! Form::button('<i class="fa fa-plus"></i> Add Method', array('type' => 'button', 'class' => 'btn btn-sm btn-default pull-right new_method')) !!}
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                            {!! Form::label('lab_id','Assign Lab',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::hidden('lab_id2', @$lab_ids2[0], ['class'=>'lab_id2']) !!}
                                {!! Form::select('lab_id['.$i.']', [], $dataM->lab_id, ['class' => 'required form-control lab_id assigned_lab_list','id'=>'assigned_lab_'.$i,'required'=>1]) !!}

                                {!! $errors->first('lab_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5">
                                {!! Form::button('<i class="fa fa-refresh"></i> Load Lab', array('type' => 'button','class' => 'load_lab load_lab11  btn btn-xs btn-default  ','onclick'=>'load_lab(this,0)')) !!}

                            </div>
                        </div>
                        <div class="lab_price_dur">

                        </div>
                        @if($user_type == 6)
                        <div class="form-group col-sm-12" style="display: none">
                                <div class="  col-sm-3 ">
                                    {!! Form::label('comment_id','Comments',['class'=>' control-label']) !!}
                                </div>
                                <div class="col-sm-5 " style="margin-top: 5px;">
                                    {!! Form::textarea('comment[]', $dataM->remarks, ['class' => 'form-control comment_id no_hide', 'size' => '30x3']) !!}
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="  col-sm-3 ">
                                    {!! Form::label('test_status_id','Select Test Status',['class'=>'control-label']) !!}
                                </div>
                                <div class="  col-sm-4 ">
                                    <?php //dd($testStatus);  
                                    if($labInchargeVerified == 0)
                                        unset($testStatus[6]);
                                    ?>
                                    {!! Form::select('test_status_id[]',$testStatus,$dataM->test_status,array('class' => 'labInchargeStatus form-control no_hide')) !!}
                                    <label class="bg-warning" style="display:none">Keep the status pending before the scientist's test</label>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="  col-sm-3 ">
                                    {!! Form::label('test_sample_id','Sample Id',['class'=>'control-label']) !!}
                                </div>
                                <div class="  col-sm-4 ">
                                    {!! Form::text('test_sample_id[]',$dataM->test_sample_id,array('class' => 'form-control no_hide required')) !!}
                                    <label class="bg-warning" style="display:none">Keep the status pending before the scientist's test</label>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        @endif
                        <br/>
                    </div><?php $i++; ?>
                @endforeach



                <?php $j = $i; ?>
                @for($i=$j;$i<5;$i++)
                    <div class="add_section_{!! $i !!} " <?php echo $i > 0 ? 'style="display: none;"' : 'style=""'; ?> >
                        <?php echo $i > 0 ? '<div class="col-md-12 span_border">' . Form::button('<i class="fa fa-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger pull-right', 'onclick' => 'hide_section(' . $i . ')')) . '</div>' : ''; ?>

                        <div class="form-group col-sm-12 {{$errors->has('job_name') ? 'has-error' : ''}}">
                            {!! Form::label('job_name','Caption',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::text('job_name['.$i.']',$dataM->job_name, ['class' => 'required form-control','required']) !!}
                                {!! $errors->first('job_name','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('tst_id') ? 'has-error' : ''}}">
                            {!! Form::label('tst_id','Test Sample Name',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('tst_id['.$i.']', $testSample, $dataM->tst_id, ['class' => 'required form-control tst_id','onchange'=>'changeTest(this)','required'=>1]) !!}
                                {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        <div class="col-md-12">
                            <!-- <div class="col-md-1"></div> -->

                            <div class="col-md-12 method_section">
                                <?php $m = 1; ?>
                                <div class="form-group {{$errors->has('method_id') ? 'has-error' : ''}}">
                                    {!! Form::label('method_id','Test Methods',['class'=>' control-label']) !!}
                                    <?php $method_ids_exp = explode(',', $dataM->method_ids); ?>
                                    @foreach($method_ids_exp as $methodRow)
                                        @if(1==1)
                                            <div class="col-md-12 add_method_{!! $m++ !!} method_row">
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn btn-default method_hide" onclick="hide_method(this)"><i class="fa fa-remove"></i></button>
                                                </div>
                                                <div class="col-sm-5">

                                                    {!! Form::select('method_id['.$i.'][]', $sampleLists, $methodRow, ['class' => 'form-control method_id']) !!}
                                                    {!! $errors->first('method_id','<span class="help-block">:message</span>') !!}
                                                </div>
                                                <div class="col-sm-2">
                                                    {!! Form::text('remarks['.$i.'][]','', ['class' => 'form-control remarks_','placeholder'=>'Remarks']) !!}
                                                </div>
                                                <div class="col-sm-3 price_duration">

                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            {!! Form::button('<i class="fa fa-plus"></i> Add Method', array('type' => 'button', 'class' => 'btn btn-sm btn-default pull-right new_method')) !!}
                        </div>

                        <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                            {!! Form::label('lab_id','Assign Lab',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-4">
                                {!! Form::select('lab_id['.$i.']', [], $dataM->lab_id, ['class' => 'required form-control lab_id assigned_lab_list','id'=>'assigned_lab_'.$i,'required'=>1]) !!}

                                {!! $errors->first('lab_id','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-sm-5">
                                {!! Form::button('<i class="fa fa-refresh"></i> Load Lab', array('type' => 'button','class' => 'load_lab btn btn-xs btn-default','onclick'=>'load_lab(this,0)')) !!}
                            </div>
                        </div>
                        <div class="lab_price_dur">

                        </div>
                        <br/>
                    </div>
                @endfor
            </div><!-- /.box- -->
        </div>
        @if($user_type != 6)
            <div class="box">
                <div class="box-body">
                    <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                        {!! Form::label('total_price','Net Price',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::hidden('total_price2', $data->total_price, ['class' => 'form-control', 'id'=>'total_price2']) !!}
                            <span class="total_price">{{$data->total_price}}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 {{$errors->has('discount_price') ? 'has-error' : ''}}">
                        {!! Form::label('discount_price','Percentage of Discount (%)',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::text('discount_price', $data->discount_price, ['class' => 'digits text-right form-control','id'=>'discount_price','range'=>'0,100','placeholder'=>'', 'onClick' => 'this.select()']) !!} 
                            {!! $errors->first('discount_price','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group col-sm-12 {{$errors->has('total_net_price') ? 'has-error' : ''}}">
                        {!! Form::label('total_discount_price','Total Price After Discount',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::hidden('total_discount_price', $data->total_discount_price, ['class' => 'form-control total_discount_price2']) !!}
                            <span class="total_discount_price">{{$data->total_discount_price}} BDT</span>
                        </div>
                    </div>

                    
                    <div class="form-group col-sm-12 {{$errors->has('total_vat') ? 'has-error' : ''}}">
                        {!! Form::label('total_vat','VAT (15% of Total Price)',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::hidden('total_vat', $data->total_vat, ['class' => 'form-control total_vat']) !!}
                            <span class="total_vat2">{{$data->total_vat}} BDT</span>
                        </div>
                    </div>
                    
                    
                    <div class="form-group col-sm-12 {{$errors->has('g_total_price') ? 'has-error' : ''}}">
                        {!! Form::label('g_total_price','Total Payable',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::hidden('g_total_price', $data->total_vat + $data->total_discount_price, ['class' => 'form-control g_total_price']) !!}
                            <span class="g_total_price2">{{($data->total_vat + $data->total_discount_price)}} BDT</span>
                        </div>
                    </div>
                    

                    <div class="form-group col-sm-12 {{$errors->has('lab_id') ? 'has-error' : ''}}">
                        {!! Form::label('payable','First Time Minimum Payable',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-4">
                            {!! Form::hidden('total_price', $data->total_price, ['class' => 'form-control']) !!}

                            {!! Form::text('payable', $data->payable, ['class' => 'required digits payable2 text-right form-control payable','required'=>1,'placeholder'=>'Taka', 'onClick' => 'this.select()']) !!}

                            {!! $errors->first('payable','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div><!-- /.box-footer -->
            </div>

        @endif

        <div class="box">
            <div class="box-body">
                {!! Form::label('submitted_by','Submitted by:',['class'=>'control-label']) !!}
                {{$data->submitted_by}}
                <br/>
                {!! Form::label('contact_no','Contact No:',['class'=>'control-label']) !!}
                {{$data->submitted_contact}}
            </div>
            <div class="box-footer">
                <div class="pull-left col-md-2">
                    <a href="{{ url('/apps') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                </div>
                <div class="col-md-6">
                    {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                </div>
                <div class=" col-md-4">
                    <div class=" pull-right">
                        <?php if (in_array($data->app_status, array(1,7))) { ?>
                        {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success no_hide update-app')) !!}
                        {!! Form::hidden('submitted','1',['class'=>'submitted']) !!}
                            <?php
                                $bankHide = "display:none";
                                $sampleHide = "display:none";
                            if ($data->payable > 0) {
                                $bankHide = "";
                                $sampleHide = "display:none";
                            }elseif ($data->payable == 0) {
                                $bankHide = "display:none";
                                $sampleHide = "";
                            }
                            ?>
                        {!! Form::hidden('send_to_bank','',['class'=>'send_to_bank']) !!}
                        {!! Form::button('<i class="fa fa-credit-card"></i> Send to Bank ', array('type' => 'button', 'value'=> 'send', 'class' => 'no_hide btn btn-primary send-bank', 'style' => $bankHide)) !!}

                        {!! Form::button('<i class="fa fa-clock-o"></i> Waiting for sample ', array('type' => 'button', 'value'=> 'send', 'class' => 'no_hide btn btn-primary waiting-sample', 'style' => $sampleHide)) !!}
                        <?php
                        } elseif ($data->app_status == 6) {
                        ?>
                        {!! Form::hidden('submitted',$data["app_status"],['class'=>'submitted']) !!}
                        {!! Form::button('<i class="fa fa-credit-card"></i> Send to Lab ', array('type' => 'button', 'value'=> 'send', 'class' => 'no_hide btn btn-primary send')) !!}
                        <?php
                        } elseif ($data->app_status == 2 && $user_type == 6) {
                        ?>
                        {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success no_hide')) !!}
                        <?php }
                        ?>
                        <!--{!! Form::button('<i class="fa fa-print"></i> Print', array('type' => 'button', 'value'=> 'print', 'class' => 'btn btn-default print')) !!} -->

                    </div>
                </div>
            </div><!-- /.box-footer -->
        </div>

        {!! Form::close() !!}
    </section><!-- /.content -->
@endsection

@section('footer-script')
    <script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>

    <script>
        $( ".payable2" ).keyup(function() {
            var values = $(".payable2").val();
            

            if(values > 0){
                $(".send-bank").show();
                $(".waiting-sample").hide();
            } else {
                $(".waiting-sample").show();
                $(".send-bank").hide();
            }
        });

        $(".update-app").click(function() {
            $('.submitted').val(7);
            $('#app_entry_form').submit();
        });

        $(".waiting-sample").click(function() {
            $('.submitted').val(6);
            $('#app_entry_form').submit();
        });

        $(".send-bank").click(function() {
            $('.submitted').val(5);
            $('.send_to_bank').val('send_to_bank');
            $('#app_entry_form').submit();
        });
    </script>

    <script type="text/javascript">
        var _token = $('input[name="_token"]').val();
        $(function () {
            for (var i =<?php echo $j; ?>; i < 5; i++) {
                hide_section(i);
            }

            $(".send").click(function () {
                $('.submitted').val(2);
                $('#app_entry_form').submit();
            });

            $(".apps_from").validate({
                errorPlacement: function () {
                    return false;
                }
            });
            $('.company_id').change(function () {

                var id = $(this).val();
                $.post('/apps/ajax/company', {id: id, _token: _token}, function (response) {
                    if (response.responseCode == 1) {
                        $('.location').html(response.data.location);
                        $('.contact').html(response.data.contact);
                        $('.company_details').show();
                    }
                });
            });
//var method
            $('.new_method').click(function () {
                var obj = $(this).parent().parent();
                var new_method;
                new_method = '<div class="col-md-12 add_method_k method_row">';
                new_method += obj.find('.add_method_1').html();
                new_method += '</div>';
                obj.find('.method_section .form-group').append(new_method);
                obj.find('.method_section .form-group .remarks_').last().val('');
                obj.find('.method_section .form-group .price_duration').last().html('');
//                alert(obj.find('.method_section .form-group .remarks_').last().val(''));
                //alert(obj.find('.method_section .form-group').children('.remarks_').last().val());
                $(this).parent().parent().find('.lab_id').html('<option value="">Select Lab</option>');
            });
            $('.new_section').click(function () {
                var found = false;
                for (i = 1; i <= 4; i++) {
                    if ($('.add_section_' + i).attr('style') == 'display: none;') {
                        var obj = $('.add_section_' + i);
//                        alert('.add_section_' + i);
                        obj.find('input[name="job_name[' + i + ']"]').val('');
                        obj.find('.method_id').html('<option value="">Select Sample</option>');
                        obj.find('.tst_id ').val('');
//                        alert("done empty");
                        $('.add_section_' + i).show();
                        $('.add_section_' + i + ' input').prop('disabled', false);
                        $('.add_section_' + i + ' select').prop('disabled', false);
                        found = true;
                        break;
                    }
                }
                if (!found)
                    alert('You have added Maximum in one Application!');
//                $('html, body').animate({ scrollTop: $('.add_section_'+ section_count).offset().top }, 'slow');
            });
            $('.hide_section').click(function () {
                var section_id = $(this).attr('section-id');
                $('.add_section_' + section_id).hide();
            })
        });
        function hide_section(i) {
            $('.add_section_' + i).hide();
            $('.add_section_' + i + ' input').prop('disabled', true);
            $('.add_section_' + i + ' select').prop('disabled', true);
        }

        function hide_method(object) {
            if (confirm('Are you sure to Delete this?')) {
                var obj = $(object).parent().parent();
                // obj.find('input').prop('disabled',true);
                // obj.find('select').prop('disabled',true);

                obj.html('');
                obj.parent().parent().parent().parent().find('.lab_id').html('<option value="">Select Lab</option>'); ;
            }
        }

        function load_lab(object, flag) {
            var obj = $(object).parent().parent().parent();
            var duplicate_status = 0;
            method = [];
            obj.find('.method_id :selected').each(function (i, selected) {
                if (method.indexOf($(selected).val()) == - 1){
                    method[i] = $(selected).val();
                } else{
                    alert("Duplicate method can not be selected.");
                    duplicate_status = 1;
                    return;
                }
            });
            // console.log(method);
            if (duplicate_status){
                return false;
            }

            if ((typeof method[0] != 'undefined') && method[0] != '') {
                obj.find('.fa-refresh').addClass('animate');
                $.post('/apps/ajax/lab', {id: method, _token: _token}, function (response) {
                    if (response.responseCode == 1) {
                        var option = '<option value="">Select Labratory</option>';
                        if (response.data.length >= 1) {
                            $.each(response.data, function (id, value) {

                                var selected = obj.find('.lab_id2').val() == value.lab_id ? 'selected' : '';
                                option += '<option value="' + value.lab_id + '" ' + selected + ' >' + value.lab_name + '</option>';
                            });
                        } else {

                        }
                        obj.find('.lab_id').html(option);
                        obj.find('.lab_id').trigger('change');
                        obj.find('.fa-refresh').removeClass('animate');
                    }
                });
            }
            else {
                alert('Please select the Method');
            }
        }

        var loopTesting = 0;
        var total_samples_price = 0;
        (function ($) {
            $(".assigned_lab_list").on('change', function () {

                var lab_id = $(this).val();
                var obj = $(this).parent().parent().parent();
                obj.find('.fa-refresh').addClass('animate');
                method = [];
                obj.find('.method_id :selected').each(function (i, selected) {
                    method[i] = $(selected).val();
                });
                // obj.find('.price_duration').html('');
                var total_samples_price = 0;
                var total_price = 0;
                var total_day = [];
                var max_days;
                console.log(method);
                $.post('/apps/ajax/lab-price-dur', {method: method, lab_id: lab_id, _token: _token}, function (response) {
//                console.log(response);
                    if (response.responseCode == 1) {

                        // price_duration
                        obj.find('.method_id :selected').each(function (i, selected) {
                            if (typeof response.data[i] != 'undefined') {
                                console.log(response);
                                $(selected).parent().parent().parent().find('.price_duration').html("" + response.data[i].price + " TK &nbsp;(" + response.data[i].duration + " days)");
                                total_price += parseInt(response.data[i].price);
                                total_day[i] = parseInt(response.data[i].duration);
                            }
                        });
                        if (total_day.length){
                            max_days = Math.max.apply(Math, total_day);
                        } else{
                            max_days = 0;
                        }
                        obj.find('.lab_price_dur').html('<div class="col-sm-3"></div><div class="col-sm-4"><label>Total: </label> <b>Price:</b> <span class="t_price">' + total_price + '</span> <b>TK, &nbsp;&nbsp;Duration:</b> ' + max_days + ' days</div>');
                        obj.find('.fa-refresh').removeClass('animate');
                        var i = 0;
                        $(".content-wrapper").find('.t_price').each(function (i) {
                            //alert(parseFloat($(this).html()));
                            total_samples_price += parseFloat($(this).html());
                            /* alert(parseFloat($(this).html()));*/
                            /*
                             * total price count
                             * */
                        });

                        $(".total_price").html(total_samples_price + " BDT");
                        $("#total_price2").val(total_samples_price);
                        $('#discount_price').trigger('change');
                        loopTesting++;
                        if (loopTesting == {{$loopNum}} && {{$user_type}} == 6)
                        {
                            makeView();
                        }

                        if (loopTesting == {{$loopNum}} && {{$user_type}} == 3 && {{$data["app_status"]}} == 6)
                        {
                            makeView();
                        }

                    }

                });
            })
        })(jQuery);
        //            $("#app_entry_form").find('select').replaceWith(function () {
        //                var selectedText = $(this).find('option:selected').text();
        //                var selectedTextBold = "<b>" + selectedText + "</b>";
        //                return selectedTextBold;
        //            });

        //            $("#app_entry_form").find('button').replaceWith(function () {
        //                return '';
        //            });

        function setStatusForLabIncharge(statusId, tmpJobSection)
        {
            tmpJobSection = tmpJobSection - 1;
            
            if(statusId == 'Pending' || statusId == 'Recheck')
            {
                sectionClassName = "add_section_" + tmpJobSection;
                $("."+sectionClassName+" .labInchargeStatus option[value='6']").remove();
            }
            else
            {
                $("."+sectionClassName+" .labInchargeStatus").append('<option value="6">Verified</option>');
            }
        }
        function makeView(){

            $('#app_entry_form').find('input:not([type=button],[type=hidden])').not('.no_hide').each(function () {
                $(this).replaceWith("<span><b>" + this.value + "</b></span>");
            });
            // if select is not assigned_lab_list
            $("#app_entry_form").find('select').not(".no_hide").replaceWith(function () {
                var selectedText = $(this).find('option:selected').text();
                var selectedTextBold = "<b>" + selectedText + "</b>";
                return selectedTextBold;
            });
            $('#app_entry_form').find('textarea').not('.no_hide').each(function () {
                $(this).replaceWith("<span class=\"col-md-3\"><b>" + this.value + "</b></span>");
            });
            // if not button no hide
            $("#app_entry_form").find('button').not('.no_hide').replaceWith(function () {
                return '';
            });
            
            
        }
        
        

        $(".content-wrapper").find('.t_price').each(function (i) {
            //alert(parseFloat($(this).html()));
            total_samples_price += parseFloat($(this).html());
//                               alert(parseFloat($(this).html()));

        });
        $(".changeStatus").click(function(){
            var id = '{!!$id!!}';
            {{--var baseUrl = '{{url()}}'; --}}
            {{--alert(ss); --}}
            {{--$.post(baseUrl + 'parameter-method', {id: ss, _token: _token}, function (response)--}}
            {{--{--}}
            {{--alert(response); --}}
            {{--//                location.reload();--}}

            {{--}); --}}

            $.post('/apps/ajax/publish-result', {id: id, _token: _token}, function (response) {
                location.reload();
            });
        });
        $(".total_price").html(total_samples_price + " BDT");
        var flagFirst = 0;
        var ss = 0;
        function changeTest(object) {
            flagFirst++;
            var id = $(object).val();
            var obj = $(object).parent().parent().parent();
//        obj.find('.lab_id').html('');
            obj.find('.lab_id').html('<option value="">Select Lab</option>');
            obj.find('.price_duration').html('');
            obj.find('.lab_price_dur').html('');
            $.post('/apps/ajax/parameter-method', {id: id, _token: _token}, function (response) {
                if (response.responseCode == 1) {
                    var option = '';
//                console.log(response.data);
                    var c = 0;
                    $.each(response.data, function (id, value) {
//                    alert(response.data);
                        //alert(obj.find('.m_' + c).val() + '=' + c);
                        var selected = obj.find('.m_' + c).val() == value.method_id ? 'selected' : '';
                        c++;
                        option += '<option value="' + value.method_id + '" ' + selected + ' >' + value.param_name + '=>' + value.method_name + '</option>';
                    });
                    obj.find('.method_id').html(option);
                    if (flagFirst == 5)
                    {
                        $(".method_id").each(function(e){
                            var xxx = $(this).prev().val();
                            $(this).val(xxx);
                        });
                        ss++;
                        //    alert('333333333');
                        //obj.find('.load_lab').trigger('click');
                    }
                    //if(ss == 5)
                    obj.find('.load_lab11').trigger('click');
                }
            });
        }


        $(function () {
            $('.tst_id :selected').trigger('change');
            
//        $('.load_lab').trigger('click');
           $('#discount_price').change(function() {
                var total_price = $('#total_price2').val();
                var discount_price = $(this).val();
                if(parseInt(discount_price) <0 || parseInt(discount_price) > 100)
                {
                    alert("Percentage of Discount should be from 0 to 100");
                    return false;
                }
                var total_net_discount_price = (parseInt(total_price) * parseInt(discount_price)) / 100 ;
                $(".total_discount_price2").val(total_price-total_net_discount_price);
                $(".total_discount_price").text(total_price-total_net_discount_price + " BDT");
                
                $(".total_vat").val((total_price-total_net_discount_price)*0.15);
                $(".total_vat2").text(((total_price-total_net_discount_price)*0.15) + " BDT");

                var g_total = (total_price-total_net_discount_price)*1.15;
//              alert(total_price-total_net_discount_price*1.15);
                g_total = Math.round(g_total);
                
                $(".g_total_price").val(g_total);
                $(".g_total_price2").text(g_total + " BDT");
                $('#discount_price').val(0);
            });
        });
          
    </script>

@endsection
