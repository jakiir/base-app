@extends('master-admin-default')
@section('title')
<title>Application Result</title>
@endsection
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box-solid">
        <div class="box-header bg-teal-active">
            <h3 class="box-title"><strong><i class="fa fa-life-saver"></i> Application Result</strong></h3>
            {{--{!! link_to('apps/print-show/'.Request::segment(3),' Print',['class' => 'btn btn-primary btn-sm fa fa-print pull-right', 'target'=>'_blank']) !!}--}}
            {!! link_to('apps/app-result-download/'.Request::segment(3),' Download',['class' => 'btn btn-primary btn-sm fa fa-print pull-right', 'target'=>'_blank']) !!}
        </div>
    </div>
</section>

<!-- Main content -->
@foreach(@$app_data['app_header'] as $key=>$job)

<section class="invoice">
    <div class="col-xs-12 text-center">
        <div class="col-xs-3">

        @if($testMothods[0]->iso_logo == 1)
            {!! Html::image("iso-logo/1459590232_logo_NABL.jpg", "iso_logo",['width'=>55,'class'=>'pull-left']) !!}
        @endif
        </div><!-- /.col -->
        <div class="col-xs-5">
            <p class="lead analytics_heading">{!! Html::image("back-end/dist/img/logo.jpg", "Logo",['width'=>85]) !!}</p>
        </div>
        <div class="col-xs-2">
            <!--{!! link_to('apps/get-invoice/'.Request::segment(3),'Print',['class' => 'btn btn-primary btn-sm pull-right', 'target'=>'_blank']) !!}-->
        </div>
        <div class="col-xs-2 text-right" style="text-align: right;font-weight: normal;font-family: NikoshBAN !important;">
            জীবনের জন্য বিজ্ঞান
        </div>
    </div><!-- /.col -->
    <style>
          .tp{ font-size: 13px !important;font-family: "Times New Roman", Times, serif  !important; }
         .tpp{font-size:20px !important;font-family: "NikoshBAN"  !important;font-weight: normal; }
         .ts{font-size: 12px !important;font-family: "Times New Roman", Times, serif  !important; }
         .borders td, .ps p{ border: none !important; padding: 3px !important;font-size: 13px !important; font-family: "Times New Roman", Times, serif  !important;}
     </style>
    <div class="col-xs-12 text-center" style="border-bottom: 1px solid #eee;margin-bottom: 10px;">
    @if($app_data['job_details'][$job->scientist_id][0]->lab_id == 8)
        <span class="tp page-header print_heading" style="border:none;font-size: 16px !important;">
            Institute of National Analytical Research and Service (INARS)<br/>
        </span>
                <span class="tpp print_heading">
            ইনস্টিটিউট অব ন্যাশনাল এনালাইটিক্যাল রিসার্চ এন্ড সার্ভিস <br/>
        </span>
        @endif
                <span class="tp page-header print_heading" style="border:none;">
            BANGLADESH COUNCIL OF SCIENTIFIC AND INDUSTRIAL
            RESEARCH (BCSIR) <br/>
        </span>
        <span class="tpp print_heading">
            বাংলাদেশ বিজ্ঞান ও শিল্প গবেষণা পরিষদ ( বিসিএসআইআর )<br/><br/>
        </span>
    </div>
    <div class="col-xs-1">

    </div><!-- /.col -->
    <div class="col-xs-9">
        <p class="lead analytics_heading ts">Laboratories/Institute/Center: {{ $app_data['job_details'][$job->scientist_id][0]->lab_name }}</p>
    </div><!-- /.col -->

    <div class="col-xs-12 text-center">
        <p class="lead analytics_heading"><b style="text-decoration: underline;">ANALYSIS REPORT</b></p>
    </div><!-- /.col -->
{!! Form::open(['url' => '/apps/update-result/'.Encryption::encodeId($testMothods[0]->ts_id), 'method'=>'patch', 'class' => 'form apps_from', 'id'=>'app_entry_form','role' => 'form','enctype' =>'multipart/form-data', 'files' => 'true']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div>
               <table class="table borders">
                    <tr>
                        <td width="120">ASC Ref No </td>
                        <td width="421">: {{ $job->tracking_number }}</td>
                        <td width="179">Unit (Lab/Inst.) Ref No</td>
                        <td width="354">: {{ $job->lab_reference_no }}</td>
                    </tr>
                    <tr>
                        <td>Lab/Sample ID</td>
                        <td>: {{ $job->test_sample_id }}</td>
                        <td>Number of Sample </td>
                        <td>: {{ count($app_data['job_details']) }}</td>
                    </tr>
                    <tr>
                        <td width='120'>Sample Description</td>
                        <td>: <input name="job_name" type="text" class="form-control" value="{{ $job->job_name }}" size="10"></td>
                        <td>Application Date</td>
                        <td>:  {{ date('d/m/Y',strtotime($job->updated_at)) }}</td>

                    </tr>
                    <tr>
                        <td rowspan="2">Client's Details</td>
                        <td rowspan="2">:  {!! ucwords($job->submitted_by).'<br/> <b>'.   ucwords($job->company_name).'</b><br/> '.
                            ' Hourse#'.$job->company_house_no.$job->company_flat_no.', '.  $job->company_street.
                            $job->company_city.'-'.$job->company_zip !!}
                        </td>
                         <?php if($job->lab_incharge_id > 0)  { ?>
                        <td> Test Commencement date </td>
                         <td>: {{ date('d/m/Y',strtotime($job->test_start)) }} </td>
                         <?php } ?>
                    </tr>
                    <tr>
                      <?php  if($job->test_status == 8 )  {?>
                      <td> Test Completion date  </td>
                      <td> : {{ date('d/m/Y',strtotime($job->test_end)) }} </td>
                           <?php } ?>
                      </tr>
                </table>
            </div>
        </div><!-- /.col -->
    </div>

        <div class="form-group {{$errors->has('method_id') ? 'has-error' : ''}}">
        <?php $sl = 0;?>
        <table class="table">
            <tr>
                <th>SN#</th>
                <th>Parameter/Method</th>
                <th>Status</th>
                <th>Lab-Incharge’ Remarks</th>
            </tr>
            <?php
            $comments = '';
            $description = '';
            $result_file = '';
            if(isset($testMothods[0]))
            {
                $comments = $testMothods[0]->remarks;
                $description = $testMothods[0]->description;
                $result_file = $testMothods[0]->results_file;
                $iso_logo = $testMothods[0]->iso_logo;
            }
            ?>
            @foreach($testMothods as $methodRow)
                <div class="">

                    <tr>
                        <td>{!! ++$sl !!}</td>
                        <td>{!! $methodRow->param_name."/".$methodRow->method_name !!}</td>
                        <td>{!! $methodRow->status !!}</td>
                        <td>{!! $methodRow->remarks_li  !!}</td>
                    </tr>
                </div>
             @endforeach
             </table>
        </div>
      {!! Form::hidden('app_id',Encryption::encodeId($app_id)) !!}
    <label>Details:</label>
    <textarea id="description_editor" name="description">{!! $description !!}</textarea>
    <br/>
    <div class="form-group col-md-3">
        <label>Result:</label>
        <input type="file" name="results_file"/>
        @if(!empty($result_file))
        <a target="_blank" href="{{ url() }}/results-file/{{ $result_file }}">Download Result</a>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label>ISO Logo:</label>
        @if($iso_logo == 1)
         {!! Form::radio('iso_logo', 1, true) !!} Yes
         {!! Form::radio('iso_logo', 0, false) !!} No
        @elseif($iso_logo == 0)
         {!! Form::radio('iso_logo', 1, false) !!} Yes
         {!! Form::radio('iso_logo', 0, true) !!} No
        @else
         {!! Form::radio('iso_logo', 1, false) !!} Yes
         {!! Form::radio('iso_logo', 0, true) !!} No
        @endif
    </div>
    <div class="form-group col-md-12">
        <label>Comments:</label>
        <textarea style="width: 100%;" rows="5" name="remarks">{!! $comments !!}</textarea>
    </div>
    <div class="row">
        <div class="col-md-2 pull-right">
            <input type="submit" value="Save" class="btn btn-info" name="save">
            <input type="submit" value="Submit" onclick="return confirm('Are you sure');" class="btn btn-primary" name="submit">
        </div>
    </div>

    <br />
    <div class="row invoice-info">
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        <div class="col-sm-10 invoice-col">
            <p class="ps"><strong>Note:</strong><br/>
            <ul>
                <li style="list-style-type: lower-alpha;">The results reported here is based only on the supplied sample’s in this laboratory</li>
                <li style="list-style-type: lower-alpha;">Any complain about test report will not be acceptable after one month from the date of issuing of the said report.</li>
                <li style="list-style-type: lower-alpha;">This report/result shall not be reproduced/published without prior approval of the authority.</li>
            </ul>
            </p>
        </div><!-- /.col -->
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row invoice-info">
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        <div class="col-sm-10">
            <div class="ts" style="border-bottom:1px solid black;font-size: 12px;"><!--*The results relate only to the items tested--></div>
            <div class="text-center"><strong>Analytical Service Cell (ASC)</strong></div>
                <div class="text-center">
                    <small>
                        Dr. Qudrat-I-Khuda Road, Dhanmondi, Dhaka-1205,Bangladesh<br/>
                        Telephone:9671108,Fax: 880-02-9671108 E-mail:asc@bcsir.gov.bd
                    </small>
                </div>
            </div>
        </div><!-- /.col -->
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
  <br />
    <div class="row invoice-info">
        <div class="col-sm-2  invoice-col"></div><!-- /.col -->
        <div class="col-sm-1  invoice-col"></div><!-- /.col -->
    </div>
{!! Form::close() !!}
</section>
<?php break;?>
@endforeach


@endsection

