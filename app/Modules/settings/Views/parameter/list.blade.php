@extends('master-admin-default')

@section('title')
    <title>Parameter List</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parameter  list
            <!--<small>it all starts here</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Parameter</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                    <a href="{{ url('/settings/create-parameter') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> Add Parameter', array('type' => 'button', 'class' => 'btn btn-primary')) !!}
                    </a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="parameter_list" class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>Parameter Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($getList as $row)
                        <tr>
                                <td>{!! $row->param_name !!}</td>
                            <td>
                                {{--<i class="glyphicon glyphicon-edit"></i>--}}
                                {!! link_to('settings/edit-parameter/'. Encryption::encodeId($row->param_id),'Edit',['class' => 'btn btn-info btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.content -->

@endsection

@section('footer-script')
    <script>

        $(function () {
            $('#parameter_list').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength":20
            });
        });

    </script>
@endsection
