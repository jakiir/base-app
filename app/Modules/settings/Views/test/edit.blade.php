@extends('master-admin-default')

@section('title')
    <title>test entry</title>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Test Sample
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Test</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {{--{{dd($data)}}--}}
        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
            </div>

            {!! Form::open(['url' => '/settings/update-test/'.$id, 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form']) !!}

            <div class="box-body">

                <!-- text input -->
                <div class="form-group {{$errors->has('tst_name') ? 'has-error' : ''}}">
                    {!! Form::label('tst_name','Sample Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('tst_name',$data->tst_name,['class'=>'form-control','placeholder'=>'Enter test name...']) !!}
                        {!! $errors->first('tst_name','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div><!-- /.box-body -->


            <!-- Sample input -->
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <table class="table sample_table">
                        <caption>Parameter <button type="button" class="btn btn-sm btn-primary add_para pull-right">+Add Parameter</button> </caption>
                        <tbody>
                        @foreach($parameter_data as $row)
                            <tr style="border:1px solid yellowgreen">
                                <td> {!! Form::text('editParameter['. $row->param_id .']',$row->param_name,['class'=>'form-control','placeholder'=>'Enter Parameter name...','required'=>'required']) !!}</td>
                                <td> <a target="_blank" href="{{ url('/settings/edit-parameter/'.Encryption::encodeId($row->param_id)) }}">{!! Form::button('<i class="fa fa-edit"></i> Edit', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Lab input -->
            <div class="form-group">
                @foreach($lab_data as $lab)
                    <label class="col-md-1"></label>
                    <label class="col-md-11">
                        <span class="col-md-1"></span>
                        <span class="col-md-5">
                            {!! Form::checkbox('lab_id[]',$lab->lab_id, $lab->tst_id==$data->tst_id ? true:false, ['class'=>'lab_checkbox']) !!}
                            {{$lab->lab_name}}
                        </span>
                    </label>
                @endforeach
            </div>

            {{--<div class="col-md-12">--}}
            {{--<table class="table">--}}
            {{--<caption>Test Parameters--}}
            {{--<a href="/settings/create-parameter">{!! Form::button('+ New Parameter', array('type' => 'button', 'class' => 'btn btn-default pull-right')) !!}</a>--}}
            {{--</caption>--}}
            {{--<tr>--}}
            {{--<th>Name </th>--}}
            {{--<th>Samples </th>--}}
            {{--</tr>--}}
            {{--@foreach($parameter_data as $row)--}}
            {{--<tr>--}}
            {{--<td>{!! $row->param_name !!} </td>--}}
            {{--<td>{!! $row->sample_names !!} </td>--}}
            {{--</tr>--}}
            {{--@endforeach--}}
            {{--</table>--}}
            {{--</div>--}}



            <div class="box-footer">
                <div class="col-md-4">
                    <a href="{{ url('/settings/list-test') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                </div>
                <div class="col-md-4">
                    {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div>
            </div><!-- /.box-footer -->


            {!! Form::close() !!}

        </div>
    </section><!-- /.content -->
@endsection



@section('footer-script')
    <script>
        $(function () {
//            $('.lab_checkbox').click(function(){
//                var id = $(this).val();
//                if($(this).is(":checked")) {
//                    $('#lab_conf_' + id).show();
//                    $('#lab_conf_' + id + ' input').prop('required',true);
//                }else {
//                    $('#lab_conf_' + id).hide();
//                    $('#lab_conf_' + id + ' input').prop('required',false);
//                }
//            });


//            Modified on 19-10-2015
            $('.add_para').click(function () {
                var tr = '<tr style="border:1px solid rosybrown"><td>{!! Form::text("newParameter[]","",["class"=>"form-control","placeholder"=>"Enter Parameter name..."]) !!} </td>';
                tr += '<td>&nbsp;</td><tr>';
                $('.sample_table tbody').append(tr);
            });
        });
    </script>
@endsection