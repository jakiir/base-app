@extends('master-admin-default')

@section('title')
<title>Lab entry</title>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Lab Entry
        {{--<small>it all starts here</small>--}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li><a href="#">Lab</a></li>
        <li class="active">Create</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="col-md-6">
        <!-- Default box -->
        <div class="box box-success">
            <div class="box-body">

                <div class="box-header with-border">
                    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                </div>

                {!! Form::open(['url' => '/settings/store-lab', 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form','enctype' =>'multipart/form-data', 'files' => 'true']) !!}

                <div class="box-body">

                    <!-- text input -->
                    <div class="form-group {{$errors->has('lab_name') ? 'has-error' : ''}}">
                        {!! Form::label('lab_name','Lab Name',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('lab_name','',['class'=>'form-control','placeholder'=>'Enter test name...']) !!}
                            {!! $errors->first('lab_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="form-group {{$errors->has('lab_logo') ? 'has-error' : ''}}">
                    {!! Form::label('lab_logo','Lab Logo',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::file('lab_logo') !!}
                        {!! $errors->first('lab_logo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="box-footer">
                    <a href="{{ url('/settings/list-lab') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div><!-- /.box-footer -->


                {!! Form::close() !!}
            </div><!-- /.box-body -->
        </div>
    </div>
</section><!-- /.content -->
@endsection


@section('footer-script')
@section('footer-script')
<script language="javascript">
    $(document).ready(
            function() {

                $('.save').click(function() {
                    switch ($(this).val()) {
                        case 'save_as_new' :
                            $('.report_title').val($('.report_title').val() + "-{{ Carbon\Carbon::now() }}");
                            $('.report_form').attr('action', "{!! URL::to('/reports/store') !!}");
                            break;
                        case 'save_new':
                            $('.redirect_to_new').val(1);
                            break;
                        default:
                            //--
                    }
                    $('.report_form').submit();

                });



                $('#verifyBtn').click(function() {
                    var sql = $('.sql').val();
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: '/reports/verify',
                        type: 'POST',
                        data: {sql: sql, _token: _token},
                        dataType: 'text',
                        success: function(data) {
                            $('.results').html(data);
                            $('.results_tab a').trigger('click');
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('.results').html(jqXHR.responseText);
                            $('.results_tab a').trigger('click');
                        }
                    });
                });

                $('#showTables').click(function() {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: '/reports/tables',
                        type: 'GET',
                        data: {_token: _token},
                        dataType: 'text',
                        success: function(data) {
                            $('.db_fields').html(data);
                            $('.db_tables a').trigger('click');
                        }
                    });
                });
            });
</script>
@endsection
