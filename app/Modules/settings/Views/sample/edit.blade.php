@extends('master-admin-default')



@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Sample
            {{--<small>it all starts here</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Sample</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="col-md-6">
            <div class="box box-success">
                {!! Form::open(['url' => '/settings/update-sample/'.$id, 'method' => 'patch', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                <div class="box-body">

                    <div class="box-header with-border">
                        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                    </div>


                    <div class="form-group {{$errors->has('tst_id') ? 'has-error' : ''}}">
                        {!! Form::label('tst_id','Test Name',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('tst_id', $test_list, $data->tst_id,['class' => 'form-control']) !!}
                            {!! $errors->first('tst_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>


                    <div class="form-group {{$errors->has('param_id') ? 'has-error' : ''}}">
                        {!! Form::label('param_id','Test Parameter',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('param_id', [],  $data->param_id,['class' => 'form-control']) !!}
                            {!! $errors->first('param_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>


                    <!-- text input -->
                    <div class="form-group {{$errors->has('sample_name') ? 'has-error' : ''}}">
                        {!! Form::label('sample_name','Sample Name',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('sample_name',$data->sample_name,['class'=>'form-control','placeholder'=>'Enter Sample name...']) !!}
                            {!! $errors->first('sample_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="col-md-12">
                    {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                </div>

                <div class="box-footer">
                    <a href="{{ url('settings/list-sample') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'value'=> 'save', 'class' => 'btn btn-success  pull-right')) !!}
                </div><!-- /.box-footer -->

                {!! Form::close() !!}

            </div>
        </div>
    </section><!-- /.content -->
@endsection
