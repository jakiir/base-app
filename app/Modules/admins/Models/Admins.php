<?php namespace App\Modules\Admins\Models;

use Illuminate\Database\Eloquent\Model;

class Admins extends Model {

	//
	protected $table = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['admin_full_name', 'admin_email', 'admin_password'];

}
