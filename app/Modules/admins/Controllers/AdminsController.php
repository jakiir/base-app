<?php 
namespace App\Modules\Admins\Controllers;

use App\Http\Requests;
use App\Http\Requests\AdminRegisterRequest;
use App\Http\Controllers\Controller;
use App\Modules\Admins\Models\Admins;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("admins::index");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view("admins::create");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AdminRegisterRequest $request)
	{
		$flight = Admins::create([
			'admin_full_name' => $request->get('admin_full_name'),
			'admin_email' => $request->get('admin_email'),
			'admin_password' => bcrypt($request->get('admin_password'))
		]);

		 $email = $request->get('admin_email');
		\Mail::send('users::verify', array(
            'TOKEN_NO' => $request->get('TOKEN_NO')
                ), function($message) use ($email) {
            $message->from('base@gmail.com', 'base');
            $message->to($email);
            $message->subject('Verify your email address');
        });

		Session::flash('success', 'Registration Successfull.<br/>Please Check your mail to verify!');

        return redirect('admin/create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
