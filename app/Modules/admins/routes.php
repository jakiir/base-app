<?php

Route::group(array('module' => 'Admins', 'namespace' => 'App\Modules\Admins\Controllers'), function() {

    Route::get('/admin', "AdminsController@index")->middleware(['ocpl.checkvalid']);

    Route::get('/admin/create', "AdminsController@create")->middleware(['ocpl.checkvalid']);

    Route::get('/admin/dashboard', function() {
        return view("admins::dashboard");
    })->middleware(['ocpl.checkvalid']);

    // Route::controller('/admin', "AdminsController@index");

    Route::patch('/admin/store', "AdminsController@store")->middleware(['ocpl.checkvalid']);
    Route::resource('/admin', 'AdminsController');
    
});
