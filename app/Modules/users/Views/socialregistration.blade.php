@extends("master-front")

@section("header-script")
<meta name="csrf-token" content="{{ csrf_token() }}" />

<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >

<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >
@endsection  <!-- header script-->

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.7; border-radius:8px;">
            <h3 class="text-center">নিবন্ধন করুন</h3>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                {!! link_to('users/reSendEmail', 'Re-send Email', array('class' => 'btn btn-primary btn-block')) !!}
            </div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">
                {{ Session::get('error') }}				
            </div>
            @endif             
            <hr/>
            <div class="col-md-6 col-sm-6">
                {!! Form::open(array('url' => '/users/social_reg_store/'.$confirmationCode,'method' => 'patch', 'class' => 'form-horizontal',
                'id'=> 'social_reg_add_form')) !!}
                <fieldset>

                    {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                    {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                    {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}


                    <div class="form-group has-feedback {{ $errors->has('member_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star"> User Type</label>
                        <div class="col-lg-8">
                            {!! Form::select('member_type', $value = $user_types, '', $attributes = array('class'=>'form-control required', 
                            'placeholder' => 'Select One', 'id'=>"member_type")) !!}
                            @if($errors->first('member_type'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_type','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_nid') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star">National ID No.  </label>                                                
                        <div class="col-lg-8">
                            {!! Form::text('member_nid', $value = null, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your National ID No.','id'=>"member_nid")) !!}
                            <span class="glyphicon glyphicon-flag form-control-feedback"></span>
                            @if($errors->first('member_nid'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_nid','') }}</em>
                            </span>
                            @endif    
                        </div>                   
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_DOB') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star"> Date of Birth </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_DOB', $value = null, $attributes = array('class'=>'form-control required datepicker',
                            'placeholder'=>'Enter your Birth Date','id'=>"member_DOB", 'readonly' => "readonly")) !!}
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            @if($errors->first('member_DOB'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_DOB','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_phone') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star">Mobile Number  </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_phone', $value = null, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Mobile Number','id'=>"member_phone")) !!}
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            @if($errors->first('member_phone'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_phone','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-success"><b>Update Information</b></button>
                        </div>
                    </div>
                </fieldset>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!--</form>-->
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 col-sm-6" style="border-left:1px grey dotted;">
                <div id="tips">
                    <h4>সাইন-আপ সম্পর্কিত পরামর্শ</h4>
                    <hr>
                    <h6>আপনি এখানে গুগল অ্যাকাউন্টের মাধ্যমে সাইন-ইন করতে পারবেন এবং আমরা সেটারই পরামর্শ দিয়ে থাকি। </h6>
                    গুগল অ্যাকাউন্টে সাইন-আপ করার জন্য নিম্নোক্ত ধাপগুলো অনুসরণ করতে পারেন। 
                    <ul>
                        <li>১. উপরে বামে লাল বাটনটিতে ক্লিক করুন</li>
                        <li>২. যদি আপনি গুগুলে সাইন-ইন করা অবস্থায় না থাকেন, তাহলে আপনাকে সাইন-ইন করে নিতে হবে</li>
                        <li>৩. আপনি যদি ইতোমধ্যে গুগলে সাইন ইন করা অবস্থায় থাকেন, তাহলে আপনার অথ্যগুলো গুগল থেকে সরাসরি ফর্মে চলে আসবে।</li>                                        
                        <li>৪. কোন তথ্য বাদ পড়ে গেলে সেগুলো ফর্মে যোগ করুন, এবং "নিবন্ধন" বাটনে ক্লিক করুন।</li>
                    </ul>

                    <br>
                    <h6> আপনি যদি গুগল অ্যাকাউন্ট ব্যবহার করতে না চাইলে নিন্মোক্ত ধাপগুলো অনুসরণ করে ফর্মটি পূরণ করুন।</h6>
                    <ul>
                        <li>১. প্রতিটি ঘর উপযুক্ত তথ্যসহ পূরণ করুন।</li>
                        <li>২. অনুমতি পত্রের স্ক্যান করা কপি আপলোড করুন।</li>
                        <li>৩. ছবি আপলোড করুন।</li>
                    </ul>

                    <h6> অনুমতি পত্র (Authorization Letter) কি?</h6>
                    <p>
                        আপনি যে প্রতিষ্ঠানের পক্ষে কাজ করতে চান, সেই প্রতিষ্ঠানটির ব্যবস্থাপনা পরিচালক / প্রধান ব্যক্তি কর্তৃক স্বাক্ষর সম্বলিত একটি সম্মতি পত্র, 
                        যা তাদের Letter Head প্যাডে প্রিন্ট করা হয়ে থাকে।
                    </p>

                    <h6> ছবির মাপ / ধরণ কি হবে এবং কেন লাগবে?</h6>
                    <p>
                        ছবিটি আপনার প্রোফাইলে বিভিন্ন সময় ব্যবহার করা হবে এবং এটি ১৫০ x ১৭৫ পিক্সেল (পাসপোর্ট সাইজ) gif, jpg অথবা png ফরম্যাটে সর্বোচ্চ ৫০ কিলোবাইটের ভিতরে হতে হবে।
                        <br> আপনি গুগল অ্যাকাউন্ট ব্যবহার করলে আলাদা করে ছবি প্রয়োজন হবে না।
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection <!--- content--->

@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#make_user_id").click(function () {
        var email = $("#member_email").val();
        $("#member_username").val(email);
        $("#member_username").prop('readonly', true);
        $("#member_username").trigger("blur");
    });

    $(document).ready(function ()
    {
        $(".datepicker").datepicker({
            maxDate: "+20Y",
            //showOn: "button",
            //buttonText: "Select date",
            buttonText: "Select date",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showAnim: 'scale',
            yearRange: "-100:+40",
            minDate: "-200Y",
        });
    });</script>
@endsection <!--- footer-script--->