@extends("master-front")

@section("header-script")

@section('title')
    Ministry of Home Affairs
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />
<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >
<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >
<style>
    *{font-family:"Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;}
    .box-div{background: #edeff1; border-radius:8px;}
    .hr{border-top: 1px solid #d3d3d3;
        box-shadow: 0 1px #fff inset;}
    .footer-p{
        background: #333 none repeat scroll 0 0;
        color: #999;
        padding-bottom:10px;
    }
    footer{
        margin-top:20px;
        line-height:12px;
    }
</style>
@endsection  <!-- header script-->

@section("content")
<img style="display: block; margin-left: auto; margin-right: auto;" src="{{ asset('front-end/images/loading.gif') }}">
{!! Form::open(array('url' => '/users/save-google','method' => 'patch', 'class' => 'form-horizontal','id' => 'googleSubmit','enctype' =>'multipart/form-data')) !!}
{!! Form::hidden('member_social_type', $result['member_social_type'],array('id'=>'member_social_type')) !!}
{!! Form::hidden('member_social_id', $result['member_social_id'],array('id'=>'member_social_id')) !!}
{!! Form::hidden('member_first_name', $result['member_first_name'],array('id'=>'member_first_name')) !!}
{!! Form::hidden('member_email', $result['member_email'],array('id'=>'member_email')) !!}
{!! Form::hidden('member_username', $result['member_username'],array('id'=>'member_username')) !!}
{!! Form::hidden('member_hash', $result['member_hash'],array('id'=>'member_hash')) !!}
{!! Form::hidden('member_pic', $result['member_pic'],array('id'=>'member_pic')) !!}
{!! Form::hidden('member_status', $result['member_status'],array('id'=>'member_status')) !!}
{!! Form::hidden('member_verification', $result['member_verification'],array('id'=>'member_verification')) !!}
{!! Form::close() !!}
@endsection

@section('footer-script')
    <script language="javascript">

        $(document).ready(function(){
            setTimeout(
                $("#googleSubmit").submit()
                ,3000);

        });

        window.onload = getInfo;
    </script>
@endsection
