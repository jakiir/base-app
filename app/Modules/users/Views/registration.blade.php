@extends("master-front")

@section("header-script")
    @section("title")
        Welcome to ASC | BCSIR
    @endsection

<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >

<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >

@endsection  <!-- header script-->

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.7; border-radius:8px;">
            <h3 class="text-center">নিবন্ধন করুন</h3>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                {!! link_to('users/reSendEmail', 'Re-send Email', array('class' => 'btn btn-primary btn-sm')) !!}
            </div>
            @endif
            @if(Session::has('successformobile'))
            <div class="alert alert-success">
                {{ Session::get('successformobile') }}
            </div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">
                {{ Session::get('error') }}
            </div>
            @endif
            <hr/>
            <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-3 pull-left col-md-offset-1">
                    {!! link_to('login/google', 'Google Sign-up', array("class" => "btn btn-block btn-social btn-google", "style" => "font-weight: bold;")) !!}
                    <!-- <a class="btn btn-block btn-social btn-google" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-google']);">
                        <i class="fa fa-google-plus"></i> Sign up with Google
                    </a> -->
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                {{--<div class="col-md-6 col-sm-12 pull-left">--}}
                {{--{!! link_to('#', 'Facebook sign up', array("class" => "btn btn-block btn-social btn-facebook")) !!}--}}

                {{--<!-- <a class="btn btn-block btn-social btn-facebook" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-facebook']);">--}}
                {{--<i class="fa fa-facebook"></i> Sign up with Facebook--}}
                {{--</a> -->--}}
                {{--</div>                    --}}

                <br/>
                {!! Form::open(array('url' => '/users/store','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'reg_form')) !!}
                <!--<form class="form-horizontal">-->
                <fieldset>

                    {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                    {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                    {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}

                    <?php
                    $random_number = str_random(30);
                    ?>
                    {!! Form::hidden('TOKEN_NO', $random_number) !!}

                    <div class="form-group has-feedback {{ $errors->has('member_first_name') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star">Name</label>

                        <div class="col-lg-8">
                            {!! Form::text('member_first_name', $value = null, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Name','id'=>"member_first_name")) !!}
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if($errors->first('member_first_name'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_first_name','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star"> User Type</label>
                        <div class="col-lg-8">
                            {!! Form::select('member_type', $value = $user_types, '', $attributes = array('class'=>'form-control required',
                            'placeholder' => 'Select One', 'id'=>"member_type")) !!}
                            @if($errors->first('member_type'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_type','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <?php
                            $company_types = [
                                "" => 'Select One',
                                1 => 'Limited Company',
                                2 => 'Partnership',
                                3 => 'Propertieship',
                                4 => 'Government',
                                5 => 'Semi-Government',
                                6 => 'Student',
                                7 => 'Self Employed',
                                8=>' Group of Company'
                            ];
                    ?>

                    <div id="applicant-type" style="display: none;" class="form-group has-feedback {{ $errors->has('company_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left"> Type of Applicant</label>
                        <div class="col-lg-8">
                            {!! Form::select('company_type', $company_types, '', $attributes = array('class'=>'form-control required',
                            'id'=>"company_type")) !!}
                            @if($errors->first('company_type'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_type','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_nid') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">National ID No.  </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_nid', $value = null, $attributes = array('class'=>'form-control','placeholder'=>'Enter your National ID No.','id'=>"member_nid")) !!}

                            <span class="glyphicon glyphicon-flag form-control-feedback"></span>
                            @if($errors->first('member_nid'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_nid','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_DOB') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left"> Date of Birth </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_DOB', $value = null, $attributes = array('class'=>'form-control datepicker',
                            'placeholder'=>'Enter your Birth Date','id'=>"member_DOB", 'readonly' => "readonly")) !!}
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            @if($errors->first('member_DOB'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_DOB','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('member_phone') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left  required-star">Mobile Number  </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_phone', $value = null, $attributes = array('class'=>'form-control required','placeholder'=>'Enter your Mobile Number','id'=>"member_phone")) !!}
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            @if($errors->first('member_phone'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_phone','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('member_email') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left  member_email required-star">Email Address </label>
                        <div class="col-lg-8">
                            {!! Form::text('member_email', $value = null, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter your Email Address','id'=>"member_email")) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if($errors->first('member_email'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_email','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('institute_name') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left">Educational Institution</label>
                        <div class="col-lg-8">
                            {!! Form::text('institute_name', '', $attributes = array('class'=>'form-control',
                            'placeholder'=>'Enter Name of your Institution','id'=>"institute_name")) !!}
                             <b>(For Student only)</b>
                            <span class="glyphicon glyphicon-home form-control-feedback"></span>
                            @if($errors->first('institute_name'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('institute_name','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <fieldset id="address-view" style="display: none;">
                        <legend style="font-size: 16"><b>Address</b></legend>
                        <div class="form-group has-feedback {{ $errors->has('company_house_no') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left">House Number</label>
                            <div class="col-lg-8">
                                {!! Form::text('company_house_no', $value = null, $attributes = array('class'=>'form-control',
                                'placeholder'=>'','id'=>"company_house_no")) !!}
                                <span class="glyphicon glyphicon-home form-control-feedback"></span>
                                @if($errors->first('company_house_no'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_house_no','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_street') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left"> Street Name / Village</label>
                            <div class="col-lg-8">
                                {!! Form::text('company_street', $value = null, $attributes = array('class'=>'form-control',
                                'placeholder'=>'','id'=>"company_street")) !!}
                                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                                @if($errors->first('company_street'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_street','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_area') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left">Area </label>
                            <div class="col-lg-8">
                                {!! Form::text('company_area', $value = null, $attributes = array('class'=>'form-control',
                                'placeholder'=>'','id'=>"company_area")) !!}
                                <span class="glyphicon glyphicon-tree-conifer form-control-feedback"></span>
                                @if($errors->first('company_area'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_area','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group has-feedback {{ $errors->has('company_city') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left">City </label>
                            <div class="col-lg-8">
                                <?php $company_city = (!empty($company_city)) ? $company_city : ''; ?>
                                {!! Form::text('company_city', $value = $company_city, $attributes = array('class'=>'form-control',
                                'placeholder'=>'','id'=>"company_city")) !!}
                                <span class="glyphicon glyphicon-tower form-control-feedback"></span>
                                @if($errors->first('company_city'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_city','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_zip') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left">Zip / Post Code</label>
                            <div class="col-lg-8">
                                {!! Form::text('company_zip', $value = null, $attributes = array('class'=>'form-control','
                                placeholder'=>'','id'=>"company_zip")) !!}
                                <span class="glyphicon glyphicon-tag form-control-feedback"></span>
                                @if($errors->first('company_zip'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_zip','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_fax') ? 'has-error' : ''}}">
                            <label  class="col-lg-4 text-left">Fax Number</label>
                            <div class="col-lg-8">
                                {!! Form::text('company_fax', $value = null, $attributes = array('class'=>'form-control',
                                'placeholder'=>'','id'=>"company_fax")) !!}
                                <span class="glyphicon glyphicon-fast-forward form-control-feedback"></span>
                                @if($errors->first('company_fax'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_fax','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                    </fieldset>

<!--                    <div class="form-group">
                        <div class="col-lg-8 text-left">
                            <input type="checkbox" id="make_user_id" value="same_as_email"/>
                            &nbsp; <b>User Name same as Email Address</b>
                        </div>
                    </div>
                    <br/>

                    <div class="form-group has-feedback {{ $errors->has('member_username') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star">User Name</label>
                        <div class="col-lg-8">
                            {!! Form::text('member_username', $value = null, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter your desired User Name','id'=>"member_username")) !!}
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if($errors->first('member_username'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_username','') }}</em>
                            </span>
                            @endif
                        </div>

                    </div>-->


                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-2">
                            <button type="submit" class="btn btn-block btn-primary"><b>Submit</b></button>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-lg-12 col-lg-offset-2">
                            Already have an account? <b>{!! link_to('users/login', 'Login', array('class' => '')) !!}</b>
                        </div>
                    </div>
                </fieldset>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!--</form>-->
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 col-sm-6" style="border-left:1px grey dotted;">
                <div id="tips">
                    <h4>সাইন-আপ সম্পর্কিত পরামর্শ</h4>
                    <hr>
                    <h6>আপনি এখানে গুগল অ্যাকাউন্টের মাধ্যমে সাইন-ইন করতে পারবেন এবং আমরা সেটারই পরামর্শ দিয়ে থাকি। </h6>
                    গুগল অ্যাকাউন্টে সাইন-আপ করার জন্য নিম্নোক্ত ধাপগুলো অনুসরণ করতে পারেন।
                    <ul>
                        <li>১. উপরে বামে লাল বাটনটিতে ক্লিক করুন।</li>
                        <li>২. যদি আপনি গুগলে সাইন-ইন করা অবস্থায় না থাকেন, তাহলে আপনাকে সাইন-ইন করে নিতে হবে।</li>
                        <li>৩. আপনি যদি ইতোমধ্যে গুগলে সাইন ইন করা অবস্থায় থাকেন, তাহলে আপনার অথ্যগুলো গুগল থেকে সরাসরি ফর্মে চলে আসবে।</li>
                        <li>৪. কোন তথ্য বাদ পড়ে গেলে সেগুলো ফর্মে যোগ করুন, এবং "নিবন্ধন" বাটনে ক্লিক করুন।</li>
                    </ul>

                    <br>
                    <h6> আপনি যদি গুগল অ্যাকাউন্ট ব্যবহার করতে না চাইলে নিম্নোক্ত ধাপগুলো অনুসরণ করে ফর্মটি পূরণ করুন।</h6>
                    <ul>
                        <li>১. প্রতিটি ঘর উপযুক্ত তথ্যসহ পূরণ করুন।</li>
                        <li>২. অনুমতি পত্রের স্ক্যান করা কপি আপলোড করুন।</li>
                        <li>৩. ছবি আপলোড করুন।</li>
                    </ul>

                    <h6>ব্যবহারকারীর ধরণ (User Type) কি হবে? </h6>
                    <ul>
                        <li>১. আপনি যদি সাধারণ ব্যবহারকারী হন, তবে আবেদনকারী (Applicant) নির্বাচন করুন।</li>
                        <li>২. যদি আপনি স্বরাষ্ট্র-মন্ত্রাণালয়ের নিজস্ব ব্যবহারকারী হন, তবে বিসিএসআইআরের  অভ্যন্তরীণ ব্যবহারকারী (BCSIR Executive) নির্বাচন করুন।</li>
                    </ul>

                    <h6> অনুমতি পত্র (Authorization Letter) কি?</h6>
                    <p>
                        আপনি যে প্রতিষ্ঠানের পক্ষে কাজ করতে চান, সেই প্রতিষ্ঠানটির ব্যবস্থাপনা পরিচালক / প্রধান ব্যক্তি কর্তৃক স্বাক্ষর সম্বলিত একটি সম্মতি পত্র,
                        যা তাদের Letter Head প্যাডে প্রিন্ট করা হয়ে থাকে।
                    </p>

                    <h6> ছবির মাপ / ধরণ কি হবে এবং কেন লাগবে?</h6>
                    <p>
                        ছবিটি আপনার প্রোফাইলে বিভিন্ন সময় ব্যবহার করা হবে এবং এটি ১৫০ x ১৭৫ পিক্সেল (পাসপোর্ট সাইজ) gif, jpg অথবা png ফরম্যাটে সর্বোচ্চ ৫০ কিলোবাইটের ভিতরে হতে হবে।
                        <br> আপনি গুগল অ্যাকাউন্ট ব্যবহার করলে আলাদা করে ছবি প্রয়োজন হবে না।
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection <!--- content--->

@section('footer-script')

<link rel="stylesheet" href="{{ asset('back-end/dist/css/custom.css') }}">
<script src="{{ asset('front-end/js/jquery.validate.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#make_user_id").click(function () {
        var email = $("#member_email").val();
        $("#member_username").val(email);
        $("#member_username").prop('readonly', true);
        $("#member_username").trigger("blur");
    });

    $(document).ready(function ()
    {
        $("#reg_form").validate({
            errorPlacement: function () {
                return false;
            }
        });
        $('#member_type').on('change', function () {
            if ($(this).val() == 5) {
                $('#applicant-type').show();
            }else{
                $('#applicant-type').hide();
            }
        });
        $('#company_type').on('change', function () {
            if ($(this).val() == 6 || $(this).val() == 7) {
                $('#address-view').show();
                $('#member_email').removeClass('error');
                $('.member_email').removeClass('required-star');
                $('#member_email').removeClass('required');

            }else{
                $('#address-view').hide();
                $('#member_email').addClass('required');
                $('.member_email').addClass('required-star');
            }
        });
        $('#member_type').trigger('change');
        $('#company_type').trigger('change');

        $(".datepicker").datepicker({
            maxDate: "+20Y",
            //showOn: "button",
            //buttonText: "Select date",
            buttonText: "Select date",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showAnim: 'scale',
            yearRange: "-100:+40",
            minDate: "-200Y",
        });
    });

</script>
@endsection <!--- footer-script--->