@extends('master-admin-default')

@section('title')
<title>Update User's Desk</title>
@endsection

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-8 col-md"><br/></div>

        <div class="col-md-8 col-md-offset-2" style="background: snow; opacity:0.7; border-radius:8px;">

            <h3 class="text-center">Update User's Desk</h3>
            @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning"> {{ Session::get('error') }}	</div>
            @endif             
            <hr/>
            <div class="col-md-9 col-sm-9 col-md-offset-2">

                {!! Form::open(array('url' => '/users/assignTypeDesk/'.Encryption::encodeId($user_details->member_id),'method' => 'patch', 'class' => 'form-horizontal', 
                'id'=> 'user_assign_form')) !!}
                <fieldset>

                    <div class="form-group has-feedback {{ $errors->has('member_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-4 text-left required-star"> User Specification</label>
                        <div class="col-lg-8">
                            {!! Form::select('member_type', $value = $user_types, $user_details->member_type, $attributes = array('class'=>'form-control required', 
                            'placeholder' => 'Select One', 'id'=>"member_type")) !!}
                            @if($errors->first('member_type'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_type','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="hidden form-group has-feedback {{ $errors->has('lab_id') ? 'has-error' : ''}}" id="lab_div">
                        <label  class="col-lg-4 text-left required-star"> User's Lab </label>
                        <div class="col-lg-8">
                            {!! Form::select('lab_id', $value = $user_desks, '', $attributes = array('class'=>'form-control', 
                            'placeholder' => 'Select One','id'=>"lab_id")) !!}
                            @if($errors->first('lab_id'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('lab_id','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-2">
                            <button type="submit" class="btn btn-lg btn-block btn-primary"><b>Submit</b></button>
                        </div>
                    </div>
                </fieldset>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!--</form>-->
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection <!--- content--->

@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(
            function () {
                $("#user_assign_form").validate({
                    errorPlacement: function () {
                        return false;
                    }
                });
            });
            
            $(document).ready(function () {
        $('#member_type').on('change', function () {
            if ($(this).val() == 6 || $(this).val() == 7 || $(this).val() == 8 ) {
                $('#lab_div').removeClass('hidden');
                $('#lab_id').addClass('required');
            } else {
                $('#lab_div').addClass('hidden');
                $('#lab_id').removeClass('required');
            }
        });
    });
</script>
@endsection <!--- footer-script--->