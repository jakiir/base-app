@extends("email-template")

@section('title')
    {!! $header !!}
@endsection

@section('content')
        <h2>Please verify your email address</h2>
        <div>
            Thanks for creating an account with BCSIR. <br/>
            Please follow the link below to verify your email address <br/>
            
            {{ URL::to('users/verify/' . $TOKEN_NO) }}<br/>

            If you have any problem, please paste the above URL into your web browser.
        </div>
@endsection 
