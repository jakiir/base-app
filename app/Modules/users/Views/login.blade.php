@extends("master-front")

@section("header-script")

@section('title')
    BCSIR
@endsection

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ asset('front-end/images/favicon.ico') }}" />
<link href="{{ asset('front-end/social-buttons/assets/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front-end/social-buttons/assets/css/docs.css') }}" rel="stylesheet" >
<link href="{{ asset('front-end/social-buttons/bootstrap-social.css') }}" rel="stylesheet" >
<style>
    *{font-family:"Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;}
    .box-div{background: #edeff1; border-radius:8px;}
    .hr{border-top: 1px solid #d3d3d3;
        box-shadow: 0 1px #fff inset;}
    .footer-p{
        background: #333 none repeat scroll 0 0;
        color: #999;
        padding-bottom:10px;
    }
    footer{
        margin-top:20px;
        line-height:12px;
    }
</style>
@endsection  <!-- header script-->

@section("content")

    <div class="container" style="margin-top:30px;">

        @columnchart('Stocks', 'stocks-div')


        <div class="row box-div">

            <div class="col-md-6 pull-left">
                <h3 class="text-center">Monthly Application Volume</h3>
                <div class="col-md-12" style="margin-top: 30px;"><div id="stocks-div"></div></div>

            </div>

            <div class="col-md-6 pull-left">
                <h3 class="text-center">Login to your account!</h3>
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-warning">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <hr/>

                {!! Form::open(array('url' => '/users/checklogin','method' => 'patch', 'class' => 'form-horizontal')) !!}
                <fieldset>
                    {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                    {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                    {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}

                    <div class="form-group col-md-5">
                        {!! Form::label('login_social','Login with social accounts') !!}
                        <br/><br/>
                        {{--<i class="icon-google-plus social-icon fa fa-google-plus"></i>--}}
                        {!! link_to('login/google', 'Login with Google+', array("class" => "btn btn-block btn-social btn-google")) !!}
                                <!--                    {{--<i class="icon-facebook social-icon fa fa-facebook"></i>--}}
                        {!! link_to('login/facebook', 'Login with Facebook', array("class" => "btn btn-block btn-social btn-facebook")) !!}-->
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="form-group has-feedback {{ $errors->has('member_email') ? 'has-error' : ''}}">
                            <div>
                                {!! Form::label('login_local','Login with your email') !!}
                                {!! Form::text('member_email', $value = null, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter your Email Address','id'=>"member_email")) !!}
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if($errors->first('member_email'))
                                    <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_email','') }}</em>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('member_password') ? 'has-error' : ''}}">
                            <div>
                                {!! Form::password('member_password', $attributes = array('class'=>'form-control required','placeholder'=>'Password',
                                'id'=>"member_password")) !!}
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if($errors->first('member_password'))
                                    <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('member_password','') }}</em>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success"><b>Login</b></button><br/>
                            <span class="pull-right">
                                {!! link_to('users/forgetPassword', 'Forget Password?', array("class" => "text-right")) !!}
                            </span>
                            <br/>
                            <span class="pull-right">
                                {!! link_to('users/support', 'Need Help?', array("class" => "text-right")) !!}
                            </span>
                            <br/>
                            <span class="pull-right">
                            New User? {!! link_to('users/create', 'Sign Up', array("class" => " ")) !!}
                            </span>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {!! Form::close() !!}
                    </div>
                    <hr/>
                    <div class="col-md-2"></div>
                    <div class="col-md-8 col-md-offset-1">

                        <br/><br/>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
@endsection



@section('footer-section')

@endsection