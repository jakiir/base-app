<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UsersModel extends Model {

    protected $table = 'members';
    protected $fillable = array(
        'member_id',
        'member_username',
        'member_email',
        'member_password',
        'member_social_type',
        'member_social_id',
        'member_hash',
        'member_status',
        'member_verification',
        'member_first_name',
        'member_middle_name',
        'member_type',
        'member_subtype',
        'member_nid',
        'member_DOB',
        'member_gender',
        'member_aboutme',
        'member_street_address',
        'member_country',
        'member_city',
        'member_zip',
        'member_phone',
        'member_agree_tc',
        'member_first_login',
        'member_language',
        'lab_id',
    );


    function checkLoginCredential($email,$pass,$is_mobile){

        $query = DB::table($this->table);
        if ($is_mobile == 0){
            $query->where("member_email", $email);
        }else{
            $query->where("member_phone", $email);
        }

      $data =  $query->where("member_password", $pass)
            ->get();
        return $data;

    }

    function checkUserByEmail($member_email){

        return DB::table($this->table)
            ->where("member_email", $member_email)
            ->get();
    }

    // This function will be used to check if a value is already existed in a database or not
    function check_availability_status($field, $value, $member_social_id) {
        return DB::table($this->table)
            ->where($field, $value)
            ->where('member_social_id', $member_social_id)
            ->get();
    }

    function chekced_verified($TOKEN_NO, $data) {
        DB::table($this->table)
            ->where('member_hash', $TOKEN_NO)
            ->update($data);
    }

    function profile_update($table, $field, $check, $value) {
        return DB::table($table)->where($field, $check)->update($value);
    }

    function getUserList() {
        return DB::table($this->table)->get();
    }

    function getListWhere($field, $value) {
        return DB::table($this->table)
            ->where($field, $value)
            ->get();
    }

    function getTotalUserList() {
        return DB::table($this->table)->count();
    }

    //this function is to check if the email address is associated with members table
    //and if valid return type id
    function checkEmailAndGetType($member_email){
        return DB::table($this->table)
            ->where('member_email', $member_email)
            ->pluck('member_type');
    }

    function checkMobileAndGetType($member_email){
        return DB::table($this->table)
            ->where('member_phone', $member_email)
            ->where('member_id','>', 662)
            ->pluck('member_type');
    }


    function checkEmailAndGetMemId($member_email,$is_mobile){
        $query = DB::table($this->table);
        if ($is_mobile == 0){
            $query->where('member_email', $member_email);
        }
        else{
            $query->where('member_phone', $member_email);
        }
        return    $query->pluck('member_id');


    }

    /******************************* Users Model Class ends here **************************/
}
