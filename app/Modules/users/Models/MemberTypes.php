<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class MemberTypes extends Model {

    protected $table = 'member_types';
    protected $fillable = array(
        'mt_id',
        'mt_type_name',
        'mt_is_registarable',
        'mt_access_code',
        'mt_permission_json',
        'mt_status',
    );

    /************************ Users Model Class ends here ****************************/
}
