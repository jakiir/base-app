<?php

namespace App\Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\SocialRegRequest;
use App\Http\Requests\profileEditRequest;
use Carbon\Carbon;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\CompanyAssoc;
use App\Modules\Company\Models\CompanyType;
use Illuminate\Http\Request AS Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\ForgetPasswordRequest;
use App\Modules\Users\Models\UsersModel;
use App\Modules\Users\Models\MemberTypes;
use App\Modules\Settings\Models\Lab;
use DB;
use Encryption;
use Calendar;
use Khill\Lavacharts\Laravel\LavachartsFacade;
use Session;
use Illuminate\Support\Facades\Input;

class UsersController extends Controller {

    public function index() {
        return view("users::index");
    }

    public function create() {
        $user_types = MemberTypes::where('mt_is_registarable', 1)
            ->lists('mt_type_name', 'mt_id');

        return view("users::registration", compact("user_types"));
    }

    public function store(Request $request) {

//dd($request->all());
        $last_record = DB::table('members')->orderBy('member_id', 'desc')->first();
//        dd($last_record->member_id);
        $comapny_type = $request->get('company_type');
        if($comapny_type == 6 && $request->get('member_email') =='' || $comapny_type == 7 && $request->get('member_email') =='') {
            if ($last_record->member_id > 662){

                $this->validate($request, [
                    'member_first_name' => 'required',
                    'member_phone' => "required|unique:members,member_phone",
                ]);
            }
            else{

                $this->validate($request, [
                    'member_first_name' => 'required',
                    'member_phone' => "required",
                ]);
            }
//            save mobile number to avoid unique email validation
//           $member_email = $request->get('member_phone');
           $member_email = '';
        }
        else{


            if ($last_record->member_id > 662){
//                dd('ekse');
                $this->validate($request, [
                    'member_first_name' => 'required',
                    'member_phone' => 'required',
                    'member_email' => "required|email|unique:members,member_email",
                ]);
            }
            else{
                $this->validate($request, [
                    'member_first_name' => 'required',
                    'member_phone' => 'required',
                    'member_email' => "required",
                ]);
            }

            $member_email =  $request->get('member_email');
        }

        date_default_timezone_set('Asia/Dhaka');
        $created_at = date('Y-m-d h:i:s', time() - 36000);
        
      $user_ids =   DB::table('members')->insertGetId(
            [
                'member_first_name' => $request->get('member_first_name'),
                'member_nid' => $request->get('member_nid'),
                'member_DOB' => $request->get('member_DOB'),
                'member_phone' => $request->get('member_phone'),
                'member_email' => $member_email,

                'member_username' => $request->get('member_first_name'),
                'member_hash' => $request->get('TOKEN_NO'),
                'member_type' => $request->get('member_type'),
                'created_at' => $created_at,
                'member_status' => 'active',
        ]);
        
      $user_id_last = $user_ids;

      if($comapny_type == 6 || $comapny_type == 7)
      {
          if($comapny_type == 6)
          {
              $comapny_name = $request->get('institute_name');
          } else{
              $comapny_name = 'Self Employed';
          }
      
       $company_id = Company::create([
                'company_name' => $comapny_name,
                'company_type' => $comapny_type,
                'company_house_no' => $request->get('company_house_no'),
                'company_flat_no' => $request->get('company_flat_no'),
                'company_street' => $request->get('company_street'),
                'company_city' => $request->get('company_city'),
                'company_area' => $request->get('company_area'),
                'company_zip' => $request->get('company_zip'),
                'company_fax' => $request->get('company_fax'),
                'contact_person' => $request->get('member_first_name'),
                'contact_phone' => $request->get('member_phone'),
                'contact_email' => $request->get('member_email'),
                'institute_name' => $request->get('institute_name'),
                'company_status' => 1,
                'updated_by' => $user_id_last,
         ]);
//         dd($company_id->id);
        CompanyAssoc::create([
                'company_id' => $company_id->id,
                'member_id' => $user_id_last,
                'ca_status' => 1
         ]);
      }
        $data = ['header' => 'Verify your email address',
                 'TOKEN_NO' => $request->get('TOKEN_NO')];
        if($comapny_type == 6 && $request->get('member_email') =='' || $comapny_type == 7 && $request->get('member_email') =='') {

            $TOKEN_NO = $request->get('TOKEN_NO');
            $this->sendMobilePassword($TOKEN_NO);
            \Session::flash('successformobile', 'Thanks for signing up! Please check your mobile!');

        }
        else{
            \Mail::send('users::verify', $data, function($message) {

                $message->from('bcsir@gmail.com', 'BCSIR');
                $message->to(Input::get('member_email'));
                $message->subject('Verify your email address');
            });
            \Session::flash('success', 'Thanks for signing up! Please check your email and follow the instructions to complete the sign up process');

        }


        return redirect('users/create');

    }

    public function sendMobilePassword($TOKEN_NO){
        $user = DB::select("select member_id, member_phone, member_type from members where member_hash = '" . $TOKEN_NO . "'");

        $fetched_mobile_no = $user[0]->member_phone;
//        dd($fetched_mobile_no);
        $member_password = str_random(10);

        $member_type = $user[0]->member_type;
        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');

        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

//        $encrypted_member_password = Crypt::encrypt($member_password);
        DB::table('members')
            ->where('member_hash', $TOKEN_NO)
            ->update(array('member_verification' => 'yes', 'member_password' => md5($user[0]->member_id . '_' . $pass_encrypt_code . '_' . $member_password)));

        $params = array([
            'emailYes' => '0',
            'emailTemplate'  => 'users::message',
            'emailSubject'  => 'personal signup',
            'emailBody' => 'Please collect your test report. Thank you. Analytical service cell, BCSIR,Dhaka. Tel: +8802-9671108',
            'emailHeader' => 'Successfully Saved the Application header',
            'emailAdd' => 'shahin.fci@gmail.com',
            'mobileNo' => $fetched_mobile_no,
//                     'mobileNo' => '01767957180',
            'smsYes'   => '1',
            'smsBody'  => 'Your account password is '.$member_password.'',
        ]);
        CommonFunction::sendMessageFromSystem($params);
        return true;
    }

    public function edit($id) {
        $user_id = Encryption::decodeId($id);
        $user_details = DB::table('members')
            ->where('member_id', $user_id)
            ->first();
        $user_types = MemberTypes::where('mt_is_registarable', '!=', -1)->lists('mt_type_name', 'mt_id');
        $user_desks = Lab::lists('lab_name', 'lab_id');
        return view('users::edit', ["users" => $user_details, "user_types" => $user_types, "user_desks" =>$user_desks]);
    }

    public function update($id, UserEditRequest $request) {
        $user_id = Encryption::decodeId($id);

        $current_user_type = UsersModel::where('member_id', $user_id)
            ->pluck('member_status');
        $changed_user_type = $request->get('member_type');
        UsersModel::where('member_id', $user_id)->update([
            'lab_id' => $request->get('lab_id'),
            'member_phone' => $request->get('member_phone'),
            'member_email' => $request->get('member_email'),
            'member_nid' => $request->get('member_nid'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        if ($current_user_type != $changed_user_type) {
            $this->changeTypePass($id, $request);
        }

        \Session::flash('success', "User's Profile has been updated Successfully! An email has been sent to notify the user.");
        return redirect('users/edit/' . $id);

    }

    public function changeTypePass($id, Request $request) {
        $user_id = Encryption::decodeId($id);

        $member_details = UsersModel::where('member_id', $user_id)->first();
        $email = $member_details->member_email;

        $member_type = $request->get('member_type');
        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');

        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

        $member_password = str_random(10);

        UsersModel::where('member_id', $user_id)->update([
            'member_type' => $request->get('member_type'),
        ]);
        if ($member_details->member_social_type == '') {
            UsersModel::where('member_id', $user_id)->update([
                'member_password' => md5($user_id . '_' . $pass_encrypt_code . '_' . $member_password)
            ]);

            $body_msg = "Your Profile has been updated successfully.
                                          <br/><br/><br/>For security purpose, your password has been changed.
                                          <br/>Your New Password is: $member_password
                                       <br/>< Please change your password after login to keep it secure.";
        } else {
            $body_msg = "Your Profile has been updated successfully.
                                          <br/><br/>Please login to your BCSIR profile to see this update.";
        }

        $data = array(
            'header' => 'Notification of changing Profile',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function($message) use ($email) {
            $message->from('no-reply@bcsir.com', 'BCSIR')
                ->to($email)
                ->subject('Profile Update from BCSIR');
        });

        \Session::flash('success', "User's Profile has been activated Successfully! An email has been sent notifying the user.");
        return redirect('users/lists');
    }

    public function activate($id) {
        $user_id = Encryption::decodeId($id);

        $user_active_status = DB::table('members')
            ->where('member_id', $user_id)
            ->pluck('member_status');

        if ($user_active_status == 'active') {
            UsersModel::where('member_id', $user_id)->update([
                'member_status' => 'inactive'
            ]);
            \Session::flash('error', "User's Profile has been deactivated Successfully!");
        } else {
            UsersModel::where('member_id', $user_id)->update([
                'member_status' => 'active'
            ]);
            \Session::flash('success', "User's Profile has been activated Successfully!");
        }

        return redirect('users/lists/');
    }

    public function assign($id) {
        $user_id = Encryption::decodeId($id);
        $user_details = DB::table('members')
            ->where('member_id', $user_id)
            ->first();

        $user_types = MemberTypes::where('mt_is_registarable', 2)->lists('mt_type_name', 'mt_id');
        $user_desks = Lab::lists('lab_name', 'lab_id');
        return view('users::assign', compact("user_details", "user_desks", "user_types"));
    }

    public function assignTypeDesk($id, Request $request) {
        $user_id = Encryption::decodeId($id);

        $member_details = UsersModel::where('member_id', $user_id)->first();
        $email = $member_details->member_email;
        $member_type = $request->get('member_type');

        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');

        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

        $member_password = str_random(10);
        UsersModel::where('member_id', $user_id)->update([
            'member_type' => $request->get('member_type'),
            'lab_id' => $request->get('lab_id'),
        ]);

        if ($member_details->member_social_type == '') {
            UsersModel::where('member_id', $user_id)->update([
                'member_password' => md5($user_id . '_' . $pass_encrypt_code . '_' . $member_password)
            ]);

            $body_msg = "Your desk has been assigned successfully.
                                          <br/>For security purpose, your password has been changed.
                                          <br/><br/>Your New Password is: $member_password
                                        Please change your password after login to keep it secure.";
        } else {
            $body_msg = "Your desk has been assigned successfully.
                                          <br/><br/>Please login to your BCSIR profile to see this update.";
        }

        $data = array(
            'header' => 'Desk Assign Update',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function($message) use ($email) {
            $message->from('no-reply@bcsir.com', 'BCSIR')
                ->to($email)
                ->subject('Desk Assign Related Update from BCSIR');
        });


        \Session::flash('success', "User's Profile has been activated Successfully! An email has been sent notifying the user.");
        return redirect('users/lists');
    }

// prepared by: Tonoy Bhattacharjee Date: 07-10-2015
    public function verify($confirmationCode) {
        $TOKEN_NO = $confirmationCode;

        $user = DB::select("select member_id, member_email, member_type from members where member_hash = '" . $TOKEN_NO . "'");

        if (!$user) {
            \Session::flash('error', 'Invalid token!');
            return redirect('users/create');
        }

        $fetched_email_address = $user[0]->member_email;
        $member_password = str_random(10);

        $member_type = $user[0]->member_type;
        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');

        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

//        $encrypted_member_password = Crypt::encrypt($member_password);
        DB::table('members')
            ->where('member_hash', $TOKEN_NO)
            ->update(array('member_verification' => 'yes', 'member_password' => md5($user[0]->member_id . '_' . $pass_encrypt_code . '_' . $member_password)));

        
        $data = array(
            'header' => 'Verify your email address',
            'member_password' => $member_password
        );
        
        \Mail::send('users::activation', $data, function($message) use ($fetched_email_address) {
            $message->from('bcsir@gmail.com', 'BCSIR');
            $message->to($fetched_email_address);
            $message->subject('Account details');
        });

        \Session::flash('success', 'Please check your email for the  account activation message.');
        return redirect('users/login');
    }



    public function login() {

        //getting last four months from current month
        $strLastFour = "";
        $arrAppCount = 0;
        $arrMonthCount = array();
        $month = "";
        $stocksTable = LavachartsFacade::DataTable();

        $stocksTable->addColumns(array(
            array('date', 'Month Name'),
            array('number', 'Applications')
        ));

        for($i = 1; $i <= 4; $i++){
            $month = date('Y-m', strtotime('-'. $i .' month'));
            $arrAppCount = DB::select(DB::raw("select count(*) as countt from `apps_details` where DATE_FORMAT(created_at, '%Y-%m') = '$month'"))[0];
            $arrAppCount = $arrAppCount->countt;
            $arrMonthCount[$month] = $arrAppCount;
            $rowData = array(
                $month, $arrAppCount
            );

            $stocksTable->addRow($rowData);
        }

        LavachartsFacade::ColumnChart('Stocks')
            ->setOptions(array(
                'datatable' => $stocksTable,
                'title' => 'Applications'
            ));

        return view('users::login');
    }

    public function checklogin(LoginRequest $request) {
        if ($request->get('isValidUser')) {
            if ($request->get('statusCode') == "active") {
                /* if ($users[0]->security_profile_id > 0) {
                  if (!$this->verifySecurityProfile($users[0]->security_profile_id)) {
                  \Session::flash('error', 'Security Profile does not support login from this network or time!');
                  return Redirect('users/login')->with('status', 'error');
                  }
                  } */
                Session::put('member_id', $request->get('member_id'));
                Session::put('member_username', $request->get('member_username'));
                Session::put('member_nid', $request->get('member_nid'));
                Session::put('member_pic', $request->get('member_pic'));
                Session::put('access_code', $request->get('access_code'));
                Session::put('member_type', $request->get('member_type'));
                Session::put('member_email', $request->get('member_email'));
                Session::put('lab_id', $request->get('lab_id'));

                $this->entryAccessLog();    // access log entry

                $member_type = $request->get('member_type');

                if ($member_type == 4) {
                    return Redirect('users/dashboard');
                } else if ($member_type == 5) {
                    return Redirect('apps');
                } else {
                    return Redirect('users/dashboard');
                }
            } elseif ($request->get('statusCode') == "nomatch") {
                \Session::flash('error', 'Incorrect Password.');
                return Redirect('users/login')->with('status', 'error');
            } else {
                \Session::flash('error', 'Your account has been deactivated. Contact with Administrator.');
                return Redirect('users/login')->with('status', 'error');
            }
        } else {

            \Session::flash('error', 'Incorrect Email. Please check your email.');
            return Redirect('users/login')->with('status', 'error');
        }
    }

    public function dashboard($year = '', $month = '', $day = '') {
        $session_member_id = session('member_id');
        $DeshboardObject = DB::table('dashboard_object')->where('db_obj_status', 1)->get();

        if ($year && $month) {
            $date = date('d-M-y', strtotime("$year-$month"));
        } else {
            $date = Carbon::now();
        }

        $event_ary = array();
        $event_title = '';
        $year = \Request::get('year');
        $days = '';
        $month = \Request::get('month');
        $month = ($month == '') ? date('m') : $month;
        $year = ($year == '') ? date('Y') : $year;
        $start_date = date("Y-m-d", strtotime("01-$month-$year"));
        $end_date = date("Y-m-t", strtotime("01-$month-$year"));
//dd($start_date.'-----'.$end_date);
        $prefs = array(
            'start_day' => 'sunday',
            'month_type' => 'long',
            'day_type' => 'long',
            'show_next_prev' => TRUE,
            'next_prev_url' => url('users/dashboard'),
            'template' => '

           {table_open}<table class="calendar">{/table_open}

           {heading_row_start}<tr>{/heading_row_start}

           {heading_previous_cell}<th><a href="{previous_url}"><img src="' . url() . '/back-end/dist/img/task-r-arrow.png" border="0"></a></th>{/heading_previous_cell}
           {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
           {heading_next_cell}<th><a href="{next_url}"><img src="' . url() . '/back-end/dist/img/task-l-arrow.png" border="0"></a></th>{/heading_next_cell}

           {heading_row_end}</tr>{/heading_row_end}
           {week_row_start}<tr>{/week_row_start}
           {week_day_cell}<th class="day_header">{week_day}</th>{/week_day_cell}
           {week_row_end}</tr>{/week_row_end}
           {cal_row_start}<tr>{/cal_row_start}

           {cal_cell_start}<td>{/cal_cell_start}

           {cal_cell_content}
           <span class="day_listing" id="day{day}">{day}</span>
            <a targrt="_blank" href="' . url() . '/apps/get-job-listby-date/' . $year . '-' . $month .'-'. "{day}".'">{content}</a>
           {/cal_cell_content}
           {cal_cell_content_today}
           <div class="today">
               <span class="day_listing" id="day{day}">{day}</span>
                   <a href="' . url() . '/users/dashboard/' . $year . '/' . $month . '/{day}">
                       <span class ="message" title ="{content}"> {content}</span>
                   </a>
           </div>
           {/cal_cell_content_today}

           {cal_cell_no_content}<span class="day_listing" id="day{day}">{day}</span>{/cal_cell_no_content}
           {cal_cell_no_content_today}
           <div class="today" >
           <span class="day_listing" id="day{day}">{day}
           </span></div>{/cal_cell_no_content_today}

           {cal_cell_blank}&nbsp;{/cal_cell_blank}

           {cal_cell_end}</td>{/cal_cell_end}

           {cal_row_end}</tr>{/cal_row_end}

           {table_close}</table>{/table_close}'
        );

        Calendar::initialize($prefs);
//        dd($year.'---'.$month);
        $calender = Calendar::generate($year, $month);
//        dd($calender);
        $sqls = "SELECT GROUP_CONCAT(lab_job separator '*') as lab_jobs, delivery_dates from
                    (SELECT CONCAT(tab.lab_name,':', count(tab.job_id)) as lab_job  , substr(tab.delivery_date, 1, 10) as delivery_dates from
                    (select lab.lab_name, ts.job_id, (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) as duration,
                    DATE_ADD(ts.created_at, INTERVAL (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) DAY) as delivery_date
                    from apps_test_sample ts
                    INNER JOIN tbl_lab lab on lab.lab_id = ts.lab_id
                    where SUBSTR(DATE_ADD(ts.created_at, INTERVAL (select max(ats.duration) from apps_test_sample ats where ats.job_id = ts.job_id) DAY),1,10)
                    between '$start_date' and '$end_date'
                    group by ts.job_id) tab
                    group by tab.lab_name, delivery_dates
                    order by delivery_dates ) tab1

                    group by tab1.delivery_dates";

        $list = DB::select( DB::raw($sqls));

        $labList = '';
        $m = '';
        $y = '';
        if (is_array($list)) {
            $index = 0;
            foreach ($list as $value) {
                $lab_job = explode("*",$value->lab_jobs);
                    $event_title = "<div class='boxes'>";
                foreach($lab_job as $job) {
                    $jobs = explode(":",$job);
                    $job_name = $jobs[0];
                    $total_job = $jobs[1];

                    $event_title .= "<span class='btn btn-mini btn-default btns'>" . $job_name.' ( '.$total_job.' )' . "</span>";
                }
                    $event_title .= "</div>";
                $event_date = $value->delivery_dates;
                $timestmp = strtotime($event_date);
                $d = date('j', $timestmp);
                $m = date('n', $timestmp);
                $y = date('Y', $timestmp);

                $event_ary[$d] = $event_title;
                $index++;
            }

            $labList['data'] = $event_ary;
            $labList['month'] = $month;
            $labList['year'] = $year;
//            $labList['days'] = $d;
            $labList['title'] = $event_title;

        }

        if (empty($session_member_id)) {
            return Redirect('/users/login');
        } else {
            $logged_in_user_details = DB::table('members')
                ->where('member_id', $session_member_id)
                ->first();
            $user_member_type = CommonFunction::getUserType();
            return view('users::dashboard', ["labList"=>$labList,"date"=>$date,"calender"=>$calender,"users" => $logged_in_user_details, "user_member_type" => $user_member_type, "DeshboardObject" => $DeshboardObject]);
        }
    }

    public function logout() {
        Session::flush();
        \Session::flash('success', 'Thank you for working with us');
        return Redirect('/users/login');
    }

    public function reSendEmail() {
        return view('users::reSendEmail');
    }

    public function reSendEmailConfirm() {
        //->select(DB::raw('TOKEN_NO'))
        $result = DB::table('members')
            ->where('member_email', '=', Input::get('member_email'))->first();

        $ACTIVE_STATUS = $result->member_status;
        if ($ACTIVE_STATUS == 'inactive') {
            \Mail::send('users::verify', array('TOKEN_NO' => $result->member_hash), function($message) {
                $message->from('no-reply@bcsir.com', 'BCSIR');
                $message->to(Input::get('member_email'));
                $message->subject('Re-Send Email from BCSIR');
            });
            \Session::flash('success', 'An email has been re-send to your address.<br/>
                            Please check the newest email and follow the instructions to complete the sign up process!');
        } elseif ($ACTIVE_STATUS == 'active') {
            $body_msg = "Your account is already activated.Please check your mail for account details";

            $data = array(
                'header' => 'Account Activation Status for BCSIR',
                'param' => $body_msg
            );
            $email = Input::get('member_email');

            \Mail::send('users::message', $data, function($message) use ($email) {
                $message->from('bcsir@gmail.com', 'BCSIR')
                    ->to($email)
                    ->subject('Account Activation Status from BCSIR');
            });

            \Session::flash('success', 'Please check your email for new update!');
        }

        return redirect('users/login');
    }

    public function loginWithGoogle(Request $request, UsersModel $usersmodel) {
        // get data from request
        $code = $request->get('code');
        $data = array();

        // get google service
        $googleService = \OAuth::consumer('Google');

        // check if code is valid
        // if code is provided get user data and sign in
        if (!is_null($code)) {
            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

            $email = $result['email'];
            $member_social_id = $result['id'];
            $type = 'google';

            $user = $usersmodel->check_availability_status('member_email', $email, $member_social_id, $type);
            $thirtydigit_random_number = str_random(30);

            $result = array(
                'member_social_type' => 'google',
                'member_social_id' => $result['id'],
                'member_first_name' => $result['name'],
                'member_email' => $result['email'],
                'member_username' => $result['given_name'],
                'member_hash' => $thirtydigit_random_number,
                'member_pic' => $result['picture'],
                'member_status' => 'inactive',
                'member_verification' => 'no'
            );

            return view('users::socialPage', compact('result'));
        }
        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to google login url
            return redirect((string) $url);
        }
    }

    public function saveGoogleInfo(Request $request, UsersModel $usersmodel) {
        //$response_data = ['response_code' => '99'];
        //$email = $request->get('member_email');
        /* dd($request->all()); */
        if ($request->get('statusCode') == "signup") {
            $email = $request->get('member_email');

            $body_msg = "Thanks for creating an account with BCSIR. Please follow the link below to verify your email
            address  " . url('users/social_registration/' . \Session::getId());
            $body_msg .= "<br/>If you have problems, please paste the above URL into your web browser.";
            $data = array(
                'header' => 'Verify your email address',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($email) {

                $message->from('bcsir@gmail.com', 'BCSIR')
                    ->to($email)
                    ->subject('Verify your email address');
            });
            \Session::flash('success', 'Thanks for signing up! Please check your email and follow the instructions to complete the sign up process');

            return redirect('users/login');
        } elseif ($request->get('statusCode') == "inactive") {
            \Session::flash('error', 'Your account has been deactivated. Please contact Administrator.');
            return redirect('users/login');
        } elseif ($request->get('statusCode') == "notverified") {
            \Session::flash('error', 'Please check your email and follow the instructions to complete the sign up process or contat Administrator.');
            return redirect('users/login');
        } elseif ($request->get('statusCode') == "complete") {
            Session::put('member_id', $request->get('member_id'));
            Session::put('member_username', $request->get('member_username'));
            Session::put('member_nid', $request->get('member_nid'));
            Session::put('member_pic', $request->get('member_pic'));
            Session::put('access_code', $request->get('access_code'));
            Session::put('member_type', $request->get('member_type'));
            Session::put('member_email', $request->get('member_email'));
            Session::put('lab_id', $request->get('lab_id'));

            $this->entryAccessLog();    // access log entry
            return Redirect('users/dashboard');
        }
    }

    public function message() {
        return view('users::message');
    }

    public function social_registration($confirmationCode) {
        $user_types = MemberTypes::where('mt_is_registarable', 1)
            ->lists('mt_type_name', 'mt_id');

        return view('users::socialregistration', compact('user_types'))->with('confirmationCode', $confirmationCode);
    }

    function social_reg_store($confirmationCode, SocialRegRequest $request, UsersModel $usersmodel) {
        $TOKEN_NO = $confirmationCode;
        $user = DB::select("select member_email from members where member_hash = '" . $TOKEN_NO . "'");
        if (!$user) {
            \Session::flash('error', 'Invalid token!');
            return redirect('users/login');
        }
        $data = array(
            'member_type' => $request->get('member_type'),
            'member_nid' => $request->get('member_nid'),
            'member_DOB' => $request->get('member_DOB'),
            'member_phone' => $request->get('member_phone'),
            'member_status' => 'active',
            'member_verification' => 'yes'
        );
        $usersmodel->chekced_verified($TOKEN_NO, $data);


        \Session::flash('success', 'Thanks for signing up!');
        return redirect('users/login');
    }

    public function profileinfo() {
        $session_member_id = session('member_id');
        if (session('member_id') != "") {
            $user_details = DB::table('members')
                ->where('member_id', $session_member_id)
                ->first();
            return view('users::profileinfo', ["users" => $user_details]);
        } else {
            return Redirect('users/login');
        }
    }

    public function profile_update(profileEditRequest $request, UsersModel $usersmodel) {
        $session_member_id = session('member_id');
        if (session('member_id') != "") {
            $data = array
            (
                'member_first_name' => $request->get('member_first_name'),
                'member_DOB' => $request->get('member_DOB'),
                'member_phone' => $request->get('member_phone'),
                'member_username' => $request->get('member_username'),
            );
            $usersmodel->profile_update('members', 'member_id', $session_member_id, $data);
            $image = Input::file('image');
            if ($image) {
                $image->getClientOriginalName();
                $image->move('users/upload', $image->getClientOriginalName());
                $imgdata = array
                (
                    'member_pic' => $image->getClientOriginalName()
                );
                $usersmodel->profile_update('members', 'member_id', $session_member_id, $imgdata);
            }
            \Session::flash('success', 'Your profile has been updated successfully.');
            return redirect('users/profileinfo');
        } else {
            return Redirect('users/login');
        }
    }

    public function forgetPassword() {
        return view('users::forgetPassword');
    }

    //from forget password from link by user with email input
    public function resetPass(Request $request) {		
        $email = $request->get('member_email_phone');
		
        $users = DB::table('members')
            ->where('member_email', $email)
            ->get();
			
		$usersByMobile = DB::table('members')
		->where('member_phone', $email)
		->first();			

        $users_social_id = DB::table('members')->where('member_email', $email)->pluck('member_social_id');

        if ($users == true) {
            if ($users_social_id == "") { //for users sign up using general methods
                $token_no = hash('SHA256', "-" . $email . "-");

                $update_token_in_db = array(
                    'member_hash' => $token_no,
                );
                DB::table('members')
                    ->where('member_email', $email)
                    ->update($update_token_in_db);

                $encryted_token = \Crypt::encrypt($token_no);
                $verify_link = 'users/passVerification/' . ($encryted_token);

                $body_msg = "A request has been made to change your password.<br/>
                                        Please follow this link to reset your password.<br/>" . url($verify_link);
                $body_msg .= "<br/><br/>If you have any problem, please copy and paste this URL into your web browser.<br/><br/>
                                         <br/>If you didn't make any request to change your password, please ignore this mail completely.
                                         <br/><br/>Thanks<br/>Administration";

                $data = array(
                    'header' => 'Click to Reset your Password',
                    'param' => $body_msg
                );

                \Mail::send('users::message', $data, function($message) use ($email) {
                    $message->from('bcsir@gmail.com', 'BCSIR')
                        ->to($email)
                        ->subject('Process to change Password');
                });

                \Session::flash('success', 'Please check your email to verify Password Change');
                return redirect('users/login');
            } else { // if user is signed up using google
                \Session::flash('error', 'This email address is signed up using Google. No password is found to reset! Please try Google login.');
                return redirect('users/login');
            }
        } else if($usersByMobile == true){
			
        $fetched_mobile_no = $usersByMobile->member_phone;       
        $member_password = str_random(10);
		//dd($member_password);
        $member_type = $usersByMobile->member_type;
        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');

        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

//        $encrypted_member_password = Crypt::encrypt($member_password);
        DB::table('members')
            ->where('member_phone', $fetched_mobile_no)
            ->update(array('member_verification' => 'yes', 'member_password' => md5($usersByMobile->member_id . '_' . $pass_encrypt_code . '_' . $member_password)));

        $params = array([
            'emailYes' => '0',
            'emailTemplate'  => 'users::message',
            'emailSubject'  => 'personal signup',
            'emailBody' => 'Please collect your test report. Thank you. Analytical service cell, BCSIR,Dhaka. Tel: +8802-9671108',
            'emailHeader' => 'Successfully Saved the Application header',
            'emailAdd' => 'shahin.fci@gmail.com',
            'mobileNo' => $fetched_mobile_no,
			//'mobileNo' => '01767957180',
            'smsYes'   => '1',
            'smsBody'  => 'Your account password is '.$member_password.'',
        ]);
        CommonFunction::sendMessageFromSystem($params);	
		\Session::flash('success', 'Please check your phone to verify Password Change.');
        return redirect('users/login');
		} else {
            \Session::flash('error', 'No user with this email is existed in our current database. Please sign-up first');
            return Redirect('users/create')->with('status', 'error');
        }
    }

    //reset password from user list by Admin
    public function resetPassword($id) {
        $user_id = Encryption::decodeId($id);
        $password = str_random(10);

        $user_active_status = DB::table('members')->where('member_id', $user_id)->pluck('member_status');
        $email_address = DB::table('members')->where('member_id', $user_id)->pluck('member_email');
        $member_type = DB::table('members')->where('member_id', $user_id)->pluck('member_type');

        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');
        $pass_encrypt = explode('_', $member_access_para);
        if ($pass_encrypt[0] != ''){
            $pass_encrypt_code = $pass_encrypt[1];
        }
        else{
            \Session::flash('error', "User ID is not activated! Password can not be changed");
            return redirect('users/lists');
        }

        if ($user_active_status == 'active') {
            UsersModel::where('member_id', $user_id)->update([
                'member_password' => md5($user_id . '_' . $pass_encrypt_code . '_' . $password)
            ]);

            $body_msg = '<span style="color:#000;text-align:justify;"><b>';
            $body_msg .= 'Congratulations!</b><br/><br/>';
            $body_msg .= 'Your password has been reset successfully by BCSIR Admin.';
            $body_msg .= '<br/>New Password :' . $password;
            $body_msg .= '</span><br/><br/><br/>';
            $body_msg .= 'Please change your password after login to ensure security.';
            $body_msg .= '<br/><br/><br/>Thanks<br/><br/>';
            $body_msg .= '<b>Bangladesh Council of Scientific & Industrial Research</b>';
            $data = array(
                'header' => 'Password Reset Information',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function($message) use ($email_address) {
                $message->from('no-reply@bcsir.com', 'BCSIR')
                    ->to($email_address)
                    ->subject('Password Reset Information');
            });
            \Session::flash('success', "User's Password has been reset Successfully! An email has been sent to the user!");
        } else {
            \Session::flash('error', "User ID is not activated! Password can not be changed");
        }
        return redirect('users/lists');
    }

    //updated by user
    public function updatePassFromProfile(Request $request) {

        $session_member_id = session('member_id');

        //$email = $user_details->member_email; //$request->get('member_email');
        $old_password = $request->get('member_old_password');
        $new_password = $request->get('member_new_password');

        $users = DB::table('members')
            ->where('member_id', $session_member_id)
            ->get();

        $member_id = $users[0]->member_id;
        $member_type = $users[0]->member_type;
        $member_access_para = MemberTypes::where('mt_id', $member_type)
            ->pluck('mt_access_code');
        $pass_encrypt = explode('_', $member_access_para);
        $pass_encrypt_code = $pass_encrypt[1];

        $old_encrypted_password = md5($member_id . '_' . $pass_encrypt_code . '_' . $old_password);

        $password_match = DB::table('members')
            ->where('member_password', $old_encrypted_password)
            ->pluck('member_id');

        if ($password_match == $session_member_id) {
            DB::table('members')
                ->where('member_id', $member_id)
                ->update(array('member_password' => md5($member_id . '_' . $pass_encrypt_code . '_' . $new_password)));

            \Session::flash('success', 'Your Password has been Changed Successfully');
            return redirect('users/login');
        } else {
            \Session::flash('error', 'Password did not match!!');
            return Redirect('users/profileinfo')->with('status', 'error');
        }
    }

    function passVerification($token_no) {
        $TOKEN_NO = \Crypt::decrypt($token_no);
        $user = DB::select("select member_id,member_email, member_type from members where member_hash = '" . $TOKEN_NO . "'");

        if ($user) {
            $fetched_email_address = $user[0]->member_email;
            $member_password = str_random(10);

            $member_type = $user[0]->member_type;
            $member_access_para = MemberTypes::where('mt_id', $member_type)
                ->pluck('mt_access_code');

            $pass_encrypt = explode('_', $member_access_para);
            $pass_encrypt_code = $pass_encrypt[1];

            DB::table('members')
                ->where('member_hash', $TOKEN_NO)
                ->update(array('member_password' => md5($user[0]->member_id . '_' . $pass_encrypt_code . '_' . $member_password)));

            $body_msg = "Your New Password is: " . $member_password;
            $body_msg .= "<br/> For security purpose, please change your password after you logged in to the system.
                                            <br/><br/>Thanks<br/>Administration";

            $data = array(
                'header' => 'Your New Password Details',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function($message) use ($fetched_email_address) {
                $message->from('bcsir@gmail.com', 'BCSIR')
                    ->to($fetched_email_address)
                    ->subject('Your Password is changed');
            });


            \Session::flash('success', 'Your password has been reset successfully! Please check your mail for credential details');
            return redirect('users/login');
        } else { /* If User couldn't be found */
            \Session::flash('error', 'Invalid token! No such user is found. Please sign up first.');
            return redirect('users/create');
        }
    }

    public function lists(UsersModel $usersmodel) {
        $session_member_id = session('member_id');
        if ($session_member_id == "") {
            return Redirect('users/login');
        }
        $user_list = $usersmodel->getUserList();
        $total_user_list = $usersmodel->getTotalUserList();

        return view('users::user_list', ["lists" => $user_list, "total" => $total_user_list])->with('title', 'User List');
    }

    public function listByType($type, UsersModel $usersmodel) {
        $session_member_id = session('member_id');
        if ($session_member_id == "") {
            return Redirect('users/login');
        }

        $user_list = $usersmodel->getListWhere('member_type', $type);
        $total_user_list = $usersmodel->getTotalUserList();

        return view('users::user_list', ["lists" => $user_list, "total" => $total_user_list])->with('title', 'User List');
    }

    public function create_new() {
        $session_member_id = session('member_id');
        if ($session_member_id == "") {
            return Redirect('users/login');
        }
        return view('users::user_new')->with('title', 'Create New User');
    }

    function verifySecurityProfile($id) {
        $ip = $this->input->ip_address();
        $nets = explode('.', $ip);
        $weekname = strtoupper(date('D'));
        if (count($nets) == 4) {
            $net = $nets[0] . '.' . $nets[1] . '.' . $nets[2] . '.0';
        } else {
            $net = '0.0.0.0';
        }
        $result = $this->db->select('id')
            ->where('id', $id)
            ->where('active_status', 'yes')
            ->where("time(now()) BETWEEN work_hour_start AND work_hour_end")
            ->not_like('week_off_days', $weekname)
            ->where("(" . $this->LikeCond('ALLOWED_REMOTE_IP', '0.0.0.0') . " OR " . $this->LikeCond('ALLOWED_REMOTE_IP', $net) . " OR " . $this->LikeCond('ALLOWED_REMOTE_IP', $ip) . ")")
            ->limit(1)
            ->get('security_profile')->row()->id;

        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

    function LikeCond($filed, $value) {
        return $filed . " like '%" . $value . "%'";
    }

    public function entryAccessLog() {
        // access_log table.
//        $data = array('user_id' => $this->session->userdata('sess_user_id'),
//            'login_ip' => $this->input->ip_address(),
//            'browser_info' => $this->input->user_agent()
//        );
//       $this->db->set('login_dt', 'now()', FALSE);
//        $this->db->insert('access_log', $data);
//        $this->session->set_userdata('sess_access_log_id', $this->db->insert_id());
    }

    public function entryAccessLogout() {
        $this->db->set('logout_dt', 'now()', FALSE);
        $this->db->where('id', $this->session->userdata('sess_access_log_id'));
        $this->db->update('access_log', $data);
    }

    public function support() {
        return view("users::support");
    }

    /*     * ******************* End of Users Controller Class ********************** */
}
