@extends('master-admin-default')

@section('title')
    <title>BCSIR :: List of Reports</title>
@endsection

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-11 col-lg-11"  style="margin-top:10px;">

        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}
        <div class="panel panel-primary">

            <div class="panel-heading">
                <div class="">
                    <?php if(Session::get('member_type') == 1) {  ?>
                    <a class="" href="{{ url('/reports/create') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> Add New Report', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                    </a>
                    <?php } ?>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                    <table id="list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Report Title</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($getList['result'] as $row)
                            <tr>
                                <td>{!! $row->report_title !!}</td>
                                <td>{!! $row->status==1? '<span class="text-success">Published</span>':'<span class="text-warning">Un-published</span>' !!}</td>
                                <td>
                                    <a href="{!! url('reports/view/'. Encryption::encodeId($row->report_id)) !!}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-folder-open-o"></i> Open
                                    </a>
                                    <?php if(Session::get('member_type') == 1) {  ?>
                                    {!! link_to('reports/edit/'. Encryption::encodeId($row->report_id),'Edit',['class' => 'btn btn-default btn-xs']) !!}
                                    <?php } ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
    </div> <!-- /.col-lg-12 -->
</div><!--/row -->
</div><!--/container -->
@endsection

@section('footer-script')
<script src="{{ asset("assets/scripts/datatable/jquery.dataTables.min.js") }}" src="" type="text/javascript"></script>
<script src="{{ asset("assets/scripts/datatable/dataTables.bootstrap.min.js") }}" src="" type="text/javascript"></script>
<script src="{{ asset("assets/scripts/datatable/dataTables.responsive.min.js") }}" src="" type="text/javascript"></script>
<script src="{{ asset("assets/scripts/datatable/responsive.bootstrap.min.js") }}" src="" type="text/javascript"></script>
<script>

$(function () {
    $('#list').DataTable({
        "paging": true,
        "lengthChange": true,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "iDisplayLength": 25
    });
});

</script>
@endsection