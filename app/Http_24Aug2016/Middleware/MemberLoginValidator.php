<?php

namespace App\Http\Middleware;

use Closure;
use App\Modules\Users\Models\UsersModel AS Users;
use App\Modules\Membertype\Models\Membertype AS MemTypes;
use Session;
use DB;

class MemberLoginValidator
{
    /**
     * Custom parameters.
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     *
     * @api
     */
    public $attributes;


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        if (!empty($request->get('member_social_type'))) { //google login process

            $Users = new Users();

            $email = $request->get('member_email');
            $member_social_id = $request->get('member_social_id');
            $type = $request->get('member_social_type');

            $userCheck = $Users->check_availability_status('member_email', $email, $member_social_id);
            //dd($user);
            if (empty($user)) {

                $userEntryCheck = $Users->checkUserByEmail($email);

                if(empty($userEntryCheck)){
                    $member_id = DB::table('members')->insertGetId(
                        array(
                            'member_social_id' => $member_social_id,
                            'member_first_name' => $request->get('member_first_name'),
                            'member_email' => $email,
                            'member_username' => $request->get('member_username'),
                            'member_hash' => \Session::getId(),
                            'member_pic' => $request->get('member_pic'),
                            'member_status' => 'inactive',
                            'member_verification' => 'no'
                        )
                    );

                    $request->attributes->add(array(
                        'statusCode' => "signup",
                        'isValidUser' => true,
                        'member_id' => $member_social_id,
                        'member_first_name' => $request->get('member_first_name'),
                        'member_email' => $email,
                        'member_username' => $request->get('member_username'),
                        'member_nid' => $request->get('member_nid'),
                        'member_pic' => $request->get('member_pic'),
                    ));

                } else {


                    $updateUserSocialID = array(
                        'member_social_id' => $member_social_id,
                        'member_verification' => 'yes'
                    );
                    DB::table('members')
                        ->where('member_email', $email)
                        ->update($updateUserSocialID);

                    $request->attributes->add(array(
                        'statusCode' => "complete",
                        'isValidUser' => true,
                        'member_id' => $userEntryCheck[0]->member_id,
                        'member_first_name' => $request->get('member_first_name'),
                        'member_email' => $email,
                        'member_username' => $request->get('member_username'),
                        'member_nid' => $request->get('member_nid'),
                        'member_pic' => $request->get('member_pic'),
                        'member_type' => $userEntryCheck[0]->member_type,
                        'lab_id' => $userEntryCheck[0]->lab_id
                    ));
                }

            } elseif(!empty($user) && $user[0]->member_status == 'inactive') {
                $request->attributes->add(array(
                    'statusCode' => "inactive",
                    'isValidUser' => true,
                    'member_id' => $user[0]->member_id,
                    'member_first_name' => $user[0]->member_first_name,
                    'member_email' => $user[0]->member_email,
                    'member_username' => $user[0]->member_username,
                    'member_nid' => $user[0]->member_nid,
                    'member_pic' => $user[0]->member_pic,
                    'security_profile_id' => $user[0]->security_profile_id,

                ));
            } elseif(!empty($user) && $user[0]->member_status == 'active'){

                if (!empty($user) && $user[0]->member_verification == 'no') {
                    $request->attributes->add(array(
                        'statusCode' => "notverified",
                        'isValidUser' => false,
                        'member_id' => $user[0]->member_id,
                        'member_first_name' => $user[0]->member_first_name,
                        'member_email' => $user[0]->member_email,
                        'member_username' => $user[0]->member_username,
                        'member_nid' => $user[0]->member_nid,
                        'member_pic' => $user[0]->member_pic,
                        'security_profile_id' => $user[0]->security_profile_id,

                    ));
                } elseif (!empty($user) && $user[0]->member_verification == 'yes') {

                    $request->attributes->add(array(
                        'statusCode' => "complete",
                        'isValidUser' => true,
                        'member_id' => $user[0]->member_id,
                        'member_first_name' => $user[0]->member_first_name,
                        'member_email' => $user[0]->member_email,
                        'member_username' => $user[0]->member_username,
                        'member_nid' => $user[0]->member_nid,
                        'member_pic' => $user[0]->member_pic,
                        'security_profile_id' => $user[0]->security_profile_id,

                    ));
                }
            }

        } else { //normal login process
            $memberEmail = $request->get('member_email');
            $memberPass = $request->get('member_password');

            $Users = new Users();
            $is_mobile = 0;
            $memberType = $Users->checkEmailAndGetType($memberEmail);
//            dd($memberType);
            if (empty($memberType)) {
//                dd('mobile');
                $is_mobile = 1;
                $memberType = $Users->checkMobileAndGetType($memberEmail);

            }

            if ($memberType > 0) {

                $MemTypes = new MemTypes();
                $MemTypesData = $MemTypes->getMemberTypeData($memberType);

                $accessCode = $MemTypesData[0]->mt_access_code;
                $arrAccessCode = explode('_', $accessCode);
                $accessCodePart = $arrAccessCode[1];

                $memberId = $Users->checkEmailAndGetMemId($memberEmail,$is_mobile);

                $wholePass = $memberId.'_'.$accessCodePart.'_'.$memberPass;

//echo $memberId.'_'.$accessCodePart.'_'.$memberPass; dd($wholePass);
                $usrUniquePass = md5($wholePass);

                $userInfo = $Users->checkLoginCredential($memberEmail, $usrUniquePass,$is_mobile);
                if (count($userInfo) == 0) {
                    $request->attributes->add(array(
                        'statusCode' => "nomatch",
                        'isValidUser' => true,
                    ));
                } else {

                    if($userInfo[0]->member_status == "inactive"){
                        $request->attributes->add(array(
                            'statusCode' => "inactive",
                            'isValidUser' => true,
                            'member_id' => $userInfo[0]->member_id,
                            'member_first_name' => $userInfo[0]->member_first_name,
                            'member_email' => $userInfo[0]->member_email,
                            'access_code' => $accessCodePart,
                            'member_type' => $userInfo[0]->member_type,
                            'member_username' => $userInfo[0]->member_username,
                            'member_nid' => $userInfo[0]->member_nid,
                            'member_pic' => $userInfo[0]->member_pic,
                            'lab_id' => $userInfo[0]->lab_id,
                            'security_profile_id' => $userInfo[0]->security_profile_id,

                        ));
                    } else {
                        $request->attributes->add(array(
                            'statusCode' => "active",
                            'isValidUser' => true,
                            'member_id' => $userInfo[0]->member_id,
                            'member_first_name' => $userInfo[0]->member_first_name,
                            'member_email' => $userInfo[0]->member_email,
                            'access_code' => $accessCodePart,
                            'member_type' => $userInfo[0]->member_type,
                            'member_username' => $userInfo[0]->member_username,
                            'member_nid' => $userInfo[0]->member_nid,
                            'member_pic' => $userInfo[0]->member_pic,
                            'lab_id' => $userInfo[0]->lab_id,
                            'security_profile_id' => $userInfo[0]->security_profile_id,

                        ));
                    }
                }
            }
        }




        return $next($request);
    }
}
