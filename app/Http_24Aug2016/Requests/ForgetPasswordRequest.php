<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ForgetPasswordRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        //dd($_POST);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'member_email' => 'required|email',
        ];
    }

    public function messages() {
        return [
            'member_email.required' => 'Email Address field is required',
        ];
    }

}
