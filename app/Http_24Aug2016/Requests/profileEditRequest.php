<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class profileEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_first_name' => 'required',           // Name
            'member_DOB' => 'required',                         //Date of Birth
            'member_phone' => 'required',                      //Mobile Number 
            'member_username' => 'required'             //User Name
        ];
    }
    public function messages() {
        return [
            'member_first_name.required' => 'Name field is required',
            'member_DOB.required' => 'Date of Birth field is required',
            'member_phone.required' => 'Mobile Number field is required',
            'member_username.required' => 'User Name field is required'                       
        ];
    }    
}
