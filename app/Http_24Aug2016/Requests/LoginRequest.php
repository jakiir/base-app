<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        //dd($_POST);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'member_email' => 'required',
            'member_password' => 'required'
        ];
    }

    public function messages() {
        return [
            'member_email.required' => 'Email Address field is required',
            'member_email.unique' => 'Email Address must be unique',
            'member_password.required' => 'Password field is required',
        ];
    }

}
