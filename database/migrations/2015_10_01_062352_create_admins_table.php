<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('admins', function(Blueprint $table) {
            $table->integer('admin_id', true);
            $table->string('admin_full_name', 150);
            $table->string('admin_email', 100)->index('IndexingAdminEmail');
            $table->string('admin_password', 50);
            $table->string('admin_hash', 50);
            $table->integer('admin_type');
            $table->enum('admin_status', array('active', 'inactive'));
            $table->timestamp('admin_last_login')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('admin_language', 10);

            $table->timestamps();
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('admins');
    }

}
