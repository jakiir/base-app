<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSecurityProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('security_profile', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('profile_name', 50);
			$table->string('allowed_remote_ip', 100);
			$table->string('week_off_days', 100);
			$table->time('work_hour_start');
			$table->time('work_hour_end');
			$table->enum('active_status', array('yes','no'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('security_profile');
	}

}
