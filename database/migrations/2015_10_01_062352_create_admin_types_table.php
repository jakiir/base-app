<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('admin_types', function(Blueprint $table) {
            $table->integer('at_id', true);
            $table->string('at_type_title',200);
            $table->integer('at_is_registerable');
            $table->string('at_access_code', 20);
            $table->text('at_permission', 65535);
            $table->enum('at_status', array('active', 'inactive'))->default('inactive');
            

            $table->timestamps();
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('admin_types');
    }

}
