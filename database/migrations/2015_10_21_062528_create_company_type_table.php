<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyTypeTable extends Migration {

    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('company_type', function(Blueprint $table) {
            $table->integer('ct_id', true);
            $table->string('ct_name', 64);
            $table->boolean('ct_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('company_type');
    }

}
