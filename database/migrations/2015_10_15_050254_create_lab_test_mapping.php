clear
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabTestMapping extends Migration
{
    /**
     * Run the migrations.
     *

     * @return void
     */
    public function up()
    {
        Schema::create('tbl_lab_test_mapping', function (Blueprint $table) {
            $table->increments('lt_id');
            $table->integer('lab_id');
            $table->integer('tst_id');
            $table->integer('price');
            $table->integer('duration');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_lab_test_mapping');
    }
}
