<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessListHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('process_list_history', function(Blueprint $table)
		{
			$table->bigInteger('process_id')->index('process_id');
			$table->integer('process_type');
			$table->integer('initiated_by');
			$table->integer('closed_by');
			$table->integer('desk_id');
			$table->integer('status_id');
			$table->integer('record_id');
			$table->string('process_desc');
			$table->dateTime('updated_on');
			$table->integer('updated_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('process_list_history');
	}

}
