<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaskListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_list', function(Blueprint $table)
		{
			$table->bigInteger('task_id', true);
			$table->integer('task_type');
			$table->integer('initiated_by');
			$table->integer('closed_by');
			$table->integer('assigned_to');
			$table->enum('status', array('Initiated','Approved','Cancel','Hold'));
			$table->integer('record_id');
			$table->string('task_desc');
			$table->dateTime('updated_on');
			$table->dateTime('updated_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_list');
	}

}
