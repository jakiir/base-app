<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('companies', function (Blueprint $table) {
            $table->integer('company_id', true);
            $table->string('company_name', 80);
            $table->string('company_type', 40);
            $table->string('company_house_no', 40);
            $table->string('company_flat_no', 40)->nullable();
            $table->string('company_street', 80);
            $table->string('company_area', 80);
            $table->string('company_city', 50);
            $table->string('company_zip', 40)->nullable();
            $table->string('company_fax', 80)->nullable();
            $table->string('company_web', 120)->nullable();
            $table->string('company_status', 32);
            $table->string('incorporation_certificate', 80)->nullable();
            $table->string('partnership_deed', 80)->nullable();
            $table->string('com_trade_license', 80)->nullable();
            
            $table->string('company_tin', 64)->nullable();            
            $table->string('head_of_org', 48)->nullable();            
            $table->string('head_contact', 20)->nullable();
            $table->string('head_email', 30)->nullable();
            $table->string('contact_person', 48);
            $table->string('contact_phone', 20);
            $table->string('contact_email', 30)->nullable();
            $table->string('phone_no', 30);
            $table->string('fax_no', 30)->nullable();
            
            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('companies');
    }

}
