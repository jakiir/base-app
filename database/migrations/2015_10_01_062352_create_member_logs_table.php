<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('member_logs', function(Blueprint $table) {
            $table->integer('ML_id', true);
            $table->integer('ML_admin_id');
            $table->string('ML_ip_address', 100);
            $table->timestamp('ML_created_on')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->timestamps();
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('member_logs');
    }

}
