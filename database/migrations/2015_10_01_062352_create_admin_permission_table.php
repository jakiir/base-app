<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminPermissionTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('admin_permission', function(Blueprint $table) {
            $table->integer('ap_id', true);
            $table->integer('ap_at_id');
            $table->string('ap_module_name');
            $table->string('ap_action_name');

            $table->timestamps();
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('admin_permission');
    }

}
