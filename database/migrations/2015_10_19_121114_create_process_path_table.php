<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessPathTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('process_path', function(Blueprint $table)
		{
			$table->integer('process_type')->index('SERVICE_ID');
			$table->integer('status_from');
			$table->string('STATUS_TO', 20);
			$table->string('desk_from', 20);
			$table->string('desk_to', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('process_path');
	}

}
