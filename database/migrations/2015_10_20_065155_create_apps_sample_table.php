<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_test_sample', function (Blueprint $table) {
            $table->increments('ts_id');
            $table->integer('job_id');
            $table->integer('method_id');
            $table->integer('lab_id');
            $table->float('price');
            $table->integer('duration');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apps_test_sample');
    }
}
