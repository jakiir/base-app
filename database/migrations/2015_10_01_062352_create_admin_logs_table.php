<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('admin_logs', function(Blueprint $table) {
            $table->integer('al_id', true);
            $table->integer('al_admin_id');
            $table->string('al_ip_address', 100);

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('admin_logs');
    }

}
