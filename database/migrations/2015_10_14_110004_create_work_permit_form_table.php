<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkPermitFormTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_permit_form', function(Blueprint $table)
		{
			$table->bigInteger('APP_ID', true);
			$table->integer('VISA_PERMIT');
			$table->string('VISA_REF_NO', 65)->nullable();
			$table->string('TRACK_NO', 65);
			$table->integer('DEPT_ID');
			$table->integer('SERVICE_ID');
			$table->integer('PREV_USER_ID')->nullable();
			$table->integer('CURRENT_USER_ID');
			$table->integer('STATUS_ID');
			$table->integer('VISA_CATEGORY');
			$table->string('BANK_NAME', 256)->nullable();
			$table->string('BRANCH_NAME', 65);
			$table->string('BANK_ISSUE_DATE', 30)->nullable();
			$table->string('CHALLAN_NO', 20)->nullable();
			$table->string('CHALLAN_AMOUNT', 65);
			$table->string('CHALLAN_FILE', 65)->nullable();
			$table->text('RECOMMENDATION', 65535)->nullable();
			$table->integer('AGENDA_NO')->nullable();
			$table->string('MEETING_AGENDA', 40);
			$table->date('MEETING_DATE')->nullable();
			$table->string('MIN_RANGE_BASIC_SALARY', 60)->nullable();
			$table->string('MOHA_CLEARENCE', 65)->nullable();
			$table->boolean('CHECK_COND')->nullable();
			$table->string('CONDITIONS')->nullable();
			$table->string('ORG_CITY', 20);
			$table->string('ORG_NAME', 256);
			$table->string('ORG_HOUSE_NO', 30);
			$table->string('ORG_FLAT_NO', 30);
			$table->string('ORG_ROAD_NO', 30);
			$table->string('ORG_POST_CODE', 8);
			$table->string('ORG_POST_OFFICE', 30);
			$table->string('ORG_PHONE', 15);
			$table->string('ORG_FAX_NO', 15);
			$table->string('ORG_EMAIL', 60);
			$table->string('ORG_THANA', 20);
			$table->string('ORG_DISTRICT', 20);
			$table->integer('ORG_TYPE');
			$table->string('ORG_ACTIVITY', 200);
			$table->string('AUTH_CAPITAL', 30);
			$table->string('PAID_CAPITAL', 30);
			$table->integer('REMITTANCE');
			$table->string('INDUSTRY_TYPE', 60)->nullable();
			$table->string('INDUSTRY_TYPE_OTHER', 128)->nullable();
			$table->string('COMPANY_TYPE', 60)->nullable();
			$table->string('EXP_NAME', 256);
			$table->string('NATIONALITY', 30);
			$table->string('PASSPORT_NO', 60);
			$table->date('ISSUE_DATE');
			$table->string('ISSUE_PLACE', 100);
			$table->date('EXPR_DATE')->nullable();
			$table->string('EXP_HOME_COUNTRY', 30)->nullable();
			$table->string('EXP_HOUSE_NO', 20);
			$table->string('EXP_FLAT_NO', 20)->nullable();
			$table->string('EXP_ROAD_NO', 20)->nullable();
			$table->string('EXP_POST_CODE', 20)->nullable();
			$table->string('EXP_POST_OFFICE', 30)->nullable();
			$table->string('EXP_PHONE', 40)->nullable();
			$table->string('EXP_FAX_NO', 20)->nullable();
			$table->string('EXP_THANA', 20)->nullable();
			$table->string('EXP_DISTRICT', 20)->nullable();
			$table->string('EXP_EMAIL', 40)->nullable();
			$table->string('Q_DEG_1', 40)->nullable();
			$table->string('Q_COLG_1', 100)->nullable();
			$table->string('Q_BOARD_1', 40)->nullable();
			$table->string('Q_RESULT_1', 40)->nullable();
			$table->string('EXP_FILE', 256)->nullable();
			$table->date('DOB');
			$table->enum('MARITAL_STATUS', array('married','unmarried'));
			$table->integer('FAMILY_MEMBER_NO');
			$table->enum('FAMILY_MEMBER_STATUS', array('yes','no'));
			$table->string('FAMILY_MEMBER_NAME', 256);
			$table->string('FAMILY_MEMBER_RELATION', 50);
			$table->integer('FAMILY_MEMBER_AGE');
			$table->string('ACADEMIC_QUALIFICATION', 256);
			$table->string('POST_EMP_NAME', 256);
			$table->string('POST_EMP_JOB_DESC', 200);
			$table->string('PAPER_CLIPING', 256);
			$table->string('EMP_JUSTIFICATION', 200);
			$table->date('FIRST_APPOINT_DATE')->nullable();
			$table->date('ARRIVAL_DATE');
			$table->string('VISA_TYPE', 40);
			$table->integer('TRAVEL_VISA_CATE');
			$table->string('TRAVEL_VISA_OTHERS', 256)->nullable();
			$table->string('VISA_VALIDITY', 40);
			$table->string('PERIOD_VALIDITY', 40)->nullable();
			$table->date('PERMIT_EFCT_DATE')->nullable();
			$table->string('EMP_STATUS', 256);
			$table->string('CURRENCY', 40);
			$table->string('BASIC_SALARY', 50);
			$table->string('DEARNESS_ALLOWANCE', 50)->nullable();
			$table->string('OVERSEAS_ALLOWANCE', 50);
			$table->string('HOUSE_RENT', 50);
			$table->string('CONVEYANCE_ALLOWANCE', 50);
			$table->string('MEDICAL_ALLOWANCE', 50);
			$table->string('ENTERTAINMENT_ALLOWANCE', 50);
			$table->string('ANNUAL_BONUS', 50);
			$table->text('OTHER_BENEFIT', 65535);
			$table->text('SALARY_REMARKS', 65535)->nullable();
			$table->integer('MONTHLY_EXPENDITURE')->nullable();
			$table->string('REMITTANCE_CLAIMED', 65)->nullable();
			$table->string('PLOC1', 60);
			$table->integer('MP_LOC_EXECUTIVE');
			$table->integer('MP_LOC_STAFF');
			$table->integer('MP_LOC_TOTAL');
			$table->integer('FOR_LOC_EXECUTIVE');
			$table->integer('FOR_LOC_STAFF');
			$table->integer('FOR_LOC_TOTAL');
			$table->integer('RAT_LOC');
			$table->integer('RAT_FOR');
			$table->integer('GRAND_TOTAL');
			$table->string('WEATHER_ADVERTISEMENT', 256)->nullable();
			$table->string('EMPF_JUSTIFICATION', 200)->nullable();
			$table->string('SANCTIONING_AUTHORITY', 256)->nullable();
			$table->string('TRAINING_LOCAL', 40)->nullable();
			$table->string('EXISTING_LOCAL', 40)->nullable();
			$table->date('EFCT_START_DATE')->nullable();
			$table->date('EFCT_END_DATE')->nullable();
			$table->string('APPLICANT_NAME', 256);
			$table->string('AUTH_FULL_NAME', 65)->nullable();
			$table->string('AUTH_EMAIL', 50);
			$table->string('AUTH_MOBILE', 20);
			$table->string('AUTH_IMAGE', 256);
			$table->dateTime('ENTRY_DT');
			$table->string('ENTRY_BY', 10);
			$table->string('ENTRY_FROM', 20);
			$table->dateTime('MODIFIED_DT')->default('0000-00-00 00:00:00');
			$table->string('MODIFIED_BY', 10);
			$table->string('MODIFIED_FROM', 20);
			$table->string('PLOC2', 60)->nullable();
			$table->string('PLOC3', 60)->nullable();
			$table->string('PLOC4', 60)->nullable();
			$table->string('PLOC5', 60)->nullable();
			$table->string('PLOC6', 60)->nullable();
			$table->string('PLOC7', 60)->nullable();
			$table->string('PLOC8', 60)->nullable();
			$table->string('CURRENCY1', 60)->nullable();
			$table->string('CURRENCY2', 60)->nullable();
			$table->string('CURRENCY3', 60)->nullable();
			$table->string('CURRENCY4', 60)->nullable();
			$table->string('CURRENCY5', 60)->nullable();
			$table->string('CURRENCY6', 60)->nullable();
			$table->string('CURRENCY7', 60)->nullable();
			$table->string('CURRENCY8', 60)->nullable();
			$table->string('ABM1', 60)->nullable();
			$table->string('ABM2', 60)->nullable();
			$table->string('ABM3', 60)->nullable();
			$table->string('ABM4', 60)->nullable();
			$table->string('ABM5', 60)->nullable();
			$table->string('ABM6', 60)->nullable();
			$table->string('ABM7', 60)->nullable();
			$table->text('ABM8', 65535)->nullable();
			$table->string('ABC1', 60)->nullable();
			$table->string('ABC2', 60)->nullable();
			$table->string('ABC3', 60)->nullable();
			$table->string('ABC4', 60)->nullable();
			$table->string('ABC5', 60)->nullable();
			$table->string('ABC6', 60)->nullable();
			$table->string('ABC7', 60)->nullable();
			$table->string('ABC8', 60)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_permit_form');
	}

}
