<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDashboardObjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dashboard_object', function(Blueprint $table)
		{
			$table->integer('db_obj_id', true);
			$table->string('db_obj_title', 100)->nullable();
			$table->string('db_obj_caption')->nullable();
			$table->integer('db_user_id')->nullable();
			$table->string('db_obj_type')->nullable();
			$table->text('db_obj_para1', 65535)->nullable();
			$table->text('db_obj_para2', 65535)->nullable();
			$table->integer('db_obj_status')->nullable();
			$table->integer('db_obj_sort')->nullable();
			$table->integer('db_user_type')->nullable();

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dashboard_object');
	}

}
