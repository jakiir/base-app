<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_parameter', function (Blueprint $table) {
            $table->increments('param_id');
            $table->string('param_name');
            $table->integer('tst_id');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_parameter');
    }
}
