<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_details', function (Blueprint $table) {
            $table->increments('app_id');
            $table->string('tracking_number');
            $table->integer('app_status');
            $table->integer('company_id');
            $table->integer('user_id');
            $table->string('submitted_by');
            $table->string('submitted_contact');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apps_details');
    }
}
