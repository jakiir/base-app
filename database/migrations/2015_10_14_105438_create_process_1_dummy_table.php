<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcess1DummyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('process_1_dummy', function(Blueprint $table)
		{
			$table->integer('process_1_id', true);
			$table->string('column_1', 100);
			$table->integer('column_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('process_1_dummy');
	}

}
