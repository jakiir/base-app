<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLabMethodMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_lab_method_mapping', function(Blueprint $table) {
            $table->integer('lm_id', true);
            $table->integer('lab_id');
            $table->integer('method_id');
            $table->integer('price');
            $table->integer('duration');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_lab_method_mapping');
    }
}
