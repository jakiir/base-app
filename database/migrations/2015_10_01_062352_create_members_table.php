<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('members', function(Blueprint $table) {
            $table->integer('member_id', true);
            $table->string('member_username', 255);
            $table->string('member_email', 100);
            $table->string('member_password', 50);
            $table->enum('member_social_type', array('facebook', 'google', 'twitter', ''))->default('');
            $table->string('member_social_id', 100);
            $table->string('member_hash', 200);
            $table->enum('member_status', array('active', 'inactive'));
            $table->enum('member_verification', array('yes', 'no'))->default('no');
            $table->text('member_pic');
            $table->integer('member_type');
            $table->string('member_first_name', 50);
            $table->string('member_middle_name', 50);
            $table->string('member_last_name', 50);
            $table->bigInteger('member_nid');
            $table->date('member_DOB');
            $table->enum('member_gender', array('Male', 'Female', 'Not defined'))->default('Not defined');
            $table->string('member_aboutme', 500);
            $table->text('member_street_address', 65535);
            $table->integer('member_country');
            $table->integer('member_city');
            $table->integer('member_zip');
            $table->string('member_phone', 20);
            $table->enum('member_agree_tc', array('I AGREE'));
            $table->timestamp('member_first_login')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('member_language', 10);
            $table->integer('security_profile_id')->default(0);
            $table->index(['member_email', 'member_social_type', 'member_social_id'], 'IndexingmembersTable');

            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('members');
    }


}
