<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountryInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country_info', function(Blueprint $table)
		{
			$table->float('country_id', 10, 0)->nullable();
			$table->string('country_code', 12)->nullable();
			$table->string('country_name', 300)->nullable();
			$table->string('nationality', 60);
			$table->boolean('country_priority')->nullable()->default(2);
			$table->string('country_status', 9)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('country_info');
	}

}
