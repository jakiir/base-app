<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('config_settings', function(Blueprint $table) {
            $table->integer('CS_id', true);
            $table->string('CS_option', 100)->unique('IndexingCSoption');
            $table->string('CS_value', 500);
            $table->enum('CS_type', array('web', 'image', 'email', 'social'));
            $table->timestamp('CS_update_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->enum('CS_auto_load', array('yes', 'no'))->default('no');

            $table->timestamps();
            $table->integer('updated_by');
            $table->dateTime('updated_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('config_settings');
    }

}
