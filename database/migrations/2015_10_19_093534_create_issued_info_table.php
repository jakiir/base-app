<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIssuedInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issued_info', function(Blueprint $table)
		{
			$table->integer('issued_id', true);
			$table->string('issued_name', 45)->nullable();
			$table->string('issued_type', 45)->nullable();
			$table->string('issued_url', 45)->nullable();
			$table->string('issued_ref_no', 45)->nullable();
			$table->string('issued_status', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issued_info');
	}

}
