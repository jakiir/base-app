<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleNameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_method', function (Blueprint $table) {
            $table->increments('method_id');
            $table->string('method_name');
            $table->integer('param_id');
            
            $table->timestamps();
            $table->integer('is_locked');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_method');
    }
}
