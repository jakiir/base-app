<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_reports', function(Blueprint $table)
		{
			$table->integer('report_id', true);
			$table->string('report_title', 765)->nullable();
			$table->integer('report_type')->nullable();
			$table->string('report_para1', 9000)->nullable();
			$table->string('report_para2', 5000)->nullable();
			$table->integer('group_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('status')->nullable();
			$table->timestamps();
			$table->integer('updated_by')->nullable()->default(0);
			$table->integer('is_locked')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom_reports');
	}

}
