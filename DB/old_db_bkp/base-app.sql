-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_full_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_hash` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_type` int(11) NOT NULL,
  `admin_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `admin_last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_language` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`admin_id`),
  KEY `IndexingAdminEmail` (`admin_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `admin_logs`;
CREATE TABLE `admin_logs` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_admin_id` int(11) NOT NULL,
  `al_ip_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `admin_permission`;
CREATE TABLE `admin_permission` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_at_id` int(11) NOT NULL,
  `ap_module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ap_action_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `admin_types`;
CREATE TABLE `admin_types` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_type_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `at_is_registerable` int(11) NOT NULL,
  `at_access_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `at_permission` text COLLATE utf8_unicode_ci NOT NULL,
  `at_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `apps_details`;
CREATE TABLE `apps_details` (
  `app_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_status` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `submitted_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submitted_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` float NOT NULL,
  `tentative_duration` int(11) DEFAULT NULL,
  `payable` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `apps_details` (`app_id`, `tracking_number`, `app_status`, `company_id`, `user_id`, `submitted_by`, `submitted_contact`, `total_price`, `tentative_duration`, `payable`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	't2eef',	1,	1,	1,	'Anowar Hossain',	'01822141954',	0,	NULL,	100,	'2015-10-26 05:36:27',	'2015-10-29 08:02:36',	0,	1),
(2,	'TTTTTTTTT',	1,	1,	1,	'Anowar Hossain',	'01822141954',	0,	NULL,	0,	'2015-10-26 05:36:27',	'2015-10-27 05:01:01',	0,	1);

DROP TABLE IF EXISTS `apps_job`;
CREATE TABLE `apps_job` (
  `job_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_id` int(11) NOT NULL,
  `tst_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `apps_job` (`job_id`, `job_name`, `app_id`, `tst_id`, `lab_id`, `remarks`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(30,	'Leather Test job',	1,	1,	1,	'comment remarks',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(31,	'X-Ray Test',	1,	2,	0,	'drgfhdfh',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1);

DROP TABLE IF EXISTS `apps_payment`;
CREATE TABLE `apps_payment` (
  `payment_id` bigint(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `due_payment` double NOT NULL,
  `new_payment` double NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `apps_payment` (`payment_id`, `app_id`, `due_payment`, `new_payment`, `user_id`, `is_locked`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	1,	1000,	456,	1,	1,	0,	'2015-10-28 12:00:00',	'2015-10-28 12:23:00'),
(0,	1,	0,	10,	1,	0,	0,	'2015-10-30 22:47:17',	'2015-10-30 22:47:17');

DROP TABLE IF EXISTS `apps_result`;
CREATE TABLE `apps_result` (
  `rs_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `is_locked` int(11) NOT NULL,
  PRIMARY KEY (`rs_id`),
  KEY `ts_id` (`ts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `apps_result` (`rs_id`, `ts_id`, `status`, `remarks`, `description`, `created_at`, `updated_at`, `updated_by`, `is_locked`) VALUES
(1,	27,	'Done',	'Result of Blood Grouping & RH Typing ',	'<p><strong>sfsdf sdf</strong></p>\r\n',	'2015-10-29 07:38:43',	'2015-10-29 07:38:43',	1,	0);

DROP TABLE IF EXISTS `apps_test_sample`;
CREATE TABLE `apps_test_sample` (
  `ts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`ts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `apps_test_sample` (`ts_id`, `job_id`, `method_id`, `lab_id`, `price`, `duration`, `remarks`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(21,	30,	7,	1,	100.00,	2,	'1',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(22,	30,	1,	1,	100.00,	2,	'2',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(23,	30,	5,	1,	200.00,	3,	'3',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(24,	30,	2,	1,	350.00,	4,	'4',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(25,	30,	4,	1,	450.00,	5,	'5',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(26,	30,	3,	1,	300.00,	2,	'8',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(27,	31,	7,	2,	2563.00,	2,	'10',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(28,	31,	8,	2,	500.00,	5,	'9',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(29,	31,	6,	2,	100.00,	2,	'8',	'2015-10-29 08:02:36',	'2015-10-29 08:02:36',	0,	1),
(30,	31,	9,	2,	300.00,	2,	'7',	'2015-10-29 08:02:37',	'2015-10-29 08:02:37',	0,	1);

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `company_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `company_house_no` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `company_flat_no` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_street` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `company_area` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `company_city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company_zip` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_fax` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_web` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `incorporation_certificate` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partnership_deed` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `com_trade_license` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_tin` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `head_of_org` varchar(48) COLLATE utf8_unicode_ci DEFAULT NULL,
  `head_contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `head_email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fax_no` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `companies` (`company_id`, `company_name`, `company_type`, `company_house_no`, `company_flat_no`, `company_street`, `company_area`, `company_city`, `company_zip`, `company_fax`, `company_web`, `company_status`, `incorporation_certificate`, `partnership_deed`, `com_trade_license`, `company_tin`, `head_of_org`, `head_contact`, `head_email`, `contact_person`, `contact_phone`, `contact_email`, `phone_no`, `fax_no`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'Business Automation Ltd.',	'1',	'126',	NULL,	'12',	'',	'Dhaka',	'43',	'',	'',	'1',	NULL,	NULL,	NULL,	'',	'Md. Jahidul Islam',	'017555555555',	'',	'Md Mazharul Haque',	'0175555222222',	'mazharul@ocpl.com.bd',	'4334',	'',	'2015-10-26 03:25:32',	'2015-10-26 03:25:32',	0,	0);

DROP TABLE IF EXISTS `company_assoc`;
CREATE TABLE `company_assoc` (
  `ca_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `member_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`ca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `company_assoc` (`ca_id`, `company_id`, `member_id`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'1',	'1',	'2015-10-26 05:30:04',	'2015-10-26 05:30:04',	0,	0);

DROP TABLE IF EXISTS `company_type`;
CREATE TABLE `company_type` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `ct_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ct_status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `company_type` (`ct_id`, `ct_name`, `ct_status`) VALUES
(1,	'Limited Company ',	NULL);

DROP TABLE IF EXISTS `config_settings`;
CREATE TABLE `config_settings` (
  `CS_id` int(11) NOT NULL AUTO_INCREMENT,
  `CS_option` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CS_value` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `CS_type` enum('web','image','email','social') COLLATE utf8_unicode_ci NOT NULL,
  `CS_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CS_auto_load` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`CS_id`),
  UNIQUE KEY `IndexingCSoption` (`CS_option`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `country_info`;
CREATE TABLE `country_info` (
  `country_id` double DEFAULT NULL,
  `country_code` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `country_priority` tinyint(1) DEFAULT '2',
  `country_status` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `custom_reports`;
CREATE TABLE `custom_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_title` varchar(765) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_type` int(11) DEFAULT NULL,
  `report_para1` varchar(9000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_para2` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT '0',
  `is_locked` int(11) DEFAULT '0',
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `dashboard_object`;
CREATE TABLE `dashboard_object` (
  `db_obj_id` int(11) NOT NULL AUTO_INCREMENT,
  `db_obj_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_obj_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_user_id` int(11) DEFAULT NULL,
  `db_obj_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_obj_para1` text COLLATE utf8_unicode_ci,
  `db_obj_para2` text COLLATE utf8_unicode_ci,
  `db_obj_status` int(11) DEFAULT NULL,
  `db_obj_sort` int(11) DEFAULT NULL,
  `db_user_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`db_obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `issued_info`;
CREATE TABLE `issued_info` (
  `issued_id` int(11) NOT NULL AUTO_INCREMENT,
  `issued_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issued_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issued_url` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issued_ref_no` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issued_status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`issued_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `member_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_social_type` enum('facebook','google','twitter','') COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `member_social_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `member_hash` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `member_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `member_verification` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `member_pic` text COLLATE utf8_unicode_ci NOT NULL,
  `member_type` int(11) NOT NULL,
  `member_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_nid` bigint(20) NOT NULL,
  `member_DOB` date NOT NULL,
  `member_gender` enum('Male','Female','Not defined') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not defined',
  `member_aboutme` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `member_street_address` text COLLATE utf8_unicode_ci NOT NULL,
  `member_country` int(11) NOT NULL,
  `member_city` int(11) NOT NULL,
  `member_zip` int(11) NOT NULL,
  `member_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `member_agree_tc` enum('I AGREE') COLLATE utf8_unicode_ci NOT NULL,
  `member_first_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `member_language` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `security_profile_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`member_id`),
  KEY `IndexingmembersTable` (`member_email`,`member_social_type`,`member_social_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `members` (`member_id`, `member_username`, `member_email`, `member_password`, `member_social_type`, `member_social_id`, `member_hash`, `member_status`, `member_verification`, `member_pic`, `member_type`, `member_first_name`, `member_middle_name`, `member_last_name`, `member_nid`, `member_DOB`, `member_gender`, `member_aboutme`, `member_street_address`, `member_country`, `member_city`, `member_zip`, `member_phone`, `member_agree_tc`, `member_first_login`, `member_language`, `lab_id`, `security_profile_id`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'Anowar',	'anowar.cst@gmail.comm',	'bff42eb4a1bd91387b5656cd0e217066',	'',	'112256548914858985421',	'EZeJjMEeAOUfO0sNoV0flOz038tNFJ',	'active',	'yes',	'https://lh6.googleusercontent.com/-Tkk6ugiTAgY/AAAAAAAAAAI/AAAAAAAAF6g/BSTVZRJd848/photo.jpg',	1,	'Anowar Hossain',	'',	'',	0,	'0000-00-00',	'Not defined',	'',	'',	0,	0,	0,	'',	'I AGREE',	'2015-10-27 05:31:37',	'',	0,	0,	'0000-00-00 00:00:00',	'2015-10-29 03:30:36',	0,	0),
(7,	'anowar2@batworld.com',	'anowar2@batworld.com',	'e1910eb6b7f5817f9d78e11bca59f2bb',	'',	'',	'VeE7wQ6a7Jqk4bLsw3Psm5THv126zg',	'active',	'yes',	'',	1,	'Anowar Hossain',	'',	'',	12345678901234567,	'2015-10-04',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 08:57:24',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(8,	'do@batworld.com',	'do@batworld.com',	'bff42eb4a1bd91387b5656cd0e217066',	'',	'',	'VeE7wQ6a7Jqk4bLsw3Psm5THv126zg',	'active',	'yes',	'',	3,	'Duty Officer',	'',	'',	12345678901234567,	'2015-10-04',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 08:57:24',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(9,	'do1@batworld.com',	'do1@batworld.com',	'bff42eb4a1bd91387b5656cd0e217066',	'',	'',	'VeE7wQ6a7Jqk4bLsw3Psm5THv126zg',	'active',	'yes',	'',	3,	'Duty Officer1',	'',	'',	12345678901234567,	'2015-10-04',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 08:57:24',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(10,	'SCI@batworld.com',	'SCI@batworld.com',	'bff42eb4a1bd91387b5656cd0e217066',	'',	'',	'VeE7wQ6a7Jqk4bLsw3Psm5THv126zg',	'active',	'yes',	'',	7,	'Duty Officer1',	'',	'',	12345678901234567,	'2015-10-04',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 08:57:24',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(12,	'anowar@batworld.com',	'anowar2@batworld.com',	'e2db0348f3e507c254d4017be8dad946',	'',	'',	'OpuXE74jqnNitbVyYJDgeWw6EA9TeB',	'active',	'yes',	'',	1,	'kkkkkkkk',	'',	'',	12345678912345678,	'2015-10-07',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 11:05:41',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(13,	'Anowar',	'anowar.cst@gmail.com',	'',	'google',	'112256548914858985421',	'cefc9d0dce4cf450c281e1cb38c10d62371a4033',	'active',	'yes',	'https://lh6.googleusercontent.com/-Tkk6ugiTAgY/AAAAAAAAAAI/AAAAAAAAF6g/BSTVZRJd848/photo.jpg',	1,	'Anowar Hossain',	'',	'',	12345678912345679,	'2015-10-01',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 11:32:55',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(14,	'anowar@ocpl.com.bd',	'anowar@ocpl.com.bd',	'dbad7f156b316dc70a3b1286f49d3be7',	'',	'',	'zjwl99nBps06VJN7QZ8G9usXPseAso',	'active',	'yes',	'',	7,	'sssssssssss',	'',	'',	12345670012345678,	'2015-10-22',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-29 11:46:39',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0),
(15,	'Scientist',	'anowar@batworld.com',	'44c4af90bd764fb6b7f547e8e74eddd5',	'',	'',	'1DEPhg4JNNncSM88Rc7IUWbHHacu1z',	'active',	'yes',	'',	7,	'Scientist',	'',	'',	12345678998765432,	'2015-10-27',	'Not defined',	'',	'',	0,	0,	0,	'01822141954',	'I AGREE',	'2015-10-31 04:40:28',	'',	1,	0,	'0000-00-00 00:00:00',	'2015-10-30 22:41:29',	0,	0),
(16,	'khaleda@batworld.com',	'khaleda@batworld.com',	'6e2d0036b77829a229afc33fe0eb3408',	'',	'',	'LTjd7VTwLTX8vQgVgVJb1a2GxndxLi',	'active',	'yes',	'',	5,	'Khaleda',	'',	'',	19882696402504651,	'2015-10-20',	'Not defined',	'',	'',	0,	0,	0,	'01722393251',	'I AGREE',	'2015-10-31 04:56:44',	'',	NULL,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0);

DROP TABLE IF EXISTS `member_desk`;
CREATE TABLE `member_desk` (
  `desk_id` int(11) NOT NULL AUTO_INCREMENT,
  `desk_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `desk_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`desk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `member_desk` (`desk_id`, `desk_name`, `desk_status`) VALUES
(1,	'assistant office',	1),
(2,	'joint secretery',	1);

DROP TABLE IF EXISTS `member_logs`;
CREATE TABLE `member_logs` (
  `ML_id` int(11) NOT NULL AUTO_INCREMENT,
  `ML_admin_id` int(11) NOT NULL,
  `ML_ip_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ML_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`ML_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `member_types`;
CREATE TABLE `member_types` (
  `mt_id` int(11) NOT NULL AUTO_INCREMENT,
  `mt_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mt_is_registarable` int(5) NOT NULL,
  `mt_access_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mt_permission_json` text COLLATE utf8_unicode_ci,
  `mt_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`mt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `member_types` (`mt_id`, `mt_type_name`, `mt_is_registarable`, `mt_access_code`, `mt_permission_json`, `mt_status`, `created_at`, `updated_at`, `updated_by`, `updated_on`) VALUES
(1,	'SysAdmin',	-1,	'1_101',	NULL,	'active',	'2015-10-20 00:00:00',	'2015-10-20 00:00:00',	1,	'2015-10-21 00:00:00'),
(2,	'IT Officer',	2,	'2_202',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(3,	'Duty Officer',	2,	'3_303',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(4,	'BCSIR Executive',	1,	'4_404',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(5,	'Applicant',	1,	'5_505',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(6,	'Lab-Incharge',	2,	'6_606',	NULL,	'active',	'2015-10-28 21:00:00',	'2015-10-29 00:00:00',	1,	'0000-00-00 00:00:00'),
(7,	'Scientist',	2,	'7_707',	NULL,	'active',	'2015-10-28 21:00:00',	'2015-10-29 00:00:00',	1,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_10_01_062352_create_admin_logs_table',	1),
('2015_10_01_062352_create_admin_permission_table',	1),
('2015_10_01_062352_create_admin_types_table',	1),
('2015_10_01_062352_create_admins_table',	1),
('2015_10_01_062352_create_config_settings_table',	1),
('2015_10_01_062352_create_member_logs_table',	1),
('2015_10_01_062352_create_members_table',	1),
('2015_10_12_090958_create_companies_table',	1),
('2015_10_12_102932_app_process_path',	1),
('2015_10_12_102959_app_status',	1),
('2015_10_12_103006_create_tbl_test_name_table',	1),
('2015_10_12_103006_create_test_name_table',	1),
('2015_10_12_103008_app_process_history',	1),
('2015_10_12_103027_create_sample_name_table',	1),
('2015_10_12_103027_create_tbl_sample_name_table',	1),
('2015_10_12_103059_create_parameter_table',	1),
('2015_10_12_103059_create_tbl_lab_table',	1),
('2015_10_12_103059_create_tbl_parameter_table',	1),
('2015_10_13_111948_create_lab_table',	1),
('2015_10_14_071548_security_profile',	1),
('2015_10_14_101053_create_custom_reports_table',	1),
('2015_10_14_101053_create_dashboard_object_table',	1),
('2015_10_14_105300_create_task_list_table',	1),
('2015_10_14_105346_create_process_list_table',	1),
('2015_10_14_105405_create_process_list_history_table',	1),
('2015_10_14_105438_create_process_1_dummy_table',	1),
('2015_10_14_110004_create_work_permit_form_table',	1),
('2015_10_14_121932_create_company_assoc_table',	1),
('2015_10_15_014154_create_security_profile_table',	1),
('2015_10_15_050254_create_lab_test_mapping',	1),
('2015_10_17_064644_create_tbl_application',	1),
('2015_10_17_065108_create_tbl_app_details',	1),
('2015_10_18_111750_country_info',	1),
('2015_10_19_082002_create_tbl_lab_method_mapping',	1),
('2015_10_19_093534_create_issued_info_table',	1),
('2015_10_19_121114_create_process_path_table',	1),
('2015_10_20_064900_create_apps_table',	1),
('2015_10_20_065138_create_apps_job_table',	1),
('2015_10_20_065155_create_apps_sample_table',	1),
('2015_10_20_143259_create_country_info_table',	1),
('2015_10_21_062528_create_company_type_table',	1);

DROP TABLE IF EXISTS `process_1_dummy`;
CREATE TABLE `process_1_dummy` (
  `process_1_id` int(11) NOT NULL AUTO_INCREMENT,
  `column_1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `column_2` int(11) NOT NULL,
  PRIMARY KEY (`process_1_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `process_list`;
CREATE TABLE `process_list` (
  `process_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `process_type` int(11) NOT NULL,
  `initiated_by` int(11) NOT NULL,
  `closed_by` int(11) NOT NULL,
  `desk_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `process_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`process_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `process_list_history`;
CREATE TABLE `process_list_history` (
  `process_id` bigint(20) NOT NULL,
  `process_type` int(11) NOT NULL,
  `initiated_by` int(11) NOT NULL,
  `closed_by` int(11) NOT NULL,
  `desk_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `process_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  KEY `process_id` (`process_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `process_path`;
CREATE TABLE `process_path` (
  `process_type` int(11) NOT NULL,
  `status_from` int(11) NOT NULL,
  `STATUS_TO` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `desk_from` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `desk_to` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  KEY `SERVICE_ID` (`process_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `security_profile`;
CREATE TABLE `security_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `allowed_remote_ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `week_off_days` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `work_hour_start` time NOT NULL,
  `work_hour_end` time NOT NULL,
  `active_status` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `task_list`;
CREATE TABLE `task_list` (
  `task_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_type` int(11) NOT NULL,
  `initiated_by` int(11) NOT NULL,
  `closed_by` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `status` enum('Initiated','Approved','Cancel','Hold') COLLATE utf8_unicode_ci NOT NULL,
  `record_id` int(11) NOT NULL,
  `task_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` datetime NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_lab`;
CREATE TABLE `tbl_lab` (
  `lab_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lab_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`lab_id`),
  UNIQUE KEY `tbl_lab_lab_name_unique` (`lab_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_lab` (`lab_id`, `lab_name`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'DRiCM',	'2015-10-26 00:20:21',	'2015-10-28 00:55:50',	0,	1),
(2,	'IFRD',	'2015-10-27 05:08:45',	'2015-10-27 05:09:59',	0,	1);

DROP TABLE IF EXISTS `tbl_lab_method_mapping`;
CREATE TABLE `tbl_lab_method_mapping` (
  `lm_id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`lm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_lab_method_mapping` (`lm_id`, `lab_id`, `method_id`, `price`, `duration`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(4,	1,	1,	100,	2,	'2015-10-26 00:32:29',	'2015-10-26 00:32:52',	0,	1),
(5,	1,	5,	200,	3,	'2015-10-27 01:10:15',	'2015-10-27 01:10:15',	0,	1),
(6,	1,	2,	350,	4,	'2015-10-27 01:10:20',	'2015-10-27 01:10:20',	0,	1),
(7,	1,	3,	300,	2,	'2015-10-27 01:10:26',	'2015-10-27 01:10:26',	0,	1),
(8,	1,	4,	450,	5,	'2015-10-27 01:10:33',	'2015-10-27 01:10:33',	0,	1),
(9,	2,	6,	100,	2,	'2015-10-27 05:09:44',	'2015-10-27 05:09:44',	0,	1),
(10,	2,	7,	2563,	2,	'2015-10-27 05:09:49',	'2015-10-27 05:09:49',	0,	1),
(11,	2,	8,	500,	5,	'2015-10-27 05:09:53',	'2015-10-27 05:09:53',	0,	1),
(12,	2,	9,	300,	2,	'2015-10-27 05:09:56',	'2015-10-27 05:09:56',	0,	1),
(13,	1,	7,	100,	2,	'2015-10-28 00:45:26',	'2015-10-28 00:45:26',	0,	1),
(14,	1,	8,	300,	2,	'2015-10-28 00:45:31',	'2015-10-28 00:45:31',	0,	1),
(15,	1,	9,	50,	1,	'2015-10-28 00:45:35',	'2015-10-28 00:45:35',	0,	1),
(16,	1,	6,	5551,	1,	'2015-10-28 00:45:42',	'2015-10-28 00:45:42',	0,	1);

DROP TABLE IF EXISTS `tbl_lab_test_mapping`;
CREATE TABLE `tbl_lab_test_mapping` (
  `lt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lab_id` int(11) NOT NULL,
  `tst_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`lt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_lab_test_mapping` (`lt_id`, `lab_id`, `tst_id`, `price`, `duration`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(11,	1,	2,	0,	0,	'2015-10-27 05:22:21',	'2015-10-27 05:22:21',	0,	1),
(12,	1,	1,	0,	0,	'2015-10-28 00:56:23',	'2015-10-28 00:56:23',	0,	1),
(13,	2,	1,	0,	0,	'2015-10-28 00:56:23',	'2015-10-28 00:56:23',	0,	1);

DROP TABLE IF EXISTS `tbl_method`;
CREATE TABLE `tbl_method` (
  `method_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `param_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_method` (`method_id`, `method_name`, `param_id`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'Blood Grouping & RH Typing ',	1,	'2015-10-26 00:20:09',	'2015-10-27 01:09:36',	0,	1),
(2,	'BLOOD SUGAR F & PP',	2,	'2015-10-26 01:26:57',	'2015-10-27 01:07:51',	0,	1),
(3,	'LIPID PROFILE SERUM ',	2,	'2015-10-27 01:07:38',	'2015-10-27 01:07:51',	0,	1),
(4,	'HBAIC ',	2,	'2015-10-27 01:07:51',	'2015-10-27 01:07:51',	0,	1),
(5,	'Blood sugar (fasting & PP)',	1,	'2015-10-27 01:09:36',	'2015-10-27 01:09:36',	0,	1),
(6,	'TSH',	5,	'2015-10-27 04:59:14',	'2015-10-27 04:59:14',	0,	1),
(7,	'PFT',	4,	'2015-10-27 04:59:31',	'2015-10-27 05:00:04',	0,	1),
(8,	'RH Typing ',	4,	'2015-10-27 04:59:46',	'2015-10-27 05:00:04',	0,	1),
(9,	'XRAY CHEST PA',	4,	'2015-10-27 05:00:04',	'2015-10-27 05:00:04',	0,	1);

DROP TABLE IF EXISTS `tbl_parameter`;
CREATE TABLE `tbl_parameter` (
  `param_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `param_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tst_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_parameter` (`param_id`, `param_name`, `tst_id`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'Blood Grouping ',	1,	'2015-10-26 00:20:01',	'2015-10-28 00:56:22',	0,	1),
(2,	'Uric Acid ',	1,	'2015-10-26 01:24:33',	'2015-10-28 00:56:23',	0,	1),
(3,	'MCHC, MCV, MCH ',	1,	'2015-10-27 01:06:33',	'2015-10-28 00:56:22',	0,	1),
(4,	'X-Ray (Chest)',	2,	'2015-10-27 04:54:51',	'2015-10-27 05:22:21',	0,	1),
(5,	'Physician Consultation ',	2,	'2015-10-27 04:58:58',	'2015-10-27 05:22:21',	0,	1);

DROP TABLE IF EXISTS `tbl_test_name`;
CREATE TABLE `tbl_test_name` (
  `tst_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tst_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`tst_id`),
  UNIQUE KEY `tbl_test_name_tst_name_unique` (`tst_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_test_name` (`tst_id`, `tst_name`, `created_at`, `updated_at`, `is_locked`, `updated_by`) VALUES
(1,	'Apollo Diabetic Check ',	'2015-10-26 00:19:54',	'2015-10-28 00:56:22',	0,	1),
(2,	'X-Ray',	'2015-10-27 04:48:28',	'2015-10-27 05:22:21',	0,	1);

DROP TABLE IF EXISTS `work_permit_form`;
CREATE TABLE `work_permit_form` (
  `APP_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VISA_PERMIT` int(11) NOT NULL,
  `VISA_REF_NO` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRACK_NO` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `DEPT_ID` int(11) NOT NULL,
  `SERVICE_ID` int(11) NOT NULL,
  `PREV_USER_ID` int(11) DEFAULT NULL,
  `CURRENT_USER_ID` int(11) NOT NULL,
  `STATUS_ID` int(11) NOT NULL,
  `VISA_CATEGORY` int(11) NOT NULL,
  `BANK_NAME` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BRANCH_NAME` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `BANK_ISSUE_DATE` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHALLAN_NO` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHALLAN_AMOUNT` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `CHALLAN_FILE` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RECOMMENDATION` text COLLATE utf8_unicode_ci,
  `AGENDA_NO` int(11) DEFAULT NULL,
  `MEETING_AGENDA` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MEETING_DATE` date DEFAULT NULL,
  `MIN_RANGE_BASIC_SALARY` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOHA_CLEARENCE` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHECK_COND` tinyint(1) DEFAULT NULL,
  `CONDITIONS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORG_CITY` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_HOUSE_NO` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_FLAT_NO` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_ROAD_NO` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_POST_CODE` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_POST_OFFICE` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_PHONE` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_FAX_NO` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_EMAIL` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_THANA` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_DISTRICT` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ORG_TYPE` int(11) NOT NULL,
  `ORG_ACTIVITY` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `AUTH_CAPITAL` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `PAID_CAPITAL` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `REMITTANCE` int(11) NOT NULL,
  `INDUSTRY_TYPE` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDUSTRY_TYPE_OTHER` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY_TYPE` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `NATIONALITY` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `PASSPORT_NO` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ISSUE_DATE` date NOT NULL,
  `ISSUE_PLACE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `EXPR_DATE` date DEFAULT NULL,
  `EXP_HOME_COUNTRY` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_HOUSE_NO` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `EXP_FLAT_NO` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_ROAD_NO` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_POST_CODE` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_POST_OFFICE` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_PHONE` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_FAX_NO` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_THANA` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_DISTRICT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_EMAIL` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Q_DEG_1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Q_COLG_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Q_BOARD_1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Q_RESULT_1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXP_FILE` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOB` date NOT NULL,
  `MARITAL_STATUS` enum('married','unmarried') COLLATE utf8_unicode_ci NOT NULL,
  `FAMILY_MEMBER_NO` int(11) NOT NULL,
  `FAMILY_MEMBER_STATUS` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  `FAMILY_MEMBER_NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `FAMILY_MEMBER_RELATION` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FAMILY_MEMBER_AGE` int(11) NOT NULL,
  `ACADEMIC_QUALIFICATION` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `POST_EMP_NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `POST_EMP_JOB_DESC` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PAPER_CLIPING` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `EMP_JUSTIFICATION` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `FIRST_APPOINT_DATE` date DEFAULT NULL,
  `ARRIVAL_DATE` date NOT NULL,
  `VISA_TYPE` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `TRAVEL_VISA_CATE` int(11) NOT NULL,
  `TRAVEL_VISA_OTHERS` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VISA_VALIDITY` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `PERIOD_VALIDITY` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERMIT_EFCT_DATE` date DEFAULT NULL,
  `EMP_STATUS` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENCY` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `BASIC_SALARY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DEARNESS_ALLOWANCE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OVERSEAS_ALLOWANCE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HOUSE_RENT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CONVEYANCE_ALLOWANCE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MEDICAL_ALLOWANCE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTERTAINMENT_ALLOWANCE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ANNUAL_BONUS` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `OTHER_BENEFIT` text COLLATE utf8_unicode_ci NOT NULL,
  `SALARY_REMARKS` text COLLATE utf8_unicode_ci,
  `MONTHLY_EXPENDITURE` int(11) DEFAULT NULL,
  `REMITTANCE_CLAIMED` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `MP_LOC_EXECUTIVE` int(11) NOT NULL,
  `MP_LOC_STAFF` int(11) NOT NULL,
  `MP_LOC_TOTAL` int(11) NOT NULL,
  `FOR_LOC_EXECUTIVE` int(11) NOT NULL,
  `FOR_LOC_STAFF` int(11) NOT NULL,
  `FOR_LOC_TOTAL` int(11) NOT NULL,
  `RAT_LOC` int(11) NOT NULL,
  `RAT_FOR` int(11) NOT NULL,
  `GRAND_TOTAL` int(11) NOT NULL,
  `WEATHER_ADVERTISEMENT` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPF_JUSTIFICATION` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SANCTIONING_AUTHORITY` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRAINING_LOCAL` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXISTING_LOCAL` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EFCT_START_DATE` date DEFAULT NULL,
  `EFCT_END_DATE` date DEFAULT NULL,
  `APPLICANT_NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `AUTH_FULL_NAME` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTH_EMAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `AUTH_MOBILE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `AUTH_IMAGE` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ENTRY_DT` datetime NOT NULL,
  `ENTRY_BY` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ENTRY_FROM` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MODIFIED_DT` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `MODIFIED_BY` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `MODIFIED_FROM` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `PLOC2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC3` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC4` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC5` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC6` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC7` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLOC8` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY1` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY3` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY4` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY5` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY6` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY7` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY8` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM1` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM3` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM4` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM5` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM6` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM7` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABM8` text COLLATE utf8_unicode_ci,
  `ABC1` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC3` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC4` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC5` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC6` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC7` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABC8` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2015-10-31 09:34:26
