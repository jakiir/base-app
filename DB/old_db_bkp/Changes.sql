====================For MOHA only======================
ALTER TABLE `members`
ADD `desk_id` int NOT NULL AFTER `member_language`;


DROP TABLE IF EXISTS `member_desk`;
CREATE TABLE `member_desk` (
  `desk_id` int(11) NOT NULL AUTO_INCREMENT,
  `desk_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `desk_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`desk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `member_desk` (`desk_id`, `desk_name`, `desk_status`) VALUES
(1,	'assistant office',	1),
(2,	'joint secretery',	1);

============================================================

DROP TABLE IF EXISTS `member_types`;
CREATE TABLE `member_types` (
  `mt_id` int(11) NOT NULL,
  `mt_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mt_is_registarable` int(5) NOT NULL,
  `mt_access_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mt_permission_json` text COLLATE utf8_unicode_ci,
  `mt_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `member_types` (`mt_id`, `mt_type_name`, `mt_is_registarable`, `mt_access_code`, `mt_permission_json`, `mt_status`, `created_at`, `updated_at`, `updated_by`, `updated_on`) VALUES
(1,	'SysAdmin',	-1,	'1_101',	NULL,	'active',	'2015-10-20 06:00:00',	'2015-10-20 06:00:00',	1,	'2015-10-21 00:00:00'),
(2,	'IT Officer',	2,	'2_202',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(3,	'Desk Officer',	2,	'3_303',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(4,	'BCSIR Executive',	1,	'4_404',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(5,	'Applicant',	1,	'5_505',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(6,	'Lab Incharge',	2,	'6_606',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(5,	'Scientist',	2,	'7_707',	NULL,	'active',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00');

ALTER TABLE `apps_test_sample` ADD `remarks` VARCHAR(255) NOT NULL AFTER `duration`;
ALTER TABLE `apps_details` ADD `total_price` FLOAT NOT NULL AFTER `submitted_contact`, ADD `payable` FLOAT NOT NULL AFTER `total_price`;


INSERT INTO `base_app`.`member_types` (`mt_id`, `mt_type_name`, `mt_is_registarable`, `mt_access_code`, `mt_permission_json`, `mt_status`, `created_at`, `updated_at`, `updated_by`, `updated_on`) VALUES ('', 'Lab-Incharge', '2', '6_606', NULL, 'active', '2015-10-29 03:00:00.000000', '2015-10-29 06:00:00.000000', '1', '');


ALTER TABLE `members` CHANGE `desk_id` `lab_id` INT(11) NULL;


ALTER TABLE `members` CHANGE `desk_id` `lab_id` INT(11) NULL DEFAULT '0';



DROP TABLE IF EXISTS `apps_result`;
CREATE TABLE `apps_result` (
  `rs_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_id` int(10) unsigned NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `is_locked` int(11) NOT NULL,
  PRIMARY KEY (`rs_id`),
  KEY `ts_id` (`ts_id`),
  CONSTRAINT `apps_result_ibfk_1` FOREIGN KEY (`ts_id`) REFERENCES `apps_test_sample` (`ts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `apps_job`
ADD `lab_id` int NOT NULL AFTER `tst_id`,
ADD `scientist_id` int(11) NOT NULL AFTER `lab_id`;
