<?php
return [
    'modules'=>array(
        "company",
        "users",
        "admins",
        "settings",
        "reports",
        "apps",
        "membertype"
    ),
];