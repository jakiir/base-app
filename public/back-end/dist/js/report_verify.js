$(function () {
    $('#result').hide();

    $('#verifyBtnHide').click(function () {
        $('#result').hide();
    });


    $('#verifyBtn').click(function () {
        report_sql = $('#REPORT_PARA1').val();
        if (report_sql == '') {
            alert('Empty SQL');
            return;
        }
        $.ajax({
            url: "" + base_url + "reports/reports_verify",
            type: "post",
            data: {sql: report_sql},
            success: function (data) {
                $("#result").html(data);
                $("#loding").hide();
                $('#result').show();
            },
            beforeSend: function (xhr) {
                $("#loding").show();
            },
            error: function () {
                $("#result").html('<span class="error-msg">There is error while submit. Please fill-up all field as required.</span>');
            }
        });
    });

    $('#showTables').click(function () {
        $('#result').hide();
        $.ajax({
            url: "" + base_url + "reports/showTables",
            type: "post",
            data: {},
            success: function (data) {
                $("#result").html(data);
                collapse();
                $("#loding").hide();
                $('#result').show();
            },
            beforeSend: function (xhr) {
                $("#loding").show();
            },
            error: function () {
                $("#result").html('<span class="error-msg">There is error while submit. Please fill-up all field as required.</span>');
            }
        });
    });

    function collapse() {
        $('li.table_name').addClass('plusimageapply');
        $('li.table_name').children().addClass('selectedimage');
        $('li.table_name').children().hide();
        $('li.table_name').each(
                function (column) {
                    $(this).click(function (event) {
                        if (this == event.target) {
                            if ($(this).is('.plusimageapply')) {
                                $(this).children().show();
                                $(this).removeClass('plusimageapply');
                                $(this).addClass('minusimageapply');
                            }
                            else
                            {
                                $(this).children().hide();
                                $(this).removeClass('minusimageapply');
                                $(this).addClass('plusimageapply');
                            }
                        }
                    });
                }
        );
    }
});

function makeSql(id) {
    var div = $(id).parent().children('.sql');
    var sql = div.val();
    var exist_sql = $('#REPORT_PARA1').html();
    $('#REPORT_PARA1').html(exist_sql + '\r\r\t' + sql);
}